package nl.autovooru.android.utils;

/**
 * Created by Antonenko Viacheslav on 08/12/15.
 */
public final class Constants {

    public static final double AUTOVOORU_LATITUDE = 52.165525;
    public static final double AUTOVOORU_LONGITUDE = 5.372427;
    public static final int DEFAULT_MAP_ZOOM = 15;

}
