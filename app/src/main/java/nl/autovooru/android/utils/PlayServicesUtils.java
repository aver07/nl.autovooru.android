package nl.autovooru.android.utils;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

/**
 * Created by Antonenko Viacheslav on 29/12/15.
 */
public final class PlayServicesUtils {

    public static boolean checkGooglePlayServices(final Activity activity) {
        final GoogleApiAvailability instance = GoogleApiAvailability.getInstance();
        final int googlePlayServicesCheck = instance.isGooglePlayServicesAvailable(activity);
        switch (googlePlayServicesCheck) {
            case ConnectionResult.SUCCESS:
                return true;
            case ConnectionResult.SERVICE_DISABLED:
            case ConnectionResult.SERVICE_INVALID:
            case ConnectionResult.SERVICE_MISSING:
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                instance.showErrorDialogFragment(activity, googlePlayServicesCheck, 0);
                return false;
        }
        return false;
    }

    public static boolean checkGooglePlayServices(final Context context) {
        final GoogleApiAvailability instance = GoogleApiAvailability.getInstance();
        final int googlePlayServicesCheck = instance.isGooglePlayServicesAvailable(context);
        switch (googlePlayServicesCheck) {
            case ConnectionResult.SUCCESS:
                return true;
            case ConnectionResult.SERVICE_DISABLED:
            case ConnectionResult.SERVICE_INVALID:
            case ConnectionResult.SERVICE_MISSING:
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                return false;
        }
        return false;
    }
}
