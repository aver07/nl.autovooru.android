package nl.autovooru.android.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;

import com.squareup.picasso.Transformation;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public class CropTransformation implements Transformation {

    private final int mWidth;
    private final int mHeight;
    private Matrix mMatrix;
    private CropType mCropType = CropType.NONE;

    public CropTransformation(int width, int height) {
        this(width, height, CropType.NONE);
    }

    public CropTransformation(int width, int height, CropType cropType) {
        mWidth = width;
        mHeight = height;
        mCropType = cropType;
        mMatrix = new Matrix();
    }

    private static float getYTranslation(CropType cropType, int viewHeight, float postBitmapHeight, boolean verticalImageMode) {
        if (verticalImageMode) {
            switch (cropType) {
                case CENTER_BOTTOM:
                case LEFT_BOTTOM:
                case RIGHT_BOTTOM:
                    return viewHeight - postBitmapHeight;
                case LEFT_CENTER:
                case RIGHT_CENTER:
                    // View in the middle of the screen
                    return (viewHeight - postBitmapHeight) / 2f;
            }
        }

        // All other cases we don't need to translate
        return 0;
    }

    private static float getXTranslation(CropType cropType, int viewWidth, float postBitmapWidth, boolean verticalImageMode) {
        if (!verticalImageMode) {
            switch (cropType) {
                case RIGHT_TOP:
                case RIGHT_CENTER:
                case RIGHT_BOTTOM:
                    return viewWidth - postBitmapWidth;
                case CENTER_TOP:
                case CENTER_BOTTOM:
                    // View in the middle of the screen
                    return (viewWidth - postBitmapWidth) / 2f;
            }
        }
        // All other cases we don't need to translate
        return 0;
    }

    @Override
    public Bitmap transform(Bitmap source) {

        if (mCropType == CropType.NONE) {
            return source;
        }

        mMatrix.reset();

        final int sourceWidth = source.getWidth();
        final int sourceHeight = source.getHeight();

        final float scaleX = (float) mWidth / (float) sourceWidth;
        final float scaleY = (float) mHeight / (float) sourceHeight;
        float scale = Math.max(scaleX, scaleY);
        mMatrix.setScale(scale, scale); // Same as doing matrix.reset() and matrix.preScale(...)

        final boolean verticalImageMode = scaleX > scaleY;

        final float postDrawableWidth = sourceWidth * scale;
        final float xTranslation = getXTranslation(mCropType, mWidth, postDrawableWidth, verticalImageMode);
        final float postDrawabeHeigth = sourceHeight * scale;
        final float yTranslation = getYTranslation(mCropType, mHeight, postDrawabeHeigth, verticalImageMode);

        mMatrix.postTranslate(xTranslation, yTranslation);


        final Bitmap.Config config = source.getConfig() != null ? source.getConfig() : Bitmap.Config.ARGB_8888;
        final Bitmap bmp = Bitmap.createBitmap(mWidth, mHeight, config);

        final Canvas canvas = new Canvas(bmp);
        canvas.drawBitmap(source, mMatrix, null);

        source.recycle();

        return bmp;
    }

    @Override
    public String key() {
        return "CropTransformation(width=" + mWidth + ", height=" + mHeight + ", cropType=" + mCropType
                + ")";
    }

    public enum CropType {
        NONE(-1),
        LEFT_TOP(0),
        LEFT_CENTER(1),
        LEFT_BOTTOM(2),
        RIGHT_TOP(3),
        RIGHT_CENTER(4),
        RIGHT_BOTTOM(5),
        CENTER_TOP(6),
        CENTER_BOTTOM(7);

        // Reverse-lookup map for getting a day from an abbreviation
        private static final Map<Integer, CropType> codeLookup = new HashMap<>();

        static {
            for (CropType ft : CropType.values()) {
                codeLookup.put(ft.getCrop(), ft);
            }
        }

        final int cropType;

        CropType(int ct) {
            cropType = ct;
        }

        public static CropType get(int number) {
            return codeLookup.get(number);
        }

        public int getCrop() {
            return cropType;
        }
    }
}
