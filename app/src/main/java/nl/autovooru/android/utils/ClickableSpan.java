package nl.autovooru.android.utils;

import android.text.TextPaint;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
public abstract class ClickableSpan extends android.text.style.ClickableSpan {

    /**
     * Makes the text underlined and in the link color.
     */
    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setColor(ds.linkColor);
    }
}
