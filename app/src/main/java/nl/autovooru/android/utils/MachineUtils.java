package nl.autovooru.android.utils;

import android.content.Context;
import android.text.TextUtils;

import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.StringUtils;
import nl.autovooru.android.domain.model.machine.Machine;

/**
 * Created by Antonenko Viacheslav on 20/01/16.
 */
public final class MachineUtils {

    private MachineUtils() {

    }

    public static String getPrice(Context context, Machine machine) {
        if (StringUtils.isEmpty(machine.getPromoPrice()) || TextUtils.equals(machine.getPromoPrice(), "0")) {
            if (machine.getPrice() == 0) {
                return context.getString(R.string.empty_price);
            } else {
                return context.getString(R.string.euro, String.valueOf(machine.getPrice()));
            }
        } else {
            return context.getString(R.string.euro, machine.getPromoPrice());
        }
    }
}
