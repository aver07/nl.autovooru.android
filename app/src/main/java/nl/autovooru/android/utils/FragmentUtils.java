package nl.autovooru.android.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

/**
 * Created by Antonenko Viacheslav on 09/02/16.
 */
public final class FragmentUtils {

    private FragmentUtils() {
        // no-op
    }

    /**
     * @param fragment          The Fragment whose parent is to be found
     * @param callbackInterface The interface class that the parent should implement
     * @return The parent of fragment that implements the callbackInterface or <code>null</code>
     * if no such parent can be found
     */
    @SuppressWarnings("unchecked") // Casts are checked using runtime methods
    public static <T> T getParent(Fragment fragment, Class<T> callbackInterface) {
        final Fragment parentFragment = fragment.getParentFragment();
        if (parentFragment != null && callbackInterface.isInstance(parentFragment)) {
            return (T) parentFragment;
        } else {
            final FragmentActivity activity = fragment.getActivity();
            if (activity != null && callbackInterface.isInstance(activity)) {
                return (T) activity;
            }
        }

        return null;
    }
}
