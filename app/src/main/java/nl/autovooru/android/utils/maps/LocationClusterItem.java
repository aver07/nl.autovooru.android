package nl.autovooru.android.utils.maps;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by Antonenko Viacheslav on 18/12/15.
 */
public final class LocationClusterItem implements ClusterItem {

    private final LatLng mPosition;

    public LocationClusterItem(LatLng position) {
        mPosition = position;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

}
