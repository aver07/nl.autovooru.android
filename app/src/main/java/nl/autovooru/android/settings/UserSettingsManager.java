package nl.autovooru.android.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.NonNull;

/**
 * Created by Antonenko Viacheslav on 26/01/16.
 */
public final class UserSettingsManager {

    private static final String PREF_NAME = "user_settings";
    private static final String KEY_IS_NOTIFICATIONS_ENABLED = "is_notifications_enabled";
    private static final String KEY_IS_BID_NOTIFICATIONS_ENABLED = "is_bid_notifications_enabled";
    private static final String KEY_IS_MESSAGES_NOTIFICATIONS_ENABLED = "is_messages_notifications_enabled";
    private static final String KEY_IS_VIBRATION_ENABLED = "is_vibration_enabled";
    private static final String KEY_SOUND_URI = "sound_uri";

    private final SharedPreferences mSharedPreferences;

    public UserSettingsManager(@NonNull Context context) {
        mSharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public boolean notificationsEnabled() {
        return mSharedPreferences.getBoolean(KEY_IS_NOTIFICATIONS_ENABLED, true);
    }

    public void setNotificationsEnabled(boolean enabled) {
        mSharedPreferences.edit().putBoolean(KEY_IS_NOTIFICATIONS_ENABLED, enabled).commit();
    }

    public boolean bidNotificationsEnabled() {
        return mSharedPreferences.getBoolean(KEY_IS_BID_NOTIFICATIONS_ENABLED, true);
    }

    public void setBidNotificationsEnabled(boolean enabled) {
        mSharedPreferences.edit().putBoolean(KEY_IS_BID_NOTIFICATIONS_ENABLED, enabled).commit();
    }

    public boolean messagesNotificationsEnabled() {
        return mSharedPreferences.getBoolean(KEY_IS_MESSAGES_NOTIFICATIONS_ENABLED, true);
    }

    public void setMessagesNotificationsEnabled(boolean enabled) {
        mSharedPreferences.edit().putBoolean(KEY_IS_MESSAGES_NOTIFICATIONS_ENABLED, enabled).commit();
    }

    public boolean vibrationEnabled() {
        return mSharedPreferences.getBoolean(KEY_IS_VIBRATION_ENABLED, true);
    }

    public void setVibrationEnabled(boolean enabled) {
        mSharedPreferences.edit().putBoolean(KEY_IS_VIBRATION_ENABLED, enabled).commit();
    }

    public Uri getNotificationSound() {
        final String soundUri = mSharedPreferences.getString(KEY_SOUND_URI, null);
        if (soundUri == null) {
            return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        }
        return Uri.parse(soundUri);
    }

    public void setNotificationSound(Uri soundUri) {
        mSharedPreferences.edit().putString(KEY_SOUND_URI, soundUri.toString()).commit();
    }

}
