package nl.autovooru.android.di.components;

import dagger.Component;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.di.modules.MapsModule;
import nl.autovooru.android.ui.activity.MapsActivity;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = MapsModule.class)
public interface MapsComponent {

    void inject(MapsActivity mapsActivity);
}
