package nl.autovooru.android.di.components;

import android.app.Activity;

import dagger.Component;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.di.modules.ActivityModule;
import nl.autovooru.android.ui.fragments.NavigationFragment;

/**
 * A base component upon which fragment's components may depend.
 * Activity-level components should extend this component.
 * <p/>
 * Subtypes of ActivityComponent should be decorated with annotation:
 * {@link PerActivity}
 * <p/>
 * Created by Antonenko Viacheslav on 16/11/15.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    //Exposed to sub-graphs.
    Activity activity();

    void inject(NavigationFragment fragment);
}
