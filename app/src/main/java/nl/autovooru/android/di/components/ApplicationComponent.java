package nl.autovooru.android.di.components;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import nl.autovooru.android.BaseApp;
import nl.autovooru.android.di.modules.ApplicationModule;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.domain.repository.AuthorizationRepository;
import nl.autovooru.android.domain.repository.CommonRepository;
import nl.autovooru.android.domain.repository.FiltersRepository;
import nl.autovooru.android.domain.repository.MachinesRepository;
import nl.autovooru.android.domain.repository.UserDataRepository;
import nl.autovooru.android.push.MyGcmListenerService;
import nl.autovooru.android.push.RegistrationService;
import nl.autovooru.android.settings.UserSettingsManager;
import nl.autovooru.android.ui.activity.BaseActivity;
import nl.autovooru.android.ui.fragments.UserSettingsFragment;

/**
 * A component whose lifetime is the life of the application.
 * <p>
 * Created by Antonenko Viacheslav on 16/11/15.
 */
@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(BaseApp app);

    void inject(BaseActivity baseActivity);

    void inject(MyGcmListenerService service);

    void inject(RegistrationService service);

    void inject(UserSettingsFragment fragment);

    UserSettingsFragment.UserSettingsComponent plus(UserSettingsFragment.UserSettingsModule module);

    //Exposed to sub-graphs.
    Context context();

    AccountRepository accountRepository();

    FiltersRepository filterRepository();

    MachinesRepository machinesRepository();

    CommonRepository commonRepository();

    AuthorizationRepository authorizationRepository();

    UserDataRepository userDataRepository();

    UserSettingsManager userSettingsManager();

}
