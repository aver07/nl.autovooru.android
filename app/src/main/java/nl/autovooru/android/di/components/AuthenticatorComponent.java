package nl.autovooru.android.di.components;

import dagger.Component;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.mvp.presenter.LoginPresenter;
import nl.autovooru.android.mvp.presenter.RegistrationPresenter;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class)
public interface AuthenticatorComponent {

    RegistrationPresenter provideRegistrationPresenter();

    LoginPresenter provideLoginPresenter();
}
