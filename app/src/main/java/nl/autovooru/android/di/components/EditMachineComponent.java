package nl.autovooru.android.di.components;

import dagger.Component;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.di.modules.ActivityModule;
import nl.autovooru.android.di.modules.EditMachineModule;
import nl.autovooru.android.mvp.presenter.EditMachinePresenter;

/**
 * Created by Antonenko Viacheslav on 08/01/16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, EditMachineModule.class})
public interface EditMachineComponent {

    EditMachinePresenter provideEditMachinePresenter();
}
