package nl.autovooru.android.di.modules;

import dagger.Module;
import dagger.Provides;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.interactor.GetFilterTypesUseCase;
import nl.autovooru.android.domain.interactor.GetMachineSearchFiltersUseCase;
import nl.autovooru.android.domain.repository.FiltersRepository;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
@Module
public class MachineSearchFiltersModule {

    @Provides
    @PerActivity
    GetFilterTypesUseCase provideGetFilterTypesUseCase(FiltersRepository filtersRepository) {
        return new GetFilterTypesUseCase(filtersRepository);
    }

    @Provides
    @PerActivity
    GetMachineSearchFiltersUseCase provideGetMachineSearchFiltersUseCase(FiltersRepository filtersRepository) {
        return new GetMachineSearchFiltersUseCase(filtersRepository);
    }
}
