package nl.autovooru.android.di.components;

import dagger.Component;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.di.modules.MachineDetailsModule;
import nl.autovooru.android.mvp.presenter.MachineDetailsPresenter;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = MachineDetailsModule.class)
public interface MachineDetailsComponent {

    MachineDetailsPresenter provideMachineDetailsPresenter();
}
