package nl.autovooru.android.di.components;

import dagger.Component;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.mvp.presenter.NavigationPresenter;

/**
 * Created by Antonenko Viacheslav on 18/04/16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class)
public interface NavigationComponent {

    NavigationPresenter provideNavigationPresenter();
}
