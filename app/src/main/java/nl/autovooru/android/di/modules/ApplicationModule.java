package nl.autovooru.android.di.modules;

import android.content.Context;
import android.util.Log;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import nl.autovooru.android.BaseApp;
import nl.autovooru.android.authentication.AccountProvider;
import nl.autovooru.android.data.repository.AuthorizationRepositoryImpl;
import nl.autovooru.android.data.repository.CommonRepositoryImpl;
import nl.autovooru.android.data.repository.FiltersRepositoryImpl;
import nl.autovooru.android.data.repository.MachinesRepositoryImpl;
import nl.autovooru.android.data.repository.UserDataRepositoryImpl;
import nl.autovooru.android.domain.model.auth.AuthInfo;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.domain.repository.AuthorizationRepository;
import nl.autovooru.android.domain.repository.CommonRepository;
import nl.autovooru.android.domain.repository.FiltersRepository;
import nl.autovooru.android.domain.repository.MachinesRepository;
import nl.autovooru.android.domain.repository.UserDataRepository;
import nl.autovooru.android.settings.UserSettingsManager;

/**
 * Dagger module that provides objects which will live during the application lifecycle.
 * <p>
 * Created by Antonenko Viacheslav on 16/11/15.
 */
@Module(includes = ApiModule.class)
public class ApplicationModule {

    private final BaseApp mApplication;

    public ApplicationModule(BaseApp application) {
        mApplication = application;
    }

    @Singleton
    @Provides
    Context provideApplicationContext() {
        return mApplication;
    }

    @Singleton
    @Provides
    FiltersRepository provideFilterRepository(FiltersRepositoryImpl filterRepository) {
        return filterRepository;
    }

    @Singleton
    @Provides
    MachinesRepository provideMachinesRepository(MachinesRepositoryImpl machinesRepository) {
        return machinesRepository;
    }

    @Singleton
    @Provides
    CommonRepository provideCommonRepository(CommonRepositoryImpl commonRepository) {
        return commonRepository;
    }

    @Singleton
    @Provides
    @Named("appLanguage")
    String provideApplicationLanguage() {
        return mApplication.getResources().getConfiguration().locale.getLanguage();
    }

    @Singleton
    @Provides
    AuthorizationRepository provideAuthorizationRepository(AuthorizationRepositoryImpl repository) {
        return repository;
    }

    @Singleton
    @Provides
    UserDataRepository provideUserDataRepository(UserDataRepositoryImpl repository) {
        return repository;
    }

    @Singleton
    @Provides
    AccountRepository provideAccountRepository(Context context) {
        Log.d("provideAccount", "provideAccountRepository");
        return new AccountProvider(context);
    }

    @Provides
    AuthInfo provideAuthInfo(AccountRepository accountRepository) {
        return accountRepository.getAuthInfo();
    }

    @Provides
    @Named("userId")
    String provideUserId(AccountRepository accountRepository) {
        return accountRepository.getUserId();
    }

    @Singleton
    @Provides
    UserSettingsManager provideUserSettingsManager(Context context) {
        return new UserSettingsManager(context);
    }
}
