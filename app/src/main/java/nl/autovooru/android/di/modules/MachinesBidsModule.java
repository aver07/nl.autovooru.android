package nl.autovooru.android.di.modules;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import nl.autovooru.android.di.annotations.scope.PerActivity;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
@Module
public class MachinesBidsModule {

    private String mMachineId;

    public MachinesBidsModule(String machineId) {
        mMachineId = machineId;
    }

    @PerActivity
    @Provides
    @Named("machineId")
    public String getMachineId() {
        return mMachineId;
    }

}
