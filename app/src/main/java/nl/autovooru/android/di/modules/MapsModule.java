package nl.autovooru.android.di.modules;

import dagger.Module;
import dagger.Provides;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.interactor.SendFeedbackUseCase;
import nl.autovooru.android.domain.interactor.SendMessageUseCase;
import nl.autovooru.android.domain.repository.CommonRepository;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
@Module
public class MapsModule {

    @Provides
    @PerActivity
    SendFeedbackUseCase provideSendFeedbackUseCase(CommonRepository commonRepository) {
        return new SendFeedbackUseCase(commonRepository);
    }

    @Provides
    @PerActivity
    SendMessageUseCase provideSendMessageUseCase(CommonRepository commonRepository) {
        return new SendMessageUseCase(commonRepository);
    }
}
