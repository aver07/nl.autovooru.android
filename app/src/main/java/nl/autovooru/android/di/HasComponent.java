package nl.autovooru.android.di;

/**
 * Interface representing a contract for clients that contains a component for dependency injection.
 * <p/>
 * Created by Antonenko Viacheslav on 16/11/15.
 */
public interface HasComponent<C> {
    C getComponent();
}
