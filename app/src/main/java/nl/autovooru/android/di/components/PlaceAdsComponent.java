package nl.autovooru.android.di.components;

import dagger.Component;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.di.modules.ActivityModule;
import nl.autovooru.android.mvp.presenter.PlaceAdsPresenter;

/**
 * Created by Antonenko Viacheslav on 09/01/16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class})
public interface PlaceAdsComponent {

    PlaceAdsPresenter providePlaceAdsPresenter();
}
