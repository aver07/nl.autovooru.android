package nl.autovooru.android.di.components;

import dagger.Component;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.di.modules.ActivityModule;
import nl.autovooru.android.di.modules.UseCaseModule;
import nl.autovooru.android.mvp.presenter.AboutPresenter;
import nl.autovooru.android.mvp.presenter.HomePresenter;
import nl.autovooru.android.mvp.presenter.MyAccountPresenter;
import nl.autovooru.android.mvp.presenter.MyAdsPresenter;
import nl.autovooru.android.mvp.presenter.MyBidsPresenter;
import nl.autovooru.android.mvp.presenter.MyFavoritesPresenter;
import nl.autovooru.android.mvp.presenter.MyMessagesPresenter;
import nl.autovooru.android.mvp.presenter.PlaceAdsPresenter;
import nl.autovooru.android.mvp.presenter.SearchMachineListPresenter;

/**
 * Created by Antonenko Viacheslav on 16/11/15.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, UseCaseModule.class})
public interface UseCaseComponent extends ActivityComponent {

    HomePresenter provideHomePresenter();

    SearchMachineListPresenter provideSearchMachineListPresenter();

    AboutPresenter provideAboutPresenter();

    MyAdsPresenter provideMyAdsPresenter();

    MyFavoritesPresenter provideMyFavoritesPresenter();

    MyBidsPresenter provideMyBidsPresenter();

    MyMessagesPresenter provideMyMessagesPresenter();

    MyAccountPresenter provideMyAccountPresenter();

    PlaceAdsPresenter providePlaceAdsPresenter();
}
