package nl.autovooru.android.di.modules;

import dagger.Module;
import dagger.Provides;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.interactor.GetFilterTypesUseCase;
import nl.autovooru.android.domain.interactor.GetUserFavoritesUseCase;
import nl.autovooru.android.domain.interactor.SearchMachineListUseCase;
import nl.autovooru.android.domain.repository.FiltersRepository;
import nl.autovooru.android.domain.repository.MachinesRepository;
import nl.autovooru.android.domain.repository.UserDataRepository;

/**
 * Created by Antonenko Viacheslav on 16/11/15.
 */
@Module
public class UseCaseModule {

    @Provides
    @PerActivity
    GetFilterTypesUseCase provideGetFilterTypesUseCase(FiltersRepository filtersRepository) {
        return new GetFilterTypesUseCase(filtersRepository);
    }

    @Provides
    @PerActivity
    SearchMachineListUseCase provideSearchMachineListUseCase(MachinesRepository machinesRepository) {
        return new SearchMachineListUseCase(machinesRepository);
    }

    @Provides
    @PerActivity
    GetUserFavoritesUseCase provideGetUserFavoritesUseCase(UserDataRepository repository) {
        return new GetUserFavoritesUseCase(repository);
    }

}
