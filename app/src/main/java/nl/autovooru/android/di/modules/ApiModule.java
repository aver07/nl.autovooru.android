package nl.autovooru.android.di.modules;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import nl.autovooru.android.data.BuildConfig;
import nl.autovooru.android.data.api.Authenticator;
import nl.autovooru.android.data.api.AutovooruApiService;
import nl.autovooru.android.data.api.interceptors.AuthInterceptor;
import nl.autovooru.android.data.api.interceptors.HttpLoggingInterceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Antonenko Viacheslav on 02/11/15.
 */
@Module
public class ApiModule {

    private static final String sSERVER_API_URL = "http://app.autovooru.nl/";
    private static final String sBASIC_USERNAME = "prcode";
    private static final String sBASIC_PASSWORD = "CrjhjKtnj";

    @NonNull
    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient() {
        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            logging.setLevel(HttpLoggingInterceptor.Level.NONE);
        }

        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(new AuthInterceptor(sBASIC_USERNAME, sBASIC_PASSWORD))
                .authenticator(new Authenticator(sBASIC_USERNAME, sBASIC_PASSWORD))
                .build();
    }

    @NonNull
    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        final Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(sSERVER_API_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

        if (BuildConfig.DEBUG) {
            builder.validateEagerly(true);
        }

        return builder.build();
    }

    @NonNull
    @Provides
    @Singleton
    public AutovooruApiService provideAutovooruApiService(Retrofit retrofit) {
        return retrofit.create(AutovooruApiService.class);
    }
}
