package nl.autovooru.android.di.modules;

import dagger.Module;
import dagger.Provides;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.interactor.GetMachineDetailsUseCase;
import nl.autovooru.android.domain.interactor.SendBidUseCase;
import nl.autovooru.android.domain.repository.CommonRepository;
import nl.autovooru.android.domain.repository.MachinesRepository;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
@Module
public class MachineDetailsModule {

    @Provides
    @PerActivity
    GetMachineDetailsUseCase provideGetMachineDetailsUseCase(MachinesRepository repository) {
        return new GetMachineDetailsUseCase(repository);
    }

    @Provides
    @PerActivity
    SendBidUseCase provideSendBidUseCase(CommonRepository commonRepository) {
        return new SendBidUseCase(commonRepository);
    }
}
