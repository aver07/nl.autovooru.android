package nl.autovooru.android.di.components;

import dagger.Component;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.di.modules.ActivityModule;
import nl.autovooru.android.di.modules.MachinesBidsModule;
import nl.autovooru.android.mvp.presenter.MachinesBidsPresenter;
import nl.autovooru.android.ui.activity.MachinesBidsActivity;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, MachinesBidsModule.class})
public interface MachinesBidsComponent {

    void inject(MachinesBidsActivity activity);

    MachinesBidsPresenter provideMachinesBidsPresenter();
}
