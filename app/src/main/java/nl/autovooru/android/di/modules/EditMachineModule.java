package nl.autovooru.android.di.modules;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.model.machine.Machine;

/**
 * Created by Antonenko Viacheslav on 08/01/16.
 */
@Module
public class EditMachineModule {

    @NonNull
    private final Machine mMachine;

    public EditMachineModule(@NonNull Machine machine) {
        mMachine = machine;
    }

    @NonNull
    @PerActivity
    @Provides
    public Machine provideMachine() {
        return mMachine;
    }

}
