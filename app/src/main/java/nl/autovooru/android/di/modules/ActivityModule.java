package nl.autovooru.android.di.modules;

import android.app.Activity;

import dagger.Module;
import dagger.Provides;
import nl.autovooru.android.di.annotations.scope.PerActivity;

/**
 * A module to wrap the Activity state and expose it to the graph.
 * <p/>
 * Created by Antonenko Viacheslav on 16/11/15.
 */
@Module
public class ActivityModule {

    private final Activity mActivity;

    public ActivityModule(Activity activity) {
        mActivity = activity;
    }

    /**
     * Expose the activity to dependents in the graph.
     */
    @Provides
    @PerActivity
    Activity activity() {
        return mActivity;
    }

}
