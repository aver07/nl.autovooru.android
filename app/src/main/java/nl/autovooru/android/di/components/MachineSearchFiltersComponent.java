package nl.autovooru.android.di.components;

import dagger.Component;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.di.modules.MachineSearchFiltersModule;
import nl.autovooru.android.mvp.presenter.MachineSearchFiltersPresenter;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {MachineSearchFiltersModule.class})
public interface MachineSearchFiltersComponent {

    MachineSearchFiltersPresenter provideMachineSearchFiltersPresenter();
}
