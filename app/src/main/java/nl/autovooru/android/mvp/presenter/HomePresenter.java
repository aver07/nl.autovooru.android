package nl.autovooru.android.mvp.presenter;

import android.support.annotation.IdRes;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import nl.autovooru.android.bus.events.NavigationRequestEvent;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.mvp.view.HomeView;

/**
 * {@link Presenter} that controls communication between views and models of the presentation
 * layer.
 * <p>
 * Created by Antonenko Viacheslav on 16/11/15.
 */
@PerActivity
public class HomePresenter extends MvpBasePresenter<HomeView> {

    @Inject
    public HomePresenter() {

    }

    public void onCategoryClick(@IdRes int navigationResId) {
        EventBus.getDefault().post(new NavigationRequestEvent(navigationResId));
    }

    public void onCategoryClick(@IdRes int navigationResId, String keywords) {
        EventBus.getDefault().post(new NavigationRequestEvent(navigationResId, keywords));
    }

}
