package nl.autovooru.android.mvp.view;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import nl.autovooru.android.domain.viewmodels.MachineListViewModel;

/**
 * Created by Antonenko Viacheslav on 23/12/15.
 */
public interface MyAdsView extends MvpLceView<MachineListViewModel> {

    void addMore(MachineListViewModel data);
}
