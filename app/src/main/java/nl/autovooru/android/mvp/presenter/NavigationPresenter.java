package nl.autovooru.android.mvp.presenter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.IdRes;

import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import nl.autovooru.android.R;
import nl.autovooru.android.bus.events.AuthorizationChangedEvent;
import nl.autovooru.android.bus.events.NavigationEvent;
import nl.autovooru.android.bus.events.NavigationRequestEvent;
import nl.autovooru.android.bus.events.NavigationSelectEvent;
import nl.autovooru.android.common.utils.L;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.mvp.view.NavigationView;
import nl.autovooru.android.ui.activity.AuthenticatorActivity;
import nl.autovooru.android.ui.activity.PlaceAdsActivity;

/**
 * Created by Antonenko Viacheslav on 16/11/15.
 */
@PerActivity
public class NavigationPresenter extends MvpNullObjectBasePresenter<NavigationView> {

    private static final L LOG = L.getLogger(NavigationPresenter.class);

    private static final int DEFAULT_MENU_ITEM = R.id.nav_home;
    @Inject
    AccountRepository mAccountRepository;
    @IdRes
    private int mSelectedMenuId = -1;

    @Inject
    public NavigationPresenter() {
    }

    @Override
    public void attachView(NavigationView view) {
        super.attachView(view);
        EventBus.getDefault().register(this);
    }

    @Override
    public void detachView(boolean retainInstance) {
        EventBus.getDefault().unregister(this);
    }

    public void onStart() {
        updateAccountRelatedNavigation();
    }

    public void onResume() {
        // TODO: 21/12/15 Implement login logic
        updateAccountRelatedNavigation();

        if (mSelectedMenuId == -1) {
            onMenuItemClick(DEFAULT_MENU_ITEM);
        }
    }

    private void updateAccountRelatedNavigation() {
        LOG.d("updateAccountRelatedNavigation(); isLoggedIn=" + mAccountRepository.isLoggedIn());
        if (mAccountRepository.isLoggedIn()) {
            getView().updateUserInfoView(mAccountRepository.getUsername(), mAccountRepository.getEmail());
            getView().showUserInfoView();
            getView().showAccountRelatedView();
            getView().hideLoginView();
        } else {
            getView().hideUserInfoView();
            getView().hideAccountRelatedView();
            getView().showLoginView();
        }
    }

    public void onEventMainThread(final AuthorizationChangedEvent event) {
        LOG.d("AuthorizationChangedEvent(); isAuthorized=" + event.isAuthorized());
        if (event.isAuthorized()) {
            updateAccountRelatedNavigation();
        } else {
            updateAccountRelatedNavigation();
            onMenuItemClick(DEFAULT_MENU_ITEM);
        }
    }

    public void onEvent(final NavigationRequestEvent event) {
        if (event.getNavigationResId() != mSelectedMenuId) {
            onMenuItemClick(event.getNavigationResId(), event.getKeyword());
        }
    }

    public void onEvent(final NavigationSelectEvent event) {
        if (event.getNavigationResId() != mSelectedMenuId) {
            if (mSelectedMenuId != -1) {
                getView().uncheckMenuItem(mSelectedMenuId);
            }
            getView().checkMenuItem(event.getNavigationResId());
            mSelectedMenuId = event.getNavigationResId();
        }
    }

    public void onCategoryItemClick() {
        getView().expandOrCollapseCategory();
    }

    public void onMenuItemClick(@IdRes int menuId) {
        if (menuId == R.id.nav_sign_out) {
            mAccountRepository.logout();
        } else if (menuId == R.id.nav_add_advertise) {
            if (mAccountRepository.isLoggedIn()) {
                final Intent intent = new Intent(getView().getActivity(), PlaceAdsActivity.class);
                getView().getActivity().startActivity(intent);
            } else {
                AuthenticatorActivity.openActivity(getView().getActivity(), true);
            }
        } else {
            onMenuItemClick(menuId, null);
        }
    }

    public void onMenuItemClick(@IdRes int menuId, String keyword) {
        if (menuId != mSelectedMenuId) {
            if (mSelectedMenuId != -1) {
                getView().uncheckMenuItem(mSelectedMenuId);
            }
            getView().checkMenuItem(menuId);
            mSelectedMenuId = menuId;
            EventBus.getDefault().post(new NavigationEvent(mSelectedMenuId, keyword));
        }
    }

    public void openLoginForm(Context context) {
        AuthenticatorActivity.openActivity(context, true);
    }

    public void openRegistrationForm(Context context) {
        AuthenticatorActivity.openActivity(context, false);
    }

}
