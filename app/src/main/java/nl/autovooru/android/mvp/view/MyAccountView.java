package nl.autovooru.android.mvp.view;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import nl.autovooru.android.domain.model.user.UserInfo;

/**
 * Created by Antonenko Viacheslav on 29/12/15.
 */
public interface MyAccountView extends MvpLceView<UserInfo> {

    void showOrganizationSpecificViews();

    void hideOrganizationSpecificViews();

    void showUserSpecificViews();

    void hideUserSpecificViews();

    void showProgress();

    void hideProgress();

}
