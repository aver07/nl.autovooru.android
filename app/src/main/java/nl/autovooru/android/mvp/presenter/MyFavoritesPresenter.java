package nl.autovooru.android.mvp.presenter;

import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;

import javax.inject.Inject;

import nl.autovooru.android.domain.interactor.DefaultSubscriber;
import nl.autovooru.android.domain.interactor.GetUserFavoritesUseCase;
import nl.autovooru.android.domain.interactor.RemoveFromFavoriteUseCase;
import nl.autovooru.android.domain.interactor.SendBidUseCase;
import nl.autovooru.android.domain.interactor.SendMessageUseCase;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.domain.model.query.Order;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import nl.autovooru.android.mvp.view.MyFavoritesView;
import nl.autovooru.android.rx.RxUtils;
import rx.Subscriber;
import rx.Subscription;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public class MyFavoritesPresenter extends MvpNullObjectBasePresenter<MyFavoritesView> {

    private final Runnable mBeforeLoading = () -> getView().showProgress();
    private final Runnable mAfterLoading = () -> getView().hideProgress();
    private final RemoveFromFavoriteUseCase mRemoveFromFavoriteUseCase;
    private final GetUserFavoritesUseCase mGetUserFavoritesUseCase;
    private final SendMessageUseCase mSendMessageUseCase;
    private final AccountRepository mAccountRepository;
    private final SendBidUseCase mSendBidMessageUseCase;
    private Subscription mGetUserFavoritesSubscription;

    @Inject
    public MyFavoritesPresenter(@NonNull RemoveFromFavoriteUseCase removeFromFavoriteUseCase,
                                @NonNull GetUserFavoritesUseCase getUserFavoritesUseCase,
                                @NonNull SendBidUseCase sendBidUseCase,
                                @NonNull SendMessageUseCase sendMessageUseCase,
                                @NonNull AccountRepository accountRepository) {
        mRemoveFromFavoriteUseCase = removeFromFavoriteUseCase;
        mGetUserFavoritesUseCase = getUserFavoritesUseCase;
        mSendBidMessageUseCase = sendBidUseCase;
        mSendMessageUseCase = sendMessageUseCase;
        mAccountRepository = accountRepository;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        RxUtils.unsubscribe(mGetUserFavoritesSubscription);
    }

    public void loadFavorites(final boolean pullToRefresh) {
        getView().showLoading(pullToRefresh);
        mGetUserFavoritesSubscription = mGetUserFavoritesUseCase.load()
                .compose(RxUtils.applySchedulers())
                .subscribe(new Subscriber<MachineListViewModel>() {
                    @Override
                    public void onCompleted() {
                        getView().showContent();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, pullToRefresh);
                    }

                    @Override
                    public void onNext(MachineListViewModel machineListViewModel) {
                        getView().setData(machineListViewModel);
                    }
                });

    }

    public void loadMoreFavorites() {
        mGetUserFavoritesSubscription = mGetUserFavoritesUseCase.loadMore()
                .compose(RxUtils.applySchedulers())
                .subscribe(new Subscriber<MachineListViewModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().addMore(null);
                        getView().showError(e, true);
                    }

                    @Override
                    public void onNext(MachineListViewModel machineListViewModel) {
                        getView().addMore(machineListViewModel);
                    }
                });
    }

    public void setOrder(Order order) {
        mGetUserFavoritesUseCase.setOrder(order);
    }

    public void removeOrder() {
        mGetUserFavoritesUseCase.removeOrder();
    }

    public void removeFromFavorite(final Machine machine) {
        mRemoveFromFavoriteUseCase.removeFromFavorite(machine.getMachineId())
                .compose(RxUtils.applySchedulers())
                .compose(RxUtils.applyOpBeforeAndAfter(mBeforeLoading, mAfterLoading))
                .subscribe(new DefaultSubscriber<Void>() {
                    @Override
                    public void onCompleted() {
                        getView().showContent();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, true);
                    }

                    @Override
                    public void onNext(Void aVoid) {
                        getView().removeMachine(machine);
                    }
                });
    }

    public void sendMessage(final String machineId, String email, String name, String subject, String message) {
        mSendMessageUseCase.sendMessage(machineId, email, name, subject, message)
                .compose(RxUtils.applySchedulers())
                .subscribe(new DefaultSubscriber<Boolean>() {
                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, true);
                    }
                });
    }

    public void placeBid(String machineId, String name, String email, String bid, String message) {
        mSendBidMessageUseCase.sendBid(machineId, name, email, bid, message)
                .compose(RxUtils.applySchedulers())
                .subscribe(new DefaultSubscriber<Boolean>() {
                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, true);
                    }
                });
    }

    public String getUserEmail() {
        return mAccountRepository.getEmail();
    }

    public String getUsename() {
        return mAccountRepository.getUsername();
    }
}
