package nl.autovooru.android.mvp.presenter;

import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;

import javax.inject.Inject;

import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.interactor.DeleteMyAdUseCase;
import nl.autovooru.android.domain.interactor.MyAdsUseCase;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import nl.autovooru.android.mvp.view.MyAdsView;
import nl.autovooru.android.rx.RxUtils;
import rx.Subscriber;
import rx.Subscription;

/**
 * Created by Antonenko Viacheslav on 23/12/15.
 */
@PerActivity
public class MyAdsPresenter extends MvpNullObjectBasePresenter<MyAdsView> {

    private final MyAdsUseCase mMyAdsUseCase;
    private final DeleteMyAdUseCase mDeleteMyAdUseCase;
    private Subscription mSubscribe;

    @Inject
    public MyAdsPresenter(MyAdsUseCase myAdsUseCase,
                          DeleteMyAdUseCase deleteMyAdUseCase) {
        mMyAdsUseCase = myAdsUseCase;
        mDeleteMyAdUseCase = deleteMyAdUseCase;
    }

    @Override
    public void detachView(boolean retainInstance) {
        RxUtils.unsubscribe(mSubscribe);
        super.detachView(retainInstance);
    }

    public void deleteMyAd(@NonNull String machineId) {
        mDeleteMyAdUseCase.delete(machineId)
                .compose(RxUtils.applySchedulers())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {
                        load(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, false);
                    }

                    @Override
                    public void onNext(Void aVoid) {

                    }
                });
    }

    public void load(final boolean pullToRefresh) {
        getView().showLoading(pullToRefresh);
        RxUtils.unsubscribe(mSubscribe);

        mSubscribe = mMyAdsUseCase.load()
                .compose(RxUtils.applySchedulers())
                .subscribe(new Subscriber<MachineListViewModel>() {
                    @Override
                    public void onCompleted() {
                        getView().showContent();
                        RxUtils.unsubscribe(mSubscribe);
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, pullToRefresh);
                        RxUtils.unsubscribe(mSubscribe);
                    }

                    @Override
                    public void onNext(MachineListViewModel machineListViewModel) {
                        getView().setData(machineListViewModel);
                    }
                });
    }

    public void loadMore() {
        if (mSubscribe == null || mSubscribe.isUnsubscribed()) {
            mSubscribe = mMyAdsUseCase.loadMore()
                    .compose(RxUtils.applySchedulers())
                    .subscribe(new Subscriber<MachineListViewModel>() {
                        @Override
                        public void onCompleted() {
                            RxUtils.unsubscribe(mSubscribe);
                        }

                        @Override
                        public void onError(Throwable e) {
                            getView().showError(e, false);
                            RxUtils.unsubscribe(mSubscribe);
                        }

                        @Override
                        public void onNext(MachineListViewModel data) {
                            getView().addMore(data);
                        }
                    });
        }
    }
}
