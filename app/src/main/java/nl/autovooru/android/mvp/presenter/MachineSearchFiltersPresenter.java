package nl.autovooru.android.mvp.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Pair;

import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.interactor.GetFilterTypesUseCase;
import nl.autovooru.android.domain.interactor.GetMachineSearchFiltersUseCase;
import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.model.filters.FilterType;
import nl.autovooru.android.domain.model.machine.MachineFilter;
import nl.autovooru.android.domain.model.machine.MachineSearchFilters;
import nl.autovooru.android.domain.model.query.MachineSearchQuery;
import nl.autovooru.android.mvp.view.MachineSearchFiltersView;
import nl.autovooru.android.rx.RxUtils;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

/**
 * Created by Antonenko Viacheslav on 10/12/15.
 */
@PerActivity
public class MachineSearchFiltersPresenter extends MvpNullObjectBasePresenter<MachineSearchFiltersView> {

    private static final FilterType FILTER_TYPE = new FilterType("5", "vcat", "");

    private final GetFilterTypesUseCase mGetFilterTypesUseCase;
    private final GetMachineSearchFiltersUseCase mGetMachineSearchFiltersUseCase;
    private final Runnable mBeforeLoading = () -> getView().showProgress();
    private final Runnable mAfterLoading = () -> getView().hideProgress();

    private List<Filter> mMachineCategories;
    private MachineSearchFilters mMachineSearchFilters;
    @NonNull
    private MachineSearchFilters.MainFilters mMainFilters;

    private MachineSearchQuery mDefaultQuery;
    private MachineSearchQuery mQuery;

    private Subscription mInitialLoadingSubscriber;
    private Subscription mMachineSearchFiltersSubscriber;
    private Subscription mMachineMarkSubscriber;
    private MachineSearchFilters.ToggleFilters mToggleFilters;
    private List<MachineFilter> mOtherFilters;

    @Inject
    public MachineSearchFiltersPresenter(GetFilterTypesUseCase getFilterTypesUseCase,
                                         GetMachineSearchFiltersUseCase getMachineSearchFiltersUseCase) {
        mGetFilterTypesUseCase = getFilterTypesUseCase;
        mGetMachineSearchFiltersUseCase = getMachineSearchFiltersUseCase;
        mQuery = new MachineSearchQuery();
    }

    public void initialLoad(final MachineSearchQuery machineSearchQuery, final boolean pullToRefresh) {
        if (mMachineCategories == null || mMachineSearchFilters == null || pullToRefresh) {
            getView().showLoading(pullToRefresh);
            mDefaultQuery = machineSearchQuery;
            mQuery.apply(machineSearchQuery);

            final String vcat = mQuery.getMachineCategory().getId();
            final String mark = mQuery.getMark() != null ? mQuery.getMark().getValue() : null;

            RxUtils.unsubscribe(mInitialLoadingSubscriber);

            mInitialLoadingSubscriber = Observable.combineLatest(
                    mGetFilterTypesUseCase.get(FILTER_TYPE),
                    mGetMachineSearchFiltersUseCase.getFilters(vcat, mark), (stringListMap, machineSearchFilters) -> {
                        final List<Filter> filters = stringListMap.get(FILTER_TYPE.getKey());
                        return new Pair<>(filters, machineSearchFilters);
                    })
                    .compose(RxUtils.applySchedulers())
                    .subscribe(new Subscriber<Pair<List<Filter>, MachineSearchFilters>>() {
                        @Override
                        public void onCompleted() {
                            getView().showContent();
                        }

                        @Override
                        public void onError(Throwable e) {
                            getView().showError(e, true);
                        }

                        @Override
                        public void onNext(Pair<List<Filter>, MachineSearchFilters> result) {
                            mMachineCategories = result.first;
                            mMachineSearchFilters = result.second;
                            mMainFilters = mMachineSearchFilters.getMainFilters();
                            mToggleFilters = mMachineSearchFilters.getToggleFilters();
                            mOtherFilters = mMachineSearchFilters.getOtherFilters();

                            setupDefaultValues();
                            initUI();
                        }
                    });
        } else {
            initUI();
        }
    }

    public List<Filter> getMachineCategories() {
        return mMachineCategories;
    }

    @NonNull
    public Filter getMachineCategory() {
        return mQuery.getMachineCategory();
    }

    public void setMachineCategory(@NonNull Filter machineCategory) {
        getView().setMachineCategory(machineCategory);
        mQuery.setMachineCategory(machineCategory);
        mQuery.setMark(getDefaultValue(mMainFilters.getMachineMarks().getFirstOptionsList(), null));
        mQuery.setModel(getDefaultValue(mMainFilters.getMachineModels().getFirstOptionsList(), null));

        RxUtils.unsubscribe(mMachineSearchFiltersSubscriber);

        mMachineSearchFiltersSubscriber = mGetMachineSearchFiltersUseCase.getFilters(mQuery.getMachineCategory().getId(), null)
                .compose(RxUtils.applySchedulers())
                .compose(RxUtils.applyOpBeforeAndAfter(mBeforeLoading, mAfterLoading))
                .subscribe(new Subscriber<MachineSearchFilters>() {
                    @Override
                    public void onCompleted() {
                        getView().showContent();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, false);
                    }

                    @Override
                    public void onNext(MachineSearchFilters machineSearchFilters) {
                        mMachineSearchFilters = machineSearchFilters;
                        mMainFilters = mMachineSearchFilters.getMainFilters();
                        mToggleFilters = mMachineSearchFilters.getToggleFilters();
                        mOtherFilters = mMachineSearchFilters.getOtherFilters();

                        initUI();
                    }
                });
        // reload machine filters
    }

    public List<MachineFilter.Options> getTopMarks() {
        final MachineFilter topMachineMarks = mMainFilters.getTopMachineMarks();
        if (topMachineMarks != null) {
            return topMachineMarks.getFirstOptionsList();
        } else {
            return null;
        }
    }

    public List<MachineFilter.Options> getMachineMarks() {
        return mMainFilters.getMachineMarks().getFirstOptionsList();
    }

    public MachineFilter.Options getMark() {
        return mQuery.getMark();
    }

    public void setMark(@NonNull MachineFilter.Options mark) {
        getView().setMark(getMarkLabel(), mark.getName());
        mQuery.setMark(mark);
        mQuery.setModel(getDefaultValue(mMainFilters.getMachineModels().getFirstOptionsList(), null));

        RxUtils.unsubscribe(mMachineMarkSubscriber);

        mMachineMarkSubscriber = mGetMachineSearchFiltersUseCase.getFilters(mQuery.getMachineCategory().getId(), mark.getValue())
                .compose(RxUtils.applySchedulers())
                .compose(RxUtils.applyOpBeforeAndAfter(mBeforeLoading, mAfterLoading))
                .subscribe(new Subscriber<MachineSearchFilters>() {
                    @Override
                    public void onCompleted() {
                        getView().showContent();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, false);
                    }

                    @Override
                    public void onNext(MachineSearchFilters machineSearchFilters) {
                        mMachineSearchFilters = machineSearchFilters;
                        mMainFilters = mMachineSearchFilters.getMainFilters();
                        mToggleFilters = mMachineSearchFilters.getToggleFilters();
                        mOtherFilters = mMachineSearchFilters.getOtherFilters();

                        initUI();
                    }
                });

    }

    public List<MachineFilter.Options> getModels() {
        return mMainFilters.getMachineModels().getFirstOptionsList();
    }

    public MachineFilter.Options getModel() {
        return mQuery.getModel();
    }

    public void setModel(@NonNull MachineFilter.Options model) {
        getView().setModel(getModelsLabel(), model.getName());
        mQuery.setModel(model);
    }

    public List<MachineFilter.Options> getStartYears() {
        if (mQuery.getEndYear() != null) {
            final List<MachineFilter.Options> list = mMainFilters.getMachineYear().getFirstOptionsList();
            final int position = list.indexOf(mQuery.getEndYear());
            if (position != -1) {
                return list.subList(position, list.size());
            }
        }
        return mMainFilters.getMachineYear().getFirstOptionsList();
    }

    public MachineFilter.Options getStartYear() {
        return mQuery.getStartYear();
    }

    public void setStartYear(@NonNull MachineFilter.Options machineYear) {
        getView().setStartYear(getMachineYearLabel(), machineYear.getName());
        mQuery.setStartYear(machineYear);
    }

    public List<MachineFilter.Options> getEndYears() {
        if (mQuery.getStartYear() != null) {
            final List<MachineFilter.Options> list = mMainFilters.getMachineYear().getSecondOptionsList();
            final int position = list.indexOf(mQuery.getStartYear());
            if (position > 0) {
                return list.subList(0, position + 1);
            }
        }
        return mMainFilters.getMachineYear().getSecondOptionsList();
    }

    public MachineFilter.Options getEndYear() {
        return mQuery.getEndYear();
    }

    public void setEndYear(@NonNull MachineFilter.Options machineYear) {
        getView().setEndYear(getMachineYearLabel(), machineYear.getName());
        mQuery.setEndYear(machineYear);
    }

    //
    public List<MachineFilter.Options> getStartPrices() {
        if (mQuery.getEndPrice() != null) {
            final List<MachineFilter.Options> list = mMainFilters.getPrice().getFirstOptionsList();
            final int position = list.indexOf(mQuery.getEndPrice());
            if (position > 0) {
                return list.subList(0, position + 1);
            }
        }
        return mMainFilters.getPrice().getFirstOptionsList();
    }

    public MachineFilter.Options getStartPrice() {
        return mQuery.getStartPrice();
    }

    public void setStartPrice(@NonNull MachineFilter.Options price) {
        getView().setStartPrice(gerPriceLabel(), price.getName());
        mQuery.setStartPrice(price);
    }

    public List<MachineFilter.Options> getEndPrices() {
        if (mQuery.getStartPrice() != null) {
            final List<MachineFilter.Options> list = mMainFilters.getPrice().getSecondOptionsList();
            final int position = list.indexOf(mQuery.getStartPrice());
            if (position != -1) {
                return list.subList(position, list.size());
            }
        }
        return mMainFilters.getPrice().getSecondOptionsList();
    }

    public MachineFilter.Options getEndPrice() {
        return mQuery.getEndPrice();
    }

    public void setEndPrice(@NonNull MachineFilter.Options price) {
        getView().setEndPrice(gerPriceLabel(), (price.getName()));
        mQuery.setEndPrice(price);
    }

    public List<MachineFilter.Options> getFuels() {
        return mMainFilters.getFuel().getFirstOptionsList();
    }

    public MachineFilter.Options getFuel() {
        return mQuery.getFuel();
    }

    public void setFuel(@NonNull MachineFilter.Options fuel) {
        getView().setFuel(getFuelLabel(), fuel.getName());
        mQuery.setFuel(fuel);
    }

    public List<MachineFilter.Options> getUserTypes() {
        return mMainFilters.getUserType().getFirstOptionsList();
    }

    public MachineFilter.Options getUserType() {
        return mQuery.getUserType();
    }

    public void setUserType(@NonNull MachineFilter.Options userType) {
        getView().setDiller(getUserTypeLabel(), userType.getName());
        mQuery.setUserType(userType);
    }

    public void setKeywords(@Nullable String keywords) {
        mQuery.setKeywords(keywords);
    }

    public void setPostCode(@Nullable String postcode) {
        mQuery.setPostcode(postcode);
    }

    public List<MachineFilter.Options> getDistanceList() {
        return mMainFilters.getDistance().getFirstOptionsList();
    }

    public MachineFilter.Options getDistance() {
        return mQuery.getDistance();
    }

    public void setDistance(@NonNull MachineFilter.Options distance) {
        getView().setDistance(getDistanceLabel(), distance.getName());
        mQuery.setDistance(distance);
    }

    public List<MachineFilter.Options> getMileageList() {
        return mMainFilters.getMileage().getFirstOptionsList();
    }

    public MachineFilter.Options getMileage() {
        return mQuery.getMileage();
    }

    public void setMileage(@NonNull MachineFilter.Options mileage) {
        getView().setMileage(getMileageLabel(), mileage.getName());
        mQuery.setMileage(mileage);
    }

    public List<MachineFilter.Options> getOfferTimeList() {
        return mToggleFilters.getOfferTime().getFirstOptionsList();
    }

    public MachineFilter.Options getOfferTime() {
        return mQuery.getOfferTime();
    }

    public void setOfferTime(@NonNull MachineFilter.Options offerTime) {
        getView().setOfferTime(getOfferTimeLabel(), offerTime.getName());
        mQuery.setOfferTime(offerTime);
    }

    public List<MachineFilter.Options> getTransmissionList() {
        return mToggleFilters.getTransmission().getFirstOptionsList();
    }

    public MachineFilter.Options getTransmission() {
        return mQuery.getTransmission();
    }

    public void setTransmission(@NonNull MachineFilter.Options transmission) {
        getView().setTransmissions(getTransmissionLabel(), transmission.getName());
        mQuery.setTransmission(transmission);
    }

    public List<MachineFilter.Options> getColors() {
        return mToggleFilters.getColor().getFirstOptionsList();
    }

    public MachineFilter.Options getColor() {
        return mQuery.getColor();
    }

    public void setColor(@NonNull MachineFilter.Options color) {
        getView().setColors(getColorLabel(), color.getName());
        mQuery.setColor(color);
    }

    public List<MachineFilter.Options> getBodyList() {
        return mToggleFilters.getBody().getFirstOptionsList();
    }

    public MachineFilter.Options getBody() {
        return mQuery.getBody();
    }

    public void setBody(@NonNull MachineFilter.Options body) {
        getView().setBody(getBodyLabel(), body.getName());
        mQuery.setBody(body);
    }

    public void resetFilters() {
        final MachineSearchQuery query = new MachineSearchQuery();
        query.setMachineCategory(mDefaultQuery.getMachineCategory());
        mDefaultQuery = query;
        initialLoad(mDefaultQuery, true);
    }

    public MachineSearchQuery getFilters() {
        return mQuery;
    }

    @NonNull
    public List<MachineFilter.Options> getAllOtherFilters(String key) {
        final MachineFilter filter = getFilterByKey(mOtherFilters, key);
        if (filter != null) {
            return filter.getFirstOptionsList();
        }

        return new ArrayList<>();
    }

    public void setOtherFilter(String key, @Nullable Integer[] selectedIds) {
        final MachineFilter filter = getFilterByKey(mOtherFilters, key);
        if (filter != null) {
            final ArrayList<MachineFilter.Options> options = new ArrayList<>();
            if (selectedIds != null && selectedIds.length > 0) {
                for (Integer selectedId : selectedIds) {
                    options.add(filter.getFirstOptionsList().get(selectedId));
                }
            }

            mQuery.setOtherFilters(key, options);
            getView().setOtherFilter(key, filter.getName(), options);
        }
    }

    @Nullable
    public Integer[] getOtherFiltersSelectedPositions(String key) {
        final List<MachineFilter.Options> options = mQuery.getOtherFilters().get(key);
        if (options != null) {
            final MachineFilter filter = getFilterByKey(mOtherFilters, key);
            if (filter != null) {
                ArrayList<Integer> selectedPositions = new ArrayList<>(options.size());
                final int size = filter.getFirstOptionsList().size();
                for (int i = 0; i < size; i++) {
                    final MachineFilter.Options option = filter.getFirstOptionsList().get(i);
                    if (options.contains(option)) {
                        selectedPositions.add(i);
                    }
                }
                return selectedPositions.toArray(new Integer[selectedPositions.size()]);
            }
        }

        return null;
    }

    @Nullable
    private MachineFilter getFilterByKey(List<MachineFilter> optionsList, String key) {
        for (MachineFilter filter : optionsList) {
            if (filter.getValue().equals(key)) {
                return filter;
            }
        }
        return null;
    }

    private void initUI() {
        getView().setMachineCategory(mQuery.getMachineCategory());
        getView().setMark(getMarkLabel(), getName(mQuery.getMark()));
        getView().setModel(getModelsLabel(), getName(mQuery.getModel()));
        getView().setStartYear(getMachineYearLabel(), getName(mQuery.getStartYear()));
        getView().setEndYear(getMachineYearLabel(), getName(mQuery.getEndYear()));
        getView().setStartPrice(gerPriceLabel(), getName(mQuery.getStartPrice()));
        getView().setEndPrice(gerPriceLabel(), getName(mQuery.getEndPrice()));
        getView().setFuel(getFuelLabel(), getName(mQuery.getFuel()));
        getView().setDiller(getUserTypeLabel(), getName(mQuery.getUserType()));
        getView().setKeyword(getKeywordsLabel(), mQuery.getKeywords());
        getView().setPostCode(getPostCodeLabel(), mQuery.getPostcode());
        getView().setDistance(getDistanceLabel(), getName(mQuery.getDistance()));
        getView().setMileage(getMileageLabel(), getName(mQuery.getMileage()));

        getView().setOfferTime(getOfferTimeLabel(), getName(mQuery.getOfferTime()));
        getView().setTransmissions(getTransmissionLabel(), getName(mQuery.getTransmission()));
        getView().setColors(getColorLabel(), getName(mQuery.getColor()));
        getView().setBody(getBodyLabel(), getName(mQuery.getBody()));

        for (MachineFilter filter : mOtherFilters) {
            final List<MachineFilter.Options> options = mQuery.getOtherFilters().get(filter.getValue());
            if (options != null) {
                getView().setOtherFilter(filter.getValue(), filter.getName(), options);
            } else {
                getView().setOtherFilter(filter.getValue(), filter.getName(), new ArrayList<>());
            }
        }

        if ((mQuery.getOfferTime() != null && !TextUtils.equals(mQuery.getOfferTime().getValue(), "0"))
                || (mQuery.getTransmission() != null && !TextUtils.equals(mQuery.getTransmission().getValue(), "0"))
                || (mQuery.getColor() != null && !TextUtils.equals(mQuery.getColor().getValue(), "0"))
                || (mQuery.getBody() != null && !TextUtils.equals(mQuery.getBody().getValue(), "0"))
                || !mQuery.getOtherFilters().isEmpty()) {
            getView().showOtherFilters();
        } else {
            getView().hideOtherFilters();
        }
    }

    public String getMarkLabel() {
        return mMainFilters.getMachineMarks().getName();
    }

    public String getModelsLabel() {
        return mMainFilters.getMachineModels().getName();
    }

    public String getBodyLabel() {
        return mToggleFilters.getBody().getName();
    }

    public String getColorLabel() {
        return mToggleFilters.getColor().getName();
    }

    public String getTransmissionLabel() {
        return mToggleFilters.getTransmission().getName();
    }

    public String getOfferTimeLabel() {
        return mToggleFilters.getOfferTime().getName();
    }

    public String getMileageLabel() {
        return mMainFilters.getMileage().getName();
    }

    public String getDistanceLabel() {
        return mMainFilters.getDistance().getName();
    }

    public String getPostCodeLabel() {
        return mMainFilters.getPostcode().getName();
    }

    public String getKeywordsLabel() {
        return mMainFilters.getKeywords().getName();
    }

    public String getUserTypeLabel() {
        return mMainFilters.getUserType().getName();
    }

    public String getFuelLabel() {
        return mMainFilters.getFuel().getName();
    }

    public String gerPriceLabel() {
        return mMainFilters.getPrice().getName();
    }

    public String getMachineYearLabel() {
        return mMainFilters.getMachineYear().getName();
    }

    public String getOtherFiltersLabel(String key) {
        for (MachineFilter filter : mOtherFilters) {
            if (filter.getValue().equals(key)) {
                return filter.getName();
            }
        }

        return "";
    }

    private void setupDefaultValues() {
        mQuery.setMachineCategory(mDefaultQuery.getMachineCategory());
        mQuery.setMark(getDefaultValue(mMainFilters.getMachineMarks().getFirstOptionsList(), mQuery.getMark()));
        mQuery.setModel(getDefaultValue(mMainFilters.getMachineModels().getFirstOptionsList(), mQuery.getModel()));
        mQuery.setStartYear(getDefaultValue(mMainFilters.getMachineYear().getFirstOptionsList(), mQuery.getStartYear()));
        mQuery.setEndYear(getDefaultValue(mMainFilters.getMachineYear().getSecondOptionsList(), mQuery.getEndYear()));
        mQuery.setStartPrice(getDefaultValue(mMainFilters.getPrice().getFirstOptionsList(), mQuery.getStartPrice()));
        mQuery.setEndPrice(getDefaultValue(mMainFilters.getPrice().getSecondOptionsList(), mQuery.getEndPrice()));
        mQuery.setFuel(getDefaultValue(mMainFilters.getFuel().getFirstOptionsList(), mQuery.getFuel()));
        mQuery.setUserType(getDefaultValue(mMainFilters.getUserType().getFirstOptionsList(), mQuery.getUserType()));
        mQuery.setKeywords(mDefaultQuery.getKeywords());
        mQuery.setPostcode(null);
        mQuery.setDistance(getDefaultValue(mMainFilters.getDistance().getFirstOptionsList(), mQuery.getDistance()));
        mQuery.setMileage(getDefaultValue(mMainFilters.getMileage().getFirstOptionsList(), mQuery.getMileage()));

        mQuery.setOfferTime(getDefaultValue(mToggleFilters.getOfferTime().getFirstOptionsList(), mQuery.getOfferTime()));
        mQuery.setTransmission(getDefaultValue(mToggleFilters.getTransmission().getFirstOptionsList(), mQuery.getTransmission()));
        mQuery.setColor(getDefaultValue(mToggleFilters.getColor().getFirstOptionsList(), mQuery.getColor()));
        mQuery.setBody(getDefaultValue(mToggleFilters.getBody().getFirstOptionsList(), mQuery.getBody()));
    }

    private String getName(MachineFilter.Options options) {
        return options != null ? options.getName() : "";
    }

    @Nullable
    private <T> T getDefaultValue(List<T> values, T item) {
        if (values != null && !values.isEmpty()) {
            if (values.contains(item)) {
                return item;
            } else {
                return values.get(0);
            }
        } else {
            return null;
        }
    }
}
