package nl.autovooru.android.mvp.view;

import android.app.Activity;
import android.support.annotation.IdRes;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by Antonenko Viacheslav on 16/11/15.
 */
public interface NavigationView extends MvpView {

    void showAccountRelatedView();

    void hideAccountRelatedView();

    void showUserInfoView();

    void hideUserInfoView();

    void updateUserInfoView(String name, String email);

    void showLoginView();

    void hideLoginView();

    void checkMenuItem(@IdRes int resId);

    void uncheckMenuItem(@IdRes int resId);

    void expandOrCollapseCategory();

    Activity getActivity();
}
