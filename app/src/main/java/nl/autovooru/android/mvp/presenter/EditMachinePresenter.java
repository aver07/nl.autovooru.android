package nl.autovooru.android.mvp.presenter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;

import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.IntentUtils;
import nl.autovooru.android.common.utils.StringUtils;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.interactor.EditAutoUseCase;
import nl.autovooru.android.domain.interactor.GetAdvertFieldsUseCase;
import nl.autovooru.android.domain.interactor.GetAutoInfoByTokenUseCase;
import nl.autovooru.android.domain.interactor.GetFilterTypesUseCase;
import nl.autovooru.android.domain.interactor.GetMyMachineDetailsUseCase;
import nl.autovooru.android.domain.interactor.GetYoutubeInfoUseCase;
import nl.autovooru.android.domain.model.advert.AdvertFields;
import nl.autovooru.android.domain.model.advert.AutoInfo;
import nl.autovooru.android.domain.model.advert.YoutubeInfo;
import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.model.filters.FilterType;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.domain.model.machine.MachineFilter;
import nl.autovooru.android.domain.model.query.AdvertFieldsQuery;
import nl.autovooru.android.mvp.view.EditMachineView;
import nl.autovooru.android.rx.RxUtils;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Func2;

/**
 * Created by Antonenko Viacheslav on 08/01/16.
 */
@PerActivity
public class EditMachinePresenter extends MvpNullObjectBasePresenter<EditMachineView> {

    private static final FilterType FILTER_TYPE = new FilterType("5", "vcat", "");

    private final GetFilterTypesUseCase mGetFilterTypesUseCase;
    private final GetAdvertFieldsUseCase mGetAdvertFieldsUseCase;
    private final GetAutoInfoByTokenUseCase mGetAutoInfoByTokenUseCase;
    private final GetYoutubeInfoUseCase mGetYoutubeInfoUseCase;
    private final EditAutoUseCase mEditAutoUseCase;
    private final GetMyMachineDetailsUseCase mGetMyMachineDetailsUseCase;
    private final Runnable mBeforeLoading = () -> getView().showProgress();
    private final Runnable mAfterLoading = () -> getView().hideProgress();

    private List<Filter> mMachineCategories;
    private AdvertFields mAdvertFields;

    private final Machine mMachine;
    private final AdvertFieldsQuery mQuery;
    private AdvertFieldsQuery mDefaultQuery;
    private int mCoverIndex = 0;

    private Subscription mInitialLoadingSubscriber;
    private Subscription mAdvertFieldsSubscriber;
    private Subscription mEditAutoSubscription;

    @Inject
    public EditMachinePresenter(@NonNull GetFilterTypesUseCase getFilterTypesUseCase,
                                @NonNull GetAdvertFieldsUseCase getAdvertFieldsUseCase,
                                @NonNull GetAutoInfoByTokenUseCase getAutoInfoByTokenUseCase,
                                @NonNull GetYoutubeInfoUseCase getYoutubeInfoUseCase,
                                @NonNull EditAutoUseCase editAutoUseCase,
                                @NonNull GetMyMachineDetailsUseCase getMyMachineDetailsUseCase,
                                @NonNull Machine machine) {
        mGetFilterTypesUseCase = getFilterTypesUseCase;
        mGetAdvertFieldsUseCase = getAdvertFieldsUseCase;
        mGetAutoInfoByTokenUseCase = getAutoInfoByTokenUseCase;
        mGetYoutubeInfoUseCase = getYoutubeInfoUseCase;
        mEditAutoUseCase = editAutoUseCase;
        mGetMyMachineDetailsUseCase = getMyMachineDetailsUseCase;
        mQuery = new AdvertFieldsQuery();
        mMachine = machine;
    }

    @Override
    public void attachView(EditMachineView view) {
        super.attachView(view);

        if (mEditAutoSubscription != null && !mEditAutoSubscription.isUnsubscribed()) {
            getView().showProgress();
        }
    }

    public void initialLoad(final boolean pullToRefresh) {
        if (mInitialLoadingSubscriber != null && !mInitialLoadingSubscriber.isUnsubscribed()) {
            getView().showLoading(false);
            return;
        }
        if (mMachineCategories == null || mAdvertFields == null || pullToRefresh) {
            getView().showLoading(pullToRefresh);

            RxUtils.unsubscribe(mInitialLoadingSubscriber);

            mInitialLoadingSubscriber = mGetMyMachineDetailsUseCase.execute(mMachine.getMachineId())
                    .flatMap(editMachineDetails -> {
                        mDefaultQuery = new AdvertFieldsQuery();
                        mDefaultQuery.apply(editMachineDetails);
                        mQuery.apply(editMachineDetails);
                        mCoverIndex = 0;

                        final String categoryId = mQuery.getMachineCategory().getId();
                        final String markId = mQuery.getMark().getValue();


                        return Observable.combineLatest(
                                mGetFilterTypesUseCase.get(FILTER_TYPE),
                                mGetAdvertFieldsUseCase.getFilters(categoryId, markId), (categoryFilters, advertFields) -> {
                                    final List<Filter> filters = categoryFilters.get(FILTER_TYPE.getKey());
                                    return new Pair<>(filters, advertFields);
                                });
                    })
                    .compose(RxUtils.applySchedulers())
                    .subscribe(new Subscriber<Pair<List<Filter>, AdvertFields>>() {
                        @Override
                        public void onCompleted() {
                            getView().showContent();
                        }

                        @Override
                        public void onError(Throwable e) {
                            getView().showError(e, pullToRefresh);
                        }

                        @Override
                        public void onNext(Pair<List<Filter>, AdvertFields> result) {
                            mMachineCategories = result.first;
                            mAdvertFields = result.second;

                            setupDefaultValues();
                            initUI();
                        }
                    });
        } else {
            initUI();
        }
    }

    public void openYoutubeLink(Context context) {
        if (!StringUtils.isEmpty(mQuery.getYoutubeLink())) {
            final Intent intent = IntentUtils.openLink(mQuery.getYoutubeLink());
            if (IntentUtils.isIntentAvailable(context, intent)) {
                context.startActivity(intent);
            }
        }
    }

    public void sendAdvert(List<String> images) {
        if (mQuery.getMark() == null || mQuery.getMark().getValue().equals("0")) {
            getView().showErrorDialog(R.string.fragment_place_ads_error_empty_mark);
            return;
        }
        if (mQuery.getModel() == null || mQuery.getModel().getValue().equals("0")) {
            getView().showErrorDialog(R.string.fragment_place_ads_error_empty_model);
            return;
        }
        if (StringUtils.isEmpty(mQuery.getTitle())) {
            getView().showErrorDialog(R.string.fragment_place_ads_error_empty_name);
            return;
        }

        mQuery.setImages(images);

        RxUtils.unsubscribe(mEditAutoSubscription);

        mEditAutoSubscription = mEditAutoUseCase.execute(mMachine.getMachineId(), mQuery.toMap(), mQuery.getImages())
                .compose(RxUtils.applySchedulers())
                .compose(RxUtils.applyOpBeforeAndAfter(mBeforeLoading, mAfterLoading))
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {
                        resetFields();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, true);
                    }

                    @Override
                    public void onNext(Void aVoid) {
                        getView().finishEdit();
                    }
                });
    }

    public void resetFields() {
        removeYoutubeInfo();
        mQuery.apply(mDefaultQuery);
        initUI();
    }

    @Nullable
    public List<String> getOldImagesUrls() {
        return mMachine.getImages();
    }

    public void getYoutubeInfo(@NonNull String youtubeLink) {
        mGetYoutubeInfoUseCase.execute(youtubeLink)
                .compose(RxUtils.applySchedulers())
                .compose(RxUtils.applyOpBeforeAndAfter(mBeforeLoading, mAfterLoading))
                .subscribe(new Subscriber<YoutubeInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, true);
                    }

                    @Override
                    public void onNext(YoutubeInfo youtubeInfo) {
                        mQuery.setYoutubeLink(youtubeInfo.getLinkUrl());
                        getView().showYoutubeInfo(youtubeInfo.getImageUrl(), youtubeInfo.getLinkUrl());
                    }
                });
    }

    public void removeYoutubeInfo() {
        mQuery.setYoutubeLink(null);
        getView().hideYoutubeInfo();
    }

    public void setImages(List<String> images, int coverIndex) {
        mQuery.setImages(images);
        mCoverIndex = coverIndex;
    }

    public void loadAutoInfoByToken(@NonNull CharSequence token) {
        mGetAutoInfoByTokenUseCase.getFilters(token.toString())
                .flatMap(autoInfo -> {
                    if (autoInfo.getCategoryId() != null) {
                        String markId = null;
                        String categoryId = autoInfo.getCategoryId().toString();
                        if (autoInfo.getMarkId() != null) {
                            markId = autoInfo.getMarkId().toString();
                        }
                        final Observable<AdvertFields> advertFieldsObservable =
                                mGetAdvertFieldsUseCase.getFilters(categoryId, markId);
                        return Observable.zip(advertFieldsObservable, Observable.just(autoInfo),
                                (Func2<AdvertFields, AutoInfo, Pair<AdvertFields, AutoInfo>>) Pair::new);
                    } else {
                        return Observable.just(new Pair<>(null, autoInfo));
                    }
                })
                .compose(RxUtils.applySchedulers())
                .compose(RxUtils.applyOpBeforeAndAfter(mBeforeLoading, mAfterLoading))
                .subscribe(new Subscriber<Pair<?, AutoInfo>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, true);
                    }

                    @Override
                    public void onNext(Pair<?, AutoInfo> autoInfoPair) {
                        AutoInfo autoInfo = autoInfoPair.second;
                        if (autoInfo.getCategoryId() != null && autoInfoPair.first != null) {
                            mAdvertFields = (AdvertFields) autoInfoPair.first;
                            final String categoryId = autoInfo.getCategoryId().toString();
                            for (Filter category : mMachineCategories) {
                                if (category.getId().equals(categoryId)) {
                                    mQuery.setMachineCategory(category);
                                    final MachineFilter.Options mark = getValueById(mAdvertFields.getMarks().getFirstOptionsList(), autoInfo.getMarkId());
                                    mQuery.setMark(mark);
                                    final MachineFilter.Options model = getValueById(mAdvertFields.getModels().getFirstOptionsList(), autoInfo.getModelId());
                                    mQuery.setModel(model);
                                    break;
                                }
                            }
                        }
                        final MachineFilter.Options body = getValueById(mAdvertFields.getBody().getFirstOptionsList(), autoInfo.getBodyId());
                        if (body != null) {
                            mQuery.setBody(body);
                        }
                        final MachineFilter.Options fuel = getValueById(mAdvertFields.getFuel().getFirstOptionsList(), autoInfo.getFuelId());
                        if (fuel != null) {
                            mQuery.setFuel(fuel);
                        }
                        final MachineFilter.Options color = getValueById(mAdvertFields.getColor().getFirstOptionsList(), autoInfo.getColorId());
                        if (color != null) {
                            mQuery.setColor(color);
                        }
                        if (!StringUtils.isEmpty(autoInfo.getEngineCapacity())) {
                            mQuery.setCapacity(autoInfo.getEngineCapacity());
                        }
                        setupDefaultValues();
                        initUI();
                    }
                });
    }

    public void restoreQuery(AdvertFieldsQuery query) {
        mQuery.apply(query);
    }

    public AdvertFieldsQuery getQuery() {
        return mQuery;
    }

    public void setTitle(String title) {
        mQuery.setTitle(title);
    }

    public void setDescription(String description) {
        mQuery.setDescription(description);
    }

    public void setToken(String token) {
        mQuery.setToken(token);
    }

    public void setPrice(String price) {
        mQuery.setPrice(price);
    }

    public void setPromoPrice(String promoPrice) {
        mQuery.setPromoPrice(promoPrice);
    }

    public void setDoors(String doors) {
        mQuery.setDoors(doors);
    }

    public void setMileage(String mileage) {
        mQuery.setMileage(mileage);
    }

    public void setFirstOwner(boolean isFirstOwner) {
        mQuery.setFirstOwner(isFirstOwner);
    }

    public void setNap(boolean nap) {
        mQuery.setNap(nap);
    }

    public void setEngineCapacity(String engineCapacity) {
        mQuery.setCapacity(engineCapacity);
    }

    public List<Filter> getMachineCategories() {
        return mMachineCategories;
    }

    public void setMachineCategories(@NonNull Filter machineCategory) {
        getView().setMachineCategory(machineCategory);
        mQuery.setMachineCategory(machineCategory);
        mQuery.setMark(getDefaultValue(mAdvertFields.getMarks().getFirstOptionsList(), null));
        mQuery.setModel(getDefaultValue(mAdvertFields.getModels().getFirstOptionsList(), null));

        RxUtils.unsubscribe(mAdvertFieldsSubscriber);

        mAdvertFieldsSubscriber = mGetAdvertFieldsUseCase.getFilters(mQuery.getMachineCategory().getId(), null)
                .compose(RxUtils.applySchedulers())
                .compose(RxUtils.applyOpBeforeAndAfter(mBeforeLoading, mAfterLoading))
                .subscribe(new Subscriber<AdvertFields>() {
                    @Override
                    public void onCompleted() {
                        getView().showContent();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, false);
                    }

                    @Override
                    public void onNext(AdvertFields advertFields) {
                        mAdvertFields = advertFields;

                        initUI();
                    }
                });
    }

    @NonNull
    public Filter getMachineCategory() {
        return mQuery.getMachineCategory();
    }

    @Nullable
    public List<MachineFilter.Options> getTopMarks() {
        final MachineFilter topMachineMarks = mAdvertFields.getTopMachineMarks();
        if (topMachineMarks != null) {
            return topMachineMarks.getFirstOptionsList();
        } else {
            return null;
        }
    }

    public List<MachineFilter.Options> getMachineMarks() {
        return mAdvertFields.getMarks().getFirstOptionsList();
    }

    public MachineFilter.Options getMark() {
        return mQuery.getMark();
    }

    public void setMark(@NonNull MachineFilter.Options mark) {
        getView().setMark(getMarkLabel(), mark.getName());
        mQuery.setMark(mark);
        mQuery.setModel(getDefaultValue(mAdvertFields.getModels().getFirstOptionsList(), null));

        RxUtils.unsubscribe(mAdvertFieldsSubscriber);
        mAdvertFieldsSubscriber = mGetAdvertFieldsUseCase.getFilters(mQuery.getMachineCategory().getId(), mark.getValue())
                .compose(RxUtils.applySchedulers())
                .compose(RxUtils.applyOpBeforeAndAfter(mBeforeLoading, mAfterLoading))
                .subscribe(new Subscriber<AdvertFields>() {
                    @Override
                    public void onCompleted() {
                        getView().showContent();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, false);
                    }

                    @Override
                    public void onNext(AdvertFields advertFields) {
                        mAdvertFields = advertFields;

                        initUI();
                    }
                });
    }

    public List<MachineFilter.Options> getModels() {
        return mAdvertFields.getModels().getFirstOptionsList();
    }

    public MachineFilter.Options getModel() {
        return mQuery.getModel();
    }

    public void setModel(@NonNull MachineFilter.Options model) {
        getView().setModel(getModelLabel(), model.getName());
        mQuery.setModel(model);
    }

    public List<MachineFilter.Options> getYears() {
        return mAdvertFields.getYear().getFirstOptionsList();
    }

    public MachineFilter.Options getYear() {
        return mQuery.getYear();
    }

    public void setYear(@NonNull MachineFilter.Options year) {
        getView().setYear(getYearLabel(), year.getName());
        mQuery.setYear(year);
    }

    public List<MachineFilter.Options> getBodyList() {
        return mAdvertFields.getBody().getFirstOptionsList();
    }

    public MachineFilter.Options getBody() {
        return mQuery.getBody();
    }

    public void setBody(@NonNull MachineFilter.Options body) {
        getView().setBody(getBodyLabel(), body.getName());
        mQuery.setBody(body);
    }

    public List<MachineFilter.Options> getFuels() {
        return mAdvertFields.getFuel().getFirstOptionsList();
    }

    public MachineFilter.Options getFuel() {
        return mQuery.getFuel();
    }

    public void setFuel(@NonNull MachineFilter.Options fuel) {
        getView().setFuel(getFuelLabel(), fuel.getName());
        mQuery.setFuel(fuel);
    }

    public List<MachineFilter.Options> getColors() {
        return mAdvertFields.getColor().getFirstOptionsList();
    }

    public MachineFilter.Options getColor() {
        return mQuery.getColor();
    }

    public void setColor(@NonNull MachineFilter.Options color) {
        getView().setColors(getColorLabel(), color.getName());
        mQuery.setColor(color);
    }

    public List<MachineFilter.Options> getTransmissionList() {
        return mAdvertFields.getTransmission().getFirstOptionsList();
    }

    public MachineFilter.Options getTransmission() {
        return mQuery.getTransmission();
    }

    public void setTransmission(@NonNull MachineFilter.Options transmission) {
        getView().setTransmissions(getTransmissionLabel(), transmission.getName());
        mQuery.setTransmission(transmission);
    }

    @NonNull
    public List<MachineFilter.Options> getAllOtherFilters(String key) {
        final MachineFilter filter = getFilterByKey(mAdvertFields.getOtherFilters(), key);
        if (filter != null) {
            return filter.getFirstOptionsList();
        }

        return new ArrayList<>();
    }

    public void setOtherFilter(String key, @Nullable Integer[] selectedIds) {
        final MachineFilter filter = getFilterByKey(mAdvertFields.getOtherFilters(), key);
        if (filter != null) {
            final ArrayList<MachineFilter.Options> options = new ArrayList<>();
            if (selectedIds != null && selectedIds.length > 0) {
                for (Integer selectedId : selectedIds) {
                    options.add(filter.getFirstOptionsList().get(selectedId));
                }
            }

            mQuery.setOtherFilters(key, options);
            getView().setOtherFilter(key, filter.getName(), options);
        }
    }

    @Nullable
    public Integer[] getOtherFiltersSelectedPositions(String key) {
        final List<MachineFilter.Options> options = mQuery.getOtherFilters().get(key);
        if (options != null) {
            final MachineFilter filter = getFilterByKey(mAdvertFields.getOtherFilters(), key);
            if (filter != null) {
                ArrayList<Integer> selectedPositions = new ArrayList<>(options.size());
                final int size = filter.getFirstOptionsList().size();
                for (int i = 0; i < size; i++) {
                    final MachineFilter.Options option = filter.getFirstOptionsList().get(i);
                    if (options.contains(option)) {
                        selectedPositions.add(i);
                    }
                }
                return selectedPositions.toArray(new Integer[selectedPositions.size()]);
            }
        }

        return null;
    }

    private void setupDefaultValues() {
        if (mMachineCategories.contains(mQuery.getMachineCategory())) {
            final int indexOf = mMachineCategories.indexOf(mQuery.getMachineCategory());
            mQuery.setMachineCategory(mMachineCategories.get(indexOf));
        }
        mQuery.setMark(getDefaultValue(mAdvertFields.getMarks().getFirstOptionsList(), mQuery.getMark()));
        mQuery.setModel(getDefaultValue(mAdvertFields.getModels().getFirstOptionsList(), mQuery.getModel()));
        mQuery.setYear(getDefaultValue(mAdvertFields.getYear().getFirstOptionsList(), mQuery.getYear()));
        mQuery.setBody(getDefaultValue(mAdvertFields.getBody().getFirstOptionsList(), mQuery.getBody()));
        mQuery.setFuel(getDefaultValue(mAdvertFields.getFuel().getFirstOptionsList(), mQuery.getFuel()));
        mQuery.setColor(getDefaultValue(mAdvertFields.getColor().getFirstOptionsList(), mQuery.getColor()));
        mQuery.setTransmission(getDefaultValue(mAdvertFields.getTransmission().getFirstOptionsList(), mQuery.getTransmission()));

        //// TODO: 06/01/16 Maybe we need set default valeus for titles, text, token etc.
    }

    // Labels
    public String getTitleLabel() {
        return mAdvertFields.getName().getName();
    }

    public String getTextLabel() {
        return mAdvertFields.getDescription().getName();
    }

    public String getTokenLabel() {
        return mAdvertFields.getToken().getName();
    }

    public String getPriceLabel() {
        return mAdvertFields.getPrice().getName();
    }

    public String getPromoPriceLabel() {
        return mAdvertFields.getPromoPrice().getName();
    }

    public String getDoorsLabel() {
        return mAdvertFields.getDoors().getName();
    }

    public String getMarkLabel() {
        return mAdvertFields.getMarks().getName();
    }

    public String getModelLabel() {
        return mAdvertFields.getModels().getName();
    }

    public String getYearLabel() {
        return mAdvertFields.getYear().getName();
    }

    public String getBodyLabel() {
        return mAdvertFields.getBody().getName();
    }

    public String getFuelLabel() {
        return mAdvertFields.getFuel().getName();
    }

    public String getColorLabel() {
        return mAdvertFields.getColor().getName();
    }

    public String getTransmissionLabel() {
        return mAdvertFields.getTransmission().getName();
    }

    public String getMileageLabel() {
        return mAdvertFields.getMileage().getName();
    }

    public String getEngineCapacityLabel() {
        return mAdvertFields.getEngineCapacity().getName();
    }

    public String getFirstOwnerLabel() {
        return mAdvertFields.getFirstOwner().getName();
    }

    public String getNapLabel() {
        return mAdvertFields.getNap().getName();
    }

    public String getOtherFiltersLabel(String key) {
        for (MachineFilter filter : mAdvertFields.getOtherFilters()) {
            if (filter.getValue().equals(key)) {
                return filter.getName();
            }
        }

        return "";
    }

    private void initUI() {
        getView().setTitle(getTitleLabel(), mQuery.getTitle());
        getView().setText(getTextLabel(), mQuery.getDescription());
        getView().setToken(getTokenLabel(), mQuery.getToken());
        getView().setPrice(getPriceLabel(), mQuery.getPrice());
        getView().setPromoPrice(getPromoPriceLabel(), mQuery.getPromoPrice());
        getView().setDoors(getDoorsLabel(), mQuery.getDoors());
        getView().setMileage(getMileageLabel(), mQuery.getMileage());
        getView().setEngineCapacity(getEngineCapacityLabel(), mQuery.getCapacity());
        getView().setMachineCategory(mQuery.getMachineCategory());
        getView().setMark(getMarkLabel(), getOptionsName(mQuery.getMark()));
        getView().setModel(getModelLabel(), getOptionsName(mQuery.getModel()));
        getView().setYear(getYearLabel(), getOptionsName(mQuery.getYear()));
        getView().setBody(getBodyLabel(), getOptionsName(mQuery.getBody()));
        getView().setFuel(getFuelLabel(), getOptionsName(mQuery.getFuel()));
        getView().setColors(getColorLabel(), getOptionsName(mQuery.getColor()));
        getView().setTransmissions(getTransmissionLabel(), getOptionsName(mQuery.getTransmission()));
        getView().setFirstOwner(getFirstOwnerLabel(), mQuery.getFirstOwner());
        getView().setNap(getNapLabel(), mQuery.getNap());
        getView().setImages(mQuery.getImages(), mCoverIndex);
        for (MachineFilter filter : mAdvertFields.getOtherFilters()) {
            final List<MachineFilter.Options> options = mQuery.getOtherFilters().get(filter.getValue());
            if (options != null) {
                getView().setOtherFilter(filter.getValue(), filter.getName(), options);
            } else {
                getView().setOtherFilter(filter.getValue(), filter.getName(), new ArrayList<>());
            }
        }
        if (!StringUtils.isEmpty(mQuery.getYoutubeLink()) && !StringUtils.isEmpty(mQuery.getYoutubeImage())) {
            getView().showYoutubeInfo(mQuery.getYoutubeImage(), mQuery.getYoutubeLink());
        }

        getView().setMachineName(mDefaultQuery.getTitle());
    }

    @Nullable
    private MachineFilter getFilterByKey(List<MachineFilter> optionsList, String key) {
        for (MachineFilter filter : optionsList) {
            if (filter.getValue().equals(key)) {
                return filter;
            }
        }
        return null;
    }

    private String getOptionsName(MachineFilter.Options options) {
        return options != null ? options.getName() : "";
    }

    @Nullable
    private <T> T getDefaultValue(List<T> values, T item) {
        if (values != null && !values.isEmpty()) {
            if (values.contains(item)) {
                return item;
            } else {
                return values.get(0);
            }
        } else {
            return null;
        }
    }

    @Nullable
    private <T extends MachineFilter.Options> T getValueById(List<T> values, Integer itemId) {
        if (values != null && !values.isEmpty() && itemId != null) {
            String id = itemId.toString();
            for (T item : values) {
                if (item.getValue().equals(id)) {
                    return item;
                }
            }
        }

        return null;
    }
}