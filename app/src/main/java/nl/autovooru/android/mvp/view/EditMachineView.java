package nl.autovooru.android.mvp.view;

import android.support.annotation.StringRes;
import android.widget.EditText;
import android.widget.TextView;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import java.util.List;

import nl.autovooru.android.domain.model.advert.AdvertFields;
import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.model.machine.MachineFilter;

/**
 * Created by Antonenko Viacheslav on 08/01/16.
 */
public interface EditMachineView extends MvpLceView<AdvertFields> {

    void setField(EditText editText, String label, String value);

    void setField(EditText editText, @StringRes int label, String value);

    void setField(TextView textView, String label, String value);

    void setField(TextView textView, @StringRes int label, String value);

    void setMachineCategory(Filter category);

    void setTitle(String label, String title);

    void setText(String label, String text);

    void setMark(String label, String mark);

    void setModel(String label, String model);

    void setYear(String label, String year);

    void setToken(String label, String token);

    void setPrice(String label, String price);

    void setPromoPrice(String label, String promoPrice);

    void setDoors(String label, String doors);

    void setFuel(String label, String fuel);

    void setMileage(String label, String mileage);

    void setEngineCapacity(String label, String engineCapacity);

    void showProgress();

    void hideProgress();

    void setTransmissions(String label, String transmissionList);

    void setColors(String label, String colors);

    void setBody(String label, String body);

    void setFirstOwner(String label, boolean isFirstOwner);

    void setNap(String label, boolean nap);

    void setOtherFilter(String filterKey, String label, List<MachineFilter.Options> values);

    void removeOtherFilter(String key);

    void showYoutubeInfo(String imageUrl, String youtubeLink);

    void hideYoutubeInfo();

    void showErrorDialog(@StringRes int resId);

    void finishEdit();

    void setMachineName(String name);

    void setImages(List<String> images, int coverIndex);

}
