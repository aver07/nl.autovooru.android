package nl.autovooru.android.mvp.presenter;

import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;

import javax.inject.Inject;

import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.interactor.DefaultSubscriber;
import nl.autovooru.android.domain.interactor.DeleteAllUsersBidsUseCase;
import nl.autovooru.android.domain.interactor.DeleteUserBidUseCase;
import nl.autovooru.android.domain.interactor.GetMyBidsUseCase;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import nl.autovooru.android.mvp.view.MyBidsView;
import nl.autovooru.android.rx.RxUtils;
import rx.Subscriber;
import rx.Subscription;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
@PerActivity
public class MyBidsPresenter extends MvpNullObjectBasePresenter<MyBidsView> {

    private final DeleteUserBidUseCase mDeleteUserBidUseCase;
    private final GetMyBidsUseCase mGetMyBidsUseCase;
    private final DeleteAllUsersBidsUseCase mDeleteAllUsersBidsUseCase;
    private Subscription mGetMyBidsSubscription;


    @Inject
    public MyBidsPresenter(@NonNull GetMyBidsUseCase getMyBidsUseCase,
                           @NonNull DeleteUserBidUseCase deleteUserBidUseCase,
                           @NonNull DeleteAllUsersBidsUseCase deleteAllUsersBidsUseCase) {
        mGetMyBidsUseCase = getMyBidsUseCase;
        mDeleteUserBidUseCase = deleteUserBidUseCase;
        mDeleteAllUsersBidsUseCase = deleteAllUsersBidsUseCase;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        RxUtils.unsubscribe(mGetMyBidsSubscription);
    }

    public void load(final boolean pullToRefresh) {
        getView().showLoading(pullToRefresh);
        mGetMyBidsSubscription = mGetMyBidsUseCase.load()
                .compose(RxUtils.applySchedulers())
                .subscribe(new Subscriber<MachineListViewModel>() {
                    @Override
                    public void onCompleted() {
                        getView().showContent();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, pullToRefresh);
                    }

                    @Override
                    public void onNext(MachineListViewModel data) {
                        getView().setData(data);
                    }
                });
    }

    public void loadMore() {
        mGetMyBidsSubscription = mGetMyBidsUseCase.loadMore()
                .compose(RxUtils.applySchedulers())
                .subscribe(new Subscriber<MachineListViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().addMore(null);
                        getView().showError(e, true);
                    }

                    @Override
                    public void onNext(MachineListViewModel data) {
                        getView().addMore(data);
                    }
                });
    }

    public void removeBid(final Machine machine) {
        mDeleteUserBidUseCase.delete(machine.getBidId())
                .compose(RxUtils.applySchedulers())
                .subscribe(new DefaultSubscriber<Void>() {
                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, true);
                    }

                    @Override
                    public void onNext(Void aVoid) {
                        load(false);
                    }
                });
    }

    public void removeAllMachineBids(@NonNull Machine machine) {
        mDeleteAllUsersBidsUseCase.execute(machine.getMachineId())
                .compose(RxUtils.applySchedulers())
                .subscribe(new DefaultSubscriber<Void>() {

                    @Override
                    public void onCompleted() {
                        getView().removeMachine(machine);
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, true);
                    }

                });
    }

    public void removeAllBids() {
        mDeleteAllUsersBidsUseCase.execute(null)
                .compose(RxUtils.applySchedulers())
                .subscribe(new DefaultSubscriber<Void>() {

                    @Override
                    public void onCompleted() {
                        load(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, true);
                    }

                });
    }
}
