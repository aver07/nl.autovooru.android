package nl.autovooru.android.mvp.presenter;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import nl.autovooru.android.rx.RxUtils;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by Antonenko Viacheslav on 02/12/15.
 */
public class BaseRxLcePresenter<V extends MvpLceView<M>, M> extends MvpBasePresenter<V>
        implements MvpPresenter<V> {

    protected Subscriber<M> mSubscriber;

    /**
     * Unsubscribes the subscriber and set it to null
     */
    protected void unsubscribe() {
        if (mSubscriber != null && !mSubscriber.isUnsubscribed()) {
            mSubscriber.unsubscribe();

        }
        mSubscriber = null;
    }

    public void subscribe(Observable<M> observable, final boolean pullToRefresh) {
        if (isViewAttached()) {
            getView().showLoading(pullToRefresh);
        }

        unsubscribe();
        mSubscriber = new Subscriber<M>() {
            private boolean ptr = pullToRefresh;

            @Override
            public void onCompleted() {
                BaseRxLcePresenter.this.onCompleted();
            }

            @Override
            public void onError(Throwable e) {
                BaseRxLcePresenter.this.onError(e, ptr);
            }

            @Override
            public void onNext(M m) {
                BaseRxLcePresenter.this.onNext(m);
            }
        };
        observable.compose(RxUtils.applySchedulers());
        observable.subscribe(mSubscriber);
    }

    protected void onCompleted() {
        if (isViewAttached()) {
            getView().showContent();
        }
        unsubscribe();
    }

    protected void onError(Throwable e, boolean pullToRefresh) {
        if (isViewAttached()) {
            getView().showError(e, pullToRefresh);
        }

        unsubscribe();
    }

    protected void onNext(M data) {
        if (isViewAttached()) {
            getView().setData(data);
        }
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            unsubscribe();
        }
    }
}
