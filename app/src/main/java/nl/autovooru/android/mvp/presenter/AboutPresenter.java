package nl.autovooru.android.mvp.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.IntentUtils;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.interactor.DefaultSubscriber;
import nl.autovooru.android.domain.interactor.SendFeedbackUseCase;
import nl.autovooru.android.domain.model.machine.details.Location;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.mvp.view.AboutView;
import nl.autovooru.android.rx.RxUtils;
import nl.autovooru.android.ui.activity.AdvertisingInfoActivity;
import nl.autovooru.android.ui.activity.MapsActivity;
import nl.autovooru.android.utils.Constants;

/**
 * Created by Antonenko Viacheslav on 09/12/15.
 */
@PerActivity
public class AboutPresenter extends MvpBasePresenter<AboutView> {

    private final SendFeedbackUseCase mSendFeedbackUseCase;
    private final AccountRepository mAccountRepository;
    private final String mCompanyName;
    private final String mWebsiteUrl;
    private final String mSupportEmail;
    private final String mPhoneNumber;
    private final String mAdvertisingInfoText;
    private final String mAdvertisingTitle;
    private final String mSubscriptionsInfoText;
    private final String mSubscriptionsTitle;

    @Inject
    public AboutPresenter(@NonNull SendFeedbackUseCase sendFeedbackUseCase,
                          @NonNull AccountRepository accountRepository,
                          Context context) {
        mSendFeedbackUseCase = sendFeedbackUseCase;
        mAccountRepository = accountRepository;
        mCompanyName = context.getString(R.string.fragment_about_app_name);
        mWebsiteUrl = context.getString(R.string.config_company_site);
        mSupportEmail = context.getString(R.string.config_company_support_email);
        mPhoneNumber = context.getString(R.string.config_company_phone_number);
        mAdvertisingInfoText = context.getString(R.string.fragment_about_advertising_info_text);
        mAdvertisingTitle = context.getString(R.string.fragment_about_advertising);
        mSubscriptionsInfoText = context.getString(R.string.fragment_about_subscriptions_info_text);
        mSubscriptionsTitle = context.getString(R.string.fragment_about_subscription);
    }

    public void openMap() {
        if (isViewAttached()) {
            final ArrayList<String> fields = new ArrayList<>(1);
            fields.add(mCompanyName);
            final ArrayList<Location> locations = new ArrayList<>(1);
            locations.add(new Location(Constants.AUTOVOORU_LATITUDE, Constants.AUTOVOORU_LONGITUDE));
            MapsActivity.openActivity(
                    getView().getActivity(),
                    getView().getActivity().getString(R.string.fragment_about_maps_title),
                    fields,
                    locations,
                    mWebsiteUrl,
                    mSupportEmail,
                    mPhoneNumber,
                    true
            );
        }
    }

    public void openAdvertisingInfos() {
        if (isViewAttached()) {
            final Activity activity = getView().getActivity();
            AdvertisingInfoActivity.openActivity(activity, mAdvertisingInfoText, mAdvertisingTitle);
        }
    }

    public void openSubscriptionsInfos() {
        if (isViewAttached()) {
            final Activity activity = getView().getActivity();
            AdvertisingInfoActivity.openActivity(activity,
                    mSubscriptionsInfoText,
                    mSubscriptionsTitle);
        }
    }

    public void sendFeedback(String subject, String name, String email, String message) {
        mSendFeedbackUseCase.sendFeedback(subject, name, email, message)
                .compose(RxUtils.applySchedulers())
                .subscribe(new DefaultSubscriber<>());
    }

    public void openWebsite() {
        if (isViewAttached()) {
            final Activity activity = getView().getActivity();
            final Intent intent = IntentUtils.openLink(mWebsiteUrl);
            if (IntentUtils.isIntentAvailable(activity, intent)) {
                activity.startActivity(intent);
            }
        }
    }

    public void sendEmailToSupport() {
        if (isViewAttached()) {
            final Activity activity = getView().getActivity();
            final Intent intent = IntentUtils.sendEmail(mSupportEmail, "", "");
            if (IntentUtils.isIntentAvailable(activity, intent)) {
                activity.startActivity(intent);
            }
        }
    }

    public void callToOffice() {
        if (isViewAttached()) {
            final Activity activity = getView().getActivity();
            final Intent intent = IntentUtils.dialPhone(mPhoneNumber);
            if (IntentUtils.isIntentAvailable(activity, intent)) {
                activity.startActivity(intent);
            }
        }
    }

    public boolean isLoggedIn() {
        return mAccountRepository.isLoggedIn();
    }

    public String getUsername() {
        return mAccountRepository.getUsername();
    }

    public String getUserEmail() {
        return mAccountRepository.getEmail();
    }

}
