package nl.autovooru.android.mvp.view;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import java.util.List;

import nl.autovooru.android.domain.model.user.Bid;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public interface MachinesBidsView extends MvpLceView<List<Bid>> {


    void showProgress();

    void hideProgress();
}
