package nl.autovooru.android.mvp.view;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import java.util.List;

import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;

/**
 * Created by Antonenko Viacheslav on 24/11/15.
 */
public interface SearchMachineListView extends MvpLceView<MachineListViewModel> {

    void showMachineCategories(List<Filter> machineCategories, int selectedPosition);

    void addMoreMachines(MachineListViewModel viewModel);
}
