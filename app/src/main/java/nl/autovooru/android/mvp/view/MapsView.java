package nl.autovooru.android.mvp.view;

import android.app.Activity;
import android.support.annotation.Nullable;

/**
 * Created by Antonenko Viacheslav on 09/12/15.
 */
public interface MapsView extends View {

    @Nullable
    Activity getActivity();
}
