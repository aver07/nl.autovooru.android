package nl.autovooru.android.mvp.view;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public interface RegistrationView extends MvpView {

    void showProgress();

    void hideProgress();

    void showError(String message);

    boolean showError(Throwable throwable);

    void showOrganizationSpecificViews();

    void hideOrganizationSpecificViews();

    void showUserSpecificViews();

    void hideUserSpecificViews();

    String getName();

    String getSurname();

    String getPostcode();

    String getCity();

    String getAddress();

    String getEmail();

    String getPhone();

    String getPassword();

    String getConfirmPassword();

    String getCompanyName();

    String getAutosCounter();

    void finish();

    void showOrganizationSuccessDialog();
}
