package nl.autovooru.android.mvp.view;

import android.support.annotation.StringRes;
import android.widget.TextView;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import java.util.List;

import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.model.machine.MachineFilter;
import nl.autovooru.android.domain.model.machine.MachineSearchFilters;

/**
 * Created by Antonenko Viacheslav on 10/12/15.
 */
public interface MachineSearchFiltersView extends MvpLceView<MachineSearchFilters> {

    void setField(TextView textView, String label, String value);

    void setField(TextView textView, @StringRes int label, String value);

    void setMachineCategory(Filter category);

    void setMark(String label, String mark);

    void setModel(String label, String model);

    void setStartYear(String label, String year);

    void setEndYear(String label, String year);

    void setStartPrice(String label, String price);

    void setEndPrice(String label, String price);

    void setFuel(String label, String fuel);

    void setDiller(String label, String diller);

    void setPostCode(String label, String postCode);

    void setDistance(String label, String distance);

    void setKeyword(String label, String keyword);

    void setMileage(String label, String mileage);

    void showProgress();

    void hideProgress();

    void showOtherFilters();

    void hideOtherFilters();

    void toggleOtherFilters();

    void setOfferTime(String label, String offerTime);

    void setTransmissions(String label, String transmissionList);

    void setColors(String label, String colors);

    void setBody(String label, String body);

    void setOtherFilter(String filterKey, String label, List<MachineFilter.Options> values);

    void removeOtherFilter(String filterKey);
}
