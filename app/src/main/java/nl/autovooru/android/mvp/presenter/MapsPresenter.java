package nl.autovooru.android.mvp.presenter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import nl.autovooru.android.common.utils.IntentUtils;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.interactor.DefaultSubscriber;
import nl.autovooru.android.domain.interactor.SendFeedbackUseCase;
import nl.autovooru.android.domain.interactor.SendMessageUseCase;
import nl.autovooru.android.mvp.view.MapsView;
import nl.autovooru.android.rx.RxUtils;

/**
 * Created by Antonenko Viacheslav on 09/12/15.
 */
@PerActivity
public class MapsPresenter implements Presenter {

    private final SendFeedbackUseCase mSendFeedbackUseCase;
    private final SendMessageUseCase mSendMessageUseCase;
    @Nullable
    private MapsView mMapsView;
    @Nullable
    private String mWebsiteUrl;
    @Nullable
    private String mEmail;
    @Nullable
    private String mPhoneNumber;

    @Inject
    public MapsPresenter(SendFeedbackUseCase sendFeedbackUseCase, SendMessageUseCase sendMessageUseCase) {
        mSendFeedbackUseCase = sendFeedbackUseCase;
        mSendMessageUseCase = sendMessageUseCase;
    }


    public void attach(@NonNull MapsView mapsView,
                       @Nullable String websiteUrl,
                       @Nullable String email,
                       @Nullable String phoneNumber) {
        mMapsView = mapsView;
        mWebsiteUrl = websiteUrl;
        mEmail = email;
        mPhoneNumber = phoneNumber;
    }

    public void detach() {
        mMapsView = null;
    }

    private boolean isActivityAvailable() {
        return mMapsView != null && mMapsView.getActivity() != null;
    }

    @Nullable
    private Activity getActivity() {
        return mMapsView != null ? mMapsView.getActivity() : null;
    }

    public void sendFeedback(String subject, String name, String email, String message) {
        mSendFeedbackUseCase.sendFeedback(subject, name, email, message)
                .compose(RxUtils.applySchedulers())
                .subscribe(new DefaultSubscriber<>());
    }

    public void sendMessage(@Nullable String machineId, @Nullable String name, String email, String subject, String message) {
        mSendMessageUseCase.sendMessage(machineId, name, email, subject, message)
                .compose(RxUtils.applySchedulers())
                .subscribe(new DefaultSubscriber<>());
    }

    public void openWebsite() {
        if (isActivityAvailable()) {
            final Activity activity = getActivity();
            final Intent intent = IntentUtils.openLink(mWebsiteUrl);
            if (IntentUtils.isIntentAvailable(activity, intent)) {
                activity.startActivity(intent);
            }
        }
    }

    public void sendEmail() {
        if (isActivityAvailable()) {
            final Activity activity = getActivity();
            final Intent intent = IntentUtils.sendEmail(mEmail, "", "");
            if (IntentUtils.isIntentAvailable(activity, intent)) {
                activity.startActivity(intent);
            }
        }
    }

    public void callToOffice() {
        if (isActivityAvailable()) {
            final Activity activity = getActivity();
            final Intent intent = IntentUtils.dialPhone(mPhoneNumber);
            if (IntentUtils.isIntentAvailable(activity, intent)) {
                activity.startActivity(intent);
            }
        }
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {

    }
}
