package nl.autovooru.android.mvp.presenter;

import android.text.TextUtils;

import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;

import javax.inject.Inject;

import nl.autovooru.android.common.utils.StringUtils;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.interactor.OrganizationRegistrationUseCase;
import nl.autovooru.android.domain.interactor.UserRegistrationUseCase;
import nl.autovooru.android.domain.model.auth.AuthorizationToken;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.mvp.view.RegistrationView;
import nl.autovooru.android.rx.RxUtils;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
@PerActivity
public class RegistrationPresenter extends MvpNullObjectBasePresenter<RegistrationView> {

    private final UserRegistrationUseCase mUserRegistrationUseCase;
    private final OrganizationRegistrationUseCase mOrganizationRegistrationUseCase;
    private final AccountRepository mAccountRepository;
    private final Runnable mBeforeLoading = () -> getView().showProgress();
    private final Runnable mAfterLoading = () -> getView().hideProgress();
    private Observable<AuthorizationToken> mUserRegistrationObservable;
    private Subscription mSubscribe;

    private boolean isUserFormShown = true;
    private String mName;
    private String mEmail;
    private String mPassword;
    private Observable<Void> mOrganizationObservable;

    @Inject
    public RegistrationPresenter(UserRegistrationUseCase userRegistrationUseCase,
                                 OrganizationRegistrationUseCase organizationRegistrationUseCase,
                                 AccountRepository accountRepository) {
        mUserRegistrationUseCase = userRegistrationUseCase;
        mOrganizationRegistrationUseCase = organizationRegistrationUseCase;
        mAccountRepository = accountRepository;
    }

    @Override
    public void attachView(RegistrationView view) {
        super.attachView(view);
        if (mUserRegistrationObservable != null) {
            getView().showProgress();
            createNewUser();
        } else if (mOrganizationObservable != null) {
            getView().showProgress();
            createNewOrganiztion();
        }

        updateFormView();
    }

    @Override
    public void detachView(boolean retainInstance) {
        unsubscribe();
        super.detachView(retainInstance);
    }

    public void register() {
        if (isFieldsValuesValid()) {
            if (isUserFormShown) {
                createNewUser();
            } else {
                createNewOrganiztion();
            }
        }
    }

    public void changeForm(boolean showUserForm) {
        if (showUserForm != isUserFormShown) {
            isUserFormShown = showUserForm;
            updateFormView();
        }
    }

    private void createNewUser() {
        //// TODO: 06/01/16 Add check email
        unsubscribe();
        if (mUserRegistrationObservable == null) {
            mName = getView().getName().trim();
            mEmail = getView().getEmail().trim();
            mPassword = getView().getPassword().trim();
            mUserRegistrationObservable = mUserRegistrationUseCase.register(
                    mName,
                    getView().getSurname().trim(),
                    getView().getPostcode().trim(),
                    getView().getCity().trim(),
                    getView().getAddress().trim(),
                    getView().getPhone().trim(),
                    mEmail,
                    mPassword
            )
                    .compose(RxUtils.applySchedulers())
                    .compose(RxUtils.applyOpBeforeAndAfter(mBeforeLoading, mAfterLoading))
                    .cache();
        }
        mSubscribe = mUserRegistrationObservable
                .subscribe(new Subscriber<AuthorizationToken>() {
                    @Override
                    public void onCompleted() {
                        unsubscribe();
                        mUserRegistrationObservable = null;
                    }

                    @Override
                    public void onError(Throwable e) {
                        unsubscribe();
                        mUserRegistrationObservable = null;
                        getView().hideProgress();
                        if (!getView().showError(e)) {
                            getView().showError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(AuthorizationToken authorizationToken) {
                        if (mAccountRepository.createAccount(mName, mEmail, mPassword, authorizationToken)) {
                            getView().finish();
                        }
                    }
                });
    }

    private void createNewOrganiztion() {
        unsubscribe();
        if (mOrganizationObservable == null) {
            mName = getView().getName().trim();
            mEmail = getView().getEmail().trim();
            mPassword = null;
            mOrganizationObservable = mOrganizationRegistrationUseCase.register(
                    mName,
                    getView().getSurname().trim(),
                    getView().getCompanyName().trim(),
                    getView().getAutosCounter().trim(),
                    getView().getPostcode().trim(),
                    getView().getCity().trim(),
                    getView().getAddress().trim(),
                    getView().getPhone().trim(),
                    mEmail
            )
                    .compose(RxUtils.applySchedulers())
                    .compose(RxUtils.applyOpBeforeAndAfter(mBeforeLoading, mAfterLoading))
                    .cache();
        }
        mSubscribe = mOrganizationObservable.subscribe(new Subscriber<Void>() {
            @Override
            public void onCompleted() {
                unsubscribe();
                mOrganizationObservable = null;
            }

            @Override
            public void onError(Throwable e) {
                unsubscribe();
                mOrganizationObservable = null;
                getView().hideProgress();
                if (!getView().showError(e)) {
                    getView().showError(e.getMessage());
                }
            }

            @Override
            public void onNext(Void aVoid) {
                getView().showOrganizationSuccessDialog();
            }
        });
    }

    private boolean isFieldsValuesValid() {
        if (StringUtils.isEmpty(getView().getPostcode())) {
            getView().showError("Post code can't be empty.");
            return false;
        }
        if (StringUtils.isEmpty(getView().getCity())) {
            getView().showError("City can't be empty.");
            return false;
        }

        if (StringUtils.isEmpty(getView().getAddress())) {
            getView().showError("Address can't be empty.");
            return false;
        }
        if (StringUtils.isEmpty(getView().getEmail())) {
            getView().showError("Email can't be empty.");
            return false;
        }
        if (!getView().getEmail().contains("@")) {
            getView().showError("Invalid email.");
            return false;
        }
        if (StringUtils.isEmpty(getView().getPhone())) {
            getView().showError("Phone can't be empty.");
            return false;
        }
        if (isUserFormShown) {
            if (StringUtils.isEmpty(getView().getPassword())) {
                getView().showError("Password can't be empty.");
                return false;
            }
            if (StringUtils.isEmpty(getView().getConfirmPassword())) {
                getView().showError("Confirm password can't be empty.");
                return false;
            }
            if (!TextUtils.equals(getView().getPassword(), getView().getConfirmPassword())) {
                getView().showError("Password and confirm password not equals.");
                return false;
            }
        } else {
            if (StringUtils.isEmpty(getView().getCompanyName())) {
                getView().showError("Company name can't be empty.");
                return false;
            }
            if (StringUtils.isEmpty(getView().getAutosCounter())) {
                getView().showError("Autos counter can't be empty.");
                return false;
            }
        }

        return true;
    }

    private void updateFormView() {
        if (isUserFormShown) {
            getView().hideOrganizationSpecificViews();
            getView().showUserSpecificViews();
        } else {
            getView().hideUserSpecificViews();
            getView().showOrganizationSpecificViews();
        }
    }

    private void unsubscribe() {
        RxUtils.unsubscribe(mSubscribe);
        mSubscribe = null;
    }
}
