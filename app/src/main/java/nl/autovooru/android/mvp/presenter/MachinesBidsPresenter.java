package nl.autovooru.android.mvp.presenter;

import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.interactor.DeleteBidMessageUseCase;
import nl.autovooru.android.domain.interactor.GetMachineBidsUseCase;
import nl.autovooru.android.domain.interactor.SendBidMessageUseCase;
import nl.autovooru.android.domain.model.user.Bid;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.mvp.view.MachinesBidsView;
import nl.autovooru.android.rx.RxUtils;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
@PerActivity
public class MachinesBidsPresenter extends MvpNullObjectBasePresenter<MachinesBidsView> {

    private final Runnable mBeforeLoading = () -> getView().showProgress();
    private final Runnable mAfterLoading = () -> getView().hideProgress();
    private final GetMachineBidsUseCase mGetMachineBidsUseCase;
    private final SendBidMessageUseCase mSendBidMessageUseCase;
    private final DeleteBidMessageUseCase mDeleteBidMessageUseCase;
    private final AccountRepository mAccountRepository;
    private final String mMachineId;
    private Observable<List<Bid>> mGetMachineBidsObservable;
    private Observable<Void> mDeleteBidMessageObservable;
    private Observable<Boolean> mSendBidMessageObservable;
    private Subscriber<List<Bid>> mGetMachineBidsSubscriber;

    @Inject
    public MachinesBidsPresenter(@NonNull GetMachineBidsUseCase getMachineBidsUseCase,
                                 @NonNull SendBidMessageUseCase sendBidMessageUseCase,
                                 @NonNull DeleteBidMessageUseCase deleteBidMessageUseCase,
                                 @NonNull AccountRepository accountRepository,
                                 @NonNull @Named("machineId") String machineId) {
        mGetMachineBidsUseCase = getMachineBidsUseCase;
        mSendBidMessageUseCase = sendBidMessageUseCase;
        mDeleteBidMessageUseCase = deleteBidMessageUseCase;
        mAccountRepository = accountRepository;
        mMachineId = machineId;
    }

    @Override
    public void attachView(MachinesBidsView view) {
        super.attachView(view);
        if (mDeleteBidMessageObservable != null) {
            getView().showProgress();
        } else if (mSendBidMessageObservable != null) {
            getView().showProgress();
        }
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
    }

    public String getUsername() {
        return mAccountRepository.getUsername();
    }

    public String getUserEmail() {
        return mAccountRepository.getEmail();
    }

    public void load(final boolean pullToRefresh) {
        if (mGetMachineBidsObservable == null || pullToRefresh) {
            if (mGetMachineBidsSubscriber != null && !mGetMachineBidsSubscriber.isUnsubscribed()) {
                mGetMachineBidsSubscriber.unsubscribe();
            }
            mGetMachineBidsObservable = mGetMachineBidsUseCase.load(mMachineId)
                    .compose(RxUtils.applySchedulers())
                    .cache();
        }

        getView().showLoading(pullToRefresh);
        mGetMachineBidsSubscriber = new Subscriber<List<Bid>>() {
            @Override
            public void onCompleted() {
                getView().showContent();
            }

            @Override
            public void onError(Throwable e) {
                getView().showError(e, pullToRefresh);
            }

            @Override
            public void onNext(List<Bid> bids) {
                getView().setData(bids);
            }
        };
        mGetMachineBidsObservable.subscribe(mGetMachineBidsSubscriber);
    }

    public void sendBidMessage(String messageId, String name, String email, String subject, String message) {
        if (mSendBidMessageObservable == null) {
            mSendBidMessageObservable = mSendBidMessageUseCase.sendMessage(messageId, name, email, subject, message)
                    .compose(RxUtils.applySchedulers())
                    .compose(RxUtils.applyOpBeforeAndAfter(mBeforeLoading, mAfterLoading))
                    .cache();
        }

        mSendBidMessageObservable
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {
                        mSendBidMessageObservable = null;
                    }

                    @Override
                    public void onError(Throwable e) {
                        mSendBidMessageObservable = null;
                        getView().showError(e, false);
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {

                    }
                });
    }

    public void deleteBidMessage(String bidId) {
        if (mDeleteBidMessageObservable == null) {
            mDeleteBidMessageObservable = mDeleteBidMessageUseCase.delete(bidId)
                    .compose(RxUtils.applySchedulers())
                    .compose(RxUtils.applyOpBeforeAndAfter(mBeforeLoading, mAfterLoading))
                    .cache();
        }

        mDeleteBidMessageObservable
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {
                        mDeleteBidMessageObservable = null;
                        load(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mDeleteBidMessageObservable = null;
                        getView().showError(e, false);
                    }

                    @Override
                    public void onNext(Void aVoid) {

                    }
                });
    }
}
