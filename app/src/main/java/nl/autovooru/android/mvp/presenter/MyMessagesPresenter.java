package nl.autovooru.android.mvp.presenter;

import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;

import javax.inject.Inject;

import nl.autovooru.android.domain.interactor.DefaultSubscriber;
import nl.autovooru.android.domain.interactor.DeleteUserMessageUseCase;
import nl.autovooru.android.domain.interactor.GetUserMessagesUseCase;
import nl.autovooru.android.domain.interactor.SendAnswerUseCase;
import nl.autovooru.android.domain.model.user.Message;
import nl.autovooru.android.domain.model.user.MessagesPage;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.mvp.view.MyMessagesView;
import nl.autovooru.android.rx.RxUtils;
import rx.Subscriber;
import rx.Subscription;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public class MyMessagesPresenter extends MvpNullObjectBasePresenter<MyMessagesView> {

    private final GetUserMessagesUseCase mGetUserMessagesUseCase;
    private final SendAnswerUseCase mSendAnswerUseCase;
    private final DeleteUserMessageUseCase mDeleteUserMessageUseCase;
    private final AccountRepository mAccountRepository;
    private Subscription mGetUserMessagesSubscription;

    @Inject
    public MyMessagesPresenter(@NonNull GetUserMessagesUseCase getUserMessagesUseCase,
                               @NonNull SendAnswerUseCase sendAnswerUseCase,
                               @NonNull DeleteUserMessageUseCase deleteUserMessageUseCase,
                               @NonNull AccountRepository accountRepository) {
        mGetUserMessagesUseCase = getUserMessagesUseCase;
        mSendAnswerUseCase = sendAnswerUseCase;
        mDeleteUserMessageUseCase = deleteUserMessageUseCase;
        mAccountRepository = accountRepository;
    }

    @Override
    public void detachView(boolean retainInstance) {
        RxUtils.unsubscribe(mGetUserMessagesSubscription);
        super.detachView(retainInstance);
    }

    public void load(final boolean pullToRefresh) {
        getView().showLoading(pullToRefresh);
        mGetUserMessagesSubscription = mGetUserMessagesUseCase.load()
                .compose(RxUtils.applySchedulers())
                .subscribe(new Subscriber<MessagesPage>() {
                    @Override
                    public void onCompleted() {
                        getView().showContent();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, pullToRefresh);
                    }

                    @Override
                    public void onNext(MessagesPage data) {
                        getView().setData(data);
                    }
                });
    }

    public void loadMore() {
        if (mGetUserMessagesSubscription == null || mGetUserMessagesSubscription.isUnsubscribed()) {
            mGetUserMessagesSubscription = mGetUserMessagesUseCase.loadMore()
                    .compose(RxUtils.applySchedulers())
                    .subscribe(new Subscriber<MessagesPage>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            getView().showError(e, true);
                        }

                        @Override
                        public void onNext(MessagesPage data) {
                            getView().addMore(data);
                        }
                    });
        }
    }

    public void removeMessage(Message message) {
        mDeleteUserMessageUseCase.delete(message.getMessageId())
                .compose(RxUtils.applySchedulers())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, true);
                    }

                    @Override
                    public void onNext(Void aVoid) {
                        getView().removeItem(message);
                    }
                });
    }

    public void sendAnswer(Message message, String subject, String email, String name, String text) {
        mSendAnswerUseCase.sendMessage(
                message.getMessageId(),
                subject,
                name,
                email,
                text).compose(RxUtils.applySchedulers())
                .subscribe(new DefaultSubscriber<Void>() {
                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, true);
                    }
                });
    }

    public String getUsername() {
        return mAccountRepository.getUsername();
    }

    public String getUserEmail() {
        return mAccountRepository.getEmail();
    }
}
