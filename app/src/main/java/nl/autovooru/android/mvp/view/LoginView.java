package nl.autovooru.android.mvp.view;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
public interface LoginView extends MvpView {

    void showProgress();

    void hideProgress();

    void showError(String errorMessage);

    boolean showError(Throwable throwable);

    void finish();
}
