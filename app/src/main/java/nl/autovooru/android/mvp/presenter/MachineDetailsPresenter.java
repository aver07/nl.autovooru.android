package nl.autovooru.android.mvp.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.IntentUtils;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.interactor.AddToFavoriteUseCase;
import nl.autovooru.android.domain.interactor.DefaultSubscriber;
import nl.autovooru.android.domain.interactor.GetMachineDetailsUseCase;
import nl.autovooru.android.domain.interactor.RemoveFromFavoriteUseCase;
import nl.autovooru.android.domain.interactor.SendBidUseCase;
import nl.autovooru.android.domain.interactor.SendMessageUseCase;
import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.domain.model.machine.details.Content;
import nl.autovooru.android.domain.model.machine.details.Filters;
import nl.autovooru.android.domain.model.machine.details.MachineDetails;
import nl.autovooru.android.domain.model.machine.details.Profile;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.mvp.view.MachineDetailsView;
import nl.autovooru.android.rx.RxUtils;
import nl.autovooru.android.ui.activity.DealerMachineListActivity;
import nl.autovooru.android.ui.activity.MachineDetailsActivity;
import nl.autovooru.android.ui.activity.MapsActivity;
import nl.autovooru.android.utils.PlayServicesUtils;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

/**
 * Created by Antonenko Viacheslav on 18/12/15.
 */
@PerActivity
public class MachineDetailsPresenter extends MvpNullObjectBasePresenter<MachineDetailsView> {

    private final GetMachineDetailsUseCase mGetMachineDetailsUseCase;
    private final SendBidUseCase mSendBidUseCase;
    private final SendMessageUseCase mSendMessageUseCase;
    private final AccountRepository mAccountRepository;
    private final AddToFavoriteUseCase mAddToFavoriteUseCase;
    private final RemoveFromFavoriteUseCase mRemoveFromFavoriteUseCase;
    private Observable<MachineDetails> mDetailsObservable;
    private Subscription mSubscribe;
    @Nullable
    private Filters mFilters;
    @Nullable
    private Profile mProfile;
    @Nullable
    private String mMachineId;
    private String mDealerId;
    private Content mContentEntity;
    private boolean mIsFavorite;
    private boolean mIsMenuInitialized = false;
    private boolean mIsMachineDetailsLoaded = false;

    @Inject
    public MachineDetailsPresenter(GetMachineDetailsUseCase getMachineDetailsUseCase,
                                   SendBidUseCase sendBidUseCase,
                                   SendMessageUseCase sendMessageUseCase,
                                   AddToFavoriteUseCase addToFavoriteUseCase,
                                   RemoveFromFavoriteUseCase removeFromFavoriteUseCase,
                                   AccountRepository accountRepository) {
        mGetMachineDetailsUseCase = getMachineDetailsUseCase;
        mSendBidUseCase = sendBidUseCase;
        mSendMessageUseCase = sendMessageUseCase;
        mAddToFavoriteUseCase = addToFavoriteUseCase;
        mAccountRepository = accountRepository;
        mRemoveFromFavoriteUseCase = removeFromFavoriteUseCase;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        mIsMenuInitialized = false;
    }

    public void load(String machineId, boolean pullToRefresh) {
        getView().showLoading(pullToRefresh);

        RxUtils.unsubscribe(mSubscribe);

        if (mDetailsObservable == null || pullToRefresh) {
            mDetailsObservable = mGetMachineDetailsUseCase.getDetails(machineId)
                    .compose(RxUtils.applySchedulers()).cache();
        }

        mSubscribe = mDetailsObservable.subscribe(new Subscriber<MachineDetails>() {
            @Override
            public void onCompleted() {
                getView().showContent();
            }

            @Override
            public void onError(Throwable e) {
                getView().showError(e, pullToRefresh);
            }

            @Override
            public void onNext(MachineDetails machineDetails) {
                mFilters = machineDetails.getFilters();
                mProfile = machineDetails.getProfile();
                mMachineId = machineDetails.getMachineId();
                mContentEntity = machineDetails.getContent();
                mDealerId = machineDetails.getDealerId();
                mIsFavorite = machineDetails.isFavorite();
                mIsMachineDetailsLoaded = true;

                if (!mIsMenuInitialized) {
                    mIsMenuInitialized = true;
                    final String phone = mProfile != null ? mProfile.getPhone() : "";
                    final boolean showWebSiteFab = mProfile != null && !TextUtils.isEmpty(mProfile.getWebsiteUrl());
                    getView().initializeFAB(showWebSiteFab, phone, true, true);
                }

                getView().setData(machineDetails);

            }
        });
    }

    public boolean isLoggedIn() {
        return mAccountRepository.isLoggedIn();
    }

    public String getUsername() {
        return mAccountRepository.getUsername();
    }

    public String getUserEmail() {
        return mAccountRepository.getEmail();
    }

    @Nullable
    public Filters getFilters() {
        return mFilters;
    }

    public void openDealerInfo(@Nullable Activity activity) {
        if (activity != null) {
            if (PlayServicesUtils.checkGooglePlayServices(activity)) {
                if (mProfile != null) {
                    ArrayList<String> fields = new ArrayList<>(3);
                    if (!TextUtils.isEmpty(mProfile.getOrganizationName())) {
                        fields.add(activity.getString(R.string.fragment_details_company_name) + mProfile.getOrganizationName());
                    }
                    if (!TextUtils.isEmpty(mProfile.getAddress())) {
                        fields.add(activity.getString(R.string.fragment_details_company_address) + mProfile.getAddress());
                    }
                    if (!TextUtils.isEmpty(mProfile.getCity())) {
                        fields.add(activity.getString(R.string.fragment_details_company_city) + mProfile.getCity());
                    }
                    MapsActivity.openActivity(
                            activity,
                            activity.getString(R.string.fragment_details_maps_title),
                            fields,
                            mProfile.getLocations(),
                            mProfile.getWebsiteUrl(),
                            mProfile.getEmail(),
                            mProfile.getPhone(),
                            false);
                }
            }
        }
    }

    public void openWebsite(@Nullable Activity activity) {
        if (mProfile != null && activity != null) {
            final Intent intent = IntentUtils.openLink(mProfile.getWebsiteUrl());
            if (IntentUtils.isIntentAvailable(activity, intent)) {
                activity.startActivity(intent);
            }
        }
    }

    public void sendMessage(@Nullable String name, String email, String subject, String message) {
        mSendMessageUseCase.sendMessage(mMachineId, name, email, subject, message)
                .compose(RxUtils.applySchedulers())
                .subscribe(new DefaultSubscriber<>());
    }

    public void placeBid(String name, String email, String bid, String message) {
        mSendBidUseCase.sendBid(mMachineId, name, email, bid, message).
                compose(RxUtils.applySchedulers())
                .subscribe(new DefaultSubscriber<>());
    }

    public void call(@Nullable Activity activity) {
        if (mProfile != null && activity != null) {
            final Intent intent = IntentUtils.dialPhone(mProfile.getPhone());
            if (IntentUtils.isIntentAvailable(activity, intent)) {
                activity.startActivity(intent);
            }
        }
    }

    public void shareMachine(@Nullable Activity activity) {
        if (mContentEntity != null && activity != null) {
            final String url = activity.getString(R.string.machine_share_url, mContentEntity.getAlias());
            final Intent intent = IntentUtils.shareText("", url);
            if (IntentUtils.isIntentAvailable(activity, intent)) {
                activity.startActivity(intent);
            }
        }

    }

    public void openDetailsScreen(Context context, Machine machine) {
        MachineDetailsActivity.openActivity(context, machine.getMachineId());
    }

    public void openDealersCars(Context context, Filter category) {
        mProfile.getOrganizationName();
        DealerMachineListActivity.openDealerMachineListActivity(
                context,
                mDealerId,
                mProfile.getDealerType(),
                mProfile.getOrganizationName(),
                category);
    }

    public void toggleFavorite(String machineId) {
        mIsFavorite = !mIsFavorite;
        if (mIsFavorite) {
            mAddToFavoriteUseCase.addToFavorite(machineId)
                    .compose(RxUtils.applySchedulers())
                    .subscribe(new DefaultSubscriber<>());
        } else {
            mRemoveFromFavoriteUseCase.removeFromFavorite(machineId)
                    .compose(RxUtils.applySchedulers())
                    .subscribe(new DefaultSubscriber<>());
        }
    }

    public boolean isMachineDetailsLoaded() {
        return mIsMachineDetailsLoaded;
    }

    public boolean isFavorite() {
        return mIsFavorite;
    }
}
