package nl.autovooru.android.mvp.view;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public interface MyBidsView extends MvpLceView<MachineListViewModel> {

    void addMore(MachineListViewModel data);

    void removeMachine(Machine machine);
}
