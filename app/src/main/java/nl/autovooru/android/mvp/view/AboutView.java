package nl.autovooru.android.mvp.view;

import android.app.Activity;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by Antonenko Viacheslav on 09/12/15.
 */
public interface AboutView extends MvpView {

    void addMenu(@NonNull String title,
                 @DrawableRes int iconResId,
                 android.view.View.OnClickListener listener);

    @Nullable
    Activity getActivity();
}
