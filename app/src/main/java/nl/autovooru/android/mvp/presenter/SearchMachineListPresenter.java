package nl.autovooru.android.mvp.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;

import javax.inject.Inject;

import nl.autovooru.android.common.utils.L;
import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.interactor.AddToFavoriteUseCase;
import nl.autovooru.android.domain.interactor.DefaultSubscriber;
import nl.autovooru.android.domain.interactor.GetFilterTypesUseCase;
import nl.autovooru.android.domain.interactor.SearchMachineListUseCase;
import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.model.filters.FilterType;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.domain.model.query.MachineSearchQuery;
import nl.autovooru.android.domain.model.query.Order;
import nl.autovooru.android.mvp.view.SearchMachineListView;
import nl.autovooru.android.rx.RxUtils;
import rx.Observable;
import rx.Subscription;

/**
 * Created by Antonenko Viacheslav on 24/11/15.
 */
@PerActivity
public class SearchMachineListPresenter extends MvpNullObjectBasePresenter<SearchMachineListView> {

    private static final L LOG = L.getLogger(SearchMachineListPresenter.class);

    private static final FilterType FILTER_TYPE = new FilterType("5", "vcat", "");

    private final AddToFavoriteUseCase mAddToFavoriteUseCase;
    private final SearchMachineListUseCase mSearchMachineListUseCase;
    private final GetFilterTypesUseCase mGetFilterTypesUseCase;

    private Subscription mGetFiltersSubscribe;
    private Subscription mSearchMachineSubscribe;
    private MachineSearchQuery mMachineSearchQuery;
    private boolean mMachineCategoriesLoaded = false;

    @Inject
    public SearchMachineListPresenter(GetFilterTypesUseCase getFilterTypesUseCase,
                                      SearchMachineListUseCase searchMachineListUseCase,
                                      AddToFavoriteUseCase addToFavoriteUseCase) {
        mGetFilterTypesUseCase = getFilterTypesUseCase;
        mSearchMachineListUseCase = searchMachineListUseCase;
        mMachineSearchQuery = new MachineSearchQuery();
        mAddToFavoriteUseCase = addToFavoriteUseCase;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        RxUtils.unsubscribe(mGetFiltersSubscribe);
    }

    public void categoryChanged(Filter categoryFilter) {
        mMachineSearchQuery.setMachineCategory(categoryFilter);
        loadMachines(false);
    }

    public void setOrder(Order order) {
        mSearchMachineListUseCase.setOrder(order);
    }

    public void clearSortOrder() {
        mSearchMachineListUseCase.removeOrder();
    }

    public void setKeyword(String keywords) {
        mMachineSearchQuery.setKeywords(keywords);
    }

    public void setDealerId(String dealerId) {
        mSearchMachineListUseCase.setDealerId(dealerId);
    }

    public void addToFavorite(@NonNull Machine machine) {
        mAddToFavoriteUseCase.addToFavorite(machine.getMachineId())
                .compose(RxUtils.applySchedulers())
                .subscribe(new DefaultSubscriber<Void>() {
                    @Override
                    public void onCompleted() {
                        machine.setFavorite(true);
                    }

                });
    }

    public void loadMachineCategories() {
        getView().showLoading(true);
        RxUtils.unsubscribe(mGetFiltersSubscribe);
        mGetFiltersSubscribe = mGetFilterTypesUseCase.get(FILTER_TYPE)
                .compose(RxUtils.applySchedulers())
                .flatMap(result -> Observable.just(result.get(FILTER_TYPE.getKey())))
                .subscribe(result -> {
                    mMachineCategoriesLoaded = true;
                    int selectedPosition = 0;
                    final int size = result.size();
                    final String currentCategoryFilter = mMachineSearchQuery.getMachineCategory().getId();
                    for (int i = 0; i < size; i++) {
                        final Filter filter = result.get(i);
                        if (filter.getId().equals(currentCategoryFilter)) {
                            selectedPosition = i;
                            break;
                        }
                    }
                    getView().showContent();
                    getView().showMachineCategories(result, selectedPosition);
                }, throwable -> {
                    L.getLogger(getClass()).e("loadMachineCategories()", throwable);
                    getView().showError(throwable, true);
                });

    }

    public void loadMachines(final boolean pullToRefresh) {
        if (!mMachineCategoriesLoaded) {
            loadMachineCategories();
            return;
        }

        getView().showLoading(pullToRefresh);

        RxUtils.unsubscribe(mSearchMachineSubscribe);

        mSearchMachineListUseCase.applyFilters(mMachineSearchQuery.toMap());
        mSearchMachineSubscribe = mSearchMachineListUseCase.get()
                .compose(RxUtils.applySchedulers())
                .subscribe(machineListViewModel -> {
                    getView().setData(machineListViewModel);
                    getView().showContent();
                }, throwable -> {
                    getView().showError(throwable, pullToRefresh);
                    RxUtils.unsubscribe(mSearchMachineSubscribe);
                }, () -> RxUtils.unsubscribe(mSearchMachineSubscribe));
    }

    public void applyFilters(@Nullable MachineSearchQuery query) {
        if (query != null) {
            mMachineSearchQuery = query;
            mMachineCategoriesLoaded = false;
            loadMachines(false);
        }
    }

    public MachineSearchQuery getMachineSearchQuery() {
        return mMachineSearchQuery;
    }

    public void loadMoreMachines() {
        if (mSearchMachineSubscribe == null || mSearchMachineSubscribe.isUnsubscribed()) {
            mSearchMachineSubscribe = mSearchMachineListUseCase.getMore()
                    .compose(RxUtils.applySchedulers())
                    .subscribe(
                            machineListViewModel -> getView().addMoreMachines(machineListViewModel),
                            throwable -> {
                                RxUtils.unsubscribe(mSearchMachineSubscribe);
                                getView().showError(throwable, false);
                            },
                            () -> RxUtils.unsubscribe(mSearchMachineSubscribe));
        }
    }

}
