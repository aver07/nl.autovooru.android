package nl.autovooru.android.mvp.presenter;

import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;

import javax.inject.Inject;

import nl.autovooru.android.di.annotations.scope.PerActivity;
import nl.autovooru.android.domain.interactor.ForgotPasswordUseCase;
import nl.autovooru.android.domain.interactor.GetUserDataUseCase;
import nl.autovooru.android.domain.interactor.LoginUseCase;
import nl.autovooru.android.domain.model.user.UserInfo;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.mvp.view.LoginView;
import nl.autovooru.android.rx.RxUtils;
import rx.Subscriber;
import rx.Subscription;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
@PerActivity
public class LoginPresenter extends MvpNullObjectBasePresenter<LoginView> {

    private final Runnable mBeforeLoading = () -> getView().showProgress();
    private final Runnable mAfterLoading = () -> getView().hideProgress();
    private final LoginUseCase mLoginUseCase;
    private final ForgotPasswordUseCase mForgotPasswordUseCase;
    private final GetUserDataUseCase mGetUserDataUseCase;
    private final AccountRepository mAccountRepository;
    private Subscription mSubscribe;

    @Inject
    public LoginPresenter(@NonNull LoginUseCase loginUseCase,
                          @NonNull ForgotPasswordUseCase forgotPasswordUseCase,
                          @NonNull GetUserDataUseCase getUserDataUseCase,
                          @NonNull AccountRepository accountRepository) {
        mLoginUseCase = loginUseCase;
        mForgotPasswordUseCase = forgotPasswordUseCase;
        mGetUserDataUseCase = getUserDataUseCase;
        mAccountRepository = accountRepository;
    }

    @Override
    public void attachView(LoginView view) {
        super.attachView(view);
        if (mSubscribe != null && !mSubscribe.isUnsubscribed()) {
            getView().showProgress();
        }
    }

    public void authorize(@NonNull final String login, @NonNull final String password) {
        unsubscribe();
        mSubscribe = mLoginUseCase.authorize(login, password)
                .flatMap(response -> {
                    mAccountRepository.createAccount(null, login, password, response);
                    return mGetUserDataUseCase.load();
                })
                .compose(RxUtils.applySchedulers())
                .compose(RxUtils.applyOpBeforeAndAfter(mBeforeLoading, mAfterLoading))
                .subscribe(new Subscriber<UserInfo>() {
                    @Override
                    public void onCompleted() {
                        unsubscribe();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().hideProgress();
                        if (!getView().showError(e)) {
                            getView().showError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserInfo userInfo) {
                        mAccountRepository.updateUserInfo(userInfo);
                        getView().finish();
                    }
                });
    }

    public void forgotPassword(@NonNull String email) {
        unsubscribe();
        mSubscribe = mForgotPasswordUseCase.forgotPassword(email)
                .compose(RxUtils.applySchedulers())
                .compose(RxUtils.applyOpBeforeAndAfter(mBeforeLoading, mAfterLoading))
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {
                        unsubscribe();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().hideProgress();
                        if (!getView().showError(e)) {
                            getView().showError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(Void aVoid) {

                    }
                });
    }

    private void unsubscribe() {
        RxUtils.unsubscribe(mSubscribe);
        mSubscribe = null;
    }
}
