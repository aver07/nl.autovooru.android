package nl.autovooru.android.mvp.presenter;

import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;

import javax.inject.Inject;

import nl.autovooru.android.domain.interactor.GetUserDataUseCase;
import nl.autovooru.android.domain.interactor.SaveUserDataUseCase;
import nl.autovooru.android.domain.model.user.UserInfo;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.mvp.view.MyAccountView;
import nl.autovooru.android.rx.RxUtils;
import rx.Subscriber;
import rx.Subscription;

/**
 * Created by Antonenko Viacheslav on 29/12/15.
 */
public class MyAccountPresenter extends MvpNullObjectBasePresenter<MyAccountView> {

    private final GetUserDataUseCase mGetUserDataUseCase;
    private final SaveUserDataUseCase mSaveUserDataUseCase;
    private final AccountRepository mAccountRepository;
    private final Runnable mBeforeLoading = () -> getView().showProgress();
    private final Runnable mAfterLoading = () -> getView().hideProgress();
    private Subscription mGetUserDataSubscription;
    private boolean mIsOrganizationAccount;
    private Subscription mSaveUserDataSubscription;

    @Inject
    public MyAccountPresenter(@NonNull SaveUserDataUseCase saveUserDataUseCase,
                              @NonNull GetUserDataUseCase getUserDataUseCase,
                              @NonNull AccountRepository accountRepository) {
        mSaveUserDataUseCase = saveUserDataUseCase;
        mGetUserDataUseCase = getUserDataUseCase;
        mAccountRepository = accountRepository;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        RxUtils.unsubscribe(mGetUserDataSubscription);
        RxUtils.unsubscribe(mSaveUserDataSubscription);
    }

    public void loadUserData(final boolean pullToRefresh) {
        getView().showLoading(pullToRefresh);
        RxUtils.unsubscribe(mGetUserDataSubscription);
        mGetUserDataSubscription = mGetUserDataUseCase.load()
                .compose(RxUtils.applySchedulers())
                .subscribe(new Subscriber<UserInfo>() {
                    @Override
                    public void onCompleted() {
                        getView().showContent();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, pullToRefresh);
                    }

                    @Override
                    public void onNext(UserInfo userInfo) {
                        getView().setData(userInfo);
                        if (userInfo.getUserType().equals("org")) {
                            mIsOrganizationAccount = true;
                            getView().showOrganizationSpecificViews();
                            getView().hideUserSpecificViews();
                        } else {
                            mIsOrganizationAccount = false;
                            getView().showUserSpecificViews();
                            getView().hideOrganizationSpecificViews();
                        }
                    }
                });
    }

    public void sendUserData(final UserInfo userInfo) {
        RxUtils.unsubscribe(mSaveUserDataSubscription);
        mSaveUserDataSubscription = mSaveUserDataUseCase.execute(userInfo)
                .compose(RxUtils.applySchedulers())
                .compose(RxUtils.applyOpBeforeAndAfter(mBeforeLoading, mAfterLoading))
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {
                        getView().hideProgress();
                        getView().loadData(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showError(e, true);
                        getView().loadData(false);
                    }

                    @Override
                    public void onNext(Void aVoid) {
                        mAccountRepository.updatePassword(userInfo.getPassword());
                    }
                });
    }

    public boolean isOrganizationAccount() {
        return mIsOrganizationAccount;
    }
}
