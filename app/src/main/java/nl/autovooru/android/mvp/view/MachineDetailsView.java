package nl.autovooru.android.mvp.view;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import nl.autovooru.android.domain.model.machine.details.MachineDetails;

/**
 * Created by Antonenko Viacheslav on 18/12/15.
 */
public interface MachineDetailsView extends MvpLceView<MachineDetails> {

    void initializeFAB(boolean showWebSite, String phoneNumber, boolean showBid, boolean showSendMessage);

    void showSendMessageDialog();

    void showPlaceBidDialog();
}
