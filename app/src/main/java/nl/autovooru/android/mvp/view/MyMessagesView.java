package nl.autovooru.android.mvp.view;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import nl.autovooru.android.domain.model.user.Message;
import nl.autovooru.android.domain.model.user.MessagesPage;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public interface MyMessagesView extends MvpLceView<MessagesPage> {

    void addMore(MessagesPage data);

    void removeItem(Message message);
}
