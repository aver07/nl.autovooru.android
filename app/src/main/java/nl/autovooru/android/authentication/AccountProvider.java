package nl.autovooru.android.authentication;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.util.concurrent.atomic.AtomicBoolean;

import de.greenrobot.event.EventBus;
import nl.autovooru.android.BuildConfig;
import nl.autovooru.android.bus.events.AuthorizationChangedEvent;
import nl.autovooru.android.common.utils.ApiUtils;
import nl.autovooru.android.domain.model.auth.AuthInfo;
import nl.autovooru.android.domain.model.auth.AuthorizationToken;
import nl.autovooru.android.domain.model.user.UserInfo;
import nl.autovooru.android.domain.repository.AccountRepository;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
public class AccountProvider implements AccountRepository {

    private static final String KEY_USER_NAME = "key_user_name";
    private static final String KEY_USER_EMAIL = "key_user_email";
    private static final String KEY_USER_ID = "key_user_id";
    private static final String KEY_ACCESS_TOKEN = "key_access_token";
    private static final String KEY_CODE = "key_code";
    private static final String KEY_EXPIRE_DATE = "key_expire_date";

    private final AccountManager mAccountManager;
    private final AtomicBoolean mIsLoggedIn;

    public AccountProvider(Context context) {
        mAccountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        mIsLoggedIn = new AtomicBoolean();
        final Account[] accounts = mAccountManager.getAccountsByType(BuildConfig.ACCOUNT_TYPE);
        mIsLoggedIn.set(accounts.length > 0);
    }

    @Override
    public boolean createAccount(String name, String email, String password, AuthorizationToken authorizationToken) {
        logout();
        Account account = new Account(BuildConfig.ACCOUNT_NAME, BuildConfig.ACCOUNT_TYPE);

        final Bundle userdata = new Bundle();
        userdata.putString(KEY_USER_ID, authorizationToken.getDealerId());
        userdata.putString(KEY_USER_NAME, name);
        userdata.putString(KEY_USER_EMAIL, email);
        userdata.putString(KEY_ACCESS_TOKEN, authorizationToken.getAccessToken());
        userdata.putString(KEY_CODE, authorizationToken.getCode());
        userdata.putLong(KEY_EXPIRE_DATE, authorizationToken.getExpireDate());

        mAccountManager.addAccountExplicitly(account, password, userdata);
        mIsLoggedIn.set(true);
        EventBus.getDefault().post(new AuthorizationChangedEvent(true));
        return true;
    }

    @Override
    public void updateUserInfo(UserInfo userInfo) {
        final Account account = getAccount();
        mAccountManager.setUserData(account, KEY_USER_NAME, userInfo.getDisplayName());
    }

    @Override
    public void updatePassword(String password) {
        final Account account = getAccount();
        mAccountManager.setPassword(account, password);
    }

    @Override
    public void updateToken(AuthorizationToken authorizationToken) {
        final Account account = getAccount();
        mAccountManager.setUserData(account, KEY_ACCESS_TOKEN, authorizationToken.getAccessToken());
        mAccountManager.setUserData(account, KEY_CODE, authorizationToken.getCode());
    }

    @Override
    public void logout() {
        final Account account = getAccount();
        if (account != null) {
            if (ApiUtils.hasLOLLIPOP_MR1()) {
                mAccountManager.removeAccountExplicitly(account);
            } else {
                //noinspection deprecation
                mAccountManager.removeAccount(account, null, null);
            }
        }
        mIsLoggedIn.set(false);
        EventBus.getDefault().post(new AuthorizationChangedEvent(false));
    }

    @Override
    public boolean isLoggedIn() {
        return mIsLoggedIn.get();
    }

    @Override
    public String getUsername() {
        if (!isLoggedIn()) {
            throw new IllegalStateException("Try get account for unauthorized user");
        }
        final Account account = getAccount();
        return mAccountManager.getUserData(account, KEY_USER_NAME);
    }

    @Override
    public String getEmail() {
        if (!isLoggedIn()) {
            throw new IllegalStateException("Try get account for unauthorized user");
        }
        final Account account = getAccount();
        return mAccountManager.getUserData(account, KEY_USER_EMAIL);
    }

    @Override
    public String getPassword() {
        if (!isLoggedIn()) {
            throw new IllegalStateException("Try get account for unauthorized user");
        }
        final Account account = getAccount();
        return mAccountManager.getPassword(account);
    }

    @Nullable
    public String getUserId() {
        if (isLoggedIn()) {
            final Account account = getAccount();
            return mAccountManager.getUserData(account, KEY_USER_ID);
        } else {
            return null;
        }
    }

    @Nullable
    public AuthInfo getAuthInfo() {
        if (isLoggedIn()) {
            final Account account = getAccount();
            final String accessToken = mAccountManager.getUserData(account, KEY_ACCESS_TOKEN);
            final String code = mAccountManager.getUserData(account, KEY_CODE);
            return new AuthInfo(accessToken, code);
        } else {
            return null;
        }
    }

    private Account getAccount() {
        final Account[] accounts = mAccountManager.getAccountsByType(BuildConfig.ACCOUNT_TYPE);
        if (accounts != null && accounts.length > 0) {
            return accounts[0];
        }

        return null;
    }

}
