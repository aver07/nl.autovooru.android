package nl.autovooru.android.authentication;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
public class AutovooruAuthenticatorService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        AutovooruAuthenticator authenticator = new AutovooruAuthenticator(this);
        return authenticator.getIBinder();
    }
}
