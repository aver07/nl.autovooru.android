package nl.autovooru.android.push;

import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import nl.autovooru.android.BaseApp;
import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.L;
import nl.autovooru.android.di.components.ApplicationComponent;
import nl.autovooru.android.domain.interactor.SendPushTokenUseCase;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.domain.repository.UserDataRepository;
import nl.autovooru.android.settings.UserSettingsManager;
import rx.Subscriber;

/**
 * Created by Antonenko Viacheslav on 19/01/16.
 */
public class RegistrationService extends IntentService {

    private static final String TAG = "RegistrationService";
    protected L LOG = L.getLogger(RegistrationService.class);

    public RegistrationService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final ApplicationComponent component = ((BaseApp) getApplication()).getApplicationComponent();
        final AccountRepository accountRepository = component.accountRepository();
        final UserSettingsManager userSettingsManager = component.userSettingsManager();

        if (!accountRepository.isLoggedIn() || !userSettingsManager.notificationsEnabled()) {
            return;
        }
        try {
            final InstanceID instanceID = InstanceID.getInstance(this);
            final String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE);

            LOG.i("GCM Registration Toke: " + token);

            sendRegistrationToServer(token, component.userDataRepository());


        } catch (Exception e) {
            LOG.d("Failed to complete token refresh", e);
        }
    }

    /**
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token, UserDataRepository userDataRepository) {
        final SendPushTokenUseCase sendPushTokenUseCase = new SendPushTokenUseCase(userDataRepository);
        sendPushTokenUseCase.execute(token).subscribe(new Subscriber<Void>() {
            @Override
            public void onCompleted() {
                LOG.d("Push key sended.");
            }

            @Override
            public void onError(Throwable e) {
                LOG.e("Can't send push key", e);
            }

            @Override
            public void onNext(Void aVoid) {

            }
        });
    }
}
