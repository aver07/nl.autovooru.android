package nl.autovooru.android.push;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import nl.autovooru.android.BaseApp;
import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.StringUtils;
import nl.autovooru.android.di.components.ApplicationComponent;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.settings.UserSettingsManager;
import nl.autovooru.android.ui.activity.MachinesBidsActivity;
import nl.autovooru.android.ui.activity.MainActivity;

/**
 * Created by Antonenko Viacheslav on 19/01/16.
 */
public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";
    private static final int NOTIFICATION_BID_MESSAGE = 1;
    private static final int NOTIFICATION_MESSAGE = 2;
    private static final String TYPE_BID_MESSAGE = "bid";
    private static final String TYPE_MESSAGE = "message";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.d(TAG, "Push received.");
        final ApplicationComponent applicationComponent = ((BaseApp) getApplication()).getApplicationComponent();
        final AccountRepository accountRepository = applicationComponent.accountRepository();
        final UserSettingsManager userSettingsManager = applicationComponent.userSettingsManager();
        if (!accountRepository.isLoggedIn() || !userSettingsManager.notificationsEnabled()) {
            return;
        }
        final String type = data.getString("type");
        if (TextUtils.equals(type, TYPE_BID_MESSAGE) && userSettingsManager.bidNotificationsEnabled()) {
            final String machineId = data.getString("as_id");
            final String title = data.getString("title");
            final String body = data.getString("body");
            if (!StringUtils.isEmpty(machineId) && !StringUtils.isEmpty(title) && !StringUtils.isEmpty(body)) {
                sendMachineBidsNotification(machineId, title, body);
            }
        } else if (TextUtils.equals(type, TYPE_MESSAGE) && userSettingsManager.messagesNotificationsEnabled()) {
            final String title = data.getString("title");
            final String body = data.getString("body");
            if (!StringUtils.isEmpty(title) && !StringUtils.isEmpty(body)) {
                sendMessageNotification(title, body);
            }
        }
    }

    private void sendMessageNotification(String title, String body) {
        final ApplicationComponent applicationComponent = ((BaseApp) getApplication()).getApplicationComponent();
        final UserSettingsManager userSettingsManager = applicationComponent.userSettingsManager();

        final Intent intent = MainActivity.createIntent(getApplicationContext(), R.id.nav_posts);
        final PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_av_white)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(userSettingsManager.getNotificationSound())
                .setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent))
                .setContentIntent(pendingIntent);

        if (userSettingsManager.vibrationEnabled()) {
            notificationBuilder.setDefaults(NotificationCompat.DEFAULT_VIBRATE);
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(NOTIFICATION_MESSAGE, notificationBuilder.build());
    }

    private void sendMachineBidsNotification(String machineId, String title, String body) {
        final ApplicationComponent applicationComponent = ((BaseApp) getApplication()).getApplicationComponent();
        final UserSettingsManager userSettingsManager = applicationComponent.userSettingsManager();

        final Intent intent = MachinesBidsActivity.createIntent(getApplicationContext(), machineId);
        final TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplication());
        stackBuilder.addParentStack(MachinesBidsActivity.class);
        stackBuilder.addNextIntent(intent);

        final PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_av_white)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(userSettingsManager.getNotificationSound())
                .setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent))
                .setContentIntent(pendingIntent);

        if (userSettingsManager.vibrationEnabled()) {
            notificationBuilder.setDefaults(NotificationCompat.DEFAULT_VIBRATE);
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(NOTIFICATION_BID_MESSAGE, notificationBuilder.build());
    }
}
