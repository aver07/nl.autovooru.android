package nl.autovooru.android;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.squareup.leakcanary.RefWatcher;

import java.io.IOException;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import nl.autovooru.android.bus.events.AuthorizationChangedEvent;
import nl.autovooru.android.bus.events.NotificationStatusEvent;
import nl.autovooru.android.di.components.ApplicationComponent;
import nl.autovooru.android.di.components.DaggerApplicationComponent;
import nl.autovooru.android.di.modules.ApiModule;
import nl.autovooru.android.di.modules.ApplicationModule;
import nl.autovooru.android.domain.interactor.DefaultSubscriber;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.push.RegistrationService;
import nl.autovooru.android.rx.RxUtils;
import nl.autovooru.android.settings.UserSettingsManager;
import nl.autovooru.android.utils.PlayServicesUtils;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 01/10/15.
 */
public abstract class BaseApp extends Application {

    private RefWatcher mRefWatcher;
    private ApplicationComponent mApplicationComponent;
    @Inject
    UserSettingsManager mUserSettingsManager;
    @Inject
    AccountRepository mAccountRepository;

    public static RefWatcher getRefWatcher(Context context) {
        final BaseApp application = (BaseApp) context.getApplicationContext();
        return application.mRefWatcher;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        initializeInjector();
        mApplicationComponent.inject(this);

        mRefWatcher = installLeakCanary();
        installCrashlytics();

        EventBus.getDefault().register(this);

        if (!mApplicationComponent.accountRepository().isLoggedIn() && PlayServicesUtils.checkGooglePlayServices(this)) {
            removeGcmToken();
        }
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }

    protected abstract RefWatcher installLeakCanary();

    protected abstract void installCrashlytics();

    private void initializeInjector() {
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .apiModule(new ApiModule())
                .build();
    }

    public void onEventMainThread(final AuthorizationChangedEvent event) {
        if (event.isAuthorized() && PlayServicesUtils.checkGooglePlayServices(this)
                && mUserSettingsManager.notificationsEnabled()) {
            final Intent intent = new Intent(this, RegistrationService.class);
            startService(intent);
        } else {
            NotificationManagerCompat.from(this).cancelAll();
            if (PlayServicesUtils.checkGooglePlayServices(this)) {
                removeGcmToken();
            }
        }
    }

    public void onEventMainThread(final NotificationStatusEvent event) {
        if (mAccountRepository.isLoggedIn() && PlayServicesUtils.checkGooglePlayServices(this)
                && mUserSettingsManager.notificationsEnabled()) {
            final Intent intent = new Intent(this, RegistrationService.class);
            startService(intent);
        } else {
            NotificationManagerCompat.from(this).cancelAll();
            if (PlayServicesUtils.checkGooglePlayServices(this)) {
                removeGcmToken();
            }
        }
    }

    public void removeGcmToken() {
        Observable.defer(() -> {
            try {
                final InstanceID instanceID = InstanceID.getInstance(this);
                instanceID.deleteToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE);
                return Observable.just(true);
            } catch (IOException e) {
                e.printStackTrace();
                return Observable.just(false);
            }
        }).compose(RxUtils.applySchedulers()).subscribe(new DefaultSubscriber<>());
    }
}
