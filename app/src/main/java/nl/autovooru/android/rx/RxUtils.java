package nl.autovooru.android.rx;

import rx.Observable;
import rx.Observable.Transformer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Antonenko Viacheslav on 05/11/15.
 */
public final class RxUtils {

    private static final Transformer schedulersTransformer = new Transformer<Observable, Observable>() {
        @Override
        public Observable call(Observable observable) {
            return observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        }
    };

    private RxUtils() {

    }

    public static <T> Transformer<T, T> applySchedulers() {
        return tObservable -> tObservable.subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static <T> Transformer<T, T> applyOpBeforeAndAfter(final Runnable before, final Runnable after) {
        return tObservable -> tObservable.doOnSubscribe(before::run).doOnTerminate(after::run);
    }

    public static <T> Transformer<T, T> applyOpBefore(final Runnable before) {
        return tObservable -> tObservable.doOnSubscribe(before::run);
    }

    public static void unsubscribe(Subscription subscription) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}
