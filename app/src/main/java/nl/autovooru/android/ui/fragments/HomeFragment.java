package nl.autovooru.android.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;
import nl.autovooru.android.R;
import nl.autovooru.android.di.components.UseCaseComponent;
import nl.autovooru.android.mvp.presenter.HomePresenter;
import nl.autovooru.android.mvp.view.HomeView;
import nl.autovooru.android.ui.activity.BaseActivity;

/**
 * Created by Antonenko Viacheslav on 03/10/15.
 */
public class HomeFragment extends BaseMvpFragment<HomeView, HomePresenter> implements HomeView {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    DrawerLayout mDrawerLayout;
    Unbinder mUnbinder;
    private SearchView mSearchView;

    public static Fragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_home;
    }

    @NonNull
    @Override
    public HomePresenter createPresenter() {
        return getComponent(UseCaseComponent.class).provideHomePresenter();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final BaseActivity activity = (BaseActivity) getActivity();
        activity.setSupportActionBar(mToolbar);
        final ActionBar actionBar = activity.getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");

        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public void onDestroyView() {
        mDrawerLayout.setDrawerListener(null);
        mDrawerLayout = null;
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_home, menu);

        final MenuItem menuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getPresenter().onCategoryClick(R.id.nav_cars, query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        mSearchView.setSubmitButtonEnabled(true);
    }

    @Override
    public void onDestroyOptionsMenu() {
        mSearchView.setOnQueryTextListener(null);
        mSearchView = null;
        super.onDestroyOptionsMenu();
    }

    @OnClick({R.id.cars, R.id.oldtimers, R.id.commercial_vehicles, R.id.trucks,
            R.id.engines, R.id.damaged_cars, R.id.microcars, R.id.caravans_campers})
    public void onCategoryClick(View view) {
        switch (view.getId()) {
            case R.id.cars:
                getPresenter().onCategoryClick(R.id.nav_cars);
                break;
            case R.id.oldtimers:
                getPresenter().onCategoryClick(R.id.nav_oldtimers);
                break;
            case R.id.commercial_vehicles:
                getPresenter().onCategoryClick(R.id.nav_commercial_vehicles);
                break;
            case R.id.trucks:
                getPresenter().onCategoryClick(R.id.nav_trucks);
                break;
            case R.id.engines:
                getPresenter().onCategoryClick(R.id.nav_engines);
                break;
            case R.id.damaged_cars:
                getPresenter().onCategoryClick(R.id.nav_damaged_cars);
                break;
            case R.id.microcars:
                getPresenter().onCategoryClick(R.id.nav_microcars);
                break;
            case R.id.caravans_campers:
                getPresenter().onCategoryClick(R.id.nav_caravans);
                break;
            default:
                throw new IllegalStateException("Unknown machine menu item. Item id: " + view.getId());
        }
    }

}
