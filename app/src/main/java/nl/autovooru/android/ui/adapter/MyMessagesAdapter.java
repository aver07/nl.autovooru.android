package nl.autovooru.android.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.autovooru.android.R;
import nl.autovooru.android.domain.model.user.Message;
import nl.autovooru.android.ui.widget.recyclerview.AbstractRecyclerViewFooterAdapter;
import nl.autovooru.android.ui.widget.recyclerview.OnLoadMoreListener;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public class MyMessagesAdapter extends AbstractRecyclerViewFooterAdapter<Message> {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
    private final OnOverflowActionListener mOverflowActionListener;
    private SparseBooleanArray mExpandableIndexes = new SparseBooleanArray();

    public MyMessagesAdapter(RecyclerView recyclerView, final OnLoadMoreListener onLoadMoreListener, OnOverflowActionListener overflowActionListener) {
        super(recyclerView, onLoadMoreListener);
        mOverflowActionListener = overflowActionListener;
    }

    @Override
    public void resetItems(@NonNull List<Message> newDataSet) {
        super.resetItems(newDataSet);
        mExpandableIndexes.clear();
    }

    @Override
    public void removeItem(Message item) {
        super.removeItem(item);
        if (item != null) {
            int indexOfItem = getDataSet().indexOf(item);
            if (indexOfItem != -1) {
                mExpandableIndexes.delete(indexOfItem);
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int position) {
        final int layoutRes = R.layout.list_item_my_messages;
        final View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        final MessageViewHolder messageViewHolder = new MessageViewHolder(view, mOverflowActionListener);
        messageViewHolder.mArrowView.setOnClickListener(v -> {
            final int adapterPosition = messageViewHolder.getAdapterPosition();
            if (adapterPosition != RecyclerView.NO_POSITION) {
                if (mExpandableIndexes.get(adapterPosition)) {
                    mExpandableIndexes.delete(adapterPosition);
                    messageViewHolder.mMessageView.setVisibility(View.GONE);
                    messageViewHolder.mArrowView.setImageResource(R.drawable.ic_arrow_up);
                } else {
                    mExpandableIndexes.put(adapterPosition, true);
                    Message message = (Message) messageViewHolder.itemView.getTag();
                    messageViewHolder.mMessageView.setText(message.getMessage());
                    messageViewHolder.mMessageView.setVisibility(View.VISIBLE);
                    messageViewHolder.mArrowView.setImageResource(R.drawable.ic_arrow_down);
                }
            }
        });
        return messageViewHolder;
    }

    @Override
    protected void onBindBasicItemView(RecyclerView.ViewHolder holder, int position) {
        final MessageViewHolder viewHolder = (MessageViewHolder) holder;
        final Message message = getItem(position);

        if (message.getCreateDate() != null) {
            viewHolder.mDateView.setText(DATE_FORMAT.format(message.getCreateDate()));
        } else {
            viewHolder.mDateView.setText(null);
        }

        viewHolder.mEmailView.setText(message.getEmail());
        viewHolder.mSubjectView.setText(message.getTopic());

        if (mExpandableIndexes.get(position)) {
            viewHolder.mMessageView.setText(message.getMessage());
            viewHolder.mMessageView.setVisibility(View.VISIBLE);
            viewHolder.mArrowView.setImageResource(R.drawable.ic_arrow_down);
        } else {
            viewHolder.mArrowView.setImageResource(R.drawable.ic_arrow_up);
            viewHolder.mMessageView.setVisibility(View.GONE);
        }

        viewHolder.itemView.setTag(message);
    }

    public interface OnOverflowActionListener {
        void sendMessage(Message message);

        void removeMessage(Message message);
    }

    static class MessageViewHolder extends RecyclerView.ViewHolder {

        private final OnOverflowActionListener mOverflowActionListener;
        @BindView(R.id.date)
        TextView mDateView;
        @BindView(R.id.email)
        TextView mEmailView;
        @BindView(R.id.subject)
        TextView mSubjectView;
        @BindView(R.id.message)
        TextView mMessageView;
        @BindView(R.id.arrow)
        ImageView mArrowView;

        public MessageViewHolder(View itemView, OnOverflowActionListener overflowActionListener) {
            super(itemView);
            mOverflowActionListener = overflowActionListener;
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.overflow)
        public void showMenu(final View view) {
            final Message message = (Message) itemView.getTag();
            final PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
            popupMenu.inflate(R.menu.fragment_my_messages_list_item_overflow);
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.action_answer:
                        mOverflowActionListener.sendMessage(message);
                        return true;
                    case R.id.action_delete:
                        mOverflowActionListener.removeMessage(message);
                        return true;
                }
                return false;
            });
            popupMenu.show();
        }
    }
}
