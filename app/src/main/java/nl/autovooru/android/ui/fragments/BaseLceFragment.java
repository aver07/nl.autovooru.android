package nl.autovooru.android.ui.fragments;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.hannesdorfmann.mosby.mvp.lce.MvpLceFragment;
import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;
import com.squareup.leakcanary.RefWatcher;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import icepick.Icepick;
import nl.autovooru.android.App;
import nl.autovooru.android.BaseApp;
import nl.autovooru.android.common.utils.L;
import nl.autovooru.android.di.HasComponent;
import nl.autovooru.android.di.components.ApplicationComponent;

/**
 * Created by Antonenko Viacheslav on 01/12/15.
 */
public abstract class BaseLceFragment<CV extends View, M, V extends MvpLceView<M>, P extends MvpPresenter<V>>
        extends MvpLceFragment<CV, M, V, P> {

    protected final L LOG = L.getLogger(getClass());

    Unbinder mUnbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LOG.d("onCreate(); " + savedInstanceState);
    }

    @LayoutRes
    protected abstract int getLayoutRes();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Icepick.restoreInstanceState(this, savedInstanceState);
        return inflater.inflate(getLayoutRes(), container, false);
    }

    @CallSuper
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        LOG.d("onViewCreated(); " + savedInstanceState);
        injectDependencies();
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onStart() {
        super.onStart();
        LOG.d("onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        LOG.d("onResume()");
    }

    @CallSuper
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        LOG.d("onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        LOG.d("onStop()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LOG.d("onDestroy()");
        final RefWatcher refWatcher = App.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }

    @CallSuper
    @Override
    public void onDestroyView() {
        LOG.d("onDestroyView()");
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    /**
     * Inject the dependencies
     */
    protected void injectDependencies() {

    }

    protected ApplicationComponent getApplicationComponent() {
        return ((BaseApp) getActivity().getApplication()).getApplicationComponent();
    }

    /**
     * Gets a component for dependency injection by its type.
     */
    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }
}
