package nl.autovooru.android.ui.widget.recyclerview;

import android.support.v7.widget.RecyclerView;

/**
 * Created by Antonenko Viacheslav on 09/01/16.
 */
public interface OnStartDragListener {

    void onStartDrag(RecyclerView.ViewHolder viewHolder);
}
