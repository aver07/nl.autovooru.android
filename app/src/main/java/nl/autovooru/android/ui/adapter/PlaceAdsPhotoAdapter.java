package nl.autovooru.android.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.autovooru.android.R;
import nl.autovooru.android.ui.widget.recyclerview.AbstractRecyclerViewFooterAdapter;
import nl.autovooru.android.ui.widget.recyclerview.ItemTouchHelperAdapter;
import nl.autovooru.android.ui.widget.recyclerview.ItemTouchHelperViewHolder;
import nl.autovooru.android.ui.widget.recyclerview.OnStartDragListener;

/**
 * Created by Antonenko Viacheslav on 08/01/16.
 */
public class PlaceAdsPhotoAdapter extends AbstractRecyclerViewFooterAdapter<String>
        implements ItemTouchHelperAdapter {

    private final OnStartDragListener mDragStartListener;
    private int mCoverPhotoIndex = -1;
    private OnOverflowActionListener mOnOverflowActionListener = new OnOverflowActionListener() {
        @Override
        public void setMainPhoto(int position) {
            final int oldCoverPhotoIndex = mCoverPhotoIndex;
            mCoverPhotoIndex = position;
            notifyItemChanged(mCoverPhotoIndex);

            if (oldCoverPhotoIndex != -1) {
                notifyItemChanged(oldCoverPhotoIndex);
            }

        }

        @Override
        public void removePhoto(int position) {
            final int oldCoverPhotoIndex = mCoverPhotoIndex;
            mCoverPhotoIndex--;
            if (mCoverPhotoIndex < 0) {
                mCoverPhotoIndex = 0;
            }
            if (getItemCount() == 0) {
                mCoverPhotoIndex = -1;
            }
            notifyItemChanged(oldCoverPhotoIndex);

            removeItem(position);

            notifyItemChanged(mCoverPhotoIndex);
        }
    };

    public PlaceAdsPhotoAdapter(RecyclerView recyclerView, OnStartDragListener onStartDragListener) {
        super(recyclerView, null);
        enableLoadingMore(false);
        mDragStartListener = onStartDragListener;
    }

    public int getCoverPhotoIndex() {
        return mCoverPhotoIndex;
    }

    @Override
    public void addItems(@NonNull List<String> newDataSetItems) {
        if (mCoverPhotoIndex == -1 && newDataSetItems.size() > 0) {
            mCoverPhotoIndex = 0;
        }
        super.addItems(newDataSetItems);
    }

    @Override
    public void addItem(String item) {
        if (item != null && mCoverPhotoIndex == -1) {
            mCoverPhotoIndex = 0;
        }
        super.addItem(item);
    }

    public void resetItems(@NonNull List<String> newDataSet, int coverPhotoIndex) {
        mCoverPhotoIndex = coverPhotoIndex;
        super.resetItems(newDataSet);
    }

    @Override
    public RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int position) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.list_item_advertinsing_photo, parent, false);
        return new PhotoViewHolder(view, mOnOverflowActionListener);
    }

    @Override
    protected void onBindBasicItemView(RecyclerView.ViewHolder holder, int position) {
        PhotoViewHolder viewHolder = (PhotoViewHolder) holder;
        final String imageUrl = getItem(position);
        final Picasso picasso = Picasso.with(holder.itemView.getContext());
        picasso.load(imageUrl)
                .fit()
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                .centerInside()
                .into(viewHolder.mPhotoView);
        viewHolder.setMainPhoto(position == mCoverPhotoIndex);
        viewHolder.mPhotoView.setOnLongClickListener(v -> {
            mDragStartListener.onStartDrag(holder);
            return true;
        });
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(getDataSet(), fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        removeItem(position);
    }

    public interface OnOverflowActionListener {
        void setMainPhoto(int position);

        void removePhoto(int position);
    }

    static class PhotoViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {

        private final OnOverflowActionListener mListener;
        @BindView(R.id.photo)
        ImageView mPhotoView;
        @BindView(R.id.cover_photo)
        TextView mCoverPhotoView;
        boolean isMainPhoto = false;

        public PhotoViewHolder(View itemView, OnOverflowActionListener listener) {
            super(itemView);
            mListener = listener;
            ButterKnife.bind(this, itemView);
        }

        public void setMainPhoto(boolean isMainPhoto) {
            this.isMainPhoto = isMainPhoto;
            mCoverPhotoView.setVisibility(isMainPhoto ? View.VISIBLE : View.GONE);
        }

        @OnClick(R.id.overflow)
        public void showMenu(final View view) {
            final PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
            popupMenu.inflate(R.menu.place_ads_photo_overflow);
            final MenuItem favoriteMenuItem = popupMenu.getMenu().findItem(R.id.action_set_main_photo);
            favoriteMenuItem.setVisible(!isMainPhoto);
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.action_set_main_photo:
                        mListener.setMainPhoto(getAdapterPosition());
                        return true;
                    case R.id.action_delete:
                        mListener.removePhoto(getAdapterPosition());
                        return true;
                }
                return false;
            });
            popupMenu.show();
        }

        @Override
        public void onItemSelected() {

        }

        @Override
        public void onItemClear() {

        }
    }
}
