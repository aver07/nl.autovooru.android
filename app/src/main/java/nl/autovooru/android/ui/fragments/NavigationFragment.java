package nl.autovooru.android.ui.fragments;

import android.animation.ValueAnimator;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.util.TypedValue;
import android.view.TouchDelegate;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.AnimationUtils;
import nl.autovooru.android.common.utils.SchedulingUtils;
import nl.autovooru.android.di.components.DaggerNavigationComponent;
import nl.autovooru.android.mvp.presenter.NavigationPresenter;
import nl.autovooru.android.mvp.view.NavigationView;
import nl.autovooru.android.utils.ClickableSpan;

/**
 * Created by Antonenko Viacheslav on 17/11/15.
 */
public class NavigationFragment extends BaseMvpFragment<NavigationView, NavigationPresenter> implements NavigationView {

    @BindView(R.id.account_text_wrapper)
    View mAccountTextWrapper;
    @BindView(R.id.account_arrow)
    ImageView mAccountArrowView;
    @BindView(R.id.account_display_name)
    TextView mAccountDisplayNameView;
    @BindView(R.id.account_address)
    TextView mAccountAddressView;
    @BindView(R.id.account_sign_in)
    TextView mAccountSignInView;
    @BindView(R.id.nav_category)
    CheckedTextView mMachineCategoryLabelView;

    @BindView(R.id.nav_user_menu_wrapper)
    View mUserMenuWrapper;
    @BindView(R.id.nav_categories_wrapper)
    View mCategoriesWrapper;
    Unbinder mUnbinder;

    private ValueAnimator mCategoriesAnimator;
    private ValueAnimator mUserMenuAnimator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public NavigationPresenter createPresenter() {
        return DaggerNavigationComponent.builder()
                .applicationComponent(getApplicationComponent())
                .build().provideNavigationPresenter();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_navigation;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SchedulingUtils.doPreDraw(mCategoriesWrapper, () -> {
            mCategoriesWrapper.setVisibility(View.GONE);
            final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            mCategoriesWrapper.measure(widthSpec, heightSpec);

            mCategoriesAnimator = AnimationUtils.slideAnimator(mCategoriesWrapper, 0, mCategoriesWrapper.getMeasuredHeight());
            mMachineCategoryLabelView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_menu_up, 0);
        });
        SchedulingUtils.doPreDraw(mUserMenuWrapper, () -> {
            mUserMenuWrapper.setVisibility(View.GONE);
            final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            mUserMenuWrapper.measure(widthSpec, heightSpec);

            mUserMenuAnimator = AnimationUtils.slideAnimator(mUserMenuWrapper, 0, mUserMenuWrapper.getMeasuredHeight());
            mAccountArrowView.setImageResource(R.drawable.ic_nav_account_arrow_up);
        });
        SchedulingUtils.doPreDraw(mAccountTextWrapper, () -> {
            final Rect hitRect = new Rect();
            mAccountArrowView.getHitRect(hitRect);
            final float hitSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics());
            hitRect.left -= hitSize;
            hitRect.top -= hitSize;
            hitRect.right += hitSize;
            hitRect.bottom += hitSize;

            mAccountTextWrapper.setTouchDelegate(new TouchDelegate(hitRect, mAccountArrowView));
        });

        SpannableStringBuilder spb = new SpannableStringBuilder(getString(R.string.nav_sign_in));
        spb.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                getPresenter().openLoginForm(widget.getContext());
            }
        }, 0, spb.length(), 0);
        spb.append(" / ");
        final int start = spb.length();
        spb.append(getString(R.string.nav_register));
        spb.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                getPresenter().openRegistrationForm(widget.getContext());
            }
        }, start, spb.length(), 0);
        mAccountSignInView.setMovementMethod(LinkMovementMethod.getInstance());
        mAccountSignInView.setText(spb);
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        getPresenter().onResume();
    }

    @Override
    public void onDestroyView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    @OnClick(R.id.account_arrow)
    void toggleAccountMenu() {
        if (mUserMenuWrapper.getVisibility() == View.GONE) {
            mAccountArrowView.setImageResource(R.drawable.ic_nav_account_arrow_down);
            AnimationUtils.expand(mUserMenuWrapper, mUserMenuAnimator);
        } else {
            mAccountArrowView.setImageResource(R.drawable.ic_nav_account_arrow_up);
            AnimationUtils.collapse(mUserMenuWrapper);
        }
    }

    @OnClick({
            R.id.nav_my_advertise, R.id.nav_posts, R.id.nav_my_bids, R.id.nav_favourites, R.id.nav_my_account,
            R.id.nav_sign_out, R.id.nav_my_search, R.id.nav_add_advertise,
            R.id.nav_cars, R.id.nav_oldtimers, R.id.nav_commercial_vehicles, R.id.nav_trucks,
            R.id.nav_engines, R.id.nav_damaged_cars, R.id.nav_microcars, R.id.nav_caravans,
            R.id.nav_home, R.id.nav_settings, R.id.nav_about
    })
    public void onMenuItemClick(CheckedTextView view) {
        getPresenter().onMenuItemClick(view.getId());
    }

    @OnClick(R.id.nav_category)
    public void onCategoryItemClick() {
        getPresenter().onCategoryItemClick();
    }

    @Override
    public void showAccountRelatedView() {
        mUserMenuWrapper.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideAccountRelatedView() {
        mUserMenuWrapper.setVisibility(View.GONE);
    }

    @Override
    public void checkMenuItem(@IdRes int resId) {
        final View machineMenuItem = ButterKnife.findById(mCategoriesWrapper, resId);
        CheckedTextView menuItemView;
        if (machineMenuItem != null) {
            AnimationUtils.expand(mCategoriesWrapper, mCategoriesAnimator);
            menuItemView = (CheckedTextView) machineMenuItem;
        } else {
            menuItemView = ButterKnife.findById(getView(), resId);
        }
        menuItemView.setChecked(true);
    }

    @Override
    public void uncheckMenuItem(@IdRes int resId) {
        ((CheckedTextView) ButterKnife.findById(getView(), resId)).setChecked(false);
    }

    @Override
    public void expandOrCollapseCategory() {
        if (mCategoriesWrapper.getVisibility() == View.GONE) {
            mMachineCategoryLabelView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_menu_down, 0);
            AnimationUtils.expand(mCategoriesWrapper, mCategoriesAnimator);
        } else {
            mMachineCategoryLabelView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_menu_up, 0);
            AnimationUtils.collapse(mCategoriesWrapper);
        }
    }

    @Override
    public void showUserInfoView() {
        mAccountTextWrapper.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideUserInfoView() {
        mAccountTextWrapper.setVisibility(View.GONE);
    }

    @Override
    public void updateUserInfoView(String name, String email) {
        mAccountDisplayNameView.setText(name);
        mAccountAddressView.setText(email);
    }

    @Override
    public void showLoginView() {
        mAccountSignInView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginView() {
        mAccountSignInView.setVisibility(View.GONE);
    }
}
