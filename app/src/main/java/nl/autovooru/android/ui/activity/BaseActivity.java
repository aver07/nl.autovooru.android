package nl.autovooru.android.ui.activity;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import butterknife.ButterKnife;
import icepick.Icepick;
import nl.autovooru.android.BaseApp;
import nl.autovooru.android.common.utils.L;
import nl.autovooru.android.di.components.ApplicationComponent;
import nl.autovooru.android.di.modules.ActivityModule;

/**
 * Base {@link android.app.Activity} class for every Activity in this application.
 * <p/>
 * Created by Antonenko Viacheslav on 01/10/15.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected final L LOG = L.getLogger(getClass());
    private ActivityModule mActivityModule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        injectDependencies();
        super.onCreate(savedInstanceState);
        LOG.d("onCreate(); " + savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        LOG.d("onStart();");
    }

    @Override
    protected void onResume() {
        super.onResume();
        LOG.d("onResume();");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LOG.d("onPause();");
    }

    @Override
    protected void onStop() {
        super.onStop();
        LOG.d("onStop();");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LOG.d("onDestroy();");
    }

    @Override
    public void onContentChanged() {
        super.onContentChanged();
        ButterKnife.bind(this);
    }

    @CallSuper
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Adds a {@link Fragment} to this activity's layout.
     *
     * @param containerViewId The container view to where add the fragment.
     * @param fragment        The fragment to be added.
     */
    protected void addFragment(int containerViewId, Fragment fragment) {
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, fragment);
        fragmentTransaction.commit();
    }

    /**
     * Adds a {@link Fragment} to this activity's layout.
     *
     * @param containerViewId The container view to where add the fragment.
     * @param fragment        The fragment to be added.
     */
    protected void replaceFragment(int containerViewId, Fragment fragment) {
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(containerViewId, fragment);
        fragmentTransaction.commit();
    }

    /**
     * Get the Application component for dependency injection.
     *
     * @return {@link ApplicationComponent}
     */
    public ApplicationComponent getApplicationComponent() {
        return ((BaseApp) getApplication()).getApplicationComponent();
    }

    /**
     * Get an Activity module for dependency injection.
     *
     * @return {@link ActivityModule}
     */
    public ActivityModule getActivityModule() {
        if (mActivityModule == null) {
            mActivityModule = new ActivityModule(this);
        }
        return mActivityModule;
    }

    protected void injectDependencies() {

    }
}
