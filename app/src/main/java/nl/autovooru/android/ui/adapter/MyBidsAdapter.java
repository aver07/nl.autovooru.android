package nl.autovooru.android.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.StringRes;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.autovooru.android.R;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.ui.activity.MachineDetailsActivity;
import nl.autovooru.android.ui.widget.recyclerview.AbstractRecyclerViewFooterAdapter;
import nl.autovooru.android.ui.widget.recyclerview.OnLoadMoreListener;
import nl.autovooru.android.utils.MachineUtils;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public class MyBidsAdapter extends AbstractRecyclerViewFooterAdapter<Machine> {

    private final OnOverflowActionListener mOverflowActionListener;

    public MyBidsAdapter(RecyclerView recyclerView, final OnLoadMoreListener onLoadMoreListener, OnOverflowActionListener overflowActionListener) {
        super(recyclerView, onLoadMoreListener);
        mOverflowActionListener = overflowActionListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int position) {
        final int layoutRes = R.layout.list_item_my_bids;
        final View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new MachineViewHolder(view, mOverflowActionListener);
    }

    @Override
    protected void onBindBasicItemView(RecyclerView.ViewHolder holder, int position) {
        final MachineViewHolder viewHolder = (MachineViewHolder) holder;
        final Machine machine = getItem(position);
        final List<String> images = machine.getImages();
        if (images != null && images.size() > 0) {
            Picasso.with(holder.itemView.getContext())
                    .load(images.get(0))
                    .fit()
                    .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                    .into(viewHolder.mImageView);
        } else {
            viewHolder.mImageView.setImageDrawable(null);
        }

        viewHolder.mModelView.setText(machine.getName());
        setText(viewHolder.mTokenView, R.string.adapter_my_bids_label_token, machine.getToken());
        final Resources resources = viewHolder.itemView.getResources();

        final String myBid = resources.getString(R.string.euro, machine.getBid());
        setText(viewHolder.mBidView, R.string.adapter_my_bids_label_my_bid, myBid);

        final String price = MachineUtils.getPrice(holder.itemView.getContext(), machine);
        setText(viewHolder.mPriceView, R.string.adapter_my_bids_label_price, price);

        viewHolder.itemView.setTag(machine);
    }

    private void setText(TextView textView, @StringRes int resId, String value) {
        final Context context = textView.getContext();
        final TextAppearanceSpan labelSpan = new TextAppearanceSpan(context, R.style.MachinesBids_Label);
        final String label = context.getString(resId);
        final SpannableStringBuilder sb = new SpannableStringBuilder(label);
        sb.setSpan(labelSpan, 0, label.length(), 0);
        int start = sb.length();
        final TextAppearanceSpan valueSpan = new TextAppearanceSpan(context, R.style.MachinesBids_Value);
        sb.append(value);
        sb.setSpan(valueSpan, start, sb.length(), 0);
        textView.setText(sb);
    }

    public interface OnOverflowActionListener {
        void removeBid(Machine machine);

        void removeAllBids(Machine machine);
    }

    static class MachineViewHolder extends RecyclerView.ViewHolder {

        private final OnOverflowActionListener mOverflowActionListener;
        @BindView(R.id.image)
        ImageView mImageView;
        @BindView(R.id.bid)
        TextView mBidView;
        @BindView(R.id.model)
        TextView mModelView;
        @BindView(R.id.token)
        TextView mTokenView;
        @BindView(R.id.price)
        TextView mPriceView;

        public MachineViewHolder(View itemView, OnOverflowActionListener overflowActionListener) {
            super(itemView);
            mOverflowActionListener = overflowActionListener;
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.image)
        void openDetailPage() {
            final Machine machine = (Machine) itemView.getTag();
            MachineDetailsActivity.openActivity(itemView.getContext(), machine.getMachineId());
        }

        @OnClick(R.id.overflow)
        public void showMenu(final View view) {
            final Machine machine = (Machine) itemView.getTag();
            final PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
            popupMenu.inflate(R.menu.fragment_my_bids_list_item_overflow);
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        mOverflowActionListener.removeBid(machine);
                        return true;
                    case R.id.action_delete_all:
                        mOverflowActionListener.removeAllBids(machine);
                        return true;
                }
                return false;
            });
            popupMenu.show();
        }
    }
}
