package nl.autovooru.android.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import icepick.State;
import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.StringUtils;
import nl.autovooru.android.di.components.UseCaseComponent;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.domain.model.query.Order;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import nl.autovooru.android.exceptions.ErrorUtils;
import nl.autovooru.android.mvp.presenter.MyFavoritesPresenter;
import nl.autovooru.android.mvp.view.MyFavoritesView;
import nl.autovooru.android.ui.adapter.MyFavoritesAdapter;
import nl.autovooru.android.ui.widget.recyclerview.DividerItemDecoration;
import nl.autovooru.android.ui.widget.recyclerview.ItemClickSupport;
import nl.autovooru.android.ui.widget.recyclerview.OnLoadMoreListener;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public class MyFavoritesFragment extends BaseLceFragment<RecyclerView, MachineListViewModel,
        MyFavoritesView, MyFavoritesPresenter> implements MyFavoritesView,
        SwipeRefreshLayout.OnRefreshListener, OnLoadMoreListener, MyFavoritesAdapter.OnOverflowActionListener {

    @BindView(R.id.swipe_to_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @State
    int mCheckedSortPosition = 0;
    private MyFavoritesAdapter mAdapter;
    private MaterialDialog mProgressDialog;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_my_favorites;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        getPresenter().setOrder(getOrder(mCheckedSortPosition));

        initializeRecyclerView();
        loadData(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_my_favorites, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                showSortDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSortDialog() {
        final String[] items = getResources().getStringArray(R.array.dialog_sort_items);
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_sort_title)
                .setSingleChoiceItems(items, mCheckedSortPosition, (dialog, which) -> {
                    mCheckedSortPosition = which;
                    getPresenter().removeOrder();
                    Order order = getOrder(which);
                    getPresenter().setOrder(order);
                    getPresenter().loadFavorites(false);
                    dialog.dismiss();
                })
                .show();
    }

    private Order getOrder(int which) {
        Order order = null;
        switch (which) {
            case 0:
                order = new Order(Order.Sort.desc);
                order.addField(Order.Fields.updated);
                break;
            case 1:
                order = new Order(Order.Sort.asc);
                order.addField(Order.Fields.updated);
                break;
            case 2:
                order = new Order(Order.Sort.asc);
                order.addField(Order.Fields.price);
                break;
            case 3:
                order = new Order(Order.Sort.desc);
                order.addField(Order.Fields.price);
                break;

        }
        return order;
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return ErrorUtils.getErrorMessage(getActivity(), e);
    }

    @NonNull
    @Override
    public MyFavoritesPresenter createPresenter() {
        return getComponent(UseCaseComponent.class).provideMyFavoritesPresenter();
    }

    @Override
    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new MaterialDialog.Builder(getActivity())
                    .content(R.string.dialog_progress_content)
                    .progress(true, 0).build();
        }
        mProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.hide();
        }
    }

    @Override
    public void showContent() {
        super.showContent();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setData(MachineListViewModel data) {
        mAdapter.resetItems(data.getMachineList());
        mAdapter.enableLoadingMore(data.canLoadMore());
    }

    @Override
    public void addMore(MachineListViewModel data) {
        mAdapter.removeItem(null);
        if (data != null) {
            mAdapter.addItems(data.getMachineList());
            mAdapter.enableLoadingMore(data.canLoadMore());
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        getPresenter().loadFavorites(pullToRefresh);
    }

    @Override
    public void onLoadMore() {
        getPresenter().loadMoreFavorites();
    }

    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    public void removeMachine(Machine machine) {
        if (mAdapter != null) {
            mAdapter.removeItem(machine);
        }
    }

    @Override
    public void removeFromFavorite(Machine machine) {
        new MaterialDialog.Builder(getActivity())
                .content(R.string.dialog_remove_favorite_content)
                .positiveText(R.string.commons_dialog_ok)
                .negativeText(R.string.commons_dialog_cancel)
                .onPositive((dialog, which) -> getPresenter().removeFromFavorite(machine))
                .build().show();
    }

    @Override
    public void sendMessage(final Machine machine) {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_feedback_title)
                .customView(R.layout.dialog_send_message, true)
                .autoDismiss(false)
                .positiveText(R.string.dialog_feedback_positive_button)
                .onPositive((materialDialog, dialogAction) -> {
                    final View customView = materialDialog.getCustomView();

                    TextView subjectView = ButterKnife.findById(customView, R.id.subject);
                    TextView emailView = ButterKnife.findById(customView, R.id.email);
                    TextView messageView = ButterKnife.findById(customView, R.id.message);
                    TextView nameView = ButterKnife.findById(customView, R.id.name);
                    String errorMsg = subjectView.getResources().getString(R.string.dialog_feedback_error_message);

                    final String subject = subjectView.getText().toString();
                    if (StringUtils.isEmpty(subject)) {
                        ((TextInputLayout) subjectView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String email = emailView.getText().toString();
                    if (StringUtils.isEmpty(email)) {
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    if (!email.contains("@")) {
                        errorMsg = getString(R.string.error_wrong_email_format);
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String message = messageView.getText().toString();
                    if (StringUtils.isEmpty(message)) {
                        ((TextInputLayout) messageView.getParent()).setError(errorMsg);
                        return;
                    }

                    String name = nameView.getText().toString();
                    if (StringUtils.isEmpty(name)) {
                        ((TextInputLayout) nameView.getParent()).setError(errorMsg);
                        return;
                    }

                    getPresenter().sendMessage(
                            machine.getMachineId(),
                            name.trim(),
                            email.trim(),
                            subject.trim(),
                            message.trim());

                    materialDialog.dismiss();
                })
                .build();
        TextView emailView = ButterKnife.findById(dialog.getCustomView(), R.id.email);
        TextView nameView = ButterKnife.findById(dialog.getCustomView(), R.id.name);
        emailView.setText(getPresenter().getUserEmail());
        nameView.setText(getPresenter().getUsename());

        dialog.show();
    }

    @Override
    public void placeBid(Machine machine) {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_place_bid_title)
                .customView(R.layout.dialog_place_bid, true)
                .autoDismiss(false)
                .positiveText(R.string.dialog_place_bid_positive_button)
                .onPositive((materialDialog, dialogAction) -> {
                    final View customView = materialDialog.getCustomView();

                    TextView nameView = ButterKnife.findById(customView, R.id.name);
                    TextView emailView = ButterKnife.findById(customView, R.id.email);
                    TextView bidView = ButterKnife.findById(customView, R.id.bid);
                    TextView messageView = ButterKnife.findById(customView, R.id.message);
                    String errorMsg = emailView.getResources().getString(R.string.dialog_place_bid_error_message);

                    final String email = emailView.getText().toString();
                    if (StringUtils.isEmpty(email)) {
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    if (!email.contains("@")) {
                        errorMsg = getString(R.string.error_wrong_email_format);
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String bid = bidView.getText().toString();
                    if (StringUtils.isEmpty(bid)) {
                        ((TextInputLayout) bidView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String message = messageView.getText().toString();
                    if (StringUtils.isEmpty(message)) {
                        ((TextInputLayout) messageView.getParent()).setError(errorMsg);
                        return;
                    }

                    String name = nameView.getText().toString();
                    if (StringUtils.isEmpty(name)) {
                        ((TextInputLayout) nameView.getParent()).setError(errorMsg);
                        return;
                    }

                    getPresenter().placeBid(
                            machine.getMachineId(),
                            name.trim(),
                            email.trim(),
                            bid.trim(),
                            message.trim());

                    materialDialog.dismiss();
                })
                .build();

        TextView nameView = ButterKnife.findById(dialog.getCustomView(), R.id.name);
        TextView emailView = ButterKnife.findById(dialog.getCustomView(), R.id.email);
        emailView.setText(getPresenter().getUserEmail());
        nameView.setText(getPresenter().getUsename());

        dialog.show();
    }

    private void initializeRecyclerView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        contentView.setLayoutManager(layoutManager);
        final DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
        dividerItemDecoration.setDivider(ContextCompat.getDrawable(getActivity(), R.drawable.list_item_divider));
        contentView.addItemDecoration(dividerItemDecoration);
        contentView.setHasFixedSize(true);
        ItemClickSupport itemClickSupport = nl.autovooru.android.ui.widget.recyclerview.ItemClickSupport.addTo(contentView);
        itemClickSupport.setOnItemClickListener((recyclerView, position, v) -> {
            final Machine machine = mAdapter.getItem(position);
//            MachineDetailsActivity.openActivity(getActivity(), machine.getMachineId(),
//                    getPresenter().getMachineSearchQuery().getMachineCategory());
        });
        mAdapter = new MyFavoritesAdapter(contentView, this, this);
        contentView.setAdapter(mAdapter);
    }
}
