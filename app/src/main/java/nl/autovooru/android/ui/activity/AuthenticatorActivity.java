package nl.autovooru.android.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import nl.autovooru.android.R;
import nl.autovooru.android.di.HasComponent;
import nl.autovooru.android.di.components.AuthenticatorComponent;
import nl.autovooru.android.di.components.DaggerAuthenticatorComponent;
import nl.autovooru.android.ui.fragments.LoginFragment;
import nl.autovooru.android.ui.fragments.RegistrationFragment;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
public class AuthenticatorActivity extends BaseActivity implements HasComponent<AuthenticatorComponent> {

    private static final String EXTRA_OPEN_LOGIN_FORM = "extra_open_login_form";
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tabs)
    TabLayout mTabLayout;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    AuthenticatorComponent mAuthenticatorComponent;

    public static void openActivity(Context context, boolean openLoginForm) {
        final Intent intent = new Intent(context, AuthenticatorActivity.class);
        intent.putExtra(EXTRA_OPEN_LOGIN_FORM, openLoginForm);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authenticator);

        initializeToolbar();
        mViewPager.setAdapter(new Adapter(this, getSupportFragmentManager()));
        mViewPager.setCurrentItem(getIntent().getBooleanExtra(EXTRA_OPEN_LOGIN_FORM, true) ? 0 : 1);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void initializeToolbar() {
        setSupportActionBar(mToolbar);
        final ActionBar actionBar = getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.activity_authorization_title);
    }

    @Override
    public AuthenticatorComponent getComponent() {
        if (mAuthenticatorComponent == null) {
            mAuthenticatorComponent = DaggerAuthenticatorComponent.builder()
                    .applicationComponent(getApplicationComponent())
                    .build();
        }
        return mAuthenticatorComponent;
    }

    private static class Adapter extends FragmentPagerAdapter {

        private Context mAppContext;

        public Adapter(Context context, FragmentManager fm) {
            super(fm);
            mAppContext = context.getApplicationContext();
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new LoginFragment();
                case 1:
                    return new RegistrationFragment();
                default:
                    throw new IllegalStateException("");
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return mAppContext.getString(R.string.fragment_login_screen_title);
                case 1:
                    return mAppContext.getString(R.string.fragment_registration_title);
                default:
                    throw new IllegalStateException("");
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
