package nl.autovooru.android.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import nl.autovooru.android.R;
import nl.autovooru.android.bus.events.DeleteBidMessageEvent;
import nl.autovooru.android.bus.events.SendBidMessageEvent;
import nl.autovooru.android.common.utils.Preconditions;
import nl.autovooru.android.common.utils.StringUtils;
import nl.autovooru.android.di.HasComponent;
import nl.autovooru.android.di.components.DaggerMachinesBidsComponent;
import nl.autovooru.android.di.components.MachinesBidsComponent;
import nl.autovooru.android.di.modules.ActivityModule;
import nl.autovooru.android.di.modules.MachinesBidsModule;
import nl.autovooru.android.domain.model.user.Bid;
import nl.autovooru.android.exceptions.ErrorUtils;
import nl.autovooru.android.mvp.presenter.MachinesBidsPresenter;
import nl.autovooru.android.mvp.view.MachinesBidsView;
import nl.autovooru.android.ui.adapter.MachinesBidsAdapter;
import nl.autovooru.android.ui.widget.recyclerview.DividerItemDecoration;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public class MachinesBidsActivity extends BaseLceActivity<RecyclerView, List<Bid>, MachinesBidsView,
        MachinesBidsPresenter> implements MachinesBidsView, SwipeRefreshLayout.OnRefreshListener, HasComponent<MachinesBidsComponent> {

    private static final String EXTRA_MACHINE_ID = "extra_machine_id";
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.swipe_to_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private MachinesBidsAdapter mAdapter;
    private MaterialDialog mProgressDialog;

    public static void openActivity(@NonNull Context context, @NonNull String machineId) {
        context.startActivity(createIntent(context, machineId));
    }

    public static Intent createIntent(@NonNull Context context, @NonNull String machineId) {
        Preconditions.checkNotNull(machineId, "machineId");
        final Intent intent = new Intent(context, MachinesBidsActivity.class);
        intent.putExtra(EXTRA_MACHINE_ID, machineId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_machines_bids);

        initializeToolbar();
        initializeRecyclerView();
        mSwipeRefreshLayout.setOnRefreshListener(this);

        setRetainInstance(true);

        loadData(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(SendBidMessageEvent event) {
        final Bid bid = event.getBid();
        showSendBidMessageDialog(bid);
    }

    public void onEvent(DeleteBidMessageEvent event) {
        final Bid bid = event.getBid();
        getPresenter().deleteBidMessage(bid.getBidId());
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return ErrorUtils.getErrorMessage(this, e);
    }

    @NonNull
    @Override
    public MachinesBidsPresenter createPresenter() {
        return getComponent().provideMachinesBidsPresenter();
    }

    @Override
    public void showContent() {
        super.showContent();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setData(List<Bid> data) {
        mAdapter.resetItems(data);
        mAdapter.enableLoadingMore(false);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        getPresenter().load(pullToRefresh);
    }

    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    public void showProgress() {
        if (mProgressDialog == null) {
            initProgressDialog();
        }
        mProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.hide();
        }
    }

    private void initializeToolbar() {
        setSupportActionBar(mToolbar);
        final ActionBar actionBar = getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.activity_machines_bids_title);
    }

    private void initializeRecyclerView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        contentView.setLayoutManager(layoutManager);
        final DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST);
        dividerItemDecoration.setDivider(ContextCompat.getDrawable(this, R.drawable.list_item_divider));
        contentView.addItemDecoration(dividerItemDecoration);
        contentView.setHasFixedSize(true);
        mAdapter = new MachinesBidsAdapter(contentView);
        contentView.setAdapter(mAdapter);
    }

    private void initProgressDialog() {
        mProgressDialog = new MaterialDialog.Builder(this)
                .content(R.string.dialog_progress_content)
                .progress(true, 0).build();
    }

    private void showSendBidMessageDialog(final Bid bid) {
        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(R.string.dialog_feedback_title)
                .customView(R.layout.dialog_send_message, true)
                .autoDismiss(false)
                .positiveText(R.string.dialog_feedback_positive_button)
                .onPositive((materialDialog, dialogAction) -> {
                    final View customView = materialDialog.getCustomView();

                    TextView subjectView = ButterKnife.findById(customView, R.id.subject);
                    TextView emailView = ButterKnife.findById(customView, R.id.email);
                    TextView messageView = ButterKnife.findById(customView, R.id.message);
                    TextView nameView = ButterKnife.findById(customView, R.id.name);
                    String errorMsg = subjectView.getResources().getString(R.string.dialog_feedback_error_message);

                    final String subject = subjectView.getText().toString();
                    if (StringUtils.isEmpty(subject)) {
                        ((TextInputLayout) subjectView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String email = emailView.getText().toString();
                    if (StringUtils.isEmpty(email)) {
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    if (!email.contains("@")) {
                        errorMsg = getString(R.string.error_wrong_email_format);
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String message = messageView.getText().toString();
                    if (StringUtils.isEmpty(message)) {
                        ((TextInputLayout) messageView.getParent()).setError(errorMsg);
                        return;
                    }

                    String name = nameView.getText().toString();
                    if (StringUtils.isEmpty(name)) {
                        ((TextInputLayout) nameView.getParent()).setError(errorMsg);
                        return;
                    }

                    getPresenter().sendBidMessage(
                            bid.getMessageId(),
                            name.trim(),
                            email.trim(),
                            subject.trim(),
                            message.trim());

                    materialDialog.dismiss();
                })
                .build();

        TextView emailView = ButterKnife.findById(dialog.getCustomView(), R.id.email);
        emailView.setText(getPresenter().getUserEmail());
        TextView nameView = ButterKnife.findById(dialog.getCustomView(), R.id.name);
        nameView.setText(getPresenter().getUsername());

        dialog.show();
    }

    @Override
    public MachinesBidsComponent getComponent() {
        final String machineId = getIntent().getStringExtra(EXTRA_MACHINE_ID);
        return DaggerMachinesBidsComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(getApplicationComponent())
                .machinesBidsModule(new MachinesBidsModule(machineId))
                .build();
    }
}
