package nl.autovooru.android.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import org.parceler.Parcels;

import nl.autovooru.android.R;
import nl.autovooru.android.di.HasComponent;
import nl.autovooru.android.di.components.DaggerUseCaseComponent;
import nl.autovooru.android.di.components.UseCaseComponent;
import nl.autovooru.android.di.modules.UseCaseModule;
import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.ui.fragments.DealerSearchMachineListFragmentBuilder;

/**
 * Created by Antonenko Viacheslav on 20/12/15.
 */
public class DealerMachineListActivity extends BaseActivity implements HasComponent<UseCaseComponent> {

    private static final String EXTRA_DEALER_ID = "extra_dealer_id";
    private static final String EXTRA_MACHINE_CATEGORY = "extra_machine_category";
    private static final String EXTRA_DEALER_NAME = "extra_dealer_name";
    private static final String EXTRA_DEALER_TYPE = "extra_dealer_type";
    private UseCaseComponent mUseCaseComponent;

    public static void openDealerMachineListActivity(@NonNull Context context,
                                                     @NonNull String dealerId,
                                                     @NonNull String dealerType,
                                                     @Nullable String dealerName,
                                                     @NonNull Filter category) {
        final Intent intent = new Intent(context, DealerMachineListActivity.class);
        intent.putExtra(EXTRA_DEALER_ID, dealerId);
        intent.putExtra(EXTRA_MACHINE_CATEGORY, Parcels.wrap(category));
        intent.putExtra(EXTRA_DEALER_TYPE, dealerType);
        intent.putExtra(EXTRA_DEALER_NAME, dealerName);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_machine_list);

        if (savedInstanceState == null) {
            final String dealerId = getIntent().getStringExtra(EXTRA_DEALER_ID);
            final Filter category = Parcels.unwrap(getIntent().getParcelableExtra(EXTRA_MACHINE_CATEGORY));
            final String dealerType = getIntent().getStringExtra(EXTRA_DEALER_TYPE);
            final String dealerName = getIntent().getStringExtra(EXTRA_DEALER_NAME);

            final Fragment f = new DealerSearchMachineListFragmentBuilder(dealerType, category)
                    .dealerId(dealerId)
                    .dealerName(dealerName).build();
            final FragmentTransaction t = getSupportFragmentManager().beginTransaction();
            t.replace(R.id.fragment_container, f);
            t.commit();
        }
    }

    @Override
    public UseCaseComponent getComponent() {
        if (mUseCaseComponent == null) {
            mUseCaseComponent = DaggerUseCaseComponent.builder()
                    .applicationComponent(getApplicationComponent())
                    .activityModule(getActivityModule())
                    .useCaseModule(new UseCaseModule())
                    .build();
        }
        return mUseCaseComponent;
    }
}
