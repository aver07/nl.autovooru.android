package nl.autovooru.android.ui.fragments;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.leakcanary.RefWatcher;

import butterknife.ButterKnife;
import icepick.Icepick;
import nl.autovooru.android.App;
import nl.autovooru.android.BaseApp;
import nl.autovooru.android.common.utils.L;
import nl.autovooru.android.di.HasComponent;
import nl.autovooru.android.di.components.ApplicationComponent;

/**
 * Base {@link android.app.Fragment} class for every fragment in this application.
 * <p>
 * Created by Antonenko Viacheslav on 02/10/15.
 */
public abstract class BaseFragment extends Fragment {

    protected final L LOG = L.getLogger(getClass());

    @LayoutRes
    protected abstract int getLayoutRes();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        injectDependencies();
        super.onCreate(savedInstanceState);
        LOG.d("onCreate(); " + savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        LOG.d("onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        LOG.d("onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        LOG.d("onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        LOG.d("onStop()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LOG.d("onDestroy()");
        final RefWatcher refWatcher = App.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Icepick.restoreInstanceState(this, savedInstanceState);
        return inflater.inflate(getLayoutRes(), container, false);
    }

    @CallSuper
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        LOG.d("onViewCreated(); " + savedInstanceState);
        injectDependencies();
        ButterKnife.bind(this, view);
    }

    @CallSuper
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @CallSuper
    @Override
    public void onDestroyView() {
        LOG.d("onDestroyView()");
        super.onDestroyView();
    }

    /**
     * Inject the dependencies
     */
    protected void injectDependencies() {

    }

    protected ApplicationComponent getApplicationComponent() {
        return ((BaseApp) getActivity().getApplication()).getApplicationComponent();
    }

    /**
     * Gets a component for dependency injection by its type.
     */
    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }
}
