package nl.autovooru.android.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.hannesdorfmann.fragmentargs.bundler.ParcelerArgsBundler;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.Unbinder;
import de.greenrobot.event.EventBus;
import icepick.State;
import nl.autovooru.android.R;
import nl.autovooru.android.bus.events.AddToFavoriteEvent;
import nl.autovooru.android.di.components.UseCaseComponent;
import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.domain.model.query.MachineSearchQuery;
import nl.autovooru.android.domain.model.query.Order;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import nl.autovooru.android.exceptions.ErrorUtils;
import nl.autovooru.android.mvp.presenter.SearchMachineListPresenter;
import nl.autovooru.android.mvp.view.SearchMachineListView;
import nl.autovooru.android.ui.activity.BaseActivity;
import nl.autovooru.android.ui.activity.MachineDetailsActivity;
import nl.autovooru.android.ui.activity.MachineSearchFiltersActivity;
import nl.autovooru.android.ui.adapter.SearchMachineAdapter;
import nl.autovooru.android.ui.widget.recyclerview.DividerItemDecoration;
import nl.autovooru.android.ui.widget.recyclerview.ItemClickSupport;
import nl.autovooru.android.ui.widget.recyclerview.OnLoadMoreListener;

/**
 * Created by Antonenko Viacheslav on 17/11/15.
 */
@FragmentWithArgs
public class SearchMachineListFragment extends BaseLceFragment<SwipeRefreshLayout, MachineListViewModel, SearchMachineListView, SearchMachineListPresenter>
        implements SearchMachineListView, SwipeRefreshLayout.OnRefreshListener, OnLoadMoreListener {

    private static final int FILTERS_REQUEST_CODE = 1000;

    @BindView(R.id.action_category_filter)
    Spinner mCategorySpinner;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.machine_counter)
    TextView mMachineCounterView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.action_list)
    ImageView mActionListView;
    DrawerLayout mDrawerLayout;
    Unbinder mUnbinder;
    @Nullable
    @Arg(required = false)
    String mDefaultKeywords;
    @Arg(bundler = ParcelerArgsBundler.class)
    Filter mDefaultMachineCategory;
    @Arg(required = false)
    String mDealerId;
    @State
    boolean mShowLikeList = false;
    @State
    int mCheckedSortPosition = 0;
    MachineSearchQuery mMachineSearchQuery;

    private SearchMachineAdapter mAdapter;
    private SearchView mSearchView;
    private MenuItem mSearchMenuItem;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_search_machine_list;
    }

    @NonNull
    @Override
    public SearchMachineListPresenter createPresenter() {
        return getComponent(UseCaseComponent.class).provideSearchMachineListPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        FragmentArgs.inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        contentView.setOnRefreshListener(this);

        initializeToolbar();
        initializeRecyclerView();

        mActionListView.setImageResource(mShowLikeList ? R.drawable.ic_action_list_grey : R.drawable.ic_action_grote_grey);

        if (savedInstanceState == null) {
            mMachineSearchQuery = new MachineSearchQuery();
            mMachineSearchQuery.setKeywords(mDefaultKeywords);
            mMachineSearchQuery.setMachineCategory(mDefaultMachineCategory);

            final Order order = new Order();
            order.setSort(Order.Sort.desc);
            order.addField(Order.Fields.updated);
            getPresenter().setOrder(order);
            getPresenter().setDealerId(mDealerId);
        } else {
            mMachineSearchQuery = Parcels.unwrap(savedInstanceState.getParcelable("mMachineSearchQuery"));
        }

        getPresenter().applyFilters(mMachineSearchQuery);
        getPresenter().loadMachineCategories();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroyView() {
        ItemClickSupport.removeFrom(mRecyclerView);
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == FILTERS_REQUEST_CODE) {
            MachineSearchQuery query = Parcels.unwrap(data.getParcelableExtra("filters"));
            getPresenter().applyFilters(query);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_search_machine_list, menu);

        mSearchMenuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(mSearchMenuItem);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mSearchView.clearFocus();
                getPresenter().setKeyword(query);
                loadData(false);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        MenuItemCompat.setOnActionExpandListener(mSearchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                getPresenter().setKeyword("");
                loadData(false);
                return true;
            }
        });

        mSearchView.setSubmitButtonEnabled(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        final String keywords;
        if (mMachineSearchQuery != null) {
            keywords = mMachineSearchQuery.getKeywords();
        } else {
            keywords = mDefaultKeywords;
        }
        if (!TextUtils.isEmpty(keywords) && mSearchView != null) {
            MenuItemCompat.expandActionView(mSearchMenuItem);
            mSearchView.setQuery(keywords, true);
        }
    }

    @Override
    public void onDestroyOptionsMenu() {
        MenuItemCompat.setOnActionExpandListener(mSearchMenuItem, null);
        mSearchView.setOnQueryTextListener(null);
        mSearchView = null;
        mSearchMenuItem = null;
        super.onDestroyOptionsMenu();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("mMachineSearchQuery", Parcels.wrap(getPresenter().getMachineSearchQuery()));
    }

    @Override
    public void showMachineCategories(List<Filter> machineCategories, int selectedPosition) {
        final ArrayAdapter<Filter> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, machineCategories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCategorySpinner.setAdapter(adapter);
        mCategorySpinner.setSelection(selectedPosition);
    }

    @Override
    public void addMoreMachines(MachineListViewModel viewModel) {
        if (viewModel != null) {
            mAdapter.removeItem(null);
            mAdapter.addItems(viewModel.getMachineList());
            mAdapter.enableLoadingMore(viewModel.canLoadMore());
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        getPresenter().loadMachines(pullToRefresh);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return ErrorUtils.getErrorMessage(getActivity(), e);
    }

    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    public void onLoadMore() {
        getPresenter().loadMoreMachines();
    }

    @Override
    public void showContent() {
        super.showContent();
        contentView.setRefreshing(false);
    }

    @Override
    public void setData(MachineListViewModel data) {
        mAdapter.resetItems(data.getMachineList());
        updateCounter(data);
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        contentView.setRefreshing(false);
    }

    public void onEvent(AddToFavoriteEvent event) {
        getPresenter().addToFavorite(event.getMachine());
    }

    @OnClick(R.id.action_filters)
    void openFiltersActivity() {
        final Filter machineCategory = getPresenter().getMachineSearchQuery().getMachineCategory();
        if (machineCategory != null) {
            final Intent intent = MachineSearchFiltersActivity.openActivity(getActivity(),
                    getPresenter().getMachineSearchQuery());
            startActivityForResult(intent, FILTERS_REQUEST_CODE);
        }
    }

    @OnClick(R.id.action_sort)
    void changeSortOrder() {
        final String[] items = getResources().getStringArray(R.array.dialog_sort_items);
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_sort_title)
                .setSingleChoiceItems(items, mCheckedSortPosition, (dialog, which) -> {
                    mCheckedSortPosition = which;
                    Order order = null;
                    getPresenter().clearSortOrder();
                    switch (which) {
                        case 0:
                            order = new Order(Order.Sort.desc);
                            order.addField(Order.Fields.updated);
                            break;
                        case 1:
                            order = new Order(Order.Sort.asc);
                            order.addField(Order.Fields.updated);
                            break;
                        case 2:
                            order = new Order(Order.Sort.asc);
                            order.addField(Order.Fields.price);
                            break;
                        case 3:
                            order = new Order(Order.Sort.desc);
                            order.addField(Order.Fields.price);
                            break;

                    }
                    getPresenter().setOrder(order);
                    getPresenter().loadMachines(false);
                    dialog.dismiss();
                })
                .show();
    }

    @OnClick(R.id.action_list)
    void changeListStyle() {
        mShowLikeList = !mShowLikeList;
        mAdapter.removeItem(null);
        final List<Machine> dataSet = mAdapter.getDataSet();
        mAdapter = new SearchMachineAdapter(mRecyclerView, this, mShowLikeList);
        mAdapter.addItems(dataSet);
        mRecyclerView.setAdapter(mAdapter);
        mActionListView.setImageResource(mShowLikeList ? R.drawable.ic_action_list_grey : R.drawable.ic_action_grote_grey);
    }


    @OnItemSelected(value = R.id.action_category_filter, callback = OnItemSelected.Callback.ITEM_SELECTED)
    public void onItemSelected(AdapterView<?> parent, int position) {
        Filter filter = (Filter) parent.getItemAtPosition(position);
        getPresenter().categoryChanged(filter);
    }

    protected void updateCounter(MachineListViewModel data) {
        mMachineCounterView.setText(getString(R.string.fragment_search_machine_machine_count, data.getTotal()));
    }

    protected void initializeToolbar() {
        final BaseActivity activity = (BaseActivity) getActivity();
        activity.setSupportActionBar(mToolbar);
        final ActionBar actionBar = activity.getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.fragment_search_machine_list_title);

        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        if (mDrawerLayout != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    getActivity(), mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            mDrawerLayout.setDrawerListener(toggle);
            toggle.syncState();
        }
    }

    private void initializeRecyclerView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        final DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
        dividerItemDecoration.setDivider(ContextCompat.getDrawable(getActivity(), R.drawable.list_item_divider));
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        mRecyclerView.setHasFixedSize(true);
        ItemClickSupport itemClickSupport = nl.autovooru.android.ui.widget.recyclerview.ItemClickSupport.addTo(mRecyclerView);
        itemClickSupport.setOnItemClickListener((recyclerView, position, v) -> {
            final Machine machine = mAdapter.getItem(position);
            if (machine != null) {
                MachineDetailsActivity.openActivity(getActivity(), machine.getMachineId());
            }
        });
        mAdapter = new SearchMachineAdapter(mRecyclerView, this, mShowLikeList);
        mRecyclerView.setAdapter(mAdapter);
    }
}
