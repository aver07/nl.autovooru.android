package nl.autovooru.android.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jakewharton.rxbinding.widget.RxRadioGroup;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;
import nl.autovooru.android.R;
import nl.autovooru.android.di.components.AuthenticatorComponent;
import nl.autovooru.android.exceptions.ErrorUtils;
import nl.autovooru.android.mvp.presenter.RegistrationPresenter;
import nl.autovooru.android.mvp.view.RegistrationView;
import nl.autovooru.android.ui.dialogs.DialogProvider;
import rx.Subscription;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public class RegistrationFragment extends BaseMvpFragment<RegistrationView, RegistrationPresenter>
        implements RegistrationView, DialogProvider.InfoDialog.ButtonListener {

    private static final String DIALOG_ORGANIZATION_REGISTRATION_SUCCESS_TAG = "tag_dialog_organization_registration_success";
    private static final int DIALOG_ORGANIZATION_REGISTRATION_SUCCESS_ID = 1;

    Unbinder mUnbinder;
    @BindView(R.id.user_type)
    RadioGroup mUserTypeView;
    @BindView(R.id.organization_name)
    EditText mCompanyNameView;
    @BindView(R.id.first_name)
    EditText mFirstNameView;
    @BindView(R.id.surname)
    EditText mSurnameView;
    @BindView(R.id.post_code)
    EditText mPostcodeView;
    @BindView(R.id.city)
    EditText mCityView;
    @BindView(R.id.address)
    EditText mAddressView;
    @BindView(R.id.email)
    EditText mEmailView;
    @BindView(R.id.phone)
    EditText mPhoneView;
    @BindView(R.id.ads_count)
    EditText mAutosCountView;
    @BindView(R.id.password)
    EditText mPasswordView;
    @BindView(R.id.confirm_password)
    EditText mPasswordConfirmView;
    @BindView(R.id.privacy)
    TextView mPrivacyView;
    private MaterialDialog mProgressDialog;
    private Subscription mUserTypeSubscribe;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mUserTypeSubscribe = RxRadioGroup.checkedChanges(mUserTypeView).subscribe(integer -> {
            getPresenter().changeForm(integer == R.id.private_user);
        });
        mPasswordConfirmView.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                registration();
                return true;
            }

            return false;
        });
        mAutosCountView.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                registration();
                return true;
            }
            return false;
        });
        mPrivacyView.setMovementMethod(LinkMovementMethod.getInstance());
        mPrivacyView.setText(Html.fromHtml(getString(R.string.fragment_registration_privacy_content)));
    }

    @Override
    public void onDestroyView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
        if (mUserTypeSubscribe != null && !mUserTypeSubscribe.isUnsubscribed()) {
            mUserTypeSubscribe.unsubscribe();
        }
        super.onDestroyView();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_registration;
    }

    @Override
    protected void injectDependencies() {
    }

    @NonNull
    @Override
    public RegistrationPresenter createPresenter() {
        return getComponent(AuthenticatorComponent.class).provideRegistrationPresenter();
    }

    @Override
    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new MaterialDialog.Builder(getActivity())
                    .content(R.string.dialog_progress_content)
                    .progress(true, 0).build();
        }
        mProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.hide();
        }
    }

    @Override
    public void showError(String errorMessage) {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_error_title)
                .content(errorMessage)
                .positiveText(android.R.string.ok)
                .build()
                .show();
    }

    @Override
    public boolean showError(Throwable throwable) {
        return ErrorUtils.handleError((AppCompatActivity) getActivity(), throwable);
    }

    @OnClick(R.id.registration)
    void registration() {
        getPresenter().register();
    }

    @Override
    public void showOrganizationSpecificViews() {
        mCompanyNameView.setVisibility(View.VISIBLE);
        mAutosCountView.setVisibility(View.VISIBLE);
        mCompanyNameView.requestFocus();
    }

    @Override
    public void hideOrganizationSpecificViews() {
        mCompanyNameView.setVisibility(View.GONE);
        mAutosCountView.setVisibility(View.GONE);
    }

    @Override
    public void showUserSpecificViews() {
        mPasswordView.setVisibility(View.VISIBLE);
        mPasswordConfirmView.setVisibility(View.VISIBLE);
        mFirstNameView.requestFocus();
    }

    @Override
    public void hideUserSpecificViews() {
        mPasswordView.setVisibility(View.GONE);
        mPasswordConfirmView.setVisibility(View.GONE);
    }

    @Override
    public String getName() {
        return mFirstNameView.getText().toString();
    }

    @Override
    public String getSurname() {
        return mSurnameView.getText().toString();
    }

    @Override
    public String getPostcode() {
        return mPostcodeView.getText().toString();
    }

    @Override
    public String getCity() {
        return mCityView.getText().toString();
    }

    @Override
    public String getAddress() {
        return mAddressView.getText().toString();
    }

    @Override
    public String getEmail() {
        return mEmailView.getText().toString();
    }

    @Override
    public String getPhone() {
        return mPhoneView.getText().toString();
    }

    @Override
    public String getPassword() {
        return mPasswordView.getText().toString();
    }

    @Override
    public String getConfirmPassword() {
        return mPasswordConfirmView.getText().toString();
    }

    @Override
    public String getCompanyName() {
        return mCompanyNameView.getText().toString();
    }

    @Override
    public String getAutosCounter() {
        return mAutosCountView.getText().toString();
    }

    @Override
    public void finish() {
        if (isAdded()) {
            getActivity().finish();
        }
    }

    @Override
    public void showOrganizationSuccessDialog() {
        if (isAdded()) {
            final DialogFragment dialog = DialogProvider.createInfoDialog(
                    getChildFragmentManager(),
                    null,
                    getString(R.string.dialog_organization_registration_success_content),
                    DIALOG_ORGANIZATION_REGISTRATION_SUCCESS_ID,
                    true,
                    false);
            dialog.show(getChildFragmentManager(), DIALOG_ORGANIZATION_REGISTRATION_SUCCESS_TAG);
        }
    }

    @Override
    public void onPositive(int dialogId) {
        switch (dialogId) {
            case DIALOG_ORGANIZATION_REGISTRATION_SUCCESS_ID:
                finish();
                break;
        }
    }

    @Override
    public void onNegative(int dialogId) {

    }
}
