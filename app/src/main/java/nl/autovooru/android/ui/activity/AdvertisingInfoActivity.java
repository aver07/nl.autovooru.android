package nl.autovooru.android.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import butterknife.BindView;
import nl.autovooru.android.R;

/**
 * Created by Antonenko Viacheslav on 08/12/15.
 */
public class AdvertisingInfoActivity extends BaseActivity {

    private static final String EXTRA_INFO_TEXT = "extra_info_text";
    private static final String EXTRA_TITLE = "extra_title";
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    public static void openActivity(Context context, String infoText, String title) {
        final Intent intent = new Intent(context, AdvertisingInfoActivity.class);
        intent.putExtra(EXTRA_INFO_TEXT, infoText);
        intent.putExtra(EXTRA_TITLE, title);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertising_info);
        initializeToolbar();

        final String infoText = getIntent().getStringExtra(EXTRA_INFO_TEXT);
        ((TextView) findViewById(R.id.info)).setText(infoText);
        final String title = getIntent().getStringExtra(EXTRA_TITLE);
        setTitle(title);

    }

    private void initializeToolbar() {
        setSupportActionBar(mToolbar);
        final ActionBar actionBar = getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}
