package nl.autovooru.android.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import butterknife.BindView;
import de.greenrobot.event.EventBus;
import nl.autovooru.android.R;
import nl.autovooru.android.bus.events.NavigationEvent;
import nl.autovooru.android.bus.events.NavigationRequestEvent;
import nl.autovooru.android.di.HasComponent;
import nl.autovooru.android.di.components.DaggerUseCaseComponent;
import nl.autovooru.android.di.components.UseCaseComponent;
import nl.autovooru.android.di.modules.UseCaseModule;
import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.push.RegistrationService;
import nl.autovooru.android.ui.fragments.AboutFragment;
import nl.autovooru.android.ui.fragments.HomeFragment;
import nl.autovooru.android.ui.fragments.SearchMachineListFragmentBuilder;
import nl.autovooru.android.ui.fragments.UserDashboard;
import nl.autovooru.android.ui.fragments.UserDashboardBuilder;
import nl.autovooru.android.ui.fragments.UserSettingsFragment;

public class MainActivity extends BaseActivity implements HasComponent<UseCaseComponent> {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String EXTRA_NAV_ID = "extra_nav_id";

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    private UseCaseComponent mUseCaseComponent;
    private int navigationRequest = -1;

    public static Intent createIntent(Context context, @IdRes int navigationId) {
        final Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(EXTRA_NAV_ID, navigationId);
        return intent;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        navigationRequest = intent.getIntExtra(EXTRA_NAV_ID, -1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationService.class);
            startService(intent);
        }
        if (savedInstanceState == null) {
            navigationRequest = getIntent().getIntExtra(EXTRA_NAV_ID, -1);
        } else {
            navigationRequest = -1;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        if (navigationRequest != -1) {
            EventBus.getDefault().post(new NavigationRequestEvent(navigationRequest));
            navigationRequest = -1;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            final Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.activity_main_content_frame);
            if (fragment == null || fragment.getClass().equals(HomeFragment.class)) {
                super.onBackPressed();
            } else {
                EventBus.getDefault().post(new NavigationRequestEvent(R.id.nav_home));
            }
        }
    }

    public void onEvent(final NavigationEvent event) {
        final Fragment fragment;
        switch (event.getNavigationResId()) {
            case R.id.nav_posts:
                fragment = UserDashboardBuilder.newUserDashboard(UserDashboard.DashboardMenuItems.messages);
                break;
            case R.id.nav_my_advertise:
                fragment = UserDashboardBuilder.newUserDashboard(UserDashboard.DashboardMenuItems.my_ads);
                break;
            case R.id.nav_my_bids:
                fragment = UserDashboardBuilder.newUserDashboard(UserDashboard.DashboardMenuItems.my_bids);
                break;
            case R.id.nav_favourites:
                fragment = UserDashboardBuilder.newUserDashboard(UserDashboard.DashboardMenuItems.favorites);
                break;
            case R.id.nav_my_account:
                fragment = UserDashboardBuilder.newUserDashboard(UserDashboard.DashboardMenuItems.my_account);
                break;
            case R.id.nav_my_search:
                fragment = new Fragment();
                break;
            case R.id.nav_cars:
                fragment = createSearchMachineListFragment(new Filter("17", ""), event.getKeywords());
                break;
            case R.id.nav_oldtimers:
                fragment = createSearchMachineListFragment(new Filter("18", ""), event.getKeywords());
                break;
            case R.id.nav_commercial_vehicles:
                fragment = createSearchMachineListFragment(new Filter("19", ""), event.getKeywords());
                break;
            case R.id.nav_trucks:
                fragment = createSearchMachineListFragment(new Filter("20", ""), event.getKeywords());
                break;
            case R.id.nav_engines:
                fragment = createSearchMachineListFragment(new Filter("21", ""), event.getKeywords());
                break;
            case R.id.nav_damaged_cars:
                fragment = createSearchMachineListFragment(new Filter("22", ""), event.getKeywords());
                break;
            case R.id.nav_microcars:
                fragment = createSearchMachineListFragment(new Filter("23", ""), event.getKeywords());
                break;
            case R.id.nav_caravans:
                fragment = createSearchMachineListFragment(new Filter("24", ""), event.getKeywords());
                break;
            case R.id.nav_home:
                fragment = HomeFragment.newInstance();
                break;
            case R.id.nav_settings:
                fragment = UserSettingsFragment.newInstance();
                break;
            case R.id.nav_about:
                fragment = AboutFragment.newInstance();
                break;
            default:
                throw new IllegalStateException("Wrong navigation id");
        }
        replaceFragment(R.id.activity_main_content_frame, fragment);
        hideDrawerLayout();
    }

    private Fragment createSearchMachineListFragment(@NonNull Filter filter, @Nullable String keywords) {
        final SearchMachineListFragmentBuilder b = new SearchMachineListFragmentBuilder(filter);
        if (!TextUtils.isEmpty(keywords)) {
            b.defaultKeywords(keywords);
        }
        return b.build();
    }

    private void hideDrawerLayout() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public UseCaseComponent getComponent() {
        if (mUseCaseComponent == null) {
            mUseCaseComponent = DaggerUseCaseComponent.builder()
                    .applicationComponent(getApplicationComponent())
                    .activityModule(getActivityModule())
                    .useCaseModule(new UseCaseModule())
                    .build();
        }
        return mUseCaseComponent;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                LOG.d("This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}
