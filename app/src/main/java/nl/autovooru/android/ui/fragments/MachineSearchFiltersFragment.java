package nl.autovooru.android.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.hannesdorfmann.fragmentargs.bundler.ParcelerArgsBundler;
import com.jakewharton.rxbinding.view.RxView;
import com.jakewharton.rxbinding.widget.RxTextView;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;
import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.UiUtils;
import nl.autovooru.android.di.components.MachineSearchFiltersComponent;
import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.model.machine.MachineFilter;
import nl.autovooru.android.domain.model.machine.MachineSearchFilters;
import nl.autovooru.android.domain.model.query.MachineSearchQuery;
import nl.autovooru.android.exceptions.ErrorUtils;
import nl.autovooru.android.mvp.presenter.MachineSearchFiltersPresenter;
import nl.autovooru.android.mvp.view.MachineSearchFiltersView;
import nl.autovooru.android.ui.activity.BaseActivity;
import nl.autovooru.android.ui.widget.SearchFilterView;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Antonenko Viacheslav on 10/12/15.
 */
@FragmentWithArgs
public class MachineSearchFiltersFragment extends BaseLceFragment<SwipeRefreshLayout, MachineSearchFilters, MachineSearchFiltersView, MachineSearchFiltersPresenter>
        implements MachineSearchFiltersView, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    Unbinder mUnbinder;
    @BindView(R.id.category)
    TextView mCategoryView;
    @BindView(R.id.mark)
    TextView mMarkView;
    @BindView(R.id.model)
    TextView mModel;
    @BindView(R.id.start_year)
    TextView mStartYearView;
    @BindView(R.id.end_year)
    TextView mEndYearView;
    @BindView(R.id.start_price)
    TextView mStartPriceView;
    @BindView(R.id.end_price)
    TextView mEndPriceView;
    @BindView(R.id.fuel)
    TextView mFuelView;
    @BindView(R.id.userType)
    TextView mDillerView;
    @BindView(R.id.keywords)
    EditText mKeywordsView;
    @BindView(R.id.postCode)
    EditText mPostCodeView;
    @BindView(R.id.distance)
    TextView mDistanceView;
    @BindView(R.id.mileage)
    TextView mMileageView;
    @BindView(R.id.offer_time)
    TextView mOfferTimeView;
    @BindView(R.id.transmission)
    TextView mTransmissionsView;
    @BindView(R.id.color)
    TextView mColorView;
    @BindView(R.id.body)
    TextView mBodyView;
    @BindView(R.id.other_filters_wrapper)
    LinearLayout mOtherFiltersWrapper;
    @BindView(R.id.show_hide_other_filter)
    TextView mOtherFilterLabelView;
    @Arg(bundler = ParcelerArgsBundler.class)
    MachineSearchQuery mDefaultQuery;

    private MaterialDialog mProgressDialog;
    private CompositeSubscription mSubscriptions;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_machine_search_filters;
    }

    @NonNull
    @Override
    public MachineSearchFiltersPresenter createPresenter() {
        return getComponent(MachineSearchFiltersComponent.class).provideMachineSearchFiltersPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        FragmentArgs.inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        contentView.setOnRefreshListener(this);

        initializeToolbar();
        mProgressDialog = new MaterialDialog.Builder(getActivity())
                .content(R.string.dialog_progress_content)
                .progress(true, 0).build();

        mSubscriptions = new CompositeSubscription();
        mSubscriptions.add(RxTextView.afterTextChangeEvents(mKeywordsView).subscribe(event -> {
            getPresenter().setKeywords(event.editable().toString());
        }));
        mSubscriptions.add(RxTextView.afterTextChangeEvents(mPostCodeView).subscribe(event -> {
            getPresenter().setPostCode(event.editable().toString());
        }));
        mSubscriptions.add(RxView.focusChanges(mKeywordsView).subscribe(hasFocus ->
                        UiUtils.hideSoftKeyboard(mKeywordsView.getContext(), mKeywordsView.getWindowToken())
        ));
        mSubscriptions.add(RxView.focusChanges(mPostCodeView).subscribe(hasFocus ->
                        UiUtils.hideSoftKeyboard(mPostCodeView.getContext(), mPostCodeView.getWindowToken())
        ));


        mOtherFilterLabelView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
        mOtherFiltersWrapper.setVisibility(View.GONE);

        getPresenter().initialLoad(mDefaultQuery, false);
    }

    @Override
    public void onDestroyView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        if (!mSubscriptions.isUnsubscribed()) {
            mSubscriptions.unsubscribe();
        }

        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_machine_search_filters, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear:
                getPresenter().resetFilters();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return ErrorUtils.getErrorMessage(getActivity(), e);
    }

    @Override
    public void showContent() {
        super.showContent();
        contentView.setRefreshing(false);
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        contentView.setRefreshing(false);
    }

    @Override
    public void setData(MachineSearchFilters data) {

    }

    @Override
    public void loadData(boolean pullToRefresh) {
        getPresenter().initialLoad(mDefaultQuery, true);
    }

    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    public void showProgress() {
        mProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        mProgressDialog.hide();
    }

    @Override
    public void setField(TextView textView, @StringRes int label, @Nullable String value) {
        setField(textView, getString(label), value);
    }

    @Override
    public void setField(TextView textView, String label, @Nullable String value) {
        final TextAppearanceSpan labelSpan = new TextAppearanceSpan(getActivity(), R.style.MachineSearchFilter_Label);
        final SpannableStringBuilder sb = new SpannableStringBuilder(label);
        sb.setSpan(labelSpan, 0, label.length(), 0);
        sb.append("\n");
        int start = sb.length();
        final TextAppearanceSpan valueSpan = new TextAppearanceSpan(getActivity(), R.style.MachineSearchFilter_Value);
        sb.append(value);
        sb.setSpan(valueSpan, start, sb.length(), 0);
        textView.setText(sb);
    }

    @Override
    public void setMachineCategory(Filter category) {
        setField(mCategoryView, R.string.fragment_machine_search_filters_category, category.getName());
    }

    @OnClick(R.id.category)
    void showChooseCategoryDialog() {
        final List<Filter> machineCategories = getPresenter().getMachineCategories();
        final List<CharSequence> listItems = new ArrayList<>(machineCategories.size());
        for (Filter filter : machineCategories) {
            listItems.add(filter.getName());
        }
        final int selectedPosition = machineCategories.indexOf(getPresenter().getMachineCategory());
        new MaterialDialog.Builder(getActivity())
                .title(R.string.fragment_machine_search_filters_category)
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final Filter machineCategory = machineCategories.get(position);
                        getPresenter().setMachineCategory(machineCategory);
                    }
                    return true;
                })
                .build()
                .show();
    }

    @Override
    public void setMark(String label, @Nullable String mark) {
        setField(mMarkView, label, mark);
    }

    @OnClick(R.id.mark)
    void showChooseMarkDialog() {
        final SearchFilterView searchFilterView = new SearchFilterView(getActivity());

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(getPresenter().getMarkLabel())
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .customView(searchFilterView, false)
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .build();

        searchFilterView.setOptionsList(getPresenter().getMachineMarks());
        searchFilterView.setTopOptionsList(getPresenter().getTopMarks());
        searchFilterView.setHint(getPresenter().getMarkLabel());
        searchFilterView.setSelectedItem(getPresenter().getMark());
        searchFilterView.setOnItemClickListener(item -> {
            dialog.dismiss();
            if (!item.equals(getPresenter().getMark())) {
                getPresenter().setMark(item);
            }
        });
        dialog.show();
    }

    @Override
    public void setModel(String label, String model) {
        setField(mModel, label, model);
    }

    @OnClick(R.id.model)
    void showChooseModelDialog() {
        final SearchFilterView searchFilterView = new SearchFilterView(getActivity());

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(getPresenter().getModelsLabel())
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .customView(searchFilterView, false)
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .build();

        searchFilterView.setOptionsList(getPresenter().getModels());
        searchFilterView.setHint(getPresenter().getModelsLabel());
        searchFilterView.setSelectedItem(getPresenter().getModel());
        searchFilterView.setOnItemClickListener(item -> {
            dialog.dismiss();
            if (!item.equals(getPresenter().getModel())) {
                getPresenter().setModel(item);
            }
        });
        dialog.show();
    }

    @Override
    public void setStartYear(String label, String year) {
        setField(mStartYearView, label, year);
    }

    @Override
    public void setEndYear(String label, String year) {
        setField(mEndYearView, label, year);
    }

    @OnClick(R.id.start_year)
    void showChooseStartYearDialog() {
        final List<MachineFilter.Options> years = getPresenter().getStartYears();
        final List<CharSequence> listItems = new ArrayList<>(years.size());
        for (MachineFilter.Options year : years) {
            listItems.add(year.getName());
        }

        final int selectedPosition = years.indexOf(getPresenter().getStartYear());
        new MaterialDialog.Builder(getActivity())
                .title(getPresenter().getMachineYearLabel())
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options year = years.get(position);
                        getPresenter().setStartYear(year);
                    }
                    return true;
                })
                .build()
                .show();

    }

    @OnClick(R.id.end_year)
    void showChooseEndYearDialog() {
        final List<MachineFilter.Options> years = getPresenter().getEndYears();
        final List<CharSequence> listItems = new ArrayList<>(years.size());
        for (MachineFilter.Options year : years) {
            listItems.add(year.getName());
        }

        final int selectedPosition = years.indexOf(getPresenter().getEndYear());
        new MaterialDialog.Builder(getActivity())
                .title(getPresenter().getMachineYearLabel())
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options year = years.get(position);
                        getPresenter().setEndYear(year);
                    }
                    return true;
                })
                .build()
                .show();

    }

    @Override
    public void setStartPrice(String label, String price) {
        setField(mStartPriceView, label, price);
    }

    @Override
    public void setEndPrice(String label, String price) {
        setField(mEndPriceView, label, price);
    }

    @OnClick(R.id.start_price)
    void showChooseStartPriceDialog() {
        final List<MachineFilter.Options> prices = getPresenter().getStartPrices();
        final List<CharSequence> listItems = new ArrayList<>(prices.size());
        for (MachineFilter.Options price : prices) {
            listItems.add(price.getName());
        }

        final int selectedPosition = prices.indexOf(getPresenter().getStartPrice());
        new MaterialDialog.Builder(getActivity())
                .title(getPresenter().gerPriceLabel())
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options price = prices.get(position);
                        getPresenter().setStartPrice(price);
                    }
                    return true;
                })
                .build()
                .show();

    }

    @OnClick(R.id.end_price)
    void showChooseEndPriceDialog() {
        final List<MachineFilter.Options> prices = getPresenter().getEndPrices();
        final List<CharSequence> listItems = new ArrayList<>(prices.size());
        for (MachineFilter.Options price : prices) {
            listItems.add(price.getName());
        }

        final int selectedPosition = prices.indexOf(getPresenter().getEndPrice());
        new MaterialDialog.Builder(getActivity())
                .title(getPresenter().gerPriceLabel())
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options price = prices.get(position);
                        getPresenter().setEndPrice(price);
                    }
                    return true;
                })
                .build()
                .show();

    }

    @Override
    public void setFuel(String label, String fuel) {
        setField(mFuelView, label, fuel);
    }

    @OnClick(R.id.fuel)
    void showChooseFuelDialog() {
        final List<MachineFilter.Options> fuels = getPresenter().getFuels();
        final List<CharSequence> listItems = new ArrayList<>(fuels.size());
        for (MachineFilter.Options fuel : fuels) {
            listItems.add(fuel.getName());
        }

        final int selectedPosition = fuels.indexOf(getPresenter().getFuel());
        new MaterialDialog.Builder(getActivity())
                .title(getPresenter().getFuelLabel())
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options fuel = fuels.get(position);
                        getPresenter().setFuel(fuel);
                    }
                    return true;
                })
                .build()
                .show();

    }

    @Override
    public void setDiller(String label, String diller) {
        setField(mDillerView, label, diller);
    }

    @OnClick(R.id.userType)
    void showChooseDillerDialog() {
        final List<MachineFilter.Options> userTypes = getPresenter().getUserTypes();
        final List<CharSequence> listItems = new ArrayList<>(userTypes.size());
        for (MachineFilter.Options diller : userTypes) {
            listItems.add(diller.getName());
        }

        final int selectedPosition = userTypes.indexOf(getPresenter().getUserType());
        new MaterialDialog.Builder(getActivity())
                .title(getPresenter().getUserTypeLabel())
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options diller = userTypes.get(position);
                        getPresenter().setUserType(diller);
                    }
                    return true;
                })
                .build()
                .show();

    }

    @Override
    public void setPostCode(String label, String postCode) {
        ((TextInputLayout) mPostCodeView.getParent()).setHint(label);
        mPostCodeView.setText(postCode);
    }

    @Override
    public void setDistance(String label, String distance) {
        setField(mDistanceView, label, distance);
    }

    @OnClick(R.id.distance)
    void showChooseDistanceDialog() {
        final List<MachineFilter.Options> distanceList = getPresenter().getDistanceList();
        final List<CharSequence> listItems = new ArrayList<>(distanceList.size());
        for (MachineFilter.Options distance : distanceList) {
            listItems.add(distance.getName());
        }

        final int selectedPosition = distanceList.indexOf(getPresenter().getDistance());
        new MaterialDialog.Builder(getActivity())
                .title(getPresenter().getDistanceLabel())
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options distance = distanceList.get(position);
                        getPresenter().setDistance(distance);
                    }
                    return true;
                })
                .build()
                .show();
    }

    @Override
    public void setKeyword(String label, String keyword) {
        ((TextInputLayout) mKeywordsView.getParent()).setHint(label);
        mKeywordsView.setText(keyword);
    }

    @Override
    public void setMileage(String label, String mileage) {
        setField(mMileageView, label, mileage);
    }

    @OnClick(R.id.mileage)
    void showChooseMileageDialog() {
        final List<MachineFilter.Options> mileageList = getPresenter().getMileageList();
        final List<CharSequence> listItems = new ArrayList<>(mileageList.size());
        for (MachineFilter.Options mileage : mileageList) {
            listItems.add(mileage.getName());
        }

        final int selectedPosition = mileageList.indexOf(getPresenter().getMileage());
        new MaterialDialog.Builder(getActivity())
                .title(getPresenter().getMileageLabel())
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options mileage = mileageList.get(position);
                        getPresenter().setMileage(mileage);
                    }
                    return true;
                })
                .build()
                .show();
    }

    @Override
    public void showOtherFilters() {
        mOtherFilterLabelView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_up, 0);
        mOtherFiltersWrapper.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideOtherFilters() {
        mOtherFilterLabelView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
        mOtherFiltersWrapper.setVisibility(View.GONE);
    }

    @OnClick(R.id.show_hide_other_filter)
    @Override
    public void toggleOtherFilters() {
        if (mOtherFiltersWrapper.getVisibility() == View.GONE) {
            showOtherFilters();
        } else {
            hideOtherFilters();
        }
    }

    @Override
    public void setOfferTime(String label, String offerTime) {
        setField(mOfferTimeView, label, offerTime);
    }

    @OnClick(R.id.offer_time)
    void showChooseOfferTimeDialog() {
        final List<MachineFilter.Options> offerTimeList = getPresenter().getOfferTimeList();
        final List<CharSequence> listItems = new ArrayList<>(offerTimeList.size());
        for (MachineFilter.Options offerTime : offerTimeList) {
            listItems.add(offerTime.getName());
        }

        final int selectedPosition = offerTimeList.indexOf(getPresenter().getOfferTime());
        new MaterialDialog.Builder(getActivity())
                .title(getPresenter().getOfferTimeLabel())
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options offerTime = offerTimeList.get(position);
                        getPresenter().setOfferTime(offerTime);
                    }
                    return true;
                })
                .build()
                .show();
    }

    @Override
    public void setTransmissions(String label, String transmission) {
        setField(mTransmissionsView, label, transmission);
    }

    @OnClick(R.id.transmission)
    void showChooseTransmissionDialog() {
        final List<MachineFilter.Options> offerTimetransmissionListist = getPresenter().getTransmissionList();
        final List<CharSequence> listItems = new ArrayList<>(offerTimetransmissionListist.size());
        for (MachineFilter.Options transmission : offerTimetransmissionListist) {
            listItems.add(transmission.getName());
        }

        final int selectedPosition = offerTimetransmissionListist.indexOf(getPresenter().getTransmission());
        new MaterialDialog.Builder(getActivity())
                .title(getPresenter().getTransmissionLabel())
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options transmission = offerTimetransmissionListist.get(position);
                        getPresenter().setTransmission(transmission);
                    }
                    return true;
                })
                .build()
                .show();
    }

    @Override
    public void setColors(String label, String color) {
        setField(mColorView, label, color);
    }

    @OnClick(R.id.color)
    void showChooseColorDialog() {
        final List<MachineFilter.Options> colors = getPresenter().getColors();
        final List<CharSequence> listItems = new ArrayList<>(colors.size());
        for (MachineFilter.Options color : colors) {
            listItems.add(color.getName());
        }

        final int selectedPosition = colors.indexOf(getPresenter().getColor());
        new MaterialDialog.Builder(getActivity())
                .title(getPresenter().getColorLabel())
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options color = colors.get(position);
                        getPresenter().setColor(color);
                    }
                    return true;
                })
                .build()
                .show();
    }

    @Override
    public void setBody(String label, String body) {
        setField(mBodyView, label, body);
    }

    @OnClick(R.id.body)
    void showChooseBodyDialog() {
        final List<MachineFilter.Options> bodyList = getPresenter().getBodyList();
        final List<CharSequence> listItems = new ArrayList<>(bodyList.size());
        for (MachineFilter.Options body : bodyList) {
            listItems.add(body.getName());
        }

        final int selectedPosition = bodyList.indexOf(getPresenter().getBody());
        new MaterialDialog.Builder(getActivity())
                .title(getPresenter().getBodyLabel())
                .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options body = bodyList.get(position);
                        getPresenter().setBody(body);
                    }
                    return true;
                })
                .build()
                .show();
    }

    @Override
    public void setOtherFilter(final String filterKey, String label, List<MachineFilter.Options> values) {
        TextView filterView = (TextView) mOtherFiltersWrapper.findViewWithTag(filterKey);
        if (filterView == null) {
            final LayoutInflater inflater = getActivity().getLayoutInflater();
            filterView = (TextView) inflater.inflate(R.layout.include_search_field, mOtherFiltersWrapper, false);
            filterView.setTag(filterKey);
            filterView.setOnClickListener(v -> {
                final List<MachineFilter.Options> allOtherFilters = getPresenter().getAllOtherFilters(filterKey);
                final List<CharSequence> listItems = new ArrayList<>(allOtherFilters.size());
                for (MachineFilter.Options option : allOtherFilters) {
                    listItems.add(option.getName());
                }

                final Integer[] selectedPosition = getPresenter().getOtherFiltersSelectedPositions(filterKey);
                new MaterialDialog.Builder(getActivity())
                        .title(getPresenter().getOtherFiltersLabel(filterKey))
                        .backgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_100))
                        .items(listItems.toArray(new CharSequence[listItems.size()]))
                        .itemsCallbackMultiChoice(selectedPosition, (materialDialog, integers, charSequences) -> {
                            getPresenter().setOtherFilter(filterKey, integers);
                            return true;
                        })
                        .positiveText(android.R.string.ok)
                        .build()
                        .show();
            });
            mOtherFiltersWrapper.addView(filterView);
        }

        setField(filterView, label, TextUtils.join(", ", values));
    }


    @Override
    public void removeOtherFilter(String filterKey) {
        final View view = mOtherFiltersWrapper.findViewWithTag(filterKey);
        mOtherFiltersWrapper.removeView(view);
    }

    @OnClick(R.id.apply)
    void apply() {
        final MachineSearchQuery query = getPresenter().getFilters();
        final Intent data = new Intent();
        data.putExtra("filters", Parcels.wrap(query));
        getActivity().setResult(Activity.RESULT_OK, data);
        getActivity().finish();
    }

    private void initializeToolbar() {
        final BaseActivity activity = (BaseActivity) getActivity();
        activity.setSupportActionBar(mToolbar);
        final ActionBar actionBar = activity.getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.fragment_search_machine_list_title);
    }
}
