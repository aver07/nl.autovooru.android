package nl.autovooru.android.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import nl.autovooru.android.ui.fragments.ImageFragmentBuilder;

/**
 * Created by Antonenko Viacheslav on 19/12/15.
 */
public class ImageAdapter extends FragmentPagerAdapter {

    public List<String> mImages;

    public ImageAdapter(FragmentManager fm, @NonNull List<String> images) {
        super(fm);
        mImages = images;
    }

    @Override
    public Fragment getItem(int position) {
        return ImageFragmentBuilder.newImageFragment(mImages.get(position));
    }

    @Override
    public int getCount() {
        return mImages.size();
    }
}
