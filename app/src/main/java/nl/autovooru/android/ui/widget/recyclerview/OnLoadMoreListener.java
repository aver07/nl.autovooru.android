package nl.autovooru.android.ui.widget.recyclerview;

/**
 * Created by Antonenko Viacheslav on 02/12/15.
 */
public interface OnLoadMoreListener {

    void onLoadMore();
}
