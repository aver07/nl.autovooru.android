package nl.autovooru.android.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewAfterTextChangeEvent;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;
import nl.autovooru.android.R;
import nl.autovooru.android.di.components.AuthenticatorComponent;
import nl.autovooru.android.domain.interactor.DefaultSubscriber;
import nl.autovooru.android.exceptions.ErrorUtils;
import nl.autovooru.android.mvp.presenter.LoginPresenter;
import nl.autovooru.android.mvp.view.LoginView;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
public class LoginFragment extends BaseMvpFragment<LoginView, LoginPresenter> implements LoginView {

    @BindView(R.id.email)
    EditText mEmailView;
    @BindView(R.id.password)
    EditText mPasswordView;
    @BindView(R.id.login)
    Button mLoginView;
    Unbinder mUnbinder;
    private MaterialDialog mProgressDialog;
    private CompositeSubscription mCompositeSubscription;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mCompositeSubscription = new CompositeSubscription();
        final Observable<TextViewAfterTextChangeEvent> emailObservable = RxTextView.afterTextChangeEvents(mEmailView).skip(1);
        final Observable<TextViewAfterTextChangeEvent> passwordObservable = RxTextView.afterTextChangeEvents(mPasswordView).skip(1);
        mCompositeSubscription.add(Observable.combineLatest(emailObservable, passwordObservable, (email, password) -> {
            if (TextUtils.isEmpty(password.editable().toString()) || TextUtils.isEmpty(email.editable().toString())) {
                mLoginView.setEnabled(false);
            } else {
                mLoginView.setEnabled(true);
            }
            return null;
        }).subscribe(new DefaultSubscriber<>()));
        mLoginView.setEnabled(false);
    }

    @Override
    public void onDestroyView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
        mCompositeSubscription.unsubscribe();
        super.onDestroyView();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_login;
    }

    @NonNull
    @Override
    public LoginPresenter createPresenter() {
        return getComponent(AuthenticatorComponent.class).provideLoginPresenter();
    }

    @Override
    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new MaterialDialog.Builder(getActivity())
                    .content(R.string.dialog_progress_content)
                    .progress(true, 0).build();
        }
        mProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        mProgressDialog.hide();
    }

    @Override
    public void showError(String errorMessage) {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_error_title)
                .content(errorMessage)
                .positiveText(android.R.string.ok)
                .build()
                .show();
    }

    @Override
    public boolean showError(Throwable throwable) {
        return getActivity() != null && ErrorUtils.handleError((AppCompatActivity) getActivity(), throwable);
    }

    @OnClick(R.id.login)
    void login() {
        final String login = mEmailView.getText().toString();
        final String password = mPasswordView.getText().toString();
        getPresenter().authorize(login, password);
    }

    @OnClick(R.id.forgot_password)
    void forgotPassword() {
        final String predefinedEmail = mEmailView.getText().toString();
        new MaterialDialog.Builder(getActivity())
                .title(R.string.fragment_login_screen_forgot_password_btn)
                .inputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
                .input(getString(R.string.fragment_login_screen_forgot_password_email_label), predefinedEmail, false, (dialog, input) -> {

                })
                .contentColorRes(R.color.black)
                .positiveText(android.R.string.ok)
                .onPositive((dialog1, which) -> {
                    final String email = dialog1.getInputEditText().getText().toString();
                    if (!TextUtils.isEmpty(email) && email.trim().length() > 0) {
                        getPresenter().forgotPassword(email.trim());
                    }
                })
                .build()
                .show();
    }

    @Override
    public void finish() {
        getActivity().finish();
    }
}
