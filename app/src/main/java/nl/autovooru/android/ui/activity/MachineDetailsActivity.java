package nl.autovooru.android.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;

import nl.autovooru.android.R;
import nl.autovooru.android.di.HasComponent;
import nl.autovooru.android.di.components.DaggerMachineDetailsComponent;
import nl.autovooru.android.di.components.MachineDetailsComponent;
import nl.autovooru.android.di.modules.MachineDetailsModule;
import nl.autovooru.android.ui.fragments.MachineDetailsFragment;
import nl.autovooru.android.ui.fragments.MachineDetailsFragmentBuilder;

/**
 * Created by Antonenko Viacheslav on 18/12/15.
 */
public class MachineDetailsActivity extends BaseActivity implements HasComponent<MachineDetailsComponent> {

    private static final String EXTRA_MACHINE_ID = "extra_machine_id";

    public static void openActivity(Context context,
                                    @NonNull String machineId) {
        final Intent intent = new Intent(context, MachineDetailsActivity.class);
        intent.putExtra(EXTRA_MACHINE_ID, machineId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_machine_details);

        if (savedInstanceState == null) {
            final String machineId = getIntent().getStringExtra(EXTRA_MACHINE_ID);
            final MachineDetailsFragment f = MachineDetailsFragmentBuilder.newMachineDetailsFragment(machineId);
            final FragmentTransaction t = getSupportFragmentManager().beginTransaction();
            t.replace(R.id.fragment_container, f);
            t.commit();
        }
    }

    @Override
    public MachineDetailsComponent getComponent() {
        return DaggerMachineDetailsComponent.builder()
                .applicationComponent(getApplicationComponent())
                .machineDetailsModule(new MachineDetailsModule())
                .build();
    }
}
