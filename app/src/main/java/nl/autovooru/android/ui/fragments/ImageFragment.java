package nl.autovooru.android.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.SchedulingUtils;
import nl.autovooru.android.utils.CropTransformation;
import nl.autovooru.android.utils.CropTransformation.CropType;

/**
 * Created by Antonenko Viacheslav on 19/12/15.
 */
@FragmentWithArgs
public class ImageFragment extends Fragment {

    @Arg
    String mUrl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentArgs.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_image, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SchedulingUtils.doPreDraw(view, () -> {
            if (!view.getViewTreeObserver().isAlive()) {
                return;
            }

            final int width = view.getMeasuredWidth();
            final int height = view.getMeasuredHeight();

            if (width <= 0 || height <= 0) {
                return;
            }

            final CropTransformation cropTransformation = new CropTransformation(
                    width, height, CropType.CENTER_BOTTOM);
            Picasso.with(getActivity()).load(mUrl)
                    .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                    .transform(cropTransformation)
                    .into(((ImageView) view));
        });
    }
}
