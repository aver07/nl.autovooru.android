package nl.autovooru.android.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

import java.util.HashMap;
import java.util.Map;

import nl.autovooru.android.R;

/**
 * Created by Antonenko Viacheslav on 25/11/15.
 */
public class AspectRatioImageView extends ImageView {
    // NOTE: These must be kept in sync with the AspectRatioImageView attributes in attrs.xml.
    public static final int MEASUREMENT_WIDTH = 0;
    public static final int MEASUREMENT_HEIGHT = 1;

    private static final float DEFAULT_ASPECT_RATIO = 1f;
    private static final boolean DEFAULT_ASPECT_RATIO_ENABLED = false;
    private static final int DEFAULT_DOMINANT_MEASUREMENT = MEASUREMENT_WIDTH;

    private float aspectRatio;
    private boolean aspectRatioEnabled;
    private int dominantMeasurement;

    private Matrix matrix;
    private CropType cropType = CropType.NONE;

    public AspectRatioImageView(Context context) {
        this(context, null);
    }

    public AspectRatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        parseAttributes(attrs);
        initImageView();
    }

    private void parseAttributes(AttributeSet attrs) {
        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.AspectRatioImageView);
        aspectRatio = a.getFloat(R.styleable.AspectRatioImageView_aspectRatio, DEFAULT_ASPECT_RATIO);
        aspectRatioEnabled = a.getBoolean(R.styleable.AspectRatioImageView_aspectRatioEnabled,
                DEFAULT_ASPECT_RATIO_ENABLED);
        dominantMeasurement = a.getInt(R.styleable.AspectRatioImageView_dominantMeasurement,
                DEFAULT_DOMINANT_MEASUREMENT);

        final int crop = a.getInt(R.styleable.AspectRatioImageView_crop, CropType.NONE.getCrop());
        if (crop >= 0) {
            setScaleType(ScaleType.MATRIX);
            cropType = CropType.get(crop);
        }

        a.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (!aspectRatioEnabled) return;

        int newWidth;
        int newHeight;
        switch (dominantMeasurement) {
            case MEASUREMENT_WIDTH:
                newWidth = getMeasuredWidth();
//                newHeight = (int) (newWidth * aspectRatio);
                newHeight = (int) (newWidth / aspectRatio);
                break;

            case MEASUREMENT_HEIGHT:
                newHeight = getMeasuredHeight();
                newWidth = (int) (newHeight * aspectRatio);
                break;

            default:
                throw new IllegalStateException("Unknown measurement with ID " + dominantMeasurement);
        }

        setMeasuredDimension(newWidth, newHeight);
    }

    /**
     * Get the aspect ratio for this image view.
     */
    public float getAspectRatio() {
        return aspectRatio;
    }

    /**
     * Set the aspect ratio for this image view. This will update the view instantly.
     */
    public void setAspectRatio(float aspectRatio) {
        this.aspectRatio = aspectRatio;
        if (aspectRatioEnabled) {
            requestLayout();
        }
    }

    /**
     * Get whether or not forcing the aspect ratio is enabled.
     */
    public boolean getAspectRatioEnabled() {
        return aspectRatioEnabled;
    }

    /**
     * set whether or not forcing the aspect ratio is enabled. This will re-layout the view.
     */
    public void setAspectRatioEnabled(boolean aspectRatioEnabled) {
        this.aspectRatioEnabled = aspectRatioEnabled;
        requestLayout();
    }

    /**
     * Get the dominant measurement for the aspect ratio.
     */
    public int getDominantMeasurement() {
        return dominantMeasurement;
    }

    /**
     * Set the dominant measurement for the aspect ratio.
     *
     * @see #MEASUREMENT_WIDTH
     * @see #MEASUREMENT_HEIGHT
     */
    public void setDominantMeasurement(int dominantMeasurement) {
        if (dominantMeasurement != MEASUREMENT_HEIGHT && dominantMeasurement != MEASUREMENT_WIDTH) {
            throw new IllegalArgumentException("Invalid measurement type.");
        }
        this.dominantMeasurement = dominantMeasurement;
        requestLayout();
    }

    /**
     * Set crop type for the {@link ImageView}
     *
     * @param cropType A {@link CropType} desired scaling mode.
     */
    public void setCropType(CropType cropType) {
        if (cropType == null) {
            throw new NullPointerException();
        }

        if (this.cropType != cropType) {
            this.cropType = cropType;

            setWillNotCacheDrawing(false);

            requestLayout();
            invalidate();
        }
    }

    /**
     * Return the current crop type in use by this ImageView.
     *
     * @return a {@link CropType} in use by this ImageView
     */
    public CropType getCropType() {
        return cropType;
    }

    private void initImageView() {
        if (cropType != CropType.NONE) {
            matrix = new Matrix();
        }
    }

    private Matrix getCropImageMatrix() {
        final boolean preApi18 = Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2;
        if (preApi18) {
            return matrix == null ? getImageMatrix() : matrix;
        } else {
            return getImageMatrix();
        }
    }

    @Override
    protected boolean setFrame(int l, int t, int r, int b) {
        final boolean changed = super.setFrame(l, t, r, b);

        final Drawable drawable = getDrawable();
        if (!isInEditMode() && drawable != null) {
            computeImageMatrix(drawable);
        }
        return changed;
    }

    private void computeImageMatrix(Drawable drawable) {
        final int viewWidth = getWidth() - getPaddingLeft() - getPaddingRight();
        final int viewHeight = getHeight() - getPaddingTop() - getPaddingBottom();

        if (cropType != CropType.NONE && viewHeight > 0 && viewWidth > 0) {
            final Matrix matrix = getCropImageMatrix();

            final int drawableWidth = drawable.getIntrinsicWidth();
            final int drawableHeight = drawable.getIntrinsicHeight();

            final float scaleY = (float) viewHeight / (float) drawableHeight;
            final float scaleX = (float) viewWidth / (float) drawableWidth;
            final float scale = scaleX > scaleY ? scaleX : scaleY;
            matrix.setScale(scale, scale); // Same as doing matrix.reset() and matrix.preScale(...)

            final boolean verticalImageMode = scaleX > scaleY;

            final float postDrawableWidth = drawableWidth * scale;
            final float xTranslation = getXTranslation(cropType, viewWidth, postDrawableWidth, verticalImageMode);
            final float postDrawabeHeigth = drawableHeight * scale;
            final float yTranslation = getYTranslation(cropType, viewHeight, postDrawabeHeigth, verticalImageMode);

            matrix.postTranslate(xTranslation, yTranslation);
            setImageMatrix(matrix);
        }
    }

    private float getYTranslation(CropType cropType, int viewHeight, float postDrawabeHeigth, boolean verticalImageMode) {
        if (verticalImageMode) {
            switch (cropType) {
                case CENTER_BOTTOM:
                case LEFT_BOTTOM:
                case RIGHT_BOTTOM:
                    return viewHeight - postDrawabeHeigth;
                case LEFT_CENTER:
                case RIGHT_CENTER:
                    // View in the middle of the screen
                    return (viewHeight - postDrawabeHeigth) / 2f;
            }
        }

        // All other cases we don't need to translate
        return 0;
    }

    private float getXTranslation(CropType cropType, int viewWidth, float postDrawableWidth, boolean verticalImageMode) {
        if (!verticalImageMode) {
            switch (cropType) {
                case RIGHT_TOP:
                case RIGHT_CENTER:
                case RIGHT_BOTTOM:
                    return viewWidth - postDrawableWidth;
                case CENTER_TOP:
                case CENTER_BOTTOM:
                    // View in the middle of the screen
                    return (viewWidth - postDrawableWidth) / 2f;
            }
        }
        // All other cases we don't need to translate
        return 0;
    }

    /**
     * Options for cropping the bounds of an image to the bounds of this view.
     */
    public enum CropType {
        NONE(-1),
        LEFT_TOP(0),
        LEFT_CENTER(1),
        LEFT_BOTTOM(2),
        RIGHT_TOP(3),
        RIGHT_CENTER(4),
        RIGHT_BOTTOM(5),
        CENTER_TOP(6),
        CENTER_BOTTOM(7);

        final int cropType;

        // Reverse-lookup map for getting a day from an abbreviation
        private static final Map<Integer, CropType> codeLookup = new HashMap<>();

        static {
            for (CropType ft : CropType.values()) {
                codeLookup.put(ft.getCrop(), ft);
            }
        }

        CropType(int ct) {
            cropType = ct;
        }

        public int getCrop() {
            return cropType;
        }

        public static CropType get(int number) {
            return codeLookup.get(number);
        }
    }
}
