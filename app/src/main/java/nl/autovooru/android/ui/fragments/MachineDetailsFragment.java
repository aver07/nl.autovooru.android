package nl.autovooru.android.ui.fragments;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.TextAppearanceSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import icepick.State;
import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.StringUtils;
import nl.autovooru.android.di.components.MachineDetailsComponent;
import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.domain.model.machine.MachineFilter;
import nl.autovooru.android.domain.model.machine.details.Content;
import nl.autovooru.android.domain.model.machine.details.Filters;
import nl.autovooru.android.domain.model.machine.details.Info;
import nl.autovooru.android.domain.model.machine.details.MachineDetails;
import nl.autovooru.android.domain.model.machine.details.Profile;
import nl.autovooru.android.domain.model.machine.details.Statistic;
import nl.autovooru.android.exceptions.ErrorUtils;
import nl.autovooru.android.mvp.presenter.MachineDetailsPresenter;
import nl.autovooru.android.mvp.view.MachineDetailsView;
import nl.autovooru.android.ui.activity.BaseActivity;
import nl.autovooru.android.ui.adapter.ImageAdapter;
import nl.autovooru.android.utils.MachineUtils;

/**
 * Created by Antonenko Viacheslav on 18/12/15.
 */
@FragmentWithArgs
public class MachineDetailsFragment extends BaseLceFragment<NestedScrollView, MachineDetails,
        MachineDetailsView, MachineDetailsPresenter>
        implements MachineDetailsView, SwipeRefreshLayout.OnRefreshListener, AppBarLayout.OnOffsetChangedListener {

    @BindView(R.id.swipe_to_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    Unbinder mUnbinder;
    @BindView(R.id.fab)
    FloatingActionsMenu mFloatingActionsMenu;
    @BindView(R.id.content_foreground)
    View mForegroundView;
    @BindView(R.id.toolbar_foreground)
    View mToolbarForegroundView;
    @BindView(R.id.description)
    TextView mDescriptionView;
    @BindView(R.id.description_arrow)
    TextView mDescriptionArrowView;
    @BindView(R.id.options_arrow)
    TextView mOptionsArrowView;
    @BindView(R.id.options_wrapper)
    LinearLayout mOptionsWrapperView;
    @BindView(R.id.images)
    ViewPager mImagesViewPager;
    @BindView(R.id.similar_auto_wrapper)
    LinearLayoutCompat mSimilarAutoWrapper;
    @BindView(R.id.page)
    TextView mPageView;
    @BindView(R.id.appBarLayout)
    AppBarLayout mAppBarLayout;
    @NonNull
    @Arg
    String mMachineId;
    @Nullable
    @State
    Parcelable mMachineCategory;
    @State
    boolean mIsDescriptionOpen;
    @State
    boolean mIsOptionsOpen;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_machine_details;
    }

    @NonNull
    @Override
    public MachineDetailsPresenter createPresenter() {
        return getComponent(MachineDetailsComponent.class).provideMachineDetailsPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        FragmentArgs.inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        initializeToolbar();

        loadData(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        mAppBarLayout.addOnOffsetChangedListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mAppBarLayout.removeOnOffsetChangedListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mFloatingActionsMenu != null && mFloatingActionsMenu.isExpanded()) {
            mFloatingActionsMenu.collapseImmediately();
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_machine_details, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        final MenuItem favoriteMenuItem = menu.findItem(R.id.action_add_to_favorite);
        if (getPresenter().isLoggedIn() && getPresenter().isMachineDetailsLoaded()) {
            if (getPresenter().isFavorite()) {
                favoriteMenuItem.setIcon(R.drawable.ic_action_favorite_white);
            } else {
                favoriteMenuItem.setIcon(R.drawable.ic_action_favorite_outline_white);
            }
        } else {
            favoriteMenuItem.setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                getPresenter().shareMachine(getActivity());
                return true;
            case R.id.action_add_to_favorite:
                getPresenter().toggleFavorite(mMachineId);
                getActivity().supportInvalidateOptionsMenu();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return ErrorUtils.getErrorMessage(getActivity(), e);
    }

    @Override
    public void setData(MachineDetails data) {
        getActivity().supportInvalidateOptionsMenu();

        final Content content = data.getContent();
        if (content != null) {
            TextView nameView = ButterKnife.findById(getView(), R.id.name);
            if (StringUtils.isEmpty(content.getName())) {
                nameView.setVisibility(View.GONE);
            } else {
                nameView.setVisibility(View.VISIBLE);
                nameView.setText(content.getName());
            }

            final SpannableStringBuilder spb = new SpannableStringBuilder();
            final String userDescription = content.getUserDescription();
            if (!TextUtils.isEmpty(userDescription)) {
                spb.append(Html.fromHtml(userDescription));
            }
            final String generatedDescription = content.getGeneratedDescription();
            if (!TextUtils.isEmpty(generatedDescription)) {
                if (spb.length() > 0) {
                    spb.append("\n\n");
                }
                spb.append(generatedDescription);
            }
            mDescriptionView.setText(spb);
            updateDescription();
        }

        final Info info = data.getInfo();
        if (info != null) {
            setField(R.id.release_year, R.string.fragment_details_release_year, info.getReleaseYear());
            setField(R.id.mileage, R.string.fragment_details_mileage, getString(R.string.mileage, info.getMileage()));
            setField(R.id.fuel, R.string.fragment_details_fuel, info.getFuel());

            final String price;
            boolean emptyPrice = false;
            if (StringUtils.isEmpty(info.getPrice()) || TextUtils.equals(info.getPrice(), "0")
                    || TextUtils.equals(info.getPrice(), "0.00")) {
                price = getString(R.string.empty_price);
                emptyPrice = true;
            } else {
                price = getString(R.string.euro, info.getPrice());
            }

            if (StringUtils.isEmpty(info.getSalePrice())) {
                setField(R.id.price, R.string.fragment_details_price, price);
            } else {
                final String promoPrice;
                if (TextUtils.equals(info.getSalePrice(), "0")) {
                    setField(R.id.price, R.string.fragment_details_price, price);
                    promoPrice = getString(R.string.empty_price);
                } else {
                    setPrice(R.id.price, R.string.fragment_details_price, price, !emptyPrice);
                    promoPrice = getString(R.string.euro, info.getSalePrice());
                }
                setPrice(R.id.promo_price, R.string.fragment_details_promo_price, promoPrice, false);
            }

            setField(R.id.mark, R.string.fragment_details_mark, info.getMark());
            setField(R.id.model, R.string.fragment_details_model, info.getModel());
            String engineSize = info.getEngineSize();
            if (!TextUtils.isEmpty(engineSize)) {
                engineSize += " cc";
            }
            setField(R.id.engine_capacity, R.string.fragment_details_engine_capacity, engineSize);
            setField(R.id.body, R.string.fragment_details_body, info.getBody());
            setField(R.id.transmission, R.string.fragment_details_transmission, info.getTransmission());

            String token = info.getToken();
            final TextView licenseNumberView = ButterKnife.findById(getView(), R.id.license_number);
            if (TextUtils.isEmpty(token)) {
                licenseNumberView.setVisibility(View.GONE);
                ((View) licenseNumberView.getParent()).setVisibility(View.GONE);
            } else {
                ((View) licenseNumberView.getParent()).setVisibility(View.VISIBLE);
                licenseNumberView.setVisibility(View.VISIBLE);
                token = token.toUpperCase().trim();
                licenseNumberView.setText(token);
            }
        } else {
            // hide all related views
            ((View) ButterKnife.findById(getView(), R.id.release_year).getParent()).setVisibility(View.GONE);
        }

        final Statistic statistic = data.getStatistic();
        if (statistic != null) {
            setField(R.id.hits, R.string.fragment_details_hits, statistic.getHits());
        }

        updateOptions();

        final Profile profile = data.getProfile();
        if (profile != null) {
            setField(R.id.company_name, R.string.fragment_details_company_name, profile.getOrganizationName());
            setField(R.id.company_address, R.string.fragment_details_company_address, profile.getAddress());
            setField(R.id.company_city, R.string.fragment_details_company_city, profile.getCity());
        }

        if (!data.getImagesList().isEmpty()) {
            mImagesViewPager.setAdapter(new ImageAdapter(getChildFragmentManager(), data.getImagesList()));
            mImagesViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    mPageView.setText(getString(R.string.image_label, (position + 1), data.getImagesList().size()));
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            mPageView.setText(getString(R.string.image_label, "1", data.getImagesList().size()));
        }

        final List<Machine> similarAutos = data.getSimilarAutos();
        mSimilarAutoWrapper.removeAllViews();
        if (similarAutos.isEmpty()) {
            ((View) mSimilarAutoWrapper.getParent().getParent()).setVisibility(View.GONE);
        } else {
            ((View) mSimilarAutoWrapper.getParent().getParent()).setVisibility(View.VISIBLE);
            for (Machine machine : similarAutos) {
                mSimilarAutoWrapper.addView(addSimilarAuto(machine));
            }
        }

        if (data.getFilters() != null) {
            ButterKnife.findById(getView(), R.id.open_dealer_cars).setVisibility(View.VISIBLE);
            final MachineFilter.Options category = data.getFilters().getCategory();
            if (category != null) {
                Filter filter = new Filter(category.getValue(), category.getName());
                mMachineCategory = Parcels.wrap(filter);
            }
        } else {
            ButterKnife.findById(getView(), R.id.open_dealer_cars).setVisibility(View.GONE);
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        getPresenter().load(mMachineId, pullToRefresh);
    }

    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    public void showContent() {
        super.showContent();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @OnClick(R.id.description_arrow)
    void expandOrCollapseDescription() {
        mIsDescriptionOpen = !mIsDescriptionOpen;
        updateDescription();
    }

    @OnClick(R.id.options_arrow)
    void expandOrCollapseOptions() {
        mIsOptionsOpen = !mIsOptionsOpen;
        updateOptions();
    }

    @OnClick(R.id.open_dealer_info)
    void openDealerInfo() {
        getPresenter().openDealerInfo(getActivity());
    }

    @OnClick(R.id.open_dealer_cars)
    void openDealersCars() {
        if (mMachineCategory != null) {
            getPresenter().openDealersCars(getActivity(), Parcels.unwrap(mMachineCategory));
        }
    }

    private void updateDescription() {
        if (mIsDescriptionOpen) {
            mDescriptionArrowView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
            mDescriptionView.setMaxLines(Integer.MAX_VALUE);
        } else {
            mDescriptionArrowView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_up, 0);
            mDescriptionView.setMaxLines(2);
        }
    }

    private void updateOptions() {
        mOptionsWrapperView.removeAllViews();
        final Filters filters = getPresenter().getFilters();
        if (filters != null && !filters.getOtherFilters().isEmpty()) {
            ((View) mOptionsWrapperView.getParent()).setVisibility(View.VISIBLE);
            final List<MachineFilter> otherFilters = filters.getOtherFilters();

            final Drawable checkIcon = ContextCompat.getDrawable(getActivity(), R.drawable.ic_option_check);
            checkIcon.setBounds(0, 0, checkIcon.getIntrinsicWidth(), checkIcon.getIntrinsicHeight());

            for (MachineFilter filter : otherFilters) {
                if (!filter.getFirstOptionsList().isEmpty()) {
                    final TextView optionsView = new TextView(getContext());
                    optionsView.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                    optionsView.setLineSpacing(0f, 1.2f);
                    final SpannableStringBuilder spb = new SpannableStringBuilder();
                    spb.append(filter.getName()).append(":");
                    if (mIsOptionsOpen) {
                        spb.append("\n");
                    } else {
                        spb.append("  ");
                        optionsView.setMaxLines(1);
                        optionsView.setEllipsize(TextUtils.TruncateAt.END);
                        optionsView.setHorizontallyScrolling(true);
                    }

                    for (MachineFilter.Options options : filter.getFirstOptionsList()) {
                        spb.append(" ");
                        spb.setSpan(new ImageSpan(checkIcon, ImageSpan.ALIGN_BASELINE), spb.length() - 1, spb.length(), 0);
                        spb.append(" ").append(options.getName());
                        if (mIsOptionsOpen) {
                            spb.append("\n");
                        } else {
                            spb.append("  ");
                        }
                    }
                    optionsView.setText(spb);
                    mOptionsWrapperView.addView(optionsView);
                }
            }
        } else {
            ((View) mOptionsWrapperView.getParent()).setVisibility(View.GONE);
        }
        if (mIsOptionsOpen) {
            mOptionsArrowView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
        } else {
            mOptionsArrowView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_up, 0);
        }
    }

    private View addSimilarAuto(final Machine machine) {
        final View similarAutoView = getActivity().getLayoutInflater().inflate(R.layout.view_similar_auto, mSimilarAutoWrapper, false);
        final ImageView imageView = ButterKnife.findById(similarAutoView, R.id.image);
        final TextView nameView = ButterKnife.findById(similarAutoView, R.id.name);
        final TextView priceView = ButterKnife.findById(similarAutoView, R.id.price);
        final TextView cityView = ButterKnife.findById(similarAutoView, R.id.city);

        Picasso.with(getActivity())
                .load(machine.getImages().get(0))
                .fit()
                .centerCrop()
                .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                .into(imageView);
        nameView.setText(machine.getName());
        final String price = MachineUtils.getPrice(getActivity(), machine);
        priceView.setText(price);

        cityView.setText(machine.getCity());
        similarAutoView.setOnClickListener(v -> getPresenter().openDetailsScreen(v.getContext(), machine));
        return similarAutoView;
    }

    private void initializeToolbar() {
        final BaseActivity activity = (BaseActivity) getActivity();
        activity.setSupportActionBar(mToolbar);
        final ActionBar actionBar = activity.getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
        mCollapsingToolbarLayout.setTitleEnabled(false);
        actionBar.setTitle(R.string.fragment_details_title);
    }

    @Override
    public void initializeFAB(boolean showWebSite, String phoneNumber, boolean showBid, boolean showSendMessage) {
        if (showWebSite) {
            addMenu(getString(R.string.fab_website), R.drawable.ic_fab_website, v -> getPresenter().openWebsite(getActivity()));
        }
        if (showBid) {
            addMenu(getString(R.string.fab_place_bid), R.drawable.ic_fab_place_bid, v -> {
                mFloatingActionsMenu.collapse();
                showPlaceBidDialog();
            });
        }
        if (showSendMessage) {
            addMenu(getString(R.string.fab_send_message), R.drawable.ic_fab_send_message, v -> {
                showSendMessageDialog();
                mFloatingActionsMenu.collapse();
            });
        }
        if (!TextUtils.isEmpty(phoneNumber)) {
            addMenu(phoneNumber, R.drawable.ic_fab_call, v -> getPresenter().call(getActivity()));
        }
        mFloatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                updateForeground();
            }

            @Override
            public void onMenuCollapsed() {
                updateForeground();
            }
        });
        updateForeground();
    }

    private void updateForeground() {
        if (mFloatingActionsMenu.isExpanded()) {
            mForegroundView.setClickable(true);
            mForegroundView.setBackgroundResource(R.color.black_transparency_30);
            mToolbarForegroundView.setClickable(true);
            mToolbarForegroundView.setBackgroundResource(R.color.black_transparency_30);
        } else {
            mForegroundView.setClickable(false);
            mForegroundView.setBackgroundColor(Color.TRANSPARENT);
            mToolbarForegroundView.setClickable(false);
            mToolbarForegroundView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private void addMenu(@NonNull String title, @DrawableRes int iconResId, View.OnClickListener onClickListener) {
        final FloatingActionButton menuItem = new FloatingActionButton(getActivity());
        menuItem.setSize(FloatingActionButton.SIZE_MINI);
        menuItem.setColorNormalResId(R.color.colorAccent);
        menuItem.setColorPressedResId(R.color.colorAccent);
        menuItem.setIcon(iconResId);
        menuItem.setTitle(title);
        menuItem.setOnClickListener(onClickListener);

        mFloatingActionsMenu.addButton(menuItem);
    }

    @Override
    public void showSendMessageDialog() {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_feedback_title)
                .customView(R.layout.dialog_send_message, true)
                .autoDismiss(false)
                .positiveText(R.string.dialog_feedback_positive_button)
                .onPositive((materialDialog, dialogAction) -> {
                    final View customView = materialDialog.getCustomView();

                    TextView subjectView = ButterKnife.findById(customView, R.id.subject);
                    TextView emailView = ButterKnife.findById(customView, R.id.email);
                    TextView messageView = ButterKnife.findById(customView, R.id.message);
                    TextView nameView = ButterKnife.findById(customView, R.id.name);
                    String errorMsg = subjectView.getResources().getString(R.string.dialog_feedback_error_message);

                    final String subject = subjectView.getText().toString();
                    if (StringUtils.isEmpty(subject)) {
                        ((TextInputLayout) subjectView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String email = emailView.getText().toString();
                    if (StringUtils.isEmpty(email)) {
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    if (!email.contains("@")) {
                        errorMsg = getString(R.string.error_wrong_email_format);
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String message = messageView.getText().toString();
                    if (StringUtils.isEmpty(message)) {
                        ((TextInputLayout) messageView.getParent()).setError(errorMsg);
                        return;
                    }

                    String name = nameView.getText().toString();
                    if (StringUtils.isEmpty(name)) {
                        ((TextInputLayout) nameView.getParent()).setError(errorMsg);
                        return;
                    }

                    getPresenter().sendMessage(
                            name,
                            email,
                            subject,
                            message);

                    materialDialog.dismiss();
                })
                .build();

        if (getPresenter().isLoggedIn()) {
            TextView emailView = ButterKnife.findById(dialog.getCustomView(), R.id.email);
            TextView nameView = ButterKnife.findById(dialog.getCustomView(), R.id.name);
            emailView.setText(getPresenter().getUserEmail());
            nameView.setText(getPresenter().getUsername());
        }
        dialog.show();
    }

    @Override
    public void showPlaceBidDialog() {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_place_bid_title)
                .customView(R.layout.dialog_place_bid, true)
                .autoDismiss(false)
                .positiveText(R.string.dialog_place_bid_positive_button)
                .onPositive((materialDialog, dialogAction) -> {
                    final View customView = materialDialog.getCustomView();

                    TextView nameView = ButterKnife.findById(customView, R.id.name);
                    TextView emailView = ButterKnife.findById(customView, R.id.email);
                    TextView bidView = ButterKnife.findById(customView, R.id.bid);
                    TextView messageView = ButterKnife.findById(customView, R.id.message);
                    String errorMsg = emailView.getResources().getString(R.string.dialog_place_bid_error_message);

                    final String email = emailView.getText().toString();
                    if (StringUtils.isEmpty(email)) {
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    if (!email.contains("@")) {
                        errorMsg = getString(R.string.error_wrong_email_format);
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String bid = bidView.getText().toString();
                    if (StringUtils.isEmpty(bid)) {
                        ((TextInputLayout) bidView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String message = messageView.getText().toString();

                    if (StringUtils.isEmpty(message)) {
                        ((TextInputLayout) messageView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String name = nameView.getText().toString();
                    if (StringUtils.isEmpty(name)) {
                        ((TextInputLayout) nameView.getParent()).setError(errorMsg);
                        return;
                    }

                    getPresenter().placeBid(
                            name,
                            email,
                            bid,
                            message);

                    materialDialog.dismiss();
                })
                .build();
        if (getPresenter().isLoggedIn()) {
            TextView emailView = ButterKnife.findById(dialog.getCustomView(), R.id.email);
            TextView nameView = ButterKnife.findById(dialog.getCustomView(), R.id.name);
            emailView.setText(getPresenter().getUserEmail());
            nameView.setText(getPresenter().getUsername());
        }
        dialog.show();
    }

    private void setField(@IdRes int labelId, @StringRes int label, @Nullable String value) {
        setField(ButterKnife.findById(getView(), labelId), getString(label), value);
    }

    private void setField(TextView textView, String label, @Nullable String value) {
        if (TextUtils.isEmpty(value)) {
            textView.setVisibility(View.GONE);
            return;
        } else {
            textView.setVisibility(View.VISIBLE);
        }

        final TextAppearanceSpan labelSpan = new TextAppearanceSpan(getActivity(), R.style.MachineDetails_Label);
        final SpannableStringBuilder sb = new SpannableStringBuilder(label);
        sb.setSpan(labelSpan, 0, label.length(), 0);
        int start = sb.length();
        final TextAppearanceSpan valueSpan = new TextAppearanceSpan(getActivity(), R.style.MachineDetails_Value);
        sb.append(value);
        sb.setSpan(valueSpan, start, sb.length(), 0);
        textView.setText(sb);
    }

    private void setPrice(@IdRes int viewId, @StringRes int labelId, @Nullable String price, boolean strikeValue) {
        TextView textView = ButterKnife.findById(getView(), viewId);
        if (TextUtils.isEmpty(price)) {
            textView.setVisibility(View.GONE);
            return;
        } else {
            textView.setVisibility(View.VISIBLE);
        }

        final String label = getString(labelId);
        final TextAppearanceSpan labelSpan = new TextAppearanceSpan(getActivity(), R.style.MachineDetails_Label);
        final SpannableStringBuilder sb = new SpannableStringBuilder(label);
        sb.setSpan(labelSpan, 0, label.length(), 0);
        int start = sb.length();
        final TextAppearanceSpan valueSpan = new TextAppearanceSpan(getActivity(), R.style.MachineDetails_Value);
        sb.append(price);
        sb.setSpan(valueSpan, start, sb.length(), 0);
        if (strikeValue) {
            sb.setSpan(new StrikethroughSpan(), start, sb.length(), 0);
        }
        textView.setText(sb);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        mSwipeRefreshLayout.setEnabled(verticalOffset == 0);
    }
}
