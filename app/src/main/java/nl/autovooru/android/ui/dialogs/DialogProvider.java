package nl.autovooru.android.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;

import com.afollestad.materialdialogs.MaterialDialog;

import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.Preconditions;
import nl.autovooru.android.utils.FragmentUtils;

/**
 * Created by Antonenko Viacheslav on 01/02/16.
 */
public final class DialogProvider {

    private DialogProvider() {
        // no-op
    }

    public static DialogFragment createErrorDialog(@NonNull FragmentManager fm, @Nullable String title, @NonNull String message) {
        Preconditions.checkNotNull(fm, "FragmentManager can't be null");
        Preconditions.checkNotNull(message, "message can't be null");

        return ErrorDialog.newInstance(title, message);
    }

    public static DialogFragment createInfoDialog(@NonNull FragmentManager fm,
                                                  @Nullable String title, @NonNull String message,
                                                  int dialogId,
                                                  boolean showPositiveButton,
                                                  boolean showNegativeButton) {
        Preconditions.checkNotNull(fm, "FragmentManager can't be null");
        Preconditions.checkNotNull(message, "message can't be null");

        return InfoDialog.newInstance(title, message, dialogId, showPositiveButton, showNegativeButton);
    }

    public static class InfoDialog extends DialogFragment {

        private static final String ARGS_TITLE = "args_title";
        private static final String ARGS_CONTENT = "args_content";
        private static final String ARGS_DIALOG_ID = "args_dialog_id";
        private static final String ARGS_SHOW_POSITIVE_BUTTON = "args_show_positive_button";
        private static final String ARGS_SHOW_NEGATIVE_BUTTON = "args_show_negative_button";

        public static InfoDialog newInstance(@Nullable String title, @NonNull String content,
                                             int dialogId,
                                             boolean showPositiveButton,
                                             boolean showNegativeButton) {
            final Bundle args = new Bundle();
            args.putString(ARGS_TITLE, title);
            args.putString(ARGS_CONTENT, content);
            args.putInt(ARGS_DIALOG_ID, dialogId);
            args.putBoolean(ARGS_SHOW_POSITIVE_BUTTON, showPositiveButton);
            args.putBoolean(ARGS_SHOW_NEGATIVE_BUTTON, showNegativeButton);

            final InfoDialog fragment = new InfoDialog();
            fragment.setArguments(args);
            return fragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                    .content(getContent())
                    .positiveText(R.string.commons_dialog_ok);

            if (!TextUtils.isEmpty(getTitle())) {
                builder.title(getTitle());
            }

            if (showPositiveButton()) {
                builder.positiveText(R.string.commons_dialog_ok);
                builder.onPositive((dialog, which) -> {
                    if (isAdded()) {
                        final ButtonListener listener = FragmentUtils.getParent(this, ButtonListener.class);
                        if (listener == null) {
                            throw new IllegalStateException("Parent must implement ButtonListener.");
                        } else {
                            listener.onPositive(getDialogId());
                        }
                    }
                });
            }

            if (showNegativeButton()) {
                builder.negativeText(R.string.commons_dialog_cancel);
                builder.onNegative((dialog, which) -> {
                    if (isAdded()) {
                        final ButtonListener listener = FragmentUtils.getParent(this, ButtonListener.class);
                        if (listener == null) {
                            throw new IllegalStateException("Parent must implement ButtonListener.");
                        } else {
                            listener.onNegative(getDialogId());
                        }
                    }
                });
            }

            return builder.build();
        }

        private String getTitle() {
            return getArguments().getString(ARGS_TITLE);
        }

        private String getContent() {
            return getArguments().getString(ARGS_CONTENT);
        }

        private int getDialogId() {
            return getArguments().getInt(ARGS_DIALOG_ID);
        }

        private boolean showPositiveButton() {
            return getArguments().getBoolean(ARGS_SHOW_POSITIVE_BUTTON);
        }

        private boolean showNegativeButton() {
            return getArguments().getBoolean(ARGS_SHOW_NEGATIVE_BUTTON);
        }

        public interface ButtonListener {
            void onPositive(int dialogId);

            void onNegative(int dialogId);
        }
    }

    public static class ErrorDialog extends DialogFragment {

        private static final String ARGS_TITLE = "args_title";
        private static final String ARGS_CONTENT = "args_content";

        public static ErrorDialog newInstance(@Nullable String title, @NonNull String content) {
            final Bundle args = new Bundle();
            args.putString(ARGS_TITLE, title);
            args.putString(ARGS_CONTENT, content);
            final ErrorDialog fragment = new ErrorDialog();
            fragment.setArguments(args);
            return fragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                    .content(getContent())
                    .positiveText(R.string.commons_dialog_ok);

            if (!TextUtils.isEmpty(getTitle())) {
                builder.title(getTitle());
            }

            return builder.build();
        }

        private String getTitle() {
            return getArguments().getString(ARGS_TITLE);
        }

        private String getContent() {
            return getArguments().getString(ARGS_CONTENT);
        }
    }
}
