package nl.autovooru.android.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.StringUtils;
import nl.autovooru.android.di.HasComponent;
import nl.autovooru.android.di.components.DaggerMapsComponent;
import nl.autovooru.android.di.components.MapsComponent;
import nl.autovooru.android.di.modules.MapsModule;
import nl.autovooru.android.domain.model.machine.details.Location;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.mvp.presenter.MapsPresenter;
import nl.autovooru.android.mvp.view.MapsView;
import nl.autovooru.android.utils.Constants;
import nl.autovooru.android.utils.maps.LocationClusterItem;
import nl.autovooru.android.utils.maps.LocationRenderer;

/**
 * Created by Antonenko Viacheslav on 09/12/15.
 */
public class MapsActivity extends BaseActivity implements OnMapReadyCallback, MapsView,
        ClusterManager.OnClusterClickListener<LocationClusterItem>, HasComponent<MapsComponent> {

    private static final String EXTRA_TITLE = "extra_title";
    private static final String EXTRA_FIELDS = "extra_fields";
    private static final String EXTRA_LOCATIONS = "extra_locations";
    private static final String EXTRA_WEBSITE = "extra_website";
    private static final String EXTRA_EMAIL = "extra_email";
    private static final String EXTRA_PHONE_NUMBER = "extra_phone_number";
    private static final String EXTRA_FROM_ABOUT = "extra_from_about";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.fab)
    FloatingActionsMenu mFloatingActionsMenu;
    @BindView(R.id.content_foreground)
    View mForegroundView;
    @BindView(R.id.toolbar_foreground)
    View mToolbarForegroundView;
    @Inject
    MapsPresenter mMapsPresenter;
    @Inject
    AccountRepository mAccountRepository;
    private GoogleMap mMap;

    public static void openActivity(@NonNull Context context,
                                    @NonNull String title,
                                    @Nullable ArrayList<String> fields,
                                    @Nullable ArrayList<Location> locations,
                                    @Nullable String website,
                                    @Nullable String email,
                                    @Nullable String phoneNumber,
                                    boolean fromAbout) {

        final Intent intent = new Intent(context, MapsActivity.class);
        intent.putExtra(EXTRA_TITLE, title);
        intent.putExtra(EXTRA_FIELDS, fields);
        intent.putExtra(EXTRA_LOCATIONS, Parcels.wrap(locations));

        intent.putExtra(EXTRA_WEBSITE, website);
        intent.putExtra(EXTRA_EMAIL, email);
        intent.putExtra(EXTRA_PHONE_NUMBER, phoneNumber);
        intent.putExtra(EXTRA_FROM_ABOUT, fromAbout);

        context.startActivity(intent);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        final String websiteUrl = getIntent().getStringExtra(EXTRA_WEBSITE);
        final String email = getIntent().getStringExtra(EXTRA_EMAIL);
        final String phoneNumber = getIntent().getStringExtra(EXTRA_PHONE_NUMBER);
        final boolean fromAbout = getIntent().getBooleanExtra(EXTRA_FROM_ABOUT, false);

        mMapsPresenter.attach(this, websiteUrl, email, phoneNumber);

        initializeToolbar();
        initializeFAB(websiteUrl, email, phoneNumber, fromAbout);
        if (savedInstanceState == null) {
            initializeFields();
        }
        initializeMap();
    }

    private void initializeFields() {
        final ArrayList<String> fields = getIntent().getStringArrayListExtra(EXTRA_FIELDS);
        final LayoutInflater inflater = getLayoutInflater();
        LinearLayout fieldWrapperView = ButterKnife.findById(this, R.id.fields_wrapper);
        for (String field : fields) {
            TextView fieldView = (TextView) inflater.inflate(R.layout.view_maps_field, fieldWrapperView, false);
            fieldView.setText(field);
            fieldWrapperView.addView(fieldView);
        }
    }

    private void initializeMap() {
        // Performance issue http://stackoverflow.com/questions/26178212/first-launch-of-activity-with-google-maps-is-very-slow/27599226#27599226
        new Handler().postDelayed(() -> {
            if (!isFinishing()) {
                FragmentManager fm = getSupportFragmentManager();
                SupportMapFragment mapFragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.mapContainer, mapFragment).commit();
                mapFragment.getMapAsync(this);
            }
        }, 300);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapsPresenter.detach();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        new Handler().postDelayed(() -> {
            if (!isFinishing()) {
                final ArrayList<Location> locations = Parcels.unwrap(getIntent().getParcelableExtra(EXTRA_LOCATIONS));
                if (locations != null) {
                    initClusterManager(googleMap, locations);
                }
            }
        }, 300);
    }

    private void initClusterManager(GoogleMap map, List<Location> locations) {
        final ClusterManager<LocationClusterItem> manager = new ClusterManager<>(this, map);
        map.setOnCameraChangeListener(manager);
        manager.setOnClusterClickListener(this);

        final LatLngBounds.Builder builder = LatLngBounds.builder();
        final ArrayList<LocationClusterItem> items = new ArrayList<>(locations.size());
        for (final Location location : locations) {
            final LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            builder.include(latLng);
            items.add(new LocationClusterItem(latLng));

        }
        manager.addItems(items);
        manager.setRenderer(new LocationRenderer(getActivity(), map, manager));
        if (!locations.isEmpty()) {
            if (locations.size() == 1) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(builder.build().getCenter(), Constants.DEFAULT_MAP_ZOOM));
            } else {
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 100));
            }
        }
    }

    @Override
    public boolean onClusterClick(Cluster<LocationClusterItem> myItemCluster) {
        final LatLngBounds.Builder builder = LatLngBounds.builder();
        for (LocationClusterItem item : myItemCluster.getItems()) {
            builder.include(item.getPosition());
        }
        if (myItemCluster.getItems().size() == 1) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(builder.build().getCenter(), Constants.DEFAULT_MAP_ZOOM));
        } else {
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 100));
        }

        return false;
    }

    private void initializeFAB(String websiteUrl, String email, String phoneNumber, boolean fromAbout) {
        if (!TextUtils.isEmpty(websiteUrl)) {
            addMenu(getString(R.string.fab_website), R.drawable.ic_fab_website, v -> mMapsPresenter.openWebsite());
        }

        if (fromAbout) {
            if (!TextUtils.isEmpty(email)) {
                addMenu(getString(R.string.fab_support), R.drawable.ic_fab_email, v -> mMapsPresenter.sendEmail());
            }
            addMenu(getString(R.string.fab_send_feedback), R.drawable.ic_fab_send_message, v -> {
                mFloatingActionsMenu.collapse();
                showSendFeedbackDialog();
            });
        } else {
            addMenu(getString(R.string.fab_send_message), R.drawable.ic_fab_send_message, v -> {
                mFloatingActionsMenu.collapse();
                showSendMessageDialog();
            });
        }

        if (!TextUtils.isEmpty(phoneNumber)) {
            addMenu(phoneNumber, R.drawable.ic_fab_call, v -> mMapsPresenter.callToOffice());
        }
        mFloatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                updateForeground();
            }

            @Override
            public void onMenuCollapsed() {
                updateForeground();
            }
        });
        updateForeground();
    }

    private void updateForeground() {
        if (mFloatingActionsMenu.isExpanded()) {
            mForegroundView.setClickable(true);
            mForegroundView.setBackgroundResource(R.color.black_transparency_30);
            mToolbarForegroundView.setClickable(true);
            mToolbarForegroundView.setBackgroundResource(R.color.black_transparency_30);
        } else {
            mForegroundView.setClickable(false);
            mForegroundView.setBackgroundColor(Color.TRANSPARENT);
            mToolbarForegroundView.setClickable(false);
            mToolbarForegroundView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private void initializeToolbar() {
        setSupportActionBar(mToolbar);
        final ActionBar actionBar = getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getIntent().getStringExtra(EXTRA_TITLE));
    }

    @Nullable
    @Override
    public Activity getActivity() {
        return this;
    }

    private void addMenu(@NonNull String title, @DrawableRes int iconResId, View.OnClickListener onClickListener) {
        final FloatingActionButton menuItem = new FloatingActionButton(this);
        menuItem.setSize(FloatingActionButton.SIZE_MINI);
        menuItem.setColorNormalResId(R.color.colorAccent);
        menuItem.setColorPressedResId(R.color.colorAccent);
        menuItem.setIcon(iconResId);
        menuItem.setTitle(title);
        menuItem.setOnClickListener(onClickListener);

        mFloatingActionsMenu.addButton(menuItem);
    }

    @Override
    protected void injectDependencies() {
        getComponent().inject(this);
    }

    private void showSendMessageDialog() {
        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(R.string.dialog_feedback_title)
                .customView(R.layout.dialog_send_message, true)
                .autoDismiss(false)
                .positiveText(R.string.dialog_feedback_positive_button)
                .onPositive((materialDialog, dialogAction) -> {
                    final View customView = materialDialog.getCustomView();

                    TextView subjectView = ButterKnife.findById(customView, R.id.subject);
                    TextView emailView = ButterKnife.findById(customView, R.id.email);
                    TextView messageView = ButterKnife.findById(customView, R.id.message);
                    TextView nameView = ButterKnife.findById(customView, R.id.name);
                    String errorMsg = subjectView.getResources().getString(R.string.dialog_feedback_error_message);

                    final String subject = subjectView.getText().toString();
                    if (StringUtils.isEmpty(subject)) {
                        ((TextInputLayout) subjectView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String email = emailView.getText().toString();
                    if (StringUtils.isEmpty(email)) {
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    if (!email.contains("@")) {
                        errorMsg = getString(R.string.error_wrong_email_format);
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String message = messageView.getText().toString();
                    if (StringUtils.isEmpty(message)) {
                        ((TextInputLayout) messageView.getParent()).setError(errorMsg);
                        return;
                    }

                    String name = nameView.getText().toString();
                    if (StringUtils.isEmpty(name)) {
                        ((TextInputLayout) nameView.getParent()).setError(errorMsg);
                        return;
                    }

                    mMapsPresenter.sendMessage(
                            null,
                            name.trim(),
                            email.trim(),
                            subject.trim(),
                            message.trim());

                    materialDialog.dismiss();
                })
                .build();

        if (mAccountRepository.isLoggedIn()) {
            TextView emailView = ButterKnife.findById(dialog.getCustomView(), R.id.email);
            TextView nameView = ButterKnife.findById(dialog.getCustomView(), R.id.name);
            emailView.setText(mAccountRepository.getEmail());
            nameView.setText(mAccountRepository.getUsername());
        }
        dialog.show();
    }

    private void showSendFeedbackDialog() {
        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(R.string.dialog_feedback_title)
                .customView(R.layout.dialog_send_feedback, true)
                .autoDismiss(false)
                .positiveText(R.string.dialog_feedback_positive_button)
                .onPositive((materialDialog, dialogAction) -> {
                    final View customView = materialDialog.getCustomView();

                    TextView subjectView = ButterKnife.findById(customView, R.id.subject);
                    TextView nameView = ButterKnife.findById(customView, R.id.name);
                    TextView emailView = ButterKnife.findById(customView, R.id.email);
                    TextView messageView = ButterKnife.findById(customView, R.id.message);
                    String errorMsg = subjectView.getResources().getString(R.string.dialog_feedback_error_message);

                    final String subject = subjectView.getText().toString();
                    if (StringUtils.isEmpty(subject)) {
                        ((TextInputLayout) subjectView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String name = nameView.getText().toString();
                    if (StringUtils.isEmpty(name)) {
                        ((TextInputLayout) nameView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String email = emailView.getText().toString();
                    if (StringUtils.isEmpty(email)) {
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    if (!email.contains("@")) {
                        errorMsg = getString(R.string.error_wrong_email_format);
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String message = messageView.getText().toString();
                    if (StringUtils.isEmpty(message)) {
                        ((TextInputLayout) messageView.getParent()).setError(errorMsg);
                        return;
                    }

                    mMapsPresenter.sendFeedback(
                            subject.trim(),
                            name.trim(),
                            email.trim(),
                            message.trim());

                    materialDialog.dismiss();
                })
                .build();

        if (mAccountRepository.isLoggedIn()) {
            TextView emailView = ButterKnife.findById(dialog.getCustomView(), R.id.email);
            TextView nameView = ButterKnife.findById(dialog.getCustomView(), R.id.name);
            emailView.setText(mAccountRepository.getEmail());
            nameView.setText(mAccountRepository.getUsername());
        }

        dialog.show();
    }

    @Override
    public MapsComponent getComponent() {
        return DaggerMapsComponent.builder()
                .applicationComponent(getApplicationComponent())
                .mapsModule(new MapsModule())
                .build();
    }
}
