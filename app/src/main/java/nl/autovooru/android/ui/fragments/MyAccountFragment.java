package nl.autovooru.android.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;
import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.StringUtils;
import nl.autovooru.android.di.components.UseCaseComponent;
import nl.autovooru.android.domain.model.user.UserInfo;
import nl.autovooru.android.exceptions.ErrorUtils;
import nl.autovooru.android.mvp.presenter.MyAccountPresenter;
import nl.autovooru.android.mvp.view.MyAccountView;

/**
 * Created by Antonenko Viacheslav on 29/12/15.
 */
public class MyAccountFragment extends BaseLceFragment<NestedScrollView, UserInfo, MyAccountView, MyAccountPresenter>
        implements MyAccountView, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.swipe_to_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.company_name)
    EditText mCompanyNameView;
    @BindView(R.id.name)
    EditText mNameView;
    @BindView(R.id.surname)
    EditText mSurnameView;
    @BindView(R.id.address)
    EditText mAddressView;
    @BindView(R.id.country)
    EditText mCountryView;
    @BindView(R.id.city)
    EditText mCityView;
    @BindView(R.id.post_code)
    EditText mPostcodeView;
    @BindView(R.id.phone)
    EditText mPhoneView;
    @BindView(R.id.email)
    EditText mEmailView;
    @BindView(R.id.website)
    EditText mWebsiteView;
    @BindView(R.id.password)
    EditText mPasswordView;
    @BindView(R.id.confirm_password)
    EditText mConfirmPasswordView;

    Unbinder mUnbinder;
    private MaterialDialog mProgressDialog;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_my_account;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        loadData(false);
    }

    @Override
    public void onDestroyView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return ErrorUtils.getErrorMessage(getActivity(), e);
    }

    @NonNull
    @Override
    public MyAccountPresenter createPresenter() {
        return getComponent(UseCaseComponent.class).provideMyAccountPresenter();
    }

    @Override
    public void showContent() {
        super.showContent();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        getPresenter().loadUserData(pullToRefresh);
    }

    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    public void setData(UserInfo data) {
        mNameView.setText(data.getName());
        mSurnameView.setText(data.getSurname());
        mAddressView.setText(data.getAddress());
        mCountryView.setText(data.getCountry());
        mCityView.setText(data.getCity());
        mPostcodeView.setText(data.getPostcode());
        mPhoneView.setText(data.getPhoneNumber());
        mEmailView.setText(data.getEmail());
        mWebsiteView.setText(data.getWebsiteUrl());
    }

    @Override
    public void showOrganizationSpecificViews() {
        mCompanyNameView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideOrganizationSpecificViews() {
        mCompanyNameView.setVisibility(View.GONE);
    }

    @Override
    public void showUserSpecificViews() {
        mNameView.setVisibility(View.VISIBLE);
        mSurnameView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideUserSpecificViews() {
        mNameView.setVisibility(View.GONE);
        mSurnameView.setVisibility(View.GONE);
    }

    @OnClick(R.id.apply)
    void applyChanges() {
        final String errorMsg = getResources().getString(R.string.dialog_feedback_error_message);
        final String companyName = getText(mCompanyNameView);
        final String name = getText(mNameView);
        final String surname = getText(mSurnameView);
        if (getPresenter().isOrganizationAccount()) {
            if (!isFieldValid(mCompanyNameView, errorMsg)) {
                return;
            }
        } else {
            if (!isFieldValid(mNameView, errorMsg)) {
                return;
            }
            if (!isFieldValid(mSurnameView, errorMsg)) {
                return;
            }
        }

        final String address = getText(mAddressView);
        if (!isFieldValid(mAddressView, errorMsg)) {
            return;
        }

        final String country = getText(mCountryView);
        if (!isFieldValid(mCountryView, errorMsg)) {
            return;
        }

        final String city = getText(mCityView);
        if (!isFieldValid(mCityView, errorMsg)) {
            return;
        }

        final String postcode = getText(mPostcodeView);
        if (!isFieldValid(mPostcodeView, errorMsg)) {
            return;
        }

        final String phoneNumber = getText(mPhoneView);
        if (!isFieldValid(mPhoneView, errorMsg)) {
            return;
        }

        final String email = getText(mEmailView);
        if (!isFieldValid(mEmailView, errorMsg)) {
            return;
        }

        final String websiteUrl = getText(mWebsiteView);

        UserInfo userInfo = new UserInfo();
        if (getPresenter().isOrganizationAccount()) {
            userInfo.setCompanyName(companyName);
        } else {
            userInfo.setName(name);
            userInfo.setSurname(surname);
        }

        userInfo.setAddress(address);
        userInfo.setCountry(country);
        userInfo.setCity(city);
        userInfo.setPostcode(postcode);
        userInfo.setPhoneNumber(phoneNumber);
        userInfo.setEmail(email);
        userInfo.setWebsiteUrl(websiteUrl);

        final String password = getText(mPasswordView);
        final String confirmPassword = getText(mConfirmPasswordView);
        if (!StringUtils.isEmpty(password) || !StringUtils.isEmpty(confirmPassword)) {
            if (TextUtils.equals(password, confirmPassword)) {
                userInfo.setPassword(password);
            } else {
                ((TextInputLayout) mPasswordView.getParent()).setError("Password must be equals.");
                return;
            }
        }

        getPresenter().sendUserData(userInfo);
    }

    private boolean isFieldValid(EditText editText, String errorText) {
        final TextInputLayout textInputLayout = (TextInputLayout) editText.getParent();
        if (TextUtils.isEmpty(getText(editText))) {
            contentView.smoothScrollTo(0, textInputLayout.getScrollY());
            textInputLayout.setError(errorText);
            editText.requestFocus();
            return false;
        } else {
            textInputLayout.setError(null);
            textInputLayout.setErrorEnabled(false);
            return true;
        }
    }

    @Override
    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new MaterialDialog.Builder(getActivity())
                    .content(R.string.dialog_progress_content)
                    .progress(true, 0).build();
        }
        mProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        mProgressDialog.hide();
    }

    private String getText(EditText editText) {
        return editText.getText().toString().trim();
    }

    private void showError(EditText editText, String message) {

    }
}
