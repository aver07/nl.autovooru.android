package nl.autovooru.android.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import org.parceler.Parcels;

import nl.autovooru.android.R;
import nl.autovooru.android.di.HasComponent;
import nl.autovooru.android.di.components.DaggerMachineSearchFiltersComponent;
import nl.autovooru.android.di.components.MachineSearchFiltersComponent;
import nl.autovooru.android.di.modules.MachineSearchFiltersModule;
import nl.autovooru.android.domain.model.query.MachineSearchQuery;
import nl.autovooru.android.ui.fragments.MachineSearchFiltersFragment;
import nl.autovooru.android.ui.fragments.MachineSearchFiltersFragmentBuilder;

/**
 * Created by Antonenko Viacheslav on 10/12/15.
 */
public class MachineSearchFiltersActivity extends BaseActivity implements HasComponent<MachineSearchFiltersComponent> {

    private static final String EXTRA_QUERY = "extra_query";

    public static Intent openActivity(@NonNull Context context,
                                      @NonNull MachineSearchQuery query) {
        final Intent intent = new Intent(context, MachineSearchFiltersActivity.class);
        intent.putExtra(EXTRA_QUERY, Parcels.wrap(query));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_machine_search_filters);

        if (savedInstanceState == null) {
            MachineSearchQuery query = Parcels.unwrap(getIntent().getParcelableExtra(EXTRA_QUERY));
            final MachineSearchFiltersFragment fragment = MachineSearchFiltersFragmentBuilder
                    .newMachineSearchFiltersFragment(query);

            final FragmentManager fm = getSupportFragmentManager();
            final FragmentTransaction transaction = fm.beginTransaction();
            transaction.replace(R.id.fragment_container, fragment);
            transaction.commit();
        }
    }

    @Override
    public MachineSearchFiltersComponent getComponent() {
        return DaggerMachineSearchFiltersComponent.builder()
                .applicationComponent(getApplicationComponent())
                .machineSearchFiltersModule(new MachineSearchFiltersModule())
                .build();
    }
}
