package nl.autovooru.android.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import butterknife.BindView;
import butterknife.Unbinder;
import de.greenrobot.event.EventBus;
import icepick.State;
import nl.autovooru.android.R;
import nl.autovooru.android.bus.events.NavigationSelectEvent;
import nl.autovooru.android.ui.activity.BaseActivity;
import nl.autovooru.android.ui.activity.PlaceAdsActivity;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
@FragmentWithArgs
public class UserDashboard extends BaseFragment {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    DrawerLayout mDrawerLayout;
    @BindView(R.id.tabs)
    TabLayout mTabLayout;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    Unbinder mUnbinder;
    @Arg
    DashboardMenuItems mDefaultMenuItems;
    @State
    DashboardMenuItems mSelectedMenuItems;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        FragmentArgs.inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mSelectedMenuItems == null) {
            mSelectedMenuItems = mDefaultMenuItems;
        }

        initializeToolbar();
        mViewPager.setAdapter(new DashboardAdapter(getActivity(), getChildFragmentManager()));
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setCurrentItem(mSelectedMenuItems.ordinal());
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                sendNavigationSelectEvent(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_user_dashboard, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_place_ads:
                final Intent intent = new Intent(getActivity(), PlaceAdsActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_user_dashboard;
    }

    private void sendNavigationSelectEvent(int position) {
        switch (DashboardMenuItems.values()[position]) {
            case my_ads:
                EventBus.getDefault().post(new NavigationSelectEvent(R.id.nav_my_advertise));
                break;
            case messages:
                EventBus.getDefault().post(new NavigationSelectEvent(R.id.nav_posts));
                break;
            case my_bids:
                EventBus.getDefault().post(new NavigationSelectEvent(R.id.nav_my_bids));
                break;
            case favorites:
                EventBus.getDefault().post(new NavigationSelectEvent(R.id.nav_favourites));
                break;
            case my_account:
                EventBus.getDefault().post(new NavigationSelectEvent(R.id.nav_my_account));
                break;
            default:
                throw new IllegalStateException("Unkown dashboard screen. Position :" + position);
        }
    }

    protected void initializeToolbar() {
        final BaseActivity activity = (BaseActivity) getActivity();
        activity.setSupportActionBar(mToolbar);
        final ActionBar actionBar = activity.getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.fragment_user_dashboard_title);

        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        if (mDrawerLayout != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    getActivity(), mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            mDrawerLayout.setDrawerListener(toggle);
            toggle.syncState();
        }
    }

    public enum DashboardMenuItems {
        my_ads, messages, my_bids, favorites, my_account
    }

    private static class DashboardAdapter extends FragmentPagerAdapter {

        private final Context mContext;

        public DashboardAdapter(Context context, FragmentManager fm) {
            super(fm);
            mContext = context.getApplicationContext();
        }

        @Override
        public Fragment getItem(int position) {
            switch (DashboardMenuItems.values()[position]) {
                case my_ads:
                    return new MyAdsFragment();
                case messages:
                    return new MyMessagesFragment();
                case my_bids:
                    return new MyBidsFragment();
                case favorites:
                    return new MyFavoritesFragment();
                case my_account:
                    return new MyAccountFragment();
                default:
                    throw new IllegalStateException("Unkown dashboard screen. Position :" + position);
            }
        }

        @Override
        public int getCount() {
            return DashboardMenuItems.values().length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (DashboardMenuItems.values()[position]) {
                case my_ads:
                    return mContext.getString(R.string.fragment_user_dashboard_my_ads);
                case messages:
                    return mContext.getString(R.string.fragment_user_dashboard_messages);
                case my_bids:
                    return mContext.getString(R.string.fragment_user_dashboard_bids);
                case favorites:
                    return mContext.getString(R.string.fragment_user_dashboard_favorites);
                case my_account:
                    return mContext.getString(R.string.fragment_user_dashboard_my_account);
                default:
                    throw new IllegalStateException("Unkown dashboard screen. Position :" + position);
            }
        }
    }
}
