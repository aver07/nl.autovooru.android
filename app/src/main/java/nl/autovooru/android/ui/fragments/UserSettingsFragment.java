package nl.autovooru.android.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;
import de.greenrobot.event.EventBus;
import nl.autovooru.android.R;
import nl.autovooru.android.bus.events.NotificationStatusEvent;
import nl.autovooru.android.common.utils.IntentUtils;
import nl.autovooru.android.common.utils.StringUtils;
import nl.autovooru.android.domain.interactor.DefaultSubscriber;
import nl.autovooru.android.domain.interactor.SendFeedbackUseCase;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.domain.repository.CommonRepository;
import nl.autovooru.android.rx.RxUtils;
import nl.autovooru.android.settings.UserSettingsManager;
import nl.autovooru.android.ui.activity.AboutUsActivity;
import nl.autovooru.android.ui.activity.BaseActivity;

/**
 * Created by Antonenko Viacheslav on 20/01/16.
 */
public class UserSettingsFragment extends BaseFragment {

    private static final int RINGTONE_REQUEST_CODE = 1000;

    Unbinder mUnbinder;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.notifications)
    CheckedTextView mNotificationsView;
    @BindView(R.id.bid_notifications)
    CheckedTextView mBidView;
    @BindView(R.id.message_notifications)
    CheckedTextView mMessageView;
    @BindView(R.id.vibrate)
    CheckedTextView mVibrateView;
    @BindView(R.id.sound)
    TextView mSoundView;
    DrawerLayout mDrawerLayout;
    @Inject
    UserSettingsManager mUserSettingsManager;
    @Inject
    AccountRepository mAccountRepository;
    @Inject
    SendFeedbackUseCase mSendFeedbackUseCase;

    public static Fragment newInstance() {
        return new UserSettingsFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_user_settings;
    }

    @Override
    protected void injectDependencies() {
        getApplicationComponent().plus(new UserSettingsModule()).inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeToolbar();
        initUI();
    }

    @Override
    public void onDestroyView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == RINGTONE_REQUEST_CODE) {
            final Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
            if (uri != null) {
                mUserSettingsManager.setNotificationSound(uri);
                updateSoundLabel();
            }
        }
    }

    @OnClick(R.id.notifications)
    void toggleAllNotifications() {
        mNotificationsView.toggle();
        mUserSettingsManager.setNotificationsEnabled(mNotificationsView.isChecked());
        EventBus.getDefault().post(new NotificationStatusEvent());
    }

    @OnClick(R.id.bid_notifications)
    void toggleBidNotifications() {
        mBidView.toggle();
        mUserSettingsManager.setBidNotificationsEnabled(mBidView.isChecked());
    }

    @OnClick(R.id.message_notifications)
    void toggleMessageNotifications() {
        mMessageView.toggle();
        mUserSettingsManager.setMessagesNotificationsEnabled(mMessageView.isChecked());
    }

    @OnClick(R.id.vibrate)
    void toggleVibrate() {
        mVibrateView.toggle();
        mUserSettingsManager.setVibrationEnabled(mVibrateView.isChecked());
    }

    @OnClick(R.id.sound)
    void chooseRingtone() {
        final Intent intent = IntentUtils.openRingtoneChooser(mUserSettingsManager.getNotificationSound());
        if (IntentUtils.isIntentAvailable(getActivity(), intent)) {
            startActivityForResult(intent, RINGTONE_REQUEST_CODE);
        }
    }

    @OnClick(R.id.feedback)
    void showFeedbackDialog() {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_feedback_title)
                .customView(R.layout.dialog_send_feedback, true)
                .autoDismiss(false)
                .positiveText(R.string.dialog_feedback_positive_button)
                .onPositive((materialDialog, dialogAction) -> {
                    final View customView = materialDialog.getCustomView();

                    TextView subjectView = ButterKnife.findById(customView, R.id.subject);
                    TextView nameView = ButterKnife.findById(customView, R.id.name);
                    TextView emailView = ButterKnife.findById(customView, R.id.email);
                    TextView messageView = ButterKnife.findById(customView, R.id.message);
                    String errorMsg = subjectView.getResources().getString(R.string.dialog_feedback_error_message);

                    final String subject = subjectView.getText().toString();
                    if (StringUtils.isEmpty(subject)) {
                        ((TextInputLayout) subjectView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String name = nameView.getText().toString();
                    if (StringUtils.isEmpty(name)) {
                        ((TextInputLayout) nameView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String email = emailView.getText().toString();
                    if (StringUtils.isEmpty(email)) {
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    if (!email.contains("@")) {
                        errorMsg = getString(R.string.error_wrong_email_format);
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String message = messageView.getText().toString();
                    if (StringUtils.isEmpty(subject)) {
                        ((TextInputLayout) messageView.getParent()).setError(errorMsg);
                        return;
                    }

                    mSendFeedbackUseCase.sendFeedback(subject, name, email, message)
                            .compose(RxUtils.applySchedulers())
                            .subscribe(new DefaultSubscriber<>());

                    materialDialog.dismiss();
                })
                .build();

        if (mAccountRepository.isLoggedIn()) {
            TextView nameView = ButterKnife.findById(dialog.getCustomView(), R.id.name);
            TextView emailView = ButterKnife.findById(dialog.getCustomView(), R.id.email);
            nameView.setText(mAccountRepository.getUsername());
            emailView.setText(mAccountRepository.getEmail());
        }

        dialog.show();
    }

    @OnClick(R.id.about_us)
    void openAboutUs() {
        AboutUsActivity.openActivity(getActivity());
    }

    private void initUI() {
        mNotificationsView.setEnabled(mAccountRepository.isLoggedIn());
        mBidView.setEnabled(mAccountRepository.isLoggedIn());
        mMessageView.setEnabled(mAccountRepository.isLoggedIn());

        mNotificationsView.setChecked(mUserSettingsManager.notificationsEnabled());
        mBidView.setChecked(mUserSettingsManager.bidNotificationsEnabled());
        mMessageView.setChecked(mUserSettingsManager.messagesNotificationsEnabled());
        mVibrateView.setChecked(mUserSettingsManager.vibrationEnabled());

        updateSoundLabel();
    }

    private void updateSoundLabel() {
        final TextAppearanceSpan labelSpan = new TextAppearanceSpan(getActivity(), R.style.MachineDetails_Label);
        final String label = getString(R.string.fragment_user_settings_sound);
        final SpannableStringBuilder sb = new SpannableStringBuilder(label);
        sb.setSpan(labelSpan, 0, label.length(), 0);
        int start = sb.length();
        final TextAppearanceSpan valueSpan = new TextAppearanceSpan(getActivity(), R.style.MachineDetails_Value);
        final Ringtone ringtone = RingtoneManager.getRingtone(getActivity(), mUserSettingsManager.getNotificationSound());
        sb.append(ringtone.getTitle(getActivity()));
        sb.setSpan(valueSpan, start, sb.length(), 0);
        mSoundView.setText(sb);
    }

    private void initializeToolbar() {
        final BaseActivity activity = (BaseActivity) getActivity();
        activity.setSupportActionBar(mToolbar);
        final ActionBar actionBar = activity.getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.fragment_user_settings_title);

        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();
    }

    @Subcomponent(modules = UserSettingsModule.class)
    public interface UserSettingsComponent {
        void inject(UserSettingsFragment fragment);
    }

    @Module
    public static class UserSettingsModule {

        @Provides
        @NonNull
        public SendFeedbackUseCase provideSendFeedbackUseCase(@NonNull CommonRepository commonRepository) {
            return new SendFeedbackUseCase(commonRepository);
        }
    }
}
