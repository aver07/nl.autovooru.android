package nl.autovooru.android.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.BindView;
import nl.autovooru.android.R;
import nl.autovooru.android.di.components.UseCaseComponent;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import nl.autovooru.android.exceptions.ErrorUtils;
import nl.autovooru.android.mvp.presenter.MyBidsPresenter;
import nl.autovooru.android.mvp.view.MyBidsView;
import nl.autovooru.android.ui.adapter.MyBidsAdapter;
import nl.autovooru.android.ui.widget.recyclerview.DividerItemDecoration;
import nl.autovooru.android.ui.widget.recyclerview.ItemClickSupport;
import nl.autovooru.android.ui.widget.recyclerview.OnLoadMoreListener;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public class MyBidsFragment extends BaseLceFragment<RecyclerView, MachineListViewModel, MyBidsView, MyBidsPresenter>
        implements SwipeRefreshLayout.OnRefreshListener, OnLoadMoreListener,
        MyBidsAdapter.OnOverflowActionListener, MyBidsView {

    @BindView(R.id.swipe_to_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;
    private MyBidsAdapter mAdapter;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_my_bids;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        initializeRecyclerView();
        loadData(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_my_bids, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete_all:
                new MaterialDialog.Builder(getActivity())
                        .title(R.string.dialog_remove_all_bids_content)
                        .negativeText(R.string.commons_dialog_cancel)
                        .positiveText(R.string.commons_dialog_ok)
                        .onPositive((dialog, which) -> getPresenter().removeAllBids())
                        .build().show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return ErrorUtils.getErrorMessage(getActivity(), e);
    }

    @NonNull
    @Override
    public MyBidsPresenter createPresenter() {
        return getComponent(UseCaseComponent.class).provideMyBidsPresenter();
    }

    @Override
    public void showContent() {
        super.showContent();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setData(MachineListViewModel data) {
        mAdapter.resetItems(data.getMachineList());
        mAdapter.enableLoadingMore(data.canLoadMore());
    }

    @Override
    public void addMore(MachineListViewModel data) {
        mAdapter.removeItem(null);
        if (data != null) {
            mAdapter.addItems(data.getMachineList());
            mAdapter.enableLoadingMore(data.canLoadMore());
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        getPresenter().load(pullToRefresh);
    }

    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    public void onLoadMore() {
        getPresenter().loadMore();
    }

    @Override
    public void removeBid(Machine machine) {
        if (isAdded()) {
            new MaterialDialog.Builder(getActivity())
                    .content(R.string.dialog_remove_bid_content)
                    .positiveText(R.string.commons_dialog_ok)
                    .negativeText(R.string.commons_dialog_cancel)
                    .onPositive((dialog, which) -> getPresenter().removeBid(machine))
                    .build()
                    .show();
        }
    }

    @Override
    public void removeAllBids(Machine machine) {
        if (isAdded()) {
            new MaterialDialog.Builder(getActivity())
                    .content(R.string.dialog_remove_all_machine_bids_content)
                    .positiveText(R.string.commons_dialog_ok)
                    .negativeText(R.string.commons_dialog_cancel)
                    .onPositive((dialog, which) -> getPresenter().removeAllMachineBids(machine))
                    .build()
                    .show();
        }
    }

    @Override
    public void removeMachine(Machine machine) {
        if (mAdapter != null && machine != null) {
            mAdapter.removeItem(machine);
        }
    }

    private void initializeRecyclerView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        contentView.setLayoutManager(layoutManager);
        final DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
        dividerItemDecoration.setDivider(ContextCompat.getDrawable(getActivity(), R.drawable.list_item_divider));
        contentView.addItemDecoration(dividerItemDecoration);
        contentView.setHasFixedSize(true);
        ItemClickSupport itemClickSupport = nl.autovooru.android.ui.widget.recyclerview.ItemClickSupport.addTo(contentView);
        itemClickSupport.setOnItemClickListener((recyclerView, position, v) -> {
            final Machine machine = mAdapter.getItem(position);
//            MachineDetailsActivity.openActivity(getActivity(), machine.getMachineId(),
//                    getPresenter().getMachineSearchQuery().getMachineCategory());
        });
        mAdapter = new MyBidsAdapter(contentView, this, this);
        contentView.setAdapter(mAdapter);
    }

}
