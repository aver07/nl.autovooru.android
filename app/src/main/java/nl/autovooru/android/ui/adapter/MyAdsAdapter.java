package nl.autovooru.android.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import nl.autovooru.android.R;
import nl.autovooru.android.bus.events.DeleteMyAdEvent;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.ui.activity.EditMachineActivity;
import nl.autovooru.android.ui.activity.MachineDetailsActivity;
import nl.autovooru.android.ui.activity.MachinesBidsActivity;
import nl.autovooru.android.ui.widget.recyclerview.AbstractRecyclerViewFooterAdapter;
import nl.autovooru.android.ui.widget.recyclerview.OnLoadMoreListener;
import nl.autovooru.android.utils.MachineUtils;

/**
 * Created by Antonenko Viacheslav on 23/12/15.
 */
public class MyAdsAdapter extends AbstractRecyclerViewFooterAdapter<Machine> {

    public MyAdsAdapter(RecyclerView recyclerView, final OnLoadMoreListener onLoadMoreListener) {
        super(recyclerView, onLoadMoreListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int position) {
        final int layoutRes = R.layout.list_item_my_ads;
        final View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new MachineViewHolder(view);
    }

    @Override
    protected void onBindBasicItemView(RecyclerView.ViewHolder holder, int position) {
        final MachineViewHolder viewHolder = (MachineViewHolder) holder;
        final Machine machine = getItem(position);
        final List<String> images = machine.getImages();
        if (images != null && images.size() > 0) {
            Picasso.with(holder.itemView.getContext())
                    .load(images.get(0))
                    .fit()
                    .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                    .into(viewHolder.mImageView);
        } else {
            viewHolder.mImageView.setImageDrawable(null);
        }

        final Resources r = viewHolder.itemView.getResources();
        viewHolder.mModelView.setText(machine.getName());
        viewHolder.mBidView.setText(r.getString(R.string.fragment_my_ads_bid_label, machine.getBids()));

        final String price = MachineUtils.getPrice(holder.itemView.getContext(), machine);
        viewHolder.mPriceView.setText(r.getString(R.string.fragment_my_ads_price_label, price));
        viewHolder.mHitsView.setText(r.getString(R.string.fragment_my_ads_hits_label, machine.getHits()));

        viewHolder.itemView.setTag(machine);
    }

    static class MachineViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView mImageView;
        @BindView(R.id.bid)
        TextView mBidView;
        @BindView(R.id.model)
        TextView mModelView;
        @BindView(R.id.price)
        TextView mPriceView;
        @BindView(R.id.hits)
        TextView mHitsView;
        @BindView(R.id.overflow)
        View mOverflowView;

        public MachineViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.image)
        public void openDetails(View view) {
            final Machine machine = (Machine) itemView.getTag();
            MachineDetailsActivity.openActivity(view.getContext(), machine.getMachineId());
        }

        @OnClick(R.id.overflow)
        public void showMenu(View view) {
            final Machine machine = (Machine) itemView.getTag();
            final Context context = view.getContext();
            final PopupMenu popupMenu = new PopupMenu(context, view);
            popupMenu.inflate(R.menu.fragment_my_ads_list_item_overflow);
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.action_show_bids:
                        MachinesBidsActivity.openActivity(context, machine.getMachineId());
                        return true;
                    case R.id.action_edit:
                        EditMachineActivity.openActivity(view.getContext(), machine);
                        return true;
                    case R.id.action_delete:
                        showDeleteDialog(context, machine.getMachineId());
                        return true;
                }
                return false;
            });
            popupMenu.show();
        }

        private void showDeleteDialog(Context context, String machineId) {
            new MaterialDialog.Builder(context)
                    .title(R.string.dialog_delete_ad_title)
                    .content(R.string.dialog_delete_ad_content)
                    .positiveText(R.string.commons_dialog_ok)
                    .negativeText(R.string.commons_dialog_cancel)
                    .onPositive((dialog, which) -> {
                        EventBus.getDefault().post(new DeleteMyAdEvent(machineId));
                    })
                    .build()
                    .show();
        }
    }
}
