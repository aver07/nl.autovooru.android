package nl.autovooru.android.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.StringRes;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import nl.autovooru.android.R;
import nl.autovooru.android.bus.events.DeleteBidMessageEvent;
import nl.autovooru.android.bus.events.DeleteMyAdEvent;
import nl.autovooru.android.bus.events.SendBidMessageEvent;
import nl.autovooru.android.domain.model.user.Bid;
import nl.autovooru.android.ui.widget.recyclerview.AbstractRecyclerViewFooterAdapter;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public class MachinesBidsAdapter extends AbstractRecyclerViewFooterAdapter<Bid> {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

    public MachinesBidsAdapter(RecyclerView recyclerView) {
        super(recyclerView, null);
    }

    @Override
    public RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int position) {
        final int layoutRes = R.layout.list_item_machines_bids;
        final View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new BidViewHolder(view);
    }

    @Override
    protected void onBindBasicItemView(RecyclerView.ViewHolder holder, int position) {
        final BidViewHolder viewHolder = (BidViewHolder) holder;
        final Bid bid = getItem(position);

        final Resources r = viewHolder.itemView.getResources();
        setText(viewHolder.mEmail, R.string.activity_machines_bids_label_from, bid.getEmail());

        final String bidValue = r.getString(R.string.euro, bid.getBid());
        setText(viewHolder.mBidView, R.string.activity_machines_bids_label_bid, bidValue);

        viewHolder.mDateView.setText(DATE_FORMAT.format(bid.getDate()));

        viewHolder.itemView.setTag(bid);
    }

    private void setText(TextView textView, @StringRes int resId, String value) {
        final Context context = textView.getContext();
        final TextAppearanceSpan labelSpan = new TextAppearanceSpan(context, R.style.MachinesBids_Label);
        final String label = context.getString(resId);
        final SpannableStringBuilder sb = new SpannableStringBuilder(label);
        sb.setSpan(labelSpan, 0, label.length(), 0);
        int start = sb.length();
        final TextAppearanceSpan valueSpan = new TextAppearanceSpan(context, R.style.MachinesBids_Value);
        sb.append(value);
        sb.setSpan(valueSpan, start, sb.length(), 0);
        textView.setText(sb);
    }

    static class BidViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.bid)
        TextView mBidView;
        @BindView(R.id.date)
        TextView mDateView;
        @BindView(R.id.email)
        TextView mEmail;

        public BidViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.overflow)
        public void showMenu(View view) {
            final Bid bid = (Bid) itemView.getTag();
            final Context context = view.getContext();
            final PopupMenu popupMenu = new PopupMenu(context, view);
            popupMenu.inflate(R.menu.fragment_machines_bids_list_item_overflow);
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        EventBus.getDefault().post(new DeleteBidMessageEvent(bid));
                        return true;
                    case R.id.action_send_message:
                        EventBus.getDefault().post(new SendBidMessageEvent(bid));
                        return true;
                }
                return false;
            });
            popupMenu.show();
        }

        private void showDeleteDialog(Context context, String machineId) {
            new MaterialDialog.Builder(context)
                    .title(R.string.dialog_delete_ad_title)
                    .content(R.string.dialog_delete_ad_content)
                    .positiveText(R.string.commons_dialog_ok)
                    .negativeText(R.string.commons_dialog_cancel)
                    .onPositive((dialog, which) -> {
                        EventBus.getDefault().post(new DeleteMyAdEvent(machineId));
                    })
                    .build()
                    .show();
        }
    }

}
