package nl.autovooru.android.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import butterknife.BindView;
import butterknife.Unbinder;
import de.greenrobot.event.EventBus;
import nl.autovooru.android.R;
import nl.autovooru.android.bus.events.DeleteMyAdEvent;
import nl.autovooru.android.di.components.UseCaseComponent;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import nl.autovooru.android.exceptions.ErrorUtils;
import nl.autovooru.android.mvp.presenter.MyAdsPresenter;
import nl.autovooru.android.mvp.view.MyAdsView;
import nl.autovooru.android.ui.adapter.MyAdsAdapter;
import nl.autovooru.android.ui.widget.recyclerview.DividerItemDecoration;
import nl.autovooru.android.ui.widget.recyclerview.OnLoadMoreListener;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public class MyAdsFragment extends BaseLceFragment<RecyclerView, MachineListViewModel, MyAdsView, MyAdsPresenter>
        implements MyAdsView, OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.swipe_to_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;
    Unbinder mUnbinder;

    private MyAdsAdapter mAdapter;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        initializeRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        loadData(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroyView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_my_ads;
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return ErrorUtils.getErrorMessage(getActivity(), e);
    }

    @NonNull
    @Override
    public MyAdsPresenter createPresenter() {
        return getComponent(UseCaseComponent.class).provideMyAdsPresenter();
    }

    @Override
    public void showContent() {
        super.showContent();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setData(MachineListViewModel data) {
        mAdapter.resetItems(data.getMachineList());
        mAdapter.enableLoadingMore(data.canLoadMore());
    }

    @Override
    public void addMore(MachineListViewModel data) {
        mAdapter.removeItem(null);
        mAdapter.addItems(data.getMachineList());
        mAdapter.enableLoadingMore(data.canLoadMore());
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        getPresenter().load(pullToRefresh);
    }

    @Override
    public void onLoadMore() {
        getPresenter().loadMore();
    }

    @Override
    public void onRefresh() {
        loadData(true);
    }

    public void onEvent(DeleteMyAdEvent event) {
        if (!TextUtils.isEmpty(event.getMachineId())) {
            getPresenter().deleteMyAd(event.getMachineId());
        }
    }

    private void initializeRecyclerView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        contentView.setLayoutManager(layoutManager);
        final DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
        dividerItemDecoration.setDivider(ContextCompat.getDrawable(getActivity(), R.drawable.list_item_divider));
        contentView.addItemDecoration(dividerItemDecoration);
        contentView.setHasFixedSize(true);
        mAdapter = new MyAdsAdapter(contentView, this);
        contentView.setAdapter(mAdapter);
    }
}
