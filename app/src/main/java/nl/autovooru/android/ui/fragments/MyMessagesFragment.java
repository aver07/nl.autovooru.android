package nl.autovooru.android.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.StringUtils;
import nl.autovooru.android.di.components.UseCaseComponent;
import nl.autovooru.android.domain.model.user.Message;
import nl.autovooru.android.domain.model.user.MessagesPage;
import nl.autovooru.android.exceptions.ErrorUtils;
import nl.autovooru.android.mvp.presenter.MyMessagesPresenter;
import nl.autovooru.android.mvp.view.MyMessagesView;
import nl.autovooru.android.ui.adapter.MyMessagesAdapter;
import nl.autovooru.android.ui.widget.recyclerview.DividerItemDecoration;
import nl.autovooru.android.ui.widget.recyclerview.OnLoadMoreListener;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public class MyMessagesFragment extends BaseLceFragment<RecyclerView, MessagesPage, MyMessagesView, MyMessagesPresenter>
        implements MyMessagesView, SwipeRefreshLayout.OnRefreshListener, OnLoadMoreListener, MyMessagesAdapter.OnOverflowActionListener {

    @BindView(R.id.swipe_to_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;
    private MyMessagesAdapter mAdapter;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_my_messages;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        initializeRecyclerView();
        loadData(false);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return ErrorUtils.getErrorMessage(getActivity(), e);
    }

    @NonNull
    @Override
    public MyMessagesPresenter createPresenter() {
        return getComponent(UseCaseComponent.class).provideMyMessagesPresenter();
    }

    @Override
    public void showContent() {
        super.showContent();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setData(MessagesPage data) {
        mAdapter.resetItems(data.getEntites());
        mAdapter.enableLoadingMore(data.canLoadMore());
    }

    @Override
    public void addMore(MessagesPage data) {
        mAdapter.removeItem(null);
        if (data != null) {
            mAdapter.addItems(data.getEntites());
            mAdapter.enableLoadingMore(data.canLoadMore());
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        getPresenter().load(pullToRefresh);
    }

    @Override
    public void onLoadMore() {
        LOG.d("onLoadMore()");
        getPresenter().loadMore();
    }

    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    public void removeItem(Message message) {
        if (mAdapter != null) {
            mAdapter.removeItem(message);
        }
    }

    @Override
    public void removeMessage(final Message message) {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_delete_my_message_title)
                .content(R.string.dialog_delete_my_message_content)
                .positiveText(R.string.commons_dialog_ok)
                .negativeText(R.string.commons_dialog_cancel)
                .onPositive((dialog, which) -> {
                    getPresenter().removeMessage(message);
                })
                .build()
                .show();
    }

    @Override
    public void sendMessage(Message messageItem) {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_feedback_title)
                .customView(R.layout.dialog_send_message, true)
                .autoDismiss(false)
                .positiveText(R.string.dialog_feedback_positive_button)
                .onPositive((materialDialog, dialogAction) -> {
                    final View customView = materialDialog.getCustomView();

                    TextView subjectView = ButterKnife.findById(customView, R.id.subject);
                    TextView emailView = ButterKnife.findById(customView, R.id.email);
                    TextView messageView = ButterKnife.findById(customView, R.id.message);
                    TextView nameView = ButterKnife.findById(customView, R.id.name);
                    String errorMsg = subjectView.getResources().getString(R.string.dialog_feedback_error_message);

                    final String subject = subjectView.getText().toString();
                    if (StringUtils.isEmpty(subject)) {
                        ((TextInputLayout) subjectView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String email = emailView.getText().toString();
                    if (StringUtils.isEmpty(email)) {
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    if (!email.contains("@")) {
                        errorMsg = getString(R.string.error_wrong_email_format);
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String message = messageView.getText().toString();
                    if (StringUtils.isEmpty(message)) {
                        ((TextInputLayout) messageView.getParent()).setError(errorMsg);
                        return;
                    }

                    String name = nameView.getText().toString();
                    if (StringUtils.isEmpty(name)) {
                        ((TextInputLayout) nameView.getParent()).setError(errorMsg);
                        return;
                    }

                    getPresenter().sendAnswer(
                            messageItem,
                            subject,
                            email,
                            name,
                            message);

                    materialDialog.dismiss();
                })
                .build();


        TextView subjectView = ButterKnife.findById(dialog.getCustomView(), R.id.subject);
        TextView emailView = ButterKnife.findById(dialog.getCustomView(), R.id.email);
        TextView nameView = ButterKnife.findById(dialog.getCustomView(), R.id.name);
        subjectView.setText(messageItem.getAnswerTopic());
        emailView.setText(getPresenter().getUserEmail());
        nameView.setText(getPresenter().getUsername());

        dialog.show();
    }

    private void initializeRecyclerView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        contentView.setLayoutManager(layoutManager);
        final DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
        dividerItemDecoration.setDivider(ContextCompat.getDrawable(getActivity(), R.drawable.list_item_divider));
        contentView.addItemDecoration(dividerItemDecoration);
        contentView.setHasFixedSize(true);
        mAdapter = new MyMessagesAdapter(contentView, this, this);
        contentView.setAdapter(mAdapter);
    }
}
