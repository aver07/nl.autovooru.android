package nl.autovooru.android.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;

import java.util.List;

/**
 * Created by Antonenko Viacheslav on 16/12/15.
 */
public abstract class BaseAdapter<T> extends android.widget.BaseAdapter {

    private List<T> mItems;
    private LayoutInflater mLayoutInflater;

    public BaseAdapter(Context context, List<T> items) {
        mItems = items;
        mLayoutInflater = LayoutInflater.from(context);
    }

    public LayoutInflater getLayoutInflater() {
        return mLayoutInflater;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public T getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
