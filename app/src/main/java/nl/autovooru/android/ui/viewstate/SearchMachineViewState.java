package nl.autovooru.android.ui.viewstate;

import com.hannesdorfmann.mosby.mvp.viewstate.lce.LceViewState;

import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import nl.autovooru.android.mvp.view.SearchMachineListView;

/**
 * Created by Antonenko Viacheslav on 02/12/15.
 */
public class SearchMachineViewState implements LceViewState<MachineListViewModel, SearchMachineListView> {

    @Override
    public void setStateShowContent(MachineListViewModel loadedData) {

    }

    @Override
    public void setStateShowError(Throwable e, boolean pullToRefresh) {

    }

    @Override
    public void setStateShowLoading(boolean pullToRefresh) {

    }

    @Override
    public void apply(SearchMachineListView view, boolean retained) {

    }
}
