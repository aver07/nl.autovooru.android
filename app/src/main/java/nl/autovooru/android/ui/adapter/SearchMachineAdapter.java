package nl.autovooru.android.ui.adapter;

import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import nl.autovooru.android.BaseApp;
import nl.autovooru.android.R;
import nl.autovooru.android.bus.events.AddToFavoriteEvent;
import nl.autovooru.android.common.utils.IntentUtils;
import nl.autovooru.android.common.utils.StringUtils;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.ui.widget.recyclerview.AbstractRecyclerViewFooterAdapter;
import nl.autovooru.android.ui.widget.recyclerview.OnLoadMoreListener;
import nl.autovooru.android.utils.MachineUtils;

/**
 * Created by Antonenko Viacheslav on 25/11/15.
 */
public class SearchMachineAdapter extends AbstractRecyclerViewFooterAdapter<Machine> {

    private static final String WIHT_YEAR_FORMATER = "%1$s / %2$skm";
    private static final String WIHT_FUEL_FORMATER = "%1$skm / %2$s";
    private static final String ONLY_MILEAGE_FORMATER = "%skm";
    private static final String ALL_FIELDS_FORMATTER = "%1$s / %2$skm / %3$s";

    private final boolean mShowLikeList;

    public SearchMachineAdapter(RecyclerView recyclerView, final OnLoadMoreListener onLoadMoreListener, boolean showLikeList) {
        super(recyclerView, onLoadMoreListener);
        mShowLikeList = showLikeList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int position) {
        final int layoutRes = mShowLikeList ? R.layout.list_item_search_machine : R.layout.list_item_search_machine_grid;
        final View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new MachineViewHolder(view);
    }

    @Override
    protected void onBindBasicItemView(RecyclerView.ViewHolder holder, int position) {
        final MachineViewHolder viewHolder = (MachineViewHolder) holder;
        final Machine machine = getItem(position);
        final List<String> images = machine.getImages();
        if (images != null && images.size() > 0) {
            if (mShowLikeList) {
                Picasso.with(holder.itemView.getContext())
                        .load(images.get(0))
                        .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                        .into(viewHolder.mImageView);
            } else {
                Picasso.with(holder.itemView.getContext())
                        .load(images.get(0))
                        .fit()
                        .centerCrop()
                        .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                        .into(viewHolder.mImageView);
            }

        } else {
            viewHolder.mImageView.setImageDrawable(null);
        }

        viewHolder.mModelView.setText(machine.getName());
        if (StringUtils.isEmpty(machine.getReleaseYear()) && StringUtils.isEmpty(machine.getFuel())) {
            viewHolder.mDetailsView.setText(String.format(ONLY_MILEAGE_FORMATER, machine.getMileage()));
        } else if (StringUtils.isEmpty(machine.getReleaseYear())) {
            viewHolder.mDetailsView.setText(String.format(WIHT_FUEL_FORMATER,
                    machine.getMileage(), machine.getFuel()));
        } else if (StringUtils.isEmpty(machine.getFuel())) {
            viewHolder.mDetailsView.setText(String.format(WIHT_YEAR_FORMATER,
                    machine.getReleaseYear(), machine.getMileage()));
        } else {
            viewHolder.mDetailsView.setText(String.format(ALL_FIELDS_FORMATTER,
                    machine.getReleaseYear(), machine.getMileage(), machine.getFuel()));
        }

        final String price = MachineUtils.getPrice(viewHolder.itemView.getContext(), machine);
        viewHolder.mPriceView.setText(price);

        viewHolder.mCityView.setText(machine.getCity());

        viewHolder.itemView.setTag(machine);
    }

    static class MachineViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView mImageView;
        @BindView(R.id.details)
        TextView mDetailsView;
        @BindView(R.id.model)
        TextView mModelView;
        @BindView(R.id.price)
        TextView mPriceView;
        @BindView(R.id.city)
        TextView mCityView;
        @BindView(R.id.overflow)
        View mOverflowView;

        public MachineViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.overflow)
        public void showMenu(final View view) {
            final BaseApp baseApp = (BaseApp) view.getContext().getApplicationContext();
            final AccountRepository accountRepository = baseApp.getApplicationComponent().accountRepository();
            final Machine machine = (Machine) itemView.getTag();
            final PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
            popupMenu.inflate(R.menu.fragment_search_machine_list_overflow);
            final MenuItem favoriteMenuItem = popupMenu.getMenu().findItem(R.id.action_add_to_favorite);
            if (accountRepository.isLoggedIn()) {
                if (TextUtils.equals(machine.getDealerId(), accountRepository.getUserId()) || machine.isFavorite()) {
                    favoriteMenuItem.setVisible(false);
                }
            } else {
                favoriteMenuItem.setVisible(false);
            }
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.action_share:
                        final String url = view.getContext().getString(R.string.machine_share_url, machine.getAlias());
                        final Intent intent = IntentUtils.shareText("", url);
                        if (IntentUtils.isIntentAvailable(view.getContext(), intent)) {
                            view.getContext().startActivity(intent);
                        }
                        return true;
                    case R.id.action_add_to_favorite:
                        EventBus.getDefault().post(new AddToFavoriteEvent(machine));
                        return true;
                }
                return false;
            });
            popupMenu.show();
        }
    }
}
