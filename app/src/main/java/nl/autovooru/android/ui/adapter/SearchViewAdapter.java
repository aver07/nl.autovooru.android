package nl.autovooru.android.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import nl.autovooru.android.R;
import nl.autovooru.android.domain.model.machine.MachineFilter;

/**
 * Created by Antonenko Viacheslav on 16/12/15.
 */
public class SearchViewAdapter extends BaseAdapter<MachineFilter.Options> {

    public SearchViewAdapter(Context context, List<MachineFilter.Options> items) {
        super(context, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = getLayoutInflater().inflate(R.layout.adapter_search_filter_item, parent, false);
        }

        ((TextView) convertView).setText(getItem(position).getName());

        return convertView;
    }
}
