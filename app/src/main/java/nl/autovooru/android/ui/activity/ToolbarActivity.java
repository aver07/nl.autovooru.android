package nl.autovooru.android.ui.activity;

import android.support.annotation.CallSuper;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import nl.autovooru.android.R;

/**
 * Created by Antonenko Viacheslav on 02/10/15.
 */
public abstract class ToolbarActivity extends BaseActivity {

    private Toolbar mToolbar;

    @CallSuper
    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        initToolbar();
    }

    @CallSuper
    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        initToolbar();
    }

    @CallSuper
    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        initToolbar();
    }

    protected Toolbar getToolbar() {
        return mToolbar;
    }

    protected void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar == null) {
            throw new RuntimeException("Can't find view with id='toolbar' in specified layout of class "
                    + this.getClass().getName()
                    + ". Toolbar with such ID must be present in layout used for setting content view.");
        }
        setSupportActionBar(mToolbar);
        final ActionBar actionBar = getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

}
