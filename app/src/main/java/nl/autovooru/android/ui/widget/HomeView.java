package nl.autovooru.android.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import nl.autovooru.android.R;

/**
 * Created by Antonenko Viacheslav on 11/10/15.
 */
public class HomeView extends FrameLayout {

    @BindView(R.id.imageView)
    ImageView mImageView;
    @BindView(R.id.textView)
    TextView mTextView;

    public HomeView(Context context) {
        super(context);
        init(null, 0, 0);
    }

    public HomeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0, 0);
    }

    public HomeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr, 0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public HomeView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs, defStyleAttr, defStyleRes);
    }

    private void init(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        inflate(getContext(), R.layout.widget_home_view, this);
        ButterKnife.bind(this, this);

        final TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.HomeView, defStyleAttr, defStyleRes);
        try {
            if (typedArray != null) {
                final int indexCount = typedArray.getIndexCount();
                for (int i = 0; i < indexCount; i++) {
                    int attr = typedArray.getIndex(i);
                    switch (attr) {
                        case R.styleable.HomeView_imageSrc:
                            final Drawable d = typedArray.getDrawable(attr);
                            setImage(d);
                            break;
                        case R.styleable.HomeView_text:
                            final String text = typedArray.getString(attr);
                            setText(text);
                            break;
                        case R.styleable.HomeView_textPaddingLeft:
                            final int paddingLeft = (int) typedArray.getDimension(attr, 0);
                            mTextView.setPadding(paddingLeft, mTextView.getPaddingTop(), mTextView.getPaddingRight(), mTextView.getPaddingBottom());
                            break;
                    }
                }
            }
        } finally {
            if (typedArray != null) {
                typedArray.recycle();
            }
        }
    }

    public void setText(String text) {
        mTextView.setText(text);
    }

    public void setText(@StringRes int resId) {
        mTextView.setText(resId);
    }

    public void setImage(@DrawableRes int resId) {
        mImageView.setImageResource(resId);
    }

    public void setImage(@Nullable Drawable drawable) {
        mImageView.setImageDrawable(drawable);
    }
}
