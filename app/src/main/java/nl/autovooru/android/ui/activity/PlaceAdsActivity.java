package nl.autovooru.android.ui.activity;

import android.content.Intent;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jakewharton.rxbinding.widget.RxCompoundButton;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;
import icepick.State;
import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.FractionalTouchDelegate;
import nl.autovooru.android.common.utils.IntentUtils;
import nl.autovooru.android.common.utils.StringUtils;
import nl.autovooru.android.di.HasComponent;
import nl.autovooru.android.di.components.DaggerPlaceAdsComponent;
import nl.autovooru.android.di.components.PlaceAdsComponent;
import nl.autovooru.android.domain.model.advert.AdvertFields;
import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.model.machine.MachineFilter;
import nl.autovooru.android.exceptions.ErrorUtils;
import nl.autovooru.android.mvp.presenter.PlaceAdsPresenter;
import nl.autovooru.android.mvp.view.PlaceAdsView;
import nl.autovooru.android.ui.adapter.PlaceAdsPhotoAdapter;
import nl.autovooru.android.ui.widget.SearchFilterView;
import nl.autovooru.android.ui.widget.recyclerview.DividerItemDecoration;
import nl.autovooru.android.ui.widget.recyclerview.OnStartDragListener;
import nl.autovooru.android.ui.widget.recyclerview.SimpleItemTouchHelperCallback;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Antonenko Viacheslav on 09/01/16.
 */
public class PlaceAdsActivity extends BaseLceActivity<NestedScrollView, AdvertFields, PlaceAdsView, PlaceAdsPresenter>
        implements PlaceAdsView, SwipeRefreshLayout.OnRefreshListener, HasComponent<PlaceAdsComponent>, OnStartDragListener {

    private static final int IMAGE_REQUEST_CODE = 1001;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.swipe_to_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.ad_title)
    EditText mAdTitleView;
    @BindView(R.id.ad_text)
    EditText mAdTextView;
    @BindView(R.id.token)
    EditText mTokenView;
    @BindView(R.id.price)
    EditText mPriceView;
    @BindView(R.id.promo_price)
    EditText mPromoPriceView;
    @BindView(R.id.doors)
    EditText mDoorsView;
    @BindView(R.id.mileage)
    EditText mMileageView;
    @BindView(R.id.engine_capacity)
    EditText mEngineCapacityView;
    @BindView(R.id.category)
    TextView mCategoryView;
    @BindView(R.id.mark)
    TextView mMarkView;
    @BindView(R.id.model)
    TextView mModelView;
    @BindView(R.id.year)
    TextView mYearView;
    @BindView(R.id.body)
    TextView mBodyView;
    @BindView(R.id.fuel)
    TextView mFuelView;
    @BindView(R.id.color)
    TextView mColorView;
    @BindView(R.id.transmission)
    TextView mTransmissionView;
    @BindView(R.id.first_owner)
    CheckBox mFirstOwnerView;
    @BindView(R.id.nap)
    CheckBox mNapView;
    @BindView(R.id.other_filters_wrapper)
    LinearLayout mOtherFiltersWrapper;
    @BindView(R.id.youtube_image_wrapper)
    View mYoutubeImageWrapperView;
    @BindView(R.id.youtube_image)
    ImageView mYoutubeImageView;
    @BindView(R.id.youtube_link)
    TextView mYoutubeLinkView;
    @BindView(R.id.delete_youtube_link)
    View mDeleteYoutubeLinkView;
    @BindView(R.id.photos)
    RecyclerView mPhotosView;
    Unbinder mUnbinder;
    @State
    ArrayList<String> mImages;
    @State
    int mCoverPhotoIndex;
    private MaterialDialog mProgressDialog;
    private CompositeSubscription mSubscriptions;
    private PlaceAdsPhotoAdapter mAdapter;
    private ItemTouchHelper mItemTouchHelper;

    @Override
    public PlaceAdsPresenter createPresenter() {
        return getComponent().providePlaceAdsPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_ads);

        setRetainInstance(true);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        initializeToolbar();
        initializePhotos();

        mSubscriptions = new CompositeSubscription();
        mSubscriptions.add(RxTextView.afterTextChangeEvents(mAdTitleView)
                        .subscribe(event -> getPresenter().setTitle(event.editable().toString()))
        );
        mSubscriptions.add(RxTextView.afterTextChangeEvents(mAdTextView)
                .subscribe(event -> getPresenter().setDescription(event.editable().toString())));
        mSubscriptions.add(RxTextView.afterTextChangeEvents(mTokenView)
                .subscribe(event -> getPresenter().setToken(event.editable().toString())));
        mSubscriptions.add(RxTextView.afterTextChangeEvents(mPriceView)
                .subscribe(event -> getPresenter().setPrice(event.editable().toString())));
        mSubscriptions.add(RxTextView.afterTextChangeEvents(mPromoPriceView)
                .subscribe(event -> getPresenter().setPromoPrice(event.editable().toString())));
        mSubscriptions.add(RxTextView.afterTextChangeEvents(mDoorsView)
                .subscribe(event -> getPresenter().setDoors(event.editable().toString())));
        mSubscriptions.add(RxTextView.afterTextChangeEvents(mMileageView)
                .subscribe(event -> getPresenter().setMileage(event.editable().toString())));
        mSubscriptions.add(RxTextView.afterTextChangeEvents(mEngineCapacityView)
                .subscribe(event -> getPresenter().setEngineCapacity(event.editable().toString())));
        mSubscriptions.add(RxCompoundButton.checkedChanges(mFirstOwnerView)
                .subscribe(event -> getPresenter().setFirstOwner(event)));
        mSubscriptions.add(RxCompoundButton.checkedChanges(mNapView)
                .subscribe(event -> getPresenter().setNap(event)));

        final RectF sourceFraction = new RectF(2f, 0f, 2f, 2f);
        FractionalTouchDelegate.setupDelegate((View) mDeleteYoutubeLinkView.getParent(),
                mDeleteYoutubeLinkView, sourceFraction);

        loadData(false);
    }

    @Override
    protected void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        if (mSubscriptions != null && !mSubscriptions.isUnsubscribed()) {
            mSubscriptions.unsubscribe();
        }
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mImages = new ArrayList<>(mAdapter.getDataSet());
        mCoverPhotoIndex = mAdapter.getCoverPhotoIndex();
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_REQUEST_CODE) {
                if (data == null || data.getData() == null) {
                    return;
                }

                String imagePath = IntentUtils.getPath(this, data.getData());
                if (!StringUtils.isEmpty(imagePath)) {
                    imagePath = "file://" + imagePath;
                    mAdapter.addItem(imagePath);
                    mPhotosView.smoothScrollToPosition(mAdapter.getItemCount() - 1);
                }
            }
        }
    }

    @OnClick(R.id.add_photo)
    void addPhoto() {
        final Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_REQUEST_CODE);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return ErrorUtils.getErrorMessage(this, e);
    }

    @Override
    public void showContent() {
        super.showContent();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        mSwipeRefreshLayout.setRefreshing(false);
        if (!ErrorUtils.handleError(this, e)) {
            super.showError(e, pullToRefresh);
        }
    }

    @Override
    public void setData(AdvertFields data) {

    }

    @Override
    public void loadData(boolean pullToRefresh) {
        getPresenter().initialLoad(pullToRefresh);
    }

    @Override
    public void onRefresh() {
        new MaterialDialog.Builder(this)
                .content(R.string.dialog_confirm_update_content)
                .positiveText(R.string.dialog_confirm_update_positive)
                .onPositive((dialog, which) -> loadData(true))
                .negativeText(R.string.dialog_confirm_update_negative)
                .onNegative((dialog1, which1) -> mSwipeRefreshLayout.setRefreshing(false))
                .cancelListener(dialog2 -> mSwipeRefreshLayout.setRefreshing(false))
                .build()
                .show();
    }

    @Override
    public void setField(EditText editText, String label, String value) {
        ((TextInputLayout) editText.getParent()).setHint(label);
        editText.setText(value);
    }

    @Override
    public void setField(EditText editText, @StringRes int label, String value) {
        setField(editText, getString(label), value);
    }

    @Override
    public void setField(TextView textView, @StringRes int label, @Nullable String value) {
        setField(textView, getString(label), value);
    }

    @Override
    public void setField(TextView textView, String label, @Nullable String value) {
        final TextAppearanceSpan labelSpan = new TextAppearanceSpan(this, R.style.MachineSearchFilter_Label);
        final SpannableStringBuilder sb = new SpannableStringBuilder(label);
        sb.setSpan(labelSpan, 0, label.length(), 0);
        sb.append("\n");
        int start = sb.length();
        final TextAppearanceSpan valueSpan = new TextAppearanceSpan(this, R.style.MachineSearchFilter_Value);
        sb.append(value);
        sb.setSpan(valueSpan, start, sb.length(), 0);
        textView.setText(sb);
    }

    @Override
    public void setMachineCategory(Filter category) {
        setField(mCategoryView, R.string.fragment_place_ads_hint_category, category.getName());
    }

    @OnClick(R.id.category)
    void showChooseCategoryDialog() {
        final List<Filter> machineCategories = getPresenter().getMachineCategories();
        final List<CharSequence> listItems = new ArrayList<>(machineCategories.size());
        for (Filter filter : machineCategories) {
            listItems.add(filter.getName());
        }
        final int selectedPosition = machineCategories.indexOf(getPresenter().getMachineCategory());
        new MaterialDialog.Builder(this)
                .title(R.string.fragment_machine_search_filters_category)
                .backgroundColor(ContextCompat.getColor(this, R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final Filter machineCategory = machineCategories.get(position);
                        getPresenter().setMachineCategories(machineCategory);
                    }
                    return true;
                })
                .build()
                .show();
    }

    @Override
    public void setTitle(String label, String title) {
        setField(mAdTitleView, label, title);
    }

    @Override
    public void setText(String label, String text) {
        setField(mAdTextView, label, text);
    }

    @Override
    public void setMark(String label, String mark) {
        setField(mMarkView, label, mark);
    }

    @OnClick(R.id.mark)
    void showChooseMarkDialog() {
        final SearchFilterView searchFilterView = new SearchFilterView(this);

        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(getPresenter().getMarkLabel())
                .backgroundColor(ContextCompat.getColor(this, R.color.grey_100))
                .customView(searchFilterView, false)
                .backgroundColor(ContextCompat.getColor(this, R.color.grey_100))
                .build();

        searchFilterView.setOptionsList(getPresenter().getMachineMarks());
        searchFilterView.setTopOptionsList(getPresenter().getTopMarks());
        searchFilterView.setHint(getPresenter().getMarkLabel());
        searchFilterView.setSelectedItem(getPresenter().getMark());
        searchFilterView.setOnItemClickListener(item -> {
            dialog.dismiss();
            if (!item.equals(getPresenter().getMark())) {
                getPresenter().setMark(item);
            }
        });
        dialog.show();
    }

    @Override
    public void setModel(String label, String model) {
        setField(mModelView, label, model);
    }

    @OnClick(R.id.model)
    void showChooseModelDialog() {
        final SearchFilterView searchFilterView = new SearchFilterView(this);

        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(getPresenter().getModelLabel())
                .backgroundColor(ContextCompat.getColor(this, R.color.grey_100))
                .customView(searchFilterView, false)
                .backgroundColor(ContextCompat.getColor(this, R.color.grey_100))
                .build();

        searchFilterView.setOptionsList(getPresenter().getModels());
        searchFilterView.setHint(getPresenter().getModelLabel());
        searchFilterView.setSelectedItem(getPresenter().getModel());
        searchFilterView.setOnItemClickListener(item -> {
            dialog.dismiss();
            if (!item.equals(getPresenter().getModel())) {
                getPresenter().setModel(item);
            }
        });
        dialog.show();
    }

    @Override
    public void setYear(String label, String year) {
        setField(mYearView, label, year);
    }

    @OnClick(R.id.year)
    void showChooseStartYearDialog() {
        final List<MachineFilter.Options> years = getPresenter().getYears();
        final List<CharSequence> listItems = new ArrayList<>(years.size());
        for (MachineFilter.Options year : years) {
            listItems.add(year.getName());
        }

        final int selectedPosition = years.indexOf(getPresenter().getYear());
        new MaterialDialog.Builder(this)
                .title(getPresenter().getYearLabel())
                .backgroundColor(ContextCompat.getColor(this, R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options year = years.get(position);
                        getPresenter().setYear(year);
                    }
                    return true;
                })
                .build()
                .show();

    }

    @Override
    public void setToken(String label, String token) {
        setField(mTokenView, label, token);
    }

    @Override
    public void setPrice(String label, String price) {
        setField(mPriceView, label, price);
    }

    @Override
    public void setPromoPrice(String label, String promoPrice) {
        setField(mPromoPriceView, label, promoPrice);
    }

    @Override
    public void setDoors(String label, String doors) {
        setField(mDoorsView, label, doors);
    }

    @Override
    public void setFuel(String label, String fuel) {
        setField(mFuelView, label, fuel);
    }

    @OnClick(R.id.fuel)
    void showChooseFuelDialog() {
        final List<MachineFilter.Options> fuels = getPresenter().getFuels();
        final List<CharSequence> listItems = new ArrayList<>(fuels.size());
        for (MachineFilter.Options fuel : fuels) {
            listItems.add(fuel.getName());
        }

        final int selectedPosition = fuels.indexOf(getPresenter().getFuel());
        new MaterialDialog.Builder(this)
                .title(getPresenter().getFuelLabel())
                .backgroundColor(ContextCompat.getColor(this, R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options fuel = fuels.get(position);
                        getPresenter().setFuel(fuel);
                    }
                    return true;
                })
                .build()
                .show();

    }

    @Override
    public void setMileage(String label, String mileage) {
        setField(mMileageView, label, mileage);
    }

    @Override
    public void setEngineCapacity(String label, String engineCapacity) {
        setField(mEngineCapacityView, label, engineCapacity);
    }

    @Override
    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new MaterialDialog.Builder(this)
                    .content(R.string.dialog_progress_content)
                    .progress(true, 0).build();
        }
        mProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.hide();
        }
    }

    @Override
    public void setTransmissions(String label, String transmissionList) {
        setField(mTransmissionView, label, transmissionList);
    }

    @OnClick(R.id.transmission)
    void showChooseTransmissionDialog() {
        final List<MachineFilter.Options> offerTimetransmissionListist = getPresenter().getTransmissionList();
        final List<CharSequence> listItems = new ArrayList<>(offerTimetransmissionListist.size());
        for (MachineFilter.Options transmission : offerTimetransmissionListist) {
            listItems.add(transmission.getName());
        }

        final int selectedPosition = offerTimetransmissionListist.indexOf(getPresenter().getTransmission());
        new MaterialDialog.Builder(this)
                .title(getPresenter().getTransmissionLabel())
                .backgroundColor(ContextCompat.getColor(this, R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options transmission = offerTimetransmissionListist.get(position);
                        getPresenter().setTransmission(transmission);
                    }
                    return true;
                })
                .build()
                .show();
    }

    @Override
    public void setColors(String label, String colors) {
        setField(mColorView, label, colors);
    }

    @OnClick(R.id.color)
    void showChooseColorDialog() {
        final List<MachineFilter.Options> colors = getPresenter().getColors();
        final List<CharSequence> listItems = new ArrayList<>(colors.size());
        for (MachineFilter.Options color : colors) {
            listItems.add(color.getName());
        }

        final int selectedPosition = colors.indexOf(getPresenter().getColor());
        new MaterialDialog.Builder(this)
                .title(getPresenter().getColorLabel())
                .backgroundColor(ContextCompat.getColor(this, R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options color = colors.get(position);
                        getPresenter().setColor(color);
                    }
                    return true;
                })
                .build()
                .show();
    }

    @Override
    public void setBody(String label, String body) {
        setField(mBodyView, label, body);
    }

    @OnClick(R.id.body)
    void showChooseBodyDialog() {
        final List<MachineFilter.Options> bodyList = getPresenter().getBodyList();
        final List<CharSequence> listItems = new ArrayList<>(bodyList.size());
        for (MachineFilter.Options body : bodyList) {
            listItems.add(body.getName());
        }

        final int selectedPosition = bodyList.indexOf(getPresenter().getBody());
        new MaterialDialog.Builder(this)
                .title(getPresenter().getBodyLabel())
                .backgroundColor(ContextCompat.getColor(this, R.color.grey_100))
                .items(listItems.toArray(new CharSequence[listItems.size()]))
                .itemsCallbackSingleChoice(selectedPosition, (materialDialog, view, position, charSequence) -> {
                    if (selectedPosition != position) {
                        final MachineFilter.Options body = bodyList.get(position);
                        getPresenter().setBody(body);
                    }
                    return true;
                })
                .build()
                .show();
    }

    @Override
    public void setFirstOwner(String label, boolean isFirstOwner) {
        mFirstOwnerView.setText(label);
        mFirstOwnerView.setChecked(isFirstOwner);
    }

    @Override
    public void setNap(String label, boolean nap) {
        mNapView.setText(label);
        mNapView.setChecked(nap);
    }

    @Override
    public void setOtherFilter(String filterKey, String label, List<MachineFilter.Options> values) {
        TextView filterView = (TextView) mOtherFiltersWrapper.findViewWithTag(filterKey);
        if (filterView == null) {
            final LayoutInflater inflater = getLayoutInflater();
            filterView = (TextView) inflater.inflate(R.layout.include_search_field, mOtherFiltersWrapper, false);
            filterView.setTag(filterKey);
            filterView.setOnClickListener(v -> {
                final List<MachineFilter.Options> allOtherFilters = getPresenter().getAllOtherFilters(filterKey);
                final List<CharSequence> listItems = new ArrayList<>(allOtherFilters.size());
                for (MachineFilter.Options option : allOtherFilters) {
                    listItems.add(option.getName());
                }

                final Integer[] selectedPosition = getPresenter().getOtherFiltersSelectedPositions(filterKey);
                new MaterialDialog.Builder(this)
                        .title(getPresenter().getOtherFiltersLabel(filterKey))
                        .backgroundColor(ContextCompat.getColor(this, R.color.grey_100))
                        .items(listItems.toArray(new CharSequence[listItems.size()]))
                        .itemsCallbackMultiChoice(selectedPosition, (materialDialog, integers, charSequences) -> {
                            getPresenter().setOtherFilter(filterKey, integers);
                            return true;
                        })
                        .positiveText(android.R.string.ok)
                        .build()
                        .show();
            });
            mOtherFiltersWrapper.addView(filterView);
        }

        setField(filterView, label, TextUtils.join(", ", values));
    }

    @Override
    public void removeOtherFilter(String key) {
        final View view = mOtherFiltersWrapper.findViewWithTag(key);
        mOtherFiltersWrapper.removeView(view);
    }

    @OnClick(R.id.get_youtube_info)
    void getYoutubeInfo() {
        new MaterialDialog.Builder(this)
                .title(R.string.fragment_place_ads_label_youtube_info)
                .input("", "", false, (dialog, input) -> {
                    if (!StringUtils.isEmpty(input)) {
                        getPresenter().getYoutubeInfo(input.toString());
                    }
                })
                .build()
                .show();
    }

    @OnClick(R.id.delete_youtube_link)
    void removeYoutubeLink() {
        getPresenter().removeYoutubeInfo();
    }

    @Override
    public void showYoutubeInfo(String imageUrl, String youtubeLink) {
        Picasso.with(this)
                .load(imageUrl)
                .fit()
                .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                .into(mYoutubeImageView);
        mYoutubeLinkView.setText(youtubeLink);

        mYoutubeImageWrapperView.setVisibility(View.VISIBLE);
        mYoutubeLinkView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideYoutubeInfo() {
        mYoutubeImageWrapperView.setVisibility(View.GONE);
        mYoutubeLinkView.setVisibility(View.GONE);
    }

    @OnClick(R.id.get_auto_info)
    void getAutoInfoByToken() {
        new MaterialDialog.Builder(this)
                .title(R.string.fragment_place_ads_label_auto_info_by_token)
                .input("", "", false, (dialog, input) -> {
                    if (!StringUtils.isEmpty(input)) {
                        getPresenter().loadAutoInfoByToken(input);
                    }
                })
                .build()
                .show();
    }

    @OnClick(R.id.apply)
    void apply() {
        final List<String> images = new ArrayList<>(mAdapter.getDataSet().size());
        images.addAll(mAdapter.getDataSet());
        if (mAdapter.getCoverPhotoIndex() > 0 && mAdapter.getCoverPhotoIndex() < mAdapter.getDataSet().size()) {
            Collections.swap(images, 0, mAdapter.getCoverPhotoIndex());
        }
        getPresenter().sendAdvert(images);
    }

    @OnClick(R.id.clear)
    void resetFields() {
        getPresenter().resetFields();
    }

    @Override
    public void placeAdsComplete() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void showErrorDialog(@StringRes int resId) {
        new MaterialDialog.Builder(this)
                .content(resId)
                .positiveText(R.string.commons_dialog_ok)
                .build()
                .show();
    }

    private void initializeToolbar() {
        setSupportActionBar(mToolbar);
        final ActionBar actionBar = getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.fragment_user_dashboard_place_ad);
    }

    private void initializePhotos() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mPhotosView.setLayoutManager(layoutManager);
        final DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL_LIST);
        dividerItemDecoration.setDivider(ContextCompat.getDrawable(this, R.drawable.test_divider));
        mPhotosView.addItemDecoration(dividerItemDecoration);
        mPhotosView.setHasFixedSize(true);
        mAdapter = new PlaceAdsPhotoAdapter(mPhotosView, this);
        if (mImages != null) {
            mAdapter.resetItems(mImages, mCoverPhotoIndex);
            mAdapter.enableLoadingMore(false);
        }
        mPhotosView.setAdapter(mAdapter);

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mPhotosView);
    }

    @Override
    public PlaceAdsComponent getComponent() {
        return DaggerPlaceAdsComponent.builder()
                .activityModule(getActivityModule())
                .applicationComponent(getApplicationComponent())
                .build();
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }
}
