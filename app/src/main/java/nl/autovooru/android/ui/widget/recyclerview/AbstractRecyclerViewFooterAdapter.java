package nl.autovooru.android.ui.widget.recyclerview;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import nl.autovooru.android.R;

/**
 * Created by Antonenko Viacheslav on 02/12/15.
 */
public abstract class AbstractRecyclerViewFooterAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VISIBLE_THRESHOLD = 5;

    private static final int ITEM_VIEW_TYPE_BASIC = 0;
    private static final int ITEM_VIEW_TYPE_FOOTER = 1;
    private static final int ITEM_VIEW_TYPE_HEADER = 2;

    private List<T> mDataSet = new ArrayList<>();

    private int mFirstVisibleItem = 0;
    private int mVisibleItemCount = 0;
    private int mTotalItemCount = 0;
    private int mPreviousTotal = 0;
    private boolean mLoading = false;
    private boolean mLoadingMoreEnabled = true;

    public AbstractRecyclerViewFooterAdapter(RecyclerView recyclerView, final OnLoadMoreListener onLoadMoreListener) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (!mLoadingMoreEnabled) {
                        return;
                    }
                    mTotalItemCount = layoutManager.getItemCount();
                    mVisibleItemCount = layoutManager.getChildCount();
                    mFirstVisibleItem = layoutManager.findFirstVisibleItemPosition();

                    if (mLoading) {
                        if (mTotalItemCount > mPreviousTotal) {
                            mLoading = false;
                            mPreviousTotal = mTotalItemCount;
                        }
                    }

                    if (!mLoading && (mTotalItemCount - mVisibleItemCount)
                            <= (mFirstVisibleItem + VISIBLE_THRESHOLD)) {
                        // End has been reached

                        addItem(null);
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                            mLoading = true;
                        }
                    }
                }
            });
        }
    }

    public void enableLoadingMore(boolean enable) {
        mLoadingMoreEnabled = enable;
    }

    public void resetItems(@NonNull List<T> newDataSet) {
        mLoadingMoreEnabled = true;
        mLoading = false;
        mFirstVisibleItem = 0;
        mVisibleItemCount = 0;
        mTotalItemCount = 0;
        mPreviousTotal = 0;

        mDataSet.clear();
        addItems(newDataSet);
    }

    public void addItems(@NonNull List<T> newDataSetItems) {
        mDataSet.addAll(newDataSetItems);
        notifyDataSetChanged();
    }

    public void addItem(T item) {
        if (!mDataSet.contains(item)) {
            mDataSet.add(item);
            notifyItemInserted(mDataSet.size() - 1);
        }
    }

    public void removeItem(T item) {
        int indexOfItem = mDataSet.indexOf(item);
        if (indexOfItem != -1) {
            mDataSet.remove(indexOfItem);
            notifyItemRemoved(indexOfItem);
        }
    }

    public void removeItem(int position) {
        if (position >= 0) {
            mDataSet.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Nullable
    public T getItem(int index) {
        if (mDataSet != null && mDataSet.get(index) != null) {
            return mDataSet.get(index);
        } else {
            return null;
        }
    }

    public List<T> getDataSet() {
        return mDataSet;
    }

    @Override
    public int getItemViewType(int position) {
        return mDataSet.get(position) != null ? ITEM_VIEW_TYPE_BASIC : ITEM_VIEW_TYPE_FOOTER;
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_VIEW_TYPE_BASIC) {
            return onCreateBasicItemViewHolder(parent, viewType);
        } else if (viewType == ITEM_VIEW_TYPE_FOOTER) {
            return onCreateFooterViewHolder(parent, viewType);
        } else {
            throw new IllegalStateException("Invalid type, this type of items " + viewType + " can't be handled");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == ITEM_VIEW_TYPE_BASIC) {
            onBindBasicItemView(holder, position);
        } else {
            onBindFooterView(holder, position);
        }
    }

    public abstract RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int position);

    protected abstract void onBindBasicItemView(RecyclerView.ViewHolder holder, int position);

    private RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_progress, parent, false);
        return new ProgressViewHolder(view);
    }

    private void onBindFooterView(RecyclerView.ViewHolder holder, int position) {
        ((ProgressViewHolder) holder).mProgressBar.setIndeterminate(true);
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.progressBar)
        public ProgressBar mProgressBar;

        public ProgressViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
