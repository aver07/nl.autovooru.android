package nl.autovooru.android.ui.fragments;

import android.text.TextUtils;

import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import nl.autovooru.android.R;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
@FragmentWithArgs
public class DealerSearchMachineListFragment extends SearchMachineListFragment {

    @Arg
    String mDealerType;
    @Arg(required = false)
    String mDealerName;

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();

        if (TextUtils.equals(mDealerType, "org")) {
            mToolbar.setTitle(mDealerName);
        } else {
            mToolbar.setTitle(getString(R.string.fragment_machine_search_filters_private_dealer));
        }
    }

    @Override
    protected void updateCounter(MachineListViewModel data) {
        if (TextUtils.equals(mDealerType, "org")) {
            mToolbar.setTitle(mDealerName);
            mMachineCounterView.setText(getString(R.string.fragment_search_machine_machine_organization_count, mDealerName, data.getTotal()));
        } else {
            mToolbar.setTitle(getString(R.string.fragment_machine_search_filters_private_dealer));
            super.updateCounter(data);
        }
    }
}
