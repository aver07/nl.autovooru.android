package nl.autovooru.android.ui.adapter;

import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.autovooru.android.R;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.ui.activity.MachineDetailsActivity;
import nl.autovooru.android.ui.widget.recyclerview.AbstractRecyclerViewFooterAdapter;
import nl.autovooru.android.ui.widget.recyclerview.OnLoadMoreListener;
import nl.autovooru.android.utils.MachineUtils;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public class MyFavoritesAdapter extends AbstractRecyclerViewFooterAdapter<Machine> {

    private final OnOverflowActionListener mOverflowActionListener;

    public MyFavoritesAdapter(RecyclerView recyclerView, final OnLoadMoreListener onLoadMoreListener, OnOverflowActionListener overflowActionListener) {
        super(recyclerView, onLoadMoreListener);
        mOverflowActionListener = overflowActionListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int position) {
        final int layoutRes = R.layout.list_item_search_machine_grid;
        final View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new MachineViewHolder(view, mOverflowActionListener);
    }

    @Override
    protected void onBindBasicItemView(RecyclerView.ViewHolder holder, int position) {
        final MachineViewHolder viewHolder = (MachineViewHolder) holder;
        final Machine machine = getItem(position);
        final List<String> images = machine.getImages();
        if (images != null && images.size() > 0) {
            Picasso.with(holder.itemView.getContext())
                    .load(images.get(0))
                    .fit()
                    .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                    .into(viewHolder.mImageView);
        } else {
            viewHolder.mImageView.setImageDrawable(null);
        }

        viewHolder.mModelView.setText(machine.getName());
        viewHolder.mDetailsView.setText(String.format("%1$s / %2$skm / %3$s",
                machine.getReleaseYear(),
                machine.getMileage(),
                machine.getFuel()));

        final String price = MachineUtils.getPrice(viewHolder.itemView.getContext(), machine);
        viewHolder.mPriceView.setText(price);
        viewHolder.mCityView.setText(machine.getCity());

        viewHolder.itemView.setTag(machine);
    }

    public interface OnOverflowActionListener {
        void removeFromFavorite(Machine machine);

        void sendMessage(Machine machine);

        void placeBid(Machine machine);
    }

    static class MachineViewHolder extends RecyclerView.ViewHolder {

        private final OnOverflowActionListener mOverflowActionListener;
        @BindView(R.id.image)
        ImageView mImageView;
        @BindView(R.id.details)
        TextView mDetailsView;
        @BindView(R.id.model)
        TextView mModelView;
        @BindView(R.id.price)
        TextView mPriceView;
        @BindView(R.id.city)
        TextView mCityView;

        public MachineViewHolder(View itemView, OnOverflowActionListener overflowActionListener) {
            super(itemView);
            mOverflowActionListener = overflowActionListener;
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.image)
        void openDetailPage() {
            final Machine machine = (Machine) itemView.getTag();
            MachineDetailsActivity.openActivity(itemView.getContext(), machine.getMachineId());
        }

        @OnClick(R.id.overflow)
        public void showMenu(final View view) {
            final Machine machine = (Machine) itemView.getTag();
            final PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
            popupMenu.inflate(R.menu.fragment_my_favorites_list_item_overflow);
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.action_place_bid:
                        mOverflowActionListener.placeBid(machine);
                        return true;
                    case R.id.action_send_message:
                        mOverflowActionListener.sendMessage(machine);
                        return true;
                    case R.id.action_delete:
                        mOverflowActionListener.removeFromFavorite(machine);
                        return true;
                }
                return false;
            });
            popupMenu.show();
        }
    }
}
