package nl.autovooru.android.ui.widget.recyclerview;

/**
 * Created by Antonenko Viacheslav on 09/01/16.
 */
public interface ItemTouchHelperAdapter {

    boolean onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}
