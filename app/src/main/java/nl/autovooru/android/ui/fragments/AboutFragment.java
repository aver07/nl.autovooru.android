package nl.autovooru.android.ui.fragments;

import android.animation.ValueAnimator;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.AnimationUtils;
import nl.autovooru.android.common.utils.SchedulingUtils;
import nl.autovooru.android.common.utils.StringUtils;
import nl.autovooru.android.di.components.UseCaseComponent;
import nl.autovooru.android.mvp.presenter.AboutPresenter;
import nl.autovooru.android.mvp.view.AboutView;
import nl.autovooru.android.ui.activity.BaseActivity;
import nl.autovooru.android.utils.PlayServicesUtils;

/**
 * Created by Antonenko Viacheslav on 08/12/15.
 */
public class AboutFragment extends BaseMvpFragment<AboutView, AboutPresenter> implements AboutView {

    Unbinder mUnbinder;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @Nullable
    DrawerLayout mDrawerLayout;
    @BindView(R.id.operation_time)
    View mOperationTimeView;
    @BindView(R.id.operation_time_label)
    TextView mOperationTimeLabelView;
    @BindView(R.id.fab)
    FloatingActionsMenu mFloatingActionsMenu;
    @BindView(R.id.content_foreground)
    View mForegroundView;
    @BindView(R.id.toolbar_foreground)
    View mToolbarForegroundView;

    private ValueAnimator mOperationTimeAnimator;

    public static Fragment newInstance() {
        return new AboutFragment();
    }

    @Override
    public AboutPresenter createPresenter() {
        return getComponent(UseCaseComponent.class).provideAboutPresenter();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_about;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeToolbar();
        initializeFAB();

        SchedulingUtils.doPreDraw(mOperationTimeView, () -> {
            mOperationTimeView.setVisibility(View.GONE);
            mOperationTimeLabelView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
            final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            mOperationTimeView.measure(widthSpec, heightSpec);

            mOperationTimeAnimator = AnimationUtils.slideAnimator(mOperationTimeView, 0, mOperationTimeView.getMeasuredHeight());
        });
    }

    private void initializeFAB() {
        addMenu(getString(R.string.fab_website), R.drawable.ic_fab_website, v -> getPresenter().openWebsite());
        addMenu(getString(R.string.fab_support), R.drawable.ic_fab_email, v -> getPresenter().sendEmailToSupport());
        addMenu(getString(R.string.fab_send_feedback), R.drawable.ic_fab_send_message, v -> {
            mFloatingActionsMenu.collapse();
            showSendMessageDialog();
        });
        addMenu(getString(R.string.config_company_phone_number), R.drawable.ic_fab_call, v -> getPresenter().callToOffice());

        mFloatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                updateForeground();
            }

            @Override
            public void onMenuCollapsed() {
                updateForeground();
            }
        });
        updateForeground();
    }

    @Override
    public void onDestroyView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    @OnClick(R.id.open_map)
    void openMap() {
        if (PlayServicesUtils.checkGooglePlayServices(getActivity())) {
            getPresenter().openMap();
        }
    }

    @OnClick(R.id.open_advertising_info)
    void openAdvertisingInfos() {
        getPresenter().openAdvertisingInfos();
    }

    @OnClick(R.id.open_subscriptions_info)
    void openSubscriptionsInfos() {
        getPresenter().openSubscriptionsInfos();
    }

    @OnClick(R.id.operation_time_label)
    void showOrHideOperationTime() {
        if (mOperationTimeView.getVisibility() == View.GONE) {
            mOperationTimeLabelView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_up, 0);
            AnimationUtils.expand(mOperationTimeView, mOperationTimeAnimator);
        } else {
            mOperationTimeLabelView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
            AnimationUtils.collapse(mOperationTimeView);
        }
    }

    @Override
    public void addMenu(@NonNull String title, @DrawableRes int iconResId, View.OnClickListener listener) {
        final FloatingActionButton menuItem = new FloatingActionButton(getActivity());
        menuItem.setSize(FloatingActionButton.SIZE_MINI);
        menuItem.setColorNormalResId(R.color.colorAccent);
        menuItem.setColorPressedResId(R.color.colorAccent);
        menuItem.setIcon(iconResId);
        menuItem.setTitle(title);
        menuItem.setOnClickListener(listener);
        mFloatingActionsMenu.addButton(menuItem);
    }

    private void updateForeground() {
        if (mFloatingActionsMenu.isExpanded()) {
            mForegroundView.setClickable(true);
            mForegroundView.setBackgroundResource(R.color.black_transparency_30);
            mToolbarForegroundView.setClickable(true);
            mToolbarForegroundView.setBackgroundResource(R.color.black_transparency_30);
        } else {
            mForegroundView.setClickable(false);
            mForegroundView.setBackgroundColor(Color.TRANSPARENT);
            mToolbarForegroundView.setClickable(false);
            mToolbarForegroundView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private void initializeToolbar() {
        final BaseActivity activity = (BaseActivity) getActivity();
        activity.setSupportActionBar(mToolbar);
        final ActionBar actionBar = activity.getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.fragment_about_title);

        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        if (mDrawerLayout != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    getActivity(), mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            mDrawerLayout.setDrawerListener(toggle);
            toggle.syncState();
        }
    }

    private void showSendMessageDialog() {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_feedback_title)
                .customView(R.layout.dialog_send_feedback, true)
                .autoDismiss(false)
                .positiveText(R.string.dialog_feedback_positive_button)
                .onPositive((materialDialog, dialogAction) -> {
                    final View customView = materialDialog.getCustomView();

                    TextView subjectView = ButterKnife.findById(customView, R.id.subject);
                    TextView nameView = ButterKnife.findById(customView, R.id.name);
                    TextView emailView = ButterKnife.findById(customView, R.id.email);
                    TextView messageView = ButterKnife.findById(customView, R.id.message);
                    String errorMsg = subjectView.getResources().getString(R.string.dialog_feedback_error_message);

                    final String subject = subjectView.getText().toString();
                    if (StringUtils.isEmpty(subject)) {
                        ((TextInputLayout) subjectView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String name = nameView.getText().toString();
                    if (StringUtils.isEmpty(name)) {
                        ((TextInputLayout) nameView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String email = emailView.getText().toString();
                    if (StringUtils.isEmpty(email)) {
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    if (!email.contains("@")) {
                        errorMsg = getString(R.string.error_wrong_email_format);
                        ((TextInputLayout) emailView.getParent()).setError(errorMsg);
                        return;
                    }

                    final String message = messageView.getText().toString();
                    if (StringUtils.isEmpty(subject)) {
                        ((TextInputLayout) messageView.getParent()).setError(errorMsg);
                        return;
                    }

                    getPresenter().sendFeedback(
                            subject,
                            name,
                            email,
                            message);

                    materialDialog.dismiss();
                })
                .build();

        if (getPresenter().isLoggedIn()) {
            TextView nameView = ButterKnife.findById(dialog.getCustomView(), R.id.name);
            TextView emailView = ButterKnife.findById(dialog.getCustomView(), R.id.email);
            nameView.setText(getPresenter().getUsername());
            emailView.setText(getPresenter().getUserEmail());
        }

        dialog.show();
    }
}
