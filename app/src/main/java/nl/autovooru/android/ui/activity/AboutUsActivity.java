package nl.autovooru.android.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import nl.autovooru.android.R;
import nl.autovooru.android.di.HasComponent;
import nl.autovooru.android.di.components.DaggerUseCaseComponent;
import nl.autovooru.android.di.components.UseCaseComponent;
import nl.autovooru.android.di.modules.UseCaseModule;

/**
 * Created by Antonenko Viacheslav on 26/01/16.
 */
public class AboutUsActivity extends BaseActivity implements HasComponent<UseCaseComponent> {

    public static void openActivity(@NonNull Context context) {
        context.startActivity(new Intent(context, AboutUsActivity.class));
    }

    private UseCaseComponent mUseCaseComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
    }

    @Override
    public UseCaseComponent getComponent() {
        if (mUseCaseComponent == null) {
            mUseCaseComponent = DaggerUseCaseComponent.builder()
                    .applicationComponent(getApplicationComponent())
                    .activityModule(getActivityModule())
                    .useCaseModule(new UseCaseModule())
                    .build();
        }
        return mUseCaseComponent;
    }
}
