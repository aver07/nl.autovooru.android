package nl.autovooru.android.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import nl.autovooru.android.R;
import nl.autovooru.android.domain.model.machine.MachineFilter;
import nl.autovooru.android.ui.adapter.SearchViewAdapter;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Antonenko Viacheslav on 16/12/15.
 */
public class SearchFilterView extends FrameLayout {

    @BindView(R.id.search)
    EditText mSearchView;
    @BindView(R.id.top_marks)
    LinearLayout mTopMarksView;
    @BindView(R.id.list)
    ListView mModelsList;
    private CompositeSubscription mSubscriptions = new CompositeSubscription();
    private List<MachineFilter.Options> mOptionsList;
    @Nullable
    private MachineFilter.Options mSelectedItem;
    private OnItemClickListener mOnItemClickListener;

    public SearchFilterView(Context context) {
        super(context);
        init();
    }

    public SearchFilterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SearchFilterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SearchFilterView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_search_filter, this);

        ButterKnife.bind(this);
    }

    public void setTopOptionsList(@Nullable List<MachineFilter.Options> topOptionsList) {
        if (topOptionsList != null && !topOptionsList.isEmpty()) {
            mTopMarksView.setVisibility(View.VISIBLE);
            final Picasso picasso = Picasso.with(getContext());

            final int itemsCount = topOptionsList.size();
            for (int i = 0, size = mTopMarksView.getChildCount(); i < size; i++) {
                LinearLayout linearLayout = (LinearLayout) mTopMarksView.getChildAt(i);
                final int childCount = linearLayout.getChildCount();

                for (int childIdx = 0, itemIdx = i * childCount; childIdx < childCount; childIdx++, itemIdx++) {
                    final ImageView imageView = (ImageView) linearLayout.getChildAt(childIdx);
                    if (itemIdx < itemsCount) {
                        imageView.setVisibility(View.VISIBLE);
                        final MachineFilter.Options options = topOptionsList.get(itemIdx);
                        imageView.setTag(options);
                        imageView.setOnClickListener(v -> {
                            final int position = mOptionsList.indexOf(v.getTag());
                            if (position != -1 && mOnItemClickListener != null) {
                                mOnItemClickListener.OnItemClick(mOptionsList.get(position));
                            }
                        });
                        picasso.load(options.getName()).into(imageView);
                    } else {
                        imageView.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    public void setOptionsList(List<MachineFilter.Options> optionsList) {
        mOptionsList = optionsList;
    }

    public void setHint(String hint) {
        mSearchView.setHint(getContext().getString(R.string.fragment_machine_search_filters_search, hint));
    }

    public void setSelectedItem(@Nullable MachineFilter.Options selectedItem) {
        mSelectedItem = selectedItem;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        RxTextView.afterTextChangeEvents(mSearchView)
                .flatMap(event ->
                        Observable.from(mOptionsList).filter(options ->
                                        options.getName().toLowerCase()
                                                .startsWith(event.editable().toString().toLowerCase())
                        ).toList())
                .switchIfEmpty(Observable.just(new ArrayList<>()))
                .subscribe(optionses -> {
                    final int selectedPosition = optionses.indexOf(mSelectedItem);
                    mModelsList.setAdapter(new SearchViewAdapter(getContext(), optionses));
                    mModelsList.setOnItemClickListener((parent, view, position, id) -> {
                        if (mOnItemClickListener != null) {
                            mOnItemClickListener.OnItemClick(optionses.get(position));
                        }
                    });
                    if (selectedPosition != -1) {
                        mModelsList.setSelection(selectedPosition);
                    }
                });
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mSubscriptions.unsubscribe();
    }

    public interface OnItemClickListener {
        void OnItemClick(MachineFilter.Options item);
    }
}
