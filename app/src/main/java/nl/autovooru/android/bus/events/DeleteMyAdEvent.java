package nl.autovooru.android.bus.events;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public final class DeleteMyAdEvent {

    private final String mMachineId;

    public DeleteMyAdEvent(String machineId) {
        mMachineId = machineId;
    }

    public String getMachineId() {
        return mMachineId;
    }
}
