package nl.autovooru.android.bus.events;

import android.support.annotation.IdRes;
import android.support.annotation.Nullable;

/**
 * Created by Antonenko Viacheslav on 17/11/15.
 */
public class NavigationEvent {

    @IdRes
    private final int mNavigationResId;
    @Nullable
    private String mKeywords;

    public NavigationEvent(@IdRes int navigationResId, String keywords) {
        mNavigationResId = navigationResId;
        mKeywords = keywords;
    }

    @IdRes
    public int getNavigationResId() {
        return mNavigationResId;
    }

    @Nullable
    public String getKeywords() {
        return mKeywords;
    }
}
