package nl.autovooru.android.bus.events;

import nl.autovooru.android.domain.model.machine.Machine;

/**
 * Created by Antonenko Viacheslav on 24/12/15.
 */
public class AddToFavoriteEvent {

    private final Machine mMachine;

    public AddToFavoriteEvent(Machine machine) {
        mMachine = machine;
    }

    public Machine getMachine() {
        return mMachine;
    }
}
