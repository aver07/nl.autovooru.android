package nl.autovooru.android.bus.events;

import android.support.annotation.IdRes;

/**
 * Created by Antonenko Viacheslav on 29/12/15.
 */
public class NavigationSelectEvent {

    @IdRes
    private final int mNavigationResId;

    public NavigationSelectEvent(int navigationResId) {
        mNavigationResId = navigationResId;
    }

    public int getNavigationResId() {
        return mNavigationResId;
    }
}
