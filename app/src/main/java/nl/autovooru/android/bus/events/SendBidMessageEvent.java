package nl.autovooru.android.bus.events;

import nl.autovooru.android.domain.model.user.Bid;

/**
 * Created by Antonenko Viacheslav on 26/12/15.
 */
public final class SendBidMessageEvent {

    private Bid mBid;

    public SendBidMessageEvent(Bid bid) {
        mBid = bid;
    }

    public Bid getBid() {
        return mBid;
    }
}
