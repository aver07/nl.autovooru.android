package nl.autovooru.android.bus.events;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public class AuthorizationChangedEvent {

    private final boolean mIsAuthorized;

    public AuthorizationChangedEvent(boolean isAuthorized) {

        mIsAuthorized = isAuthorized;
    }

    public boolean isAuthorized() {
        return mIsAuthorized;
    }
}
