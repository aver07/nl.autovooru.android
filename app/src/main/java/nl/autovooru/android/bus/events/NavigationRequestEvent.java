package nl.autovooru.android.bus.events;

import android.support.annotation.IdRes;

/**
 * Created by Antonenko Viacheslav on 24/11/15.
 */
public class NavigationRequestEvent {
    @IdRes
    private final int mNavigationResId;
    private final String mKeyword;

    public NavigationRequestEvent(@IdRes int navigationResId) {
        this(navigationResId, null);
    }

    public NavigationRequestEvent(@IdRes int navigationResId, String keyword) {
        mNavigationResId = navigationResId;
        mKeyword = keyword;
    }

    public String getKeyword() {
        return mKeyword;
    }

    @IdRes
    public int getNavigationResId() {
        return mNavigationResId;
    }
}
