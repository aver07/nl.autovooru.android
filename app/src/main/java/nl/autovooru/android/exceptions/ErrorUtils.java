package nl.autovooru.android.exceptions;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import java.io.IOException;

import nl.autovooru.android.R;
import nl.autovooru.android.common.utils.Preconditions;
import nl.autovooru.android.data.exceptions.authorization.EmailExistsException;
import nl.autovooru.android.data.exceptions.authorization.InvalidCredentialsException;
import nl.autovooru.android.data.exceptions.authorization.InvalidEmailException;
import nl.autovooru.android.data.exceptions.authorization.InvalidFormDataException;
import nl.autovooru.android.data.exceptions.machines.InvalidYoutubeLinkException;
import nl.autovooru.android.ui.dialogs.DialogProvider;

/**
 * Created by Antonenko Viacheslav on 26/01/16.
 */
public final class ErrorUtils {

    private static final String ERROR_DIALOG_TAG = "error_dialog_tag";

    public static String getErrorMessage(@Nullable Context context, @NonNull Throwable throwable) {
        if (context == null) {
            return throwable.getMessage();
        }
        if (throwable instanceof IOException) {
            return context.getString(R.string.error_no_internet_connection);
        } else if (throwable instanceof InvalidCredentialsException) {
            return context.getString(R.string.error_invalid_credentials);
        } else if (throwable instanceof InvalidEmailException) {
            return context.getString(R.string.error_email_not_exists);
        } else if (throwable instanceof InvalidYoutubeLinkException) {
            return context.getString(R.string.error_invalid_youtube_link);
        } else if (throwable instanceof EmailExistsException) {
            return context.getString(R.string.error_email_exists);
        } else if (throwable instanceof InvalidFormDataException) {
            return context.getString(R.string.error_invalid_form_data);
        } else {
            return throwable.getMessage();
        }
    }

    public static boolean handleError(@NonNull AppCompatActivity activity, @Nullable Throwable throwable) {
        Preconditions.checkNotNull(activity);

        if (throwable instanceof InvalidCredentialsException
                || throwable instanceof InvalidEmailException
                || throwable instanceof InvalidYoutubeLinkException
                || throwable instanceof EmailExistsException
                || throwable instanceof InvalidFormDataException) {
            final String errorMessage = getErrorMessage(activity.getApplicationContext(), throwable);
            final FragmentManager fm = activity.getSupportFragmentManager();
            final DialogFragment errorDialog = DialogProvider.createErrorDialog(fm, null, errorMessage);
            errorDialog.show(fm, ERROR_DIALOG_TAG);
            return true;
        }

        return false;
    }
}
