package nl.autovooru.android;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Antonenko Viacheslav on 02/10/15.
 */
public class App extends BaseApp {

    @Override
    protected RefWatcher installLeakCanary() {
        return LeakCanary.install(this);
    }

    protected void installCrashlytics() {
        // Set up Crashlytics, disabled for debug builds
        final Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(true).build())
                .build();

        // Initialize Fabric with the debug-disabled crashlytics.
        Fabric.with(this, crashlyticsKit);
    }
}
