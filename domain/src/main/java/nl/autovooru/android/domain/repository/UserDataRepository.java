package nl.autovooru.android.domain.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;
import java.util.Map;

import nl.autovooru.android.domain.model.advert.AdvertFields;
import nl.autovooru.android.domain.model.advert.AutoInfo;
import nl.autovooru.android.domain.model.advert.YoutubeInfo;
import nl.autovooru.android.domain.model.machine.details.EditMachineDetails;
import nl.autovooru.android.domain.model.query.Order;
import nl.autovooru.android.domain.model.user.Bid;
import nl.autovooru.android.domain.model.user.MessagesPage;
import nl.autovooru.android.domain.model.user.UserInfo;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public interface UserDataRepository {

    Observable<MachineListViewModel> myAds(int page, int itemPerPage);

    Observable<Void> addToFavorite(@NonNull String machineId);

    Observable<Void> removeFromFavorite(@NonNull String machineId);

    Observable<Void> deleteMyAd(@NonNull String machineId);

    Observable<List<Bid>> getMachineBids(@NonNull String machineId);

    Observable<Void> deleteBidMessage(@NonNull String bidId);

    Observable<UserInfo> getUserData();

    Observable<MachineListViewModel> getUserFavorites(int page, int itemPerPage, Order order);

    Observable<MachineListViewModel> getUserBids(int page, int itemPerPage);

    Observable<Void> deleteAllUsersBids(@Nullable String machineId);

    Observable<Void> deleteUserBid(@NonNull String bidId);

    Observable<MessagesPage> getUserMessages(int page, int itemPerPage);

    Observable<Void> sendAnswer(@NonNull String messageId, @NonNull String subject,
                                @NonNull String name, @NonNull String email, @NonNull String message);

    Observable<Void> deleteMessage(@NonNull String messageId);

    Observable<Void> saveUserData(@NonNull UserInfo userInfo);

    Observable<AdvertFields> getAdvertFields(@NonNull String categoryId, @Nullable String markId);

    Observable<AutoInfo> getAutoInfoByToken(@NonNull String token);

    Observable<YoutubeInfo> getYoutubeInfo(@NonNull String youtubeLink);

    Observable<Void> addAuto(@NonNull Map<String, Object> fieldsValues, @Nullable List<String> images);

    Observable<Void> editAuto(@NonNull String machineId, @NonNull Map<String, Object> fieldsValues, @Nullable List<String> images);

    Observable<EditMachineDetails> getMyMachineDetails(@NonNull String machineId);

    Observable<Void> sendPushToken(@NonNull String deviceToken);
}
