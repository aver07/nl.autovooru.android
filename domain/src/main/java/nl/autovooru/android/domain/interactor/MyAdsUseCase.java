package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.repository.UserDataRepository;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 23/12/15.
 */
public class MyAdsUseCase implements UseCase {

    private static final int ITEMS_PER_PAGE = 10;
    private static final int DEFAULT_PAGE = 1;
    private final UserDataRepository mUserDataRepository;
    private int mPage = DEFAULT_PAGE;

    @Inject
    public MyAdsUseCase(@NonNull UserDataRepository userDataRepository) {
        mUserDataRepository = userDataRepository;
    }

    public Observable<MachineListViewModel> load() {
        mPage = DEFAULT_PAGE;
        return mUserDataRepository.myAds(mPage, ITEMS_PER_PAGE);
    }

    public Observable<MachineListViewModel> loadMore() {
        mPage++;
        return mUserDataRepository.myAds(mPage, ITEMS_PER_PAGE).doOnError(throwable -> {
            mPage--;
            if (mPage < 0) {
                mPage = DEFAULT_PAGE;
            }
        });
    }


}
