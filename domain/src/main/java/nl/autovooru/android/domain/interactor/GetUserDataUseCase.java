package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.model.user.UserInfo;
import nl.autovooru.android.domain.repository.UserDataRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 26/12/15.
 */
public class GetUserDataUseCase implements UseCase {

    private final UserDataRepository mRepository;

    @Inject
    public GetUserDataUseCase(@NonNull UserDataRepository repository) {
        mRepository = repository;
    }

    public Observable<UserInfo> load() {
        return mRepository.getUserData();
    }
}
