package nl.autovooru.android.domain.model.query;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public class Order {

    public Sort mSort;
    public Set<Fields> mFields = new TreeSet<>();

    public Order() {
    }

    public Order(Sort sort) {
        mSort = sort;
    }

    public Sort getSort() {
        return mSort;
    }

    public void setSort(Sort sort) {
        mSort = sort;
    }

    public Set<Fields> getFields() {
        return mFields;
    }

    public void setFields(Set<Fields> fields) {
        mFields = fields;
    }

    public void addField(Fields field) {
        mFields.add(field);
    }

    public void removeFiled(Fields fields) {
        mFields.remove(fields);
    }

    public enum Sort {
        asc, desc
    }

    public enum Fields {
        price, updated
    }
}
