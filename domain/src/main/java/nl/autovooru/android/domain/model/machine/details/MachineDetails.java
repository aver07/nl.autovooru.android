package nl.autovooru.android.domain.model.machine.details;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

import nl.autovooru.android.domain.model.machine.Machine;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
@Parcel
public final class MachineDetails {

    @Nullable
    Statistic mStatistic;
    @Nullable
    String mMachineId;
    @Nullable
    String mDealerId;
    @Nullable
    Profile mProfile;
    @Nullable
    Content mContent;
    @Nullable
    List<String> mImagesList;
    @Nullable
    Info mInfo;
    @Nullable
    Filters mFilters;
    @Nullable
    List<Machine> mSimilarAutos;
    boolean isFavorite;

    @Nullable
    public Statistic getStatistic() {
        return mStatistic;
    }

    public void setStatistic(@Nullable Statistic statistic) {
        mStatistic = statistic;
    }

    @Nullable
    public String getMachineId() {
        return mMachineId;
    }

    public void setMachineId(@Nullable String machineId) {
        mMachineId = machineId;
    }

    @Nullable
    public String getDealerId() {
        return mDealerId;
    }

    public void setDealerId(@Nullable String dealerId) {
        mDealerId = dealerId;
    }

    @Nullable
    public Profile getProfile() {
        return mProfile;
    }

    public void setProfile(@Nullable Profile profile) {
        mProfile = profile;
    }

    @Nullable
    public Content getContent() {
        return mContent;
    }

    public void setContent(@Nullable Content content) {
        mContent = content;
    }

    @NonNull
    public List<String> getImagesList() {
        if (mImagesList == null) {
            mImagesList = new ArrayList<>();
        }
        return mImagesList;
    }

    public void setImagesList(@Nullable List<String> imagesList) {
        mImagesList = imagesList;
    }

    @Nullable
    public Info getInfo() {
        return mInfo;
    }

    public void setInfo(@Nullable Info info) {
        mInfo = info;
    }

    @Nullable
    public Filters getFilters() {
        return mFilters;
    }

    public void setFilters(@Nullable Filters filters) {
        mFilters = filters;
    }

    @NonNull
    public List<Machine> getSimilarAutos() {
        if (mSimilarAutos == null) {
            mSimilarAutos = new ArrayList<>();
        }
        return mSimilarAutos;
    }

    public void setSimilarAutos(@Nullable List<Machine> similarAutos) {
        mSimilarAutos = similarAutos;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }
}
