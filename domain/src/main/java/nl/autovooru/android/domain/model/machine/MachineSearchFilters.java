package nl.autovooru.android.domain.model.machine;

import java.util.List;

/**
 * Created by Antonenko Viacheslav on 10/12/15.
 */
public final class MachineSearchFilters {

    private MainFilters mMainFilters;
    private ToggleFilters mToggleFilters;
    private List<MachineFilter> mOtherFilters;

    public MainFilters getMainFilters() {
        return mMainFilters;
    }

    public void setMainFilters(MainFilters mainFilters) {
        this.mMainFilters = mainFilters;
    }

    public ToggleFilters getToggleFilters() {
        return mToggleFilters;
    }

    public void setToggleFilters(ToggleFilters toggleFilters) {
        mToggleFilters = toggleFilters;
    }

    public List<MachineFilter> getOtherFilters() {
        return mOtherFilters;
    }

    public void setOtherFilters(List<MachineFilter> otherFilters) {
        mOtherFilters = otherFilters;
    }

    public static class MainFilters {
        private MachineFilter mTopMachineMarks;
        private MachineFilter mMachineMarks;
        private MachineFilter mMachineModels;
        private MachineFilter mMachineYear;
        private MachineFilter mPrice;
        private MachineFilter mFuel;
        private MachineFilter mUserType;
        private MachineFilter mDistance;
        private MachineFilter mMileage;
        private MachineFilter mKeywords;
        private MachineFilter mPostcode;

        public MachineFilter getTopMachineMarks() {
            return mTopMachineMarks;
        }

        public void setTopMachineMarks(MachineFilter topMachineMarks) {
            mTopMachineMarks = topMachineMarks;
        }

        public MachineFilter getMachineMarks() {
            return mMachineMarks;
        }

        public void setMachineMarks(MachineFilter machineMarks) {
            mMachineMarks = machineMarks;
        }

        public MachineFilter getMachineModels() {
            return mMachineModels;
        }

        public void setMachineModels(MachineFilter machineModels) {
            mMachineModels = machineModels;
        }

        public MachineFilter getMachineYear() {
            return mMachineYear;
        }

        public void setMachineYear(MachineFilter machineYear) {
            mMachineYear = machineYear;
        }

        public MachineFilter getPrice() {
            return mPrice;
        }

        public void setPrice(MachineFilter price) {
            mPrice = price;
        }

        public MachineFilter getFuel() {
            return mFuel;
        }

        public void setFuel(MachineFilter fuel) {
            mFuel = fuel;
        }

        public MachineFilter getUserType() {
            return mUserType;
        }

        public void setUserType(MachineFilter userType) {
            mUserType = userType;
        }

        public MachineFilter getDistance() {
            return mDistance;
        }

        public void setDistance(MachineFilter distance) {
            mDistance = distance;
        }

        public MachineFilter getMileage() {
            return mMileage;
        }

        public void setMileage(MachineFilter mileage) {
            mMileage = mileage;
        }

        public MachineFilter getKeywords() {
            return mKeywords;
        }

        public void setKeywords(MachineFilter keywords) {
            mKeywords = keywords;
        }

        public MachineFilter getPostcode() {
            return mPostcode;
        }

        public void setPostcode(MachineFilter postcode) {
            mPostcode = postcode;
        }
    }

    public static class ToggleFilters {
        private MachineFilter mOfferTime;
        private MachineFilter mTransmission;
        private MachineFilter mColor;
        private MachineFilter mBody;

        public MachineFilter getOfferTime() {
            return mOfferTime;
        }

        public void setOfferTime(MachineFilter offerTime) {
            mOfferTime = offerTime;
        }

        public MachineFilter getTransmission() {
            return mTransmission;
        }

        public void setTransmission(MachineFilter transmission) {
            mTransmission = transmission;
        }

        public MachineFilter getColor() {
            return mColor;
        }

        public void setColor(MachineFilter color) {
            mColor = color;
        }

        public MachineFilter getBody() {
            return mBody;
        }

        public void setBody(MachineFilter body) {
            mBody = body;
        }
    }
}
