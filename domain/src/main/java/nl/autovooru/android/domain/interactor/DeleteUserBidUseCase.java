package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.repository.UserDataRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public class DeleteUserBidUseCase implements UseCase {

    private final UserDataRepository mRepository;

    @Inject
    public DeleteUserBidUseCase(@NonNull UserDataRepository repository) {
        mRepository = repository;
    }

    public Observable<Void> delete(String bidId) {
        return mRepository.deleteUserBid(bidId);
    }
}
