package nl.autovooru.android.domain.model.machine.details;

import android.support.annotation.Nullable;

import org.parceler.Parcel;

/**
 * Created by Antonenko Viacheslav on 18/12/15.
 */
@Parcel
public final class Statistic {

    @Nullable
    String mHits;

    @Nullable
    public String getHits() {
        return mHits;
    }

    public void setHits(@Nullable String hits) {
        mHits = hits;
    }
}
