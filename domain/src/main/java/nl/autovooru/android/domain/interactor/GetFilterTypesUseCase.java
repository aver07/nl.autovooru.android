package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.model.filters.FilterType;
import nl.autovooru.android.domain.repository.FiltersRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 16/11/15.
 */
public class GetFilterTypesUseCase implements UseCase {

    private final FiltersRepository mFiltersRepository;

    @Inject
    public GetFilterTypesUseCase(@NonNull FiltersRepository filtersRepository) {
        mFiltersRepository = filtersRepository;
    }

    public Observable<Map<String, List<Filter>>> get(FilterType filterType) {
        final ArrayList<String> filters = new ArrayList<>();
        filters.add(filterType.getId());
        return mFiltersRepository.getFiltersByFilterType(1, 100, filters);
    }

}
