package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.model.advert.AutoInfo;
import nl.autovooru.android.domain.repository.UserDataRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 06/01/16.
 */
public class GetAutoInfoByTokenUseCase implements UseCase {

    private final UserDataRepository mRepository;

    @Inject
    public GetAutoInfoByTokenUseCase(@NonNull UserDataRepository repository) {
        mRepository = repository;
    }

    public Observable<AutoInfo> getFilters(@NonNull String token) {
        return mRepository.getAutoInfoByToken(token);
    }
}
