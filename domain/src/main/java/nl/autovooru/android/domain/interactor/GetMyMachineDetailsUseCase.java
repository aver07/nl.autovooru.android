package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.model.machine.details.EditMachineDetails;
import nl.autovooru.android.domain.repository.UserDataRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 08/01/16.
 */
public class GetMyMachineDetailsUseCase implements UseCase {

    private final UserDataRepository mRepository;

    @Inject
    public GetMyMachineDetailsUseCase(@NonNull UserDataRepository repository) {
        mRepository = repository;
    }

    public Observable<EditMachineDetails> execute(@NonNull String machineId) {
        return mRepository.getMyMachineDetails(machineId);
    }
}
