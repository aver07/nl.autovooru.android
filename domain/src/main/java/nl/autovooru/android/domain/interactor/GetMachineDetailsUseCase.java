package nl.autovooru.android.domain.interactor;

import javax.inject.Inject;

import nl.autovooru.android.domain.model.machine.details.MachineDetails;
import nl.autovooru.android.domain.repository.MachinesRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
public final class GetMachineDetailsUseCase {

    private final MachinesRepository mRepository;

    @Inject
    public GetMachineDetailsUseCase(MachinesRepository repository) {
        mRepository = repository;
    }

    public Observable<MachineDetails> getDetails(String machineId) {
        return mRepository.loadMachineDetails(machineId);
    }
}
