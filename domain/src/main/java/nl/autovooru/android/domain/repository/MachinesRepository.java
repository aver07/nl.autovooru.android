package nl.autovooru.android.domain.repository;

import java.util.Map;

import nl.autovooru.android.domain.model.machine.details.MachineDetails;
import nl.autovooru.android.domain.model.query.Order;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import rx.Observable;

/**
 * Interface that represents a Repository for getting
 * {@link nl.autovooru.android.domain.model.machine.Machine} related data.
 * <p>
 * Created by Antonenko Viacheslav on 17/11/15.
 */
public interface MachinesRepository {

    Observable<MachineListViewModel> loadMachines(int page,
                                                  int itemPerPage,
                                                  String dealerId,
                                                  Map<String, Object> filters,
                                                  Order order);

    Observable<MachineDetails> loadMachineDetails(String machineId);
}
