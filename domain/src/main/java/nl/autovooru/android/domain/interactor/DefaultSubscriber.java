package nl.autovooru.android.domain.interactor;

/**
 * Created by Antonenko Viacheslav on 16/11/15.
 */
public class DefaultSubscriber<T> extends rx.Subscriber<T> {

    @Override
    public void onCompleted() {
        // no-op by default.
    }

    @Override
    public void onError(Throwable e) {
        // no-op by default.
    }

    @Override
    public void onNext(T t) {
        // no-op by default.
    }
}
