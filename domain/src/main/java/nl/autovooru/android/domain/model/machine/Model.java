package nl.autovooru.android.domain.model.machine;

/**
 * Created by Antonenko Viacheslav on 17/11/15.
 */
public class Model {

    private String mName;

    public Model(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }
}
