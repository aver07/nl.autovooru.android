package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import java.util.HashMap;

import javax.inject.Inject;

import nl.autovooru.android.domain.model.machine.MachineSearchFilters;
import nl.autovooru.android.domain.repository.FiltersRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 10/12/15.
 */
public class GetMachineSearchFiltersUseCase implements UseCase {

    private final FiltersRepository mRepository;

    @Inject
    public GetMachineSearchFiltersUseCase(@NonNull FiltersRepository repository) {
        mRepository = repository;
    }

    public Observable<MachineSearchFilters> getFilters(String vcat, String vmark) {
        final HashMap<String, String> filters = new HashMap<>();
        filters.put("vcat", vcat);
        if (vmark != null) {
            filters.put("vmark", vmark);
        }
        return mRepository.getMachineSearchFilters(filters);
    }
}
