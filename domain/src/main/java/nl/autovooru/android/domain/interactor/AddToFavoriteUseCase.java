package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.repository.UserDataRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 24/12/15.
 */
public class AddToFavoriteUseCase implements UseCase {

    private final UserDataRepository mRepository;

    @Inject
    public AddToFavoriteUseCase(@NonNull UserDataRepository repository) {
        mRepository = repository;
    }

    public Observable<Void> addToFavorite(@NonNull String machineId) {
        return mRepository.addToFavorite(machineId);
    }
}
