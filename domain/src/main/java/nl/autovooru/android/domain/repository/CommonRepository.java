package nl.autovooru.android.domain.repository;

import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 09/12/15.
 */
public interface CommonRepository {

    Observable<Boolean> sendFeedback(
            String subject,
            String name,
            String email,
            String message
    );

    Observable<Boolean> sendMessage(
            String machineId,
            String name,
            String email,
            String subject,
            String message
    );

    Observable<Boolean> sendBid(
            String name,
            String email,
            String bid,
            String message,
            String machineId
    );

    Observable<Boolean> sendBidMessage(
            String messageId,
            String name,
            String email,
            String subject,
            String message
    );
}
