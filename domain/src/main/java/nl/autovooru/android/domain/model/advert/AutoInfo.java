package nl.autovooru.android.domain.model.advert;

import android.support.annotation.Nullable;

import org.parceler.Parcel;

/**
 * Created by Antonenko Viacheslav on 06/01/16.
 */
@Parcel
public final class AutoInfo {

    @Nullable
    Integer mCategoryId;
    @Nullable
    Integer mMarkId;
    @Nullable
    Integer mModelId;
    @Nullable
    Integer mBodyId;
    @Nullable
    Integer mFuelId;
    @Nullable
    Integer mColorId;
    @Nullable
    String mEngineCapacity;
    @Nullable
    String mToken;

    @Nullable
    public Integer getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(@Nullable Integer categoryId) {
        mCategoryId = categoryId;
    }

    @Nullable
    public Integer getMarkId() {
        return mMarkId;
    }

    public void setMarkId(@Nullable Integer markId) {
        mMarkId = markId;
    }

    @Nullable
    public Integer getModelId() {
        return mModelId;
    }

    public void setModelId(@Nullable Integer modelId) {
        mModelId = modelId;
    }

    @Nullable
    public Integer getBodyId() {
        return mBodyId;
    }

    public void setBodyId(@Nullable Integer bodyId) {
        mBodyId = bodyId;
    }

    @Nullable
    public Integer getFuelId() {
        return mFuelId;
    }

    public void setFuelId(@Nullable Integer fuelId) {
        mFuelId = fuelId;
    }

    @Nullable
    public Integer getColorId() {
        return mColorId;
    }

    public void setColorId(@Nullable Integer colorId) {
        mColorId = colorId;
    }

    @Nullable
    public String getEngineCapacity() {
        return mEngineCapacity;
    }

    public void setEngineCapacity(@Nullable String engineCapacity) {
        mEngineCapacity = engineCapacity;
    }

    @Nullable
    public String getToken() {
        return mToken;
    }

    public void setToken(@Nullable String token) {
        mToken = token;
    }
}
