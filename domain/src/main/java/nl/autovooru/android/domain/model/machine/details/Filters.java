package nl.autovooru.android.domain.model.machine.details;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

import nl.autovooru.android.domain.model.machine.MachineFilter;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
@Parcel
public final class Filters {

    @Nullable
    MachineFilter.Options mColor;
    @Nullable
    MachineFilter.Options mBody;
    @Nullable
    MachineFilter.Options mCategory;
    @Nullable
    MachineFilter.Options mFuel;
    @Nullable
    MachineFilter.Options mMark;
    @Nullable
    MachineFilter.Options mModel;
    @Nullable
    MachineFilter.Options mTransmission;
    @Nullable
    List<MachineFilter> mOtherFilters;

    @NonNull
    public List<MachineFilter> getOtherFilters() {
        if (mOtherFilters == null) {
            mOtherFilters = new ArrayList<>();
        }
        return mOtherFilters;
    }

    public void setOtherFilters(@Nullable List<MachineFilter> otherFilters) {
        mOtherFilters = otherFilters;
    }

    @Nullable
    public MachineFilter.Options getColor() {
        return mColor;
    }

    public void setColor(@Nullable MachineFilter.Options color) {
        mColor = color;
    }

    @Nullable
    public MachineFilter.Options getBody() {
        return mBody;
    }

    public void setBody(@Nullable MachineFilter.Options body) {
        mBody = body;
    }

    @Nullable
    public MachineFilter.Options getCategory() {
        return mCategory;
    }

    public void setCategory(@Nullable MachineFilter.Options category) {
        mCategory = category;
    }

    @Nullable
    public MachineFilter.Options getFuel() {
        return mFuel;
    }

    public void setFuel(@Nullable MachineFilter.Options fuel) {
        mFuel = fuel;
    }

    @Nullable
    public MachineFilter.Options getMark() {
        return mMark;
    }

    public void setMark(@Nullable MachineFilter.Options mark) {
        mMark = mark;
    }

    @Nullable
    public MachineFilter.Options getModel() {
        return mModel;
    }

    public void setModel(@Nullable MachineFilter.Options model) {
        mModel = model;
    }

    @Nullable
    public MachineFilter.Options getTransmission() {
        return mTransmission;
    }

    public void setTransmission(@Nullable MachineFilter.Options transmission) {
        mTransmission = transmission;
    }
}
