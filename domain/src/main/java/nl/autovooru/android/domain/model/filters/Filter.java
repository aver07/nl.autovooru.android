package nl.autovooru.android.domain.model.filters;

import org.parceler.Parcel;

/**
 * Created by Antonenko Viacheslav on 18/11/15.
 */
@Parcel
public class Filter {

    String mName;
    String mId;

    public Filter() {
    }

    public Filter(String id, String name) {
        mId = id;
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public String getId() {
        return mId;
    }

    @Override
    public String toString() {
        return mName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Filter filter = (Filter) o;

        return mId.equals(filter.mId);

    }

    @Override
    public int hashCode() {
        return mId.hashCode();
    }
}
