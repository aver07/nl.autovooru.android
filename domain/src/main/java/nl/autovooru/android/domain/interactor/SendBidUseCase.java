package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.repository.CommonRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 10/12/15.
 */
public final class SendBidUseCase implements UseCase {

    private final CommonRepository mRepository;

    @Inject
    public SendBidUseCase(@NonNull final CommonRepository repository) {
        mRepository = repository;
    }

    public Observable<Boolean> sendBid(String machineId, String name, String email, String bid, String message) {
        return mRepository.sendBid(name, email, bid, message, machineId);
    }
}
