package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import nl.autovooru.android.domain.model.user.Bid;
import nl.autovooru.android.domain.repository.UserDataRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public class GetMachineBidsUseCase implements UseCase {

    private final UserDataRepository mUserDataRepository;

    @Inject
    public GetMachineBidsUseCase(@NonNull UserDataRepository userDataRepository) {
        mUserDataRepository = userDataRepository;
    }

    public Observable<List<Bid>> load(@NonNull String machineId) {
        return mUserDataRepository.getMachineBids(machineId);
    }

}
