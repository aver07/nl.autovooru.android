package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import nl.autovooru.android.domain.repository.UserDataRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public class DeleteAllUsersBidsUseCase implements UseCase {

    private final UserDataRepository mRepository;

    @Inject
    public DeleteAllUsersBidsUseCase(@NonNull UserDataRepository repository) {
        mRepository = repository;
    }

    public Observable<Void> execute(@Nullable String machineId) {
        return mRepository.deleteAllUsersBids(machineId);
    }
}
