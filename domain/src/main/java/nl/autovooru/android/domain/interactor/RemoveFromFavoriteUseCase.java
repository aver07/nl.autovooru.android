package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.repository.UserDataRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 24/12/15.
 */
public class RemoveFromFavoriteUseCase implements UseCase {

    private final UserDataRepository mRepository;

    @Inject
    public RemoveFromFavoriteUseCase(@NonNull UserDataRepository repository) {
        mRepository = repository;
    }

    public Observable<Void> removeFromFavorite(@NonNull String machineId) {
        return mRepository.removeFromFavorite(machineId);
    }
}
