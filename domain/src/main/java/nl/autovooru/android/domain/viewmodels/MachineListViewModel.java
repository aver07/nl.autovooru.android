package nl.autovooru.android.domain.viewmodels;

import java.util.ArrayList;
import java.util.List;

import nl.autovooru.android.domain.model.machine.Machine;

/**
 * Created by Antonenko Viacheslav on 17/11/15.
 */
public class MachineListViewModel implements ViewModel {

    private final int mTotal;
    private final int mCurrentPage;
    private final int mTotalPages;
    private final List<Machine> mMachineList;

    public MachineListViewModel(int total, int page, int totalPages, List<Machine> machineList) {
        mTotal = total;
        mCurrentPage = page;
        mTotalPages = totalPages;
        if (machineList == null) {
            mMachineList = new ArrayList<>();
        } else {
            mMachineList = machineList;
        }
    }

    public int getTotal() {
        return mTotal;
    }

    public List<Machine> getMachineList() {
        return mMachineList;
    }

    public boolean canLoadMore() {
        return mCurrentPage < mTotalPages;
    }

}
