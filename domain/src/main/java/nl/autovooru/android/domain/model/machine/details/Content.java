package nl.autovooru.android.domain.model.machine.details;

import android.support.annotation.Nullable;

import org.parceler.Parcel;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
@Parcel
public class Content {

    @Nullable
    String mAlias;
    @Nullable
    String mName;
    @Nullable
    String mUserDescription;
    @Nullable
    String mGeneratedDescription;

    @Nullable
    public String getAlias() {
        return mAlias;
    }

    public void setAlias(@Nullable String alias) {
        mAlias = alias;
    }

    @Nullable
    public String getName() {
        return mName;
    }

    public void setName(@Nullable String name) {
        mName = name;
    }

    @Nullable
    public String getUserDescription() {
        return mUserDescription;
    }

    public void setUserDescription(@Nullable String userDescription) {
        mUserDescription = userDescription;
    }

    @Nullable
    public String getGeneratedDescription() {
        return mGeneratedDescription;
    }

    public void setGeneratedDescription(@Nullable String generatedDescription) {
        mGeneratedDescription = generatedDescription;
    }
}
