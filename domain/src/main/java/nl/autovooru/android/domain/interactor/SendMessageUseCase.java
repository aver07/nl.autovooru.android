package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.repository.CommonRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 10/12/15.
 */
public class SendMessageUseCase implements UseCase {

    private final CommonRepository mRepository;

    @Inject
    public SendMessageUseCase(@NonNull CommonRepository repository) {
        mRepository = repository;
    }

    public Observable<Boolean> sendMessage(String machineId, String name, String email, String subject, String message) {
        return mRepository.sendMessage(machineId, name, email, subject, message);
    }
}
