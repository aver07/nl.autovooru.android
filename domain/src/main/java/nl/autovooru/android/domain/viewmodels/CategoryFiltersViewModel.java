package nl.autovooru.android.domain.viewmodels;

import java.util.List;

import nl.autovooru.android.domain.model.filters.Filter;

/**
 * Created by Antonenko Viacheslav on 16/11/15.
 */
public class CategoryFiltersViewModel implements ViewModel {

    private List<Filter> mFilterList;

    public CategoryFiltersViewModel(List<Filter> filterList) {
        mFilterList = filterList;
    }

    public List<Filter> getFilterList() {
        return mFilterList;
    }
}
