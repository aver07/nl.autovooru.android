package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.repository.AuthorizationRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public class OrganizationRegistrationUseCase implements UseCase {

    private final AuthorizationRepository mRepository;

    @Inject
    public OrganizationRegistrationUseCase(@NonNull AuthorizationRepository repository) {
        mRepository = repository;
    }

    public Observable<Void> register(String name, String surname, String companyName,
                                     String autosCount, String postcode, String city,
                                     String address, String phone, String email) {
        return mRepository.registrationNewOrganization(name, surname, companyName, autosCount,
                postcode, city, address, phone, email);
    }

}
