package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import nl.autovooru.android.domain.repository.UserDataRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 08/01/16.
 */
public class EditAutoUseCase implements UseCase {

    private final UserDataRepository mRepository;

    @Inject
    public EditAutoUseCase(@NonNull UserDataRepository repository) {
        mRepository = repository;
    }

    public Observable<Void> execute(@NonNull String machineId,
                                    @NonNull Map<String, Object> fieldsValues,
                                    @Nullable List<String> images) {
        return mRepository.editAuto(machineId, fieldsValues, images);
    }
}
