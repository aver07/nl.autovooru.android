package nl.autovooru.android.domain.model.machine.details;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
@Parcel
public final class Profile {

    @Nullable
    String mPostCode;
    @Nullable
    String mCity;
    @Nullable
    String mAddress;
    @Nullable
    String mOrganizationName;
    @Nullable
    String mPhone;
    @Nullable
    String mEmail;
    @Nullable
    String mWebsiteUrl;
    @Nullable
    ArrayList<Location> mLocations;
    @Nullable
    String mDealerType;

    @Nullable
    public String getPostCode() {
        return mPostCode;
    }

    public void setPostCode(@Nullable String postCode) {
        mPostCode = postCode;
    }

    @Nullable
    public String getCity() {
        return mCity;
    }

    public void setCity(@Nullable String city) {
        mCity = city;
    }

    @Nullable
    public String getAddress() {
        return mAddress;
    }

    public void setAddress(@Nullable String address) {
        mAddress = address;
    }

    @Nullable
    public String getOrganizationName() {
        return mOrganizationName;
    }

    public void setOrganizationName(@Nullable String organizationName) {
        mOrganizationName = organizationName;
    }

    @Nullable
    public String getPhone() {
        return mPhone;
    }

    public void setPhone(@Nullable String phone) {
        mPhone = phone;
    }

    @Nullable
    public String getEmail() {
        return mEmail;
    }

    public void setEmail(@Nullable String email) {
        mEmail = email;
    }

    @Nullable
    public String getWebsiteUrl() {
        return mWebsiteUrl;
    }

    public void setWebsiteUrl(@Nullable String websiteUrl) {
        mWebsiteUrl = websiteUrl;
    }

    @NonNull
    public ArrayList<Location> getLocations() {
        if (mLocations == null) {
            mLocations = new ArrayList<>();
        }
        return mLocations;
    }

    public void setLocations(@Nullable ArrayList<Location> locations) {
        mLocations = locations;
    }

    @Nullable
    public String getDealerType() {
        return mDealerType;
    }

    public void setDealerType(@Nullable String dealerType) {
        mDealerType = dealerType;
    }
}
