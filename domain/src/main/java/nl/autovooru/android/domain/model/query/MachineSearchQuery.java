package nl.autovooru.android.domain.model.query;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import org.parceler.Parcel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.model.machine.MachineFilter;

/**
 * Created by Antonenko Viacheslav on 14/12/15.
 */
@Parcel
public class MachineSearchQuery {

    Filter mMachineCategory;
    @Nullable
    MachineFilter.Options mMark;
    @Nullable
    MachineFilter.Options mModel;
    @Nullable
    MachineFilter.Options mStartYear;
    @Nullable
    MachineFilter.Options mEndYear;
    @Nullable
    MachineFilter.Options mStartPrice;
    @Nullable
    MachineFilter.Options mEndPrice;
    @Nullable
    MachineFilter.Options mFuel;
    @Nullable
    MachineFilter.Options mUserType;
    @Nullable
    String mKeywords;
    @Nullable
    String mPostcode;
    @Nullable
    MachineFilter.Options mDistance;
    @Nullable
    MachineFilter.Options mMileage;
    @Nullable
    MachineFilter.Options mOfferTime;
    @Nullable
    MachineFilter.Options mColor;
    @Nullable
    MachineFilter.Options mTransmission;
    @Nullable
    MachineFilter.Options mBody;
    @NonNull
    Map<String, List<MachineFilter.Options>> mOtherFilters = new HashMap<>();

    public MachineSearchQuery() {
    }

    public Filter getMachineCategory() {
        return mMachineCategory;
    }

    public void setMachineCategory(Filter machineCategory) {
        mMachineCategory = machineCategory;
    }

    @Nullable
    public MachineFilter.Options getMark() {
        return mMark;
    }

    public void setMark(@Nullable MachineFilter.Options mark) {
        mMark = mark;
    }

    @Nullable
    public MachineFilter.Options getModel() {
        return mModel;
    }

    public void setModel(@Nullable MachineFilter.Options model) {
        mModel = model;
    }

    @Nullable
    public MachineFilter.Options getStartYear() {
        return mStartYear;
    }

    public void setStartYear(@Nullable MachineFilter.Options startYear) {
        mStartYear = startYear;
    }

    @Nullable
    public MachineFilter.Options getEndYear() {
        return mEndYear;
    }

    public void setEndYear(@Nullable MachineFilter.Options endYear) {
        mEndYear = endYear;
    }

    @Nullable
    public MachineFilter.Options getStartPrice() {
        return mStartPrice;
    }

    public void setStartPrice(@Nullable MachineFilter.Options startPrice) {
        mStartPrice = startPrice;
    }

    @Nullable
    public MachineFilter.Options getEndPrice() {
        return mEndPrice;
    }

    public void setEndPrice(@Nullable MachineFilter.Options endPrice) {
        mEndPrice = endPrice;
    }

    @Nullable
    public MachineFilter.Options getFuel() {
        return mFuel;
    }

    public void setFuel(@Nullable MachineFilter.Options fuel) {
        mFuel = fuel;
    }

    @Nullable
    public MachineFilter.Options getUserType() {
        return mUserType;
    }

    public void setUserType(@Nullable MachineFilter.Options userType) {
        mUserType = userType;
    }

    @Nullable
    public String getKeywords() {
        return mKeywords;
    }

    public void setKeywords(@Nullable String keywords) {
        mKeywords = keywords;
    }

    @Nullable
    public String getPostcode() {
        return mPostcode;
    }

    public void setPostcode(@Nullable String postcode) {
        mPostcode = postcode;
    }

    @Nullable
    public MachineFilter.Options getDistance() {
        return mDistance;
    }

    public void setDistance(@Nullable MachineFilter.Options distance) {
        mDistance = distance;
    }

    @Nullable
    public MachineFilter.Options getMileage() {
        return mMileage;
    }

    public void setMileage(@Nullable MachineFilter.Options mileage) {
        mMileage = mileage;
    }

    public void setOfferTime(@Nullable MachineFilter.Options offerTime) {
        mOfferTime = offerTime;
    }

    @Nullable
    public MachineFilter.Options getOfferTime() {
        return mOfferTime;
    }

    @Nullable
    public MachineFilter.Options getColor() {
        return mColor;
    }

    public void setColor(@Nullable MachineFilter.Options color) {
        mColor = color;
    }

    @Nullable
    public MachineFilter.Options getTransmission() {
        return mTransmission;
    }

    public void setTransmission(@Nullable MachineFilter.Options transmission) {
        mTransmission = transmission;
    }

    @Nullable
    public MachineFilter.Options getBody() {
        return mBody;
    }

    public void setBody(@Nullable MachineFilter.Options body) {
        mBody = body;
    }

    @NonNull
    public Map<String, List<MachineFilter.Options>> getOtherFilters() {
        return mOtherFilters;
    }

    public void setOtherFilters(String key, List<MachineFilter.Options> options) {
        if (options == null || options.isEmpty()) {
            mOtherFilters.remove(key);
        } else {
            mOtherFilters.put(key, options);
        }
    }

    public void clearAllOtherFilters() {
        mOtherFilters.clear();
    }

    public void apply(MachineSearchQuery query) {
        mMachineCategory = query.mMachineCategory;
        mMark = query.mMark;
        mModel = query.mModel;
        mStartYear = query.mStartYear;
        mEndYear = query.mEndYear;
        mStartPrice = query.mStartPrice;
        mEndPrice = query.mEndPrice;
        mFuel = query.mFuel;
        mUserType = query.mUserType;
        mMileage = query.mMileage;
        mPostcode = query.mPostcode;
        mKeywords = query.mKeywords;
        mOfferTime = query.mOfferTime;
        mTransmission = query.mTransmission;
        mColor = query.mColor;
        mBody = query.mBody;
        mOtherFilters = query.mOtherFilters;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> filters = new HashMap<>();
        filters.put("vcat", mMachineCategory.getId());
        if (mMark != null) {
            filters.put("vmark", mMark.getValue());
        }
        if (mModel != null) {
            filters.put("vmodel", mModel.getValue());
        }
        if (mStartYear != null) {
            filters.put("year_from", mStartYear.getValue());
        }
        if (mEndYear != null) {
            filters.put("year_to", mEndYear.getValue());
        }
        if (mStartPrice != null) {
            filters.put("price_from", mStartPrice.getValue());
        }
        if (mEndPrice != null) {
            filters.put("price_to", mEndPrice.getValue());
        }
        if (mFuel != null) {
            filters.put("vfuel", mFuel.getValue());
        }
        if (mUserType != null) {
            filters.put("user_type", mUserType.getValue());
        }
        if (mDistance != null) {
            filters.put("distance", mDistance.getValue());
        }
        if (mMileage != null) {
            filters.put("mileage", mMileage.getValue());
        }
        if (!TextUtils.isEmpty(mPostcode)) {
            filters.put("post_code", mPostcode);
        }

        if (!TextUtils.isEmpty(mKeywords)) {
            filters.put("keywords", mKeywords);
        }

        if (mOfferTime != null) {
            filters.put("offer_time", mOfferTime.getValue());
        }

        if (mTransmission != null) {
            filters.put("vtrans", mTransmission.getValue());
        }

        if (mColor != null) {
            filters.put("color", mColor.getValue());
        }

        if (mBody != null) {
            filters.put("vbody", mBody.getValue());
        }

        if (!mOtherFilters.isEmpty()) {
            Map<String, String> options = new HashMap<>();
            for (Map.Entry<String, List<MachineFilter.Options>> entry : mOtherFilters.entrySet()) {
                for (MachineFilter.Options value : entry.getValue()) {
                    options.put(value.getValue(), "true");
                }
            }
            if (!options.isEmpty()) {
                filters.put("options", options);
            }
        }


        return filters;
    }

}
