package nl.autovooru.android.domain.model.advert;

import org.parceler.Parcel;

import java.util.List;

import nl.autovooru.android.domain.model.machine.MachineFilter;

/**
 * Created by Antonenko Viacheslav on 05/01/16.
 */
@Parcel
public class AdvertFields {

    MachineFilter mName;
    MachineFilter mDescription;
    MachineFilter mToken;
    MachineFilter mTopMachineMarks;
    MachineFilter mMarks;
    MachineFilter mModels;
    MachineFilter mYear;
    MachineFilter mFuel;
    MachineFilter mTransmission;
    MachineFilter mColor;
    MachineFilter mBody;
    List<MachineFilter> mOtherFilters;
    MachineFilter mPrice;
    MachineFilter mDoors;
    MachineFilter mPromoPrice;
    MachineFilter mMileage;
    MachineFilter mEngineCapacity;
    MachineFilter mFirstOwner;
    MachineFilter mNap;

    public MachineFilter getName() {
        return mName;
    }

    public void setName(MachineFilter name) {
        mName = name;
    }

    public MachineFilter getDescription() {
        return mDescription;
    }

    public void setDescription(MachineFilter description) {
        mDescription = description;
    }

    public MachineFilter getToken() {
        return mToken;
    }

    public void setToken(MachineFilter token) {
        mToken = token;
    }

    public MachineFilter getTopMachineMarks() {
        return mTopMachineMarks;
    }

    public void setTopMachineMarks(MachineFilter topMachineMarks) {
        mTopMachineMarks = topMachineMarks;
    }

    public MachineFilter getMarks() {
        return mMarks;
    }

    public void setMarks(MachineFilter marks) {
        mMarks = marks;
    }

    public MachineFilter getModels() {
        return mModels;
    }

    public void setModels(MachineFilter models) {
        mModels = models;
    }

    public MachineFilter getYear() {
        return mYear;
    }

    public void setYear(MachineFilter year) {
        mYear = year;
    }

    public MachineFilter getFuel() {
        return mFuel;
    }

    public void setFuel(MachineFilter fuel) {
        mFuel = fuel;
    }

    public MachineFilter getTransmission() {
        return mTransmission;
    }

    public void setTransmission(MachineFilter transmission) {
        mTransmission = transmission;
    }

    public MachineFilter getColor() {
        return mColor;
    }

    public void setColor(MachineFilter color) {
        mColor = color;
    }

    public MachineFilter getBody() {
        return mBody;
    }

    public void setBody(MachineFilter body) {
        mBody = body;
    }

    public List<MachineFilter> getOtherFilters() {
        return mOtherFilters;
    }

    public void setOtherFilters(List<MachineFilter> otherFilters) {
        mOtherFilters = otherFilters;
    }

    public MachineFilter getPrice() {
        return mPrice;
    }

    public void setPrice(MachineFilter price) {
        mPrice = price;
    }

    public MachineFilter getDoors() {
        return mDoors;
    }

    public void setDoors(MachineFilter doors) {
        mDoors = doors;
    }

    public MachineFilter getPromoPrice() {
        return mPromoPrice;
    }

    public void setPromoPrice(MachineFilter promoPrice) {
        mPromoPrice = promoPrice;
    }

    public MachineFilter getMileage() {
        return mMileage;
    }

    public void setMileage(MachineFilter mileage) {
        mMileage = mileage;
    }

    public MachineFilter getEngineCapacity() {
        return mEngineCapacity;
    }

    public void setEngineCapacity(MachineFilter engineCapacity) {
        mEngineCapacity = engineCapacity;
    }

    public MachineFilter getFirstOwner() {
        return mFirstOwner;
    }

    public void setFirstOwner(MachineFilter firstOwner) {
        mFirstOwner = firstOwner;
    }

    public MachineFilter getNap() {
        return mNap;
    }

    public void setNap(MachineFilter nap) {
        mNap = nap;
    }
}
