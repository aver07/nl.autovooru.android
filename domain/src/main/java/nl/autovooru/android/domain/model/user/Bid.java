package nl.autovooru.android.domain.model.user;

import java.util.Date;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public final class Bid {

    String mBidId;
    String mBid;
    String mEmail;
    long mCreateDate;
    String mMessageId;
    Date mDate;

    public Date getDate() {
        if (mDate == null) {
            mDate = new Date(mCreateDate);
        }
        return mDate;
    }

    public String getBidId() {
        return mBidId;
    }

    public void setBidId(String bidId) {
        mBidId = bidId;
    }

    public String getBid() {
        return mBid;
    }

    public void setBid(String bid) {
        mBid = bid;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public long getCreateDate() {
        return mCreateDate;
    }

    public void setCreateDate(long createDate) {
        mCreateDate = createDate;
    }

    public String getMessageId() {
        return mMessageId;
    }

    public void setMessageId(String messageId) {
        mMessageId = messageId;
    }
}
