package nl.autovooru.android.domain.model.auth;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
public class AuthorizationToken {

    private String mCode;
    private String mAccessToken;
    private long mExpireDate;
    private String mDealerId;

    public AuthorizationToken() {
    }


    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    public void setAccessToken(String accessToken) {
        mAccessToken = accessToken;
    }

    public long getExpireDate() {
        return mExpireDate;
    }

    public void setExpireDate(long expireDate) {
        mExpireDate = expireDate;
    }

    public String getDealerId() {
        return mDealerId;
    }

    public void setDealerId(String dealerId) {
        mDealerId = dealerId;
    }
}
