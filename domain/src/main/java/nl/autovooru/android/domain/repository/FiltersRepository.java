package nl.autovooru.android.domain.repository;

import java.util.List;
import java.util.Map;

import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.model.filters.FilterType;
import nl.autovooru.android.domain.model.machine.MachineSearchFilters;
import rx.Observable;

/**
 * Interface that represents a Repository for getting Filter related data.
 * <p/>
 * Created by Antonenko Viacheslav on 16/11/15.
 */
public interface FiltersRepository {

    Observable<List<FilterType>> getFilterTypes();

    Observable<Map<String, List<Filter>>> getFiltersByFilterType(int page,
                                                                 int itemsPerPage,
                                                                 List<String> filterTypeKeys);

    Observable<MachineSearchFilters> getMachineSearchFilters(Map<String, String> filters);
}
