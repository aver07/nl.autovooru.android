package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.repository.UserDataRepository;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public class GetMyBidsUseCase implements UseCase {

    private static final int ITEMS_PER_PAGE = 10;
    private static final int DEFAULT_PAGE = 1;
    private final UserDataRepository mRepository;
    private int mPage = DEFAULT_PAGE;

    @Inject
    public GetMyBidsUseCase(@NonNull UserDataRepository repository) {
        mRepository = repository;
    }

    public Observable<MachineListViewModel> load() {
        mPage = DEFAULT_PAGE;
        return mRepository.getUserBids(mPage, ITEMS_PER_PAGE);
    }

    public Observable<MachineListViewModel> loadMore() {
        mPage++;
        return mRepository.getUserBids(mPage, ITEMS_PER_PAGE).doOnError(throwable -> {
            mPage--;
            if (mPage < 0) {
                mPage = DEFAULT_PAGE;
            }
        });
    }
}
