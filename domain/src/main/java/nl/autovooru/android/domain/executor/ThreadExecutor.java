package nl.autovooru.android.domain.executor;


import rx.Scheduler;

/**
 * Executor implementation can be based on different frameworks or techniques of asynchronous
 * execution, but every implementation will execute the
 * {@link nl.autovooru.android.domain.interactor.UseCase} out of the UI thread.
 * <p/>
 * Created by Antonenko Viacheslav on 16/11/15.
 */
public interface ThreadExecutor {
    Scheduler getScheduler();
}
