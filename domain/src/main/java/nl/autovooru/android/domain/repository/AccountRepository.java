package nl.autovooru.android.domain.repository;

import nl.autovooru.android.domain.model.auth.AuthInfo;
import nl.autovooru.android.domain.model.auth.AuthorizationToken;
import nl.autovooru.android.domain.model.user.UserInfo;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public interface AccountRepository {

    boolean createAccount(String name, String email, String password, AuthorizationToken authorizationToken);

    void updateToken(AuthorizationToken authorizationToken);

    void logout();

    boolean isLoggedIn();

    String getUsername();

    String getEmail();

    String getPassword();

    String getUserId();

    AuthInfo getAuthInfo();

    void updatePassword(String newPassword);

    void updateUserInfo(UserInfo userInfo);
}
