package nl.autovooru.android.domain.model.machine;

/**
 * Created by Antonenko Viacheslav on 17/11/15.
 */
public class Mark {

    private String mName;

    public Mark(String name) {
        mName = name;
    }

    public String getMark() {
        return mName;
    }
}
