package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.model.auth.AuthorizationToken;
import nl.autovooru.android.domain.repository.AuthorizationRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
public final class LoginUseCase implements UseCase {

    private final AuthorizationRepository mRepository;

    @Inject
    public LoginUseCase(@NonNull AuthorizationRepository repository) {
        mRepository = repository;
    }

    public Observable<AuthorizationToken> authorize(@NonNull String login, @NonNull String password) {
        return mRepository.authorize(login, password);
    }

    public Observable<AuthorizationToken> refreshToken(String code) {
        return mRepository.refreshToken(code);
    }
}
