package nl.autovooru.android.domain.model.user;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public final class MessagesPage {

    private int mTotal;
    private int mCurrentPage;
    private int mTotalPages;
    private List<Message> mEntites;

    public MessagesPage() {
    }

    public int getTotal() {
        return mTotal;
    }

    public void setTotal(int total) {
        mTotal = total;
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(int currentPage) {
        mCurrentPage = currentPage;
    }

    public int getTotalPages() {
        return mTotalPages;
    }

    public void setTotalPages(int totalPages) {
        mTotalPages = totalPages;
    }

    public List<Message> getEntites() {
        if (mEntites == null) {
            mEntites = new ArrayList<>();
        }
        return mEntites;
    }

    public void setEntites(List<Message> entites) {
        mEntites = entites;
    }

    public boolean canLoadMore() {
        return mCurrentPage < mTotalPages;
    }
}
