package nl.autovooru.android.domain.model.machine.details;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by Antonenko Viacheslav on 08/01/16.
 */
@Parcel
public final class EditMachineDetails {

    String mMachineId;
    Content mContent;
    List<String> mImagesList;
    Info mInfo;
    Filters mFilters;

    public String getMachineId() {
        return mMachineId;
    }

    public void setMachineId(String machineId) {
        mMachineId = machineId;
    }

    public Content getContent() {
        return mContent;
    }

    public void setContent(Content content) {
        mContent = content;
    }

    public List<String> getImagesList() {
        return mImagesList;
    }

    public void setImagesList(List<String> imagesList) {
        mImagesList = imagesList;
    }

    public Info getInfo() {
        return mInfo;
    }

    public void setInfo(Info info) {
        mInfo = info;
    }

    public Filters getFilters() {
        return mFilters;
    }

    public void setFilters(Filters filters) {
        mFilters = filters;
    }
}
