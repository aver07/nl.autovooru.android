package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.model.user.UserInfo;
import nl.autovooru.android.domain.repository.UserDataRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 29/12/15.
 */
public class SaveUserDataUseCase implements UseCase {

    private final UserDataRepository mRepository;

    @Inject
    public SaveUserDataUseCase(@NonNull UserDataRepository repository) {
        mRepository = repository;
    }

    public Observable<Void> execute(@NonNull UserInfo userInfo) {
        return mRepository.saveUserData(userInfo);
    }
}
