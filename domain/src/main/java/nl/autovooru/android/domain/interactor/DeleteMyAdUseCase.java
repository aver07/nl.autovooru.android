package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.repository.UserDataRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public final class DeleteMyAdUseCase implements UseCase {

    private final UserDataRepository mRepository;

    @Inject
    public DeleteMyAdUseCase(@NonNull UserDataRepository repository) {
        mRepository = repository;
    }

    public Observable<Void> delete(@NonNull String machineId) {
        return mRepository.deleteMyAd(machineId);
    }
}
