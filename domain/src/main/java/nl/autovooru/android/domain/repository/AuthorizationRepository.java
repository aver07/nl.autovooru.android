package nl.autovooru.android.domain.repository;

import nl.autovooru.android.domain.model.auth.AuthorizationToken;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
public interface AuthorizationRepository {

    Observable<AuthorizationToken> authorize(String login, String password);

    Observable<AuthorizationToken> refreshToken(String code);

    Observable<Void> forgotPassword(String email);

    Observable<AuthorizationToken> registrationNewUser(String name, String surname,
                                                       String postcode, String city,
                                                       String address, String phone,
                                                       String email, String password);

    Observable<Void> registrationNewOrganization(String name, String surname, String companyName,
                                                 String autosCount, String postcode, String city,
                                                 String address, String phone, String email);
}
