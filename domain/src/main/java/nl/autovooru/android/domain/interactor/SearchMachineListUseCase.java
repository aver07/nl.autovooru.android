package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import nl.autovooru.android.domain.model.query.Order;
import nl.autovooru.android.domain.repository.MachinesRepository;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 17/11/15.
 */
public class SearchMachineListUseCase implements UseCase {

    private static final int ITEMS_PER_PAGE = 10;
    private static final int DEFAULT_PAGE = 1;

    private final MachinesRepository mRepository;
    private String mDealerId;
    private int mPage = DEFAULT_PAGE;
    private Map<String, Object> mFilters = new HashMap<>();
    private Order mOrder;

    @Inject
    public SearchMachineListUseCase(@NonNull MachinesRepository repository) {
        mRepository = repository;
    }

    public void applyFilters(Map<String, Object> filters) {
        mFilters = filters;
        mPage = DEFAULT_PAGE;
    }

    public void setOrder(Order order) {
        mOrder = order;
        mPage = DEFAULT_PAGE;
    }

    public void removeOrder() {
        mOrder = null;
        mPage = DEFAULT_PAGE;
    }

    public void setDealerId(@Nullable String dealerId) {
        mDealerId = dealerId;
        mPage = DEFAULT_PAGE;
    }

    public Observable<MachineListViewModel> get() {
        mPage = DEFAULT_PAGE;
        return mRepository.loadMachines(mPage, ITEMS_PER_PAGE, mDealerId, mFilters, mOrder);
    }

    public Observable<MachineListViewModel> getMore() {
        mPage++;
        return mRepository.loadMachines(mPage, ITEMS_PER_PAGE, mDealerId, mFilters, mOrder)
                .doOnError(throwable -> {
                    mPage--;
                    if (mPage < 0) {
                        mPage = DEFAULT_PAGE;
                    }
                });
    }

}
