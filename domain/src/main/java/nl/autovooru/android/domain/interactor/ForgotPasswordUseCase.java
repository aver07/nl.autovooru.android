package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.repository.AuthorizationRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public class ForgotPasswordUseCase implements UseCase {

    private final AuthorizationRepository mRepository;

    @Inject
    public ForgotPasswordUseCase(@NonNull AuthorizationRepository repository) {
        mRepository = repository;
    }

    public Observable<Void> forgotPassword(String email) {
        return mRepository.forgotPassword(email);
    }

}
