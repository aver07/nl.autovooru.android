package nl.autovooru.android.domain.model.advert;

/**
 * Created by Antonenko Viacheslav on 08/01/16.
 */
public class YoutubeInfo {
    private String mId;
    private String mLinkUrl;
    private String mImageUrl;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getLinkUrl() {
        return mLinkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        mLinkUrl = linkUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }
}
