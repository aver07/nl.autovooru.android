package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.repository.UserDataRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public final class SendAnswerUseCase implements UseCase {

    private final UserDataRepository mRepository;

    @Inject
    public SendAnswerUseCase(@NonNull UserDataRepository repository) {
        mRepository = repository;
    }

    public Observable<Void> sendMessage(String messageId, String subject,
                                        String name, String email, String message) {
        return mRepository.sendAnswer(messageId, subject, name, email, message);
    }
}

