package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.model.query.Order;
import nl.autovooru.android.domain.repository.UserDataRepository;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 26/12/15.
 */
public class GetUserFavoritesUseCase implements UseCase {

    private static final int ITEMS_PER_PAGE = 10;
    private static final int DEFAULT_PAGE = 1;
    private final UserDataRepository mRepository;
    private int mPage = DEFAULT_PAGE;
    private Order mOrder;

    @Inject
    public GetUserFavoritesUseCase(@NonNull UserDataRepository repository) {
        mRepository = repository;
    }

    public void setOrder(Order order) {
        mOrder = order;
        mPage = DEFAULT_PAGE;
    }

    public void removeOrder() {
        mOrder = null;
        mPage = DEFAULT_PAGE;
    }

    public Observable<MachineListViewModel> load() {
        mPage = DEFAULT_PAGE;
        return mRepository.getUserFavorites(mPage, ITEMS_PER_PAGE, mOrder);
    }

    public Observable<MachineListViewModel> loadMore() {
        mPage++;
        return mRepository.getUserFavorites(mPage, ITEMS_PER_PAGE, mOrder).doOnError(throwable -> {
            mPage--;
            if (mPage < 0) {
                mPage = DEFAULT_PAGE;
            }
        });
    }
}
