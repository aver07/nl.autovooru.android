package nl.autovooru.android.domain.model.machine;

/**
 * Created by Antonenko Viacheslav on 24/11/15.
 */
public enum Categories {
    CARS("17"),
    OLDTIMERS("18"),
    COMMERCIAL_VEHICHLE("19"),
    TRUCKS("20"),
    ENGINES("21"),
    DAMAGED_CARS("22"),
    MICROCARS("23"),
    CARAVANS_AND_CAMPERS("24");

    private String mId;

    Categories(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }
}
