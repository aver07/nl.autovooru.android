package nl.autovooru.android.domain.model.machine;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by Antonenko Viacheslav on 17/11/15.
 */
@Parcel
public class Machine {

    String mDealerId;
    String mMachineId;
    String mName;
    String mMark;
    String mModel;
    String mFuel;
    String mMileage;
    float mPrice;
    String mPromoPrice;
    String mReleaseYear;
    String mCity;
    String mAlias;
    List<String> mImages;
    String mBids;
    String mToken;
    String mHits;
    String mBidId;
    String mBid;
    boolean mFavorite;

    public Machine() {
    }

    public String getMachineId() {
        return mMachineId;
    }

    public void setMachineId(String machineId) {
        mMachineId = machineId;
    }

    public String getMark() {
        return mMark;
    }

    public void setMark(String mark) {
        mMark = mark;
    }

    public String getModel() {
        return mModel;
    }

    public void setModel(String model) {
        mModel = model;
    }

    public String getMileage() {
        return mMileage;
    }

    public void setMileage(String mileage) {
        mMileage = mileage;
    }

    public String getFuel() {
        return mFuel;
    }

    public void setFuel(String fuel) {
        mFuel = fuel;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float price) {
        mPrice = price;
    }

    public List<String> getImages() {
        return mImages;
    }

    public void setImages(List<String> images) {
        mImages = images;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getReleaseYear() {
        return mReleaseYear;
    }

    public void setReleaseYear(String releaseYear) {
        mReleaseYear = releaseYear;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getAlias() {
        return mAlias;
    }

    public void setAlias(String alias) {
        mAlias = alias;
    }

    public String getBids() {
        return mBids;
    }

    public void setBids(String bids) {
        mBids = bids;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }

    public String getHits() {
        return mHits;
    }

    public void setHits(String hits) {
        mHits = hits;
    }

    public String getDealerId() {
        return mDealerId;
    }

    public void setDealerId(String dealerId) {
        mDealerId = dealerId;
    }

    public String getBidId() {
        return mBidId;
    }

    public void setBidId(String bidId) {
        mBidId = bidId;
    }

    public String getBid() {
        return mBid;
    }

    public void setBid(String bid) {
        mBid = bid;
    }

    public String getPromoPrice() {
        return mPromoPrice;
    }

    public void setPromoPrice(String promoPrice) {
        mPromoPrice = promoPrice;
    }

    public boolean isFavorite() {
        return mFavorite;
    }

    public void setFavorite(boolean isFavorite) {
        this.mFavorite = isFavorite;
    }

    @Override
    public String toString() {
        return mModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Machine machine = (Machine) o;

        if (mDealerId != null ? !mDealerId.equals(machine.mDealerId) : machine.mDealerId != null)
            return false;
        return !(mMachineId != null ? !mMachineId.equals(machine.mMachineId) : machine.mMachineId != null);

    }

    @Override
    public int hashCode() {
        int result = mDealerId != null ? mDealerId.hashCode() : 0;
        result = 31 * result + (mMachineId != null ? mMachineId.hashCode() : 0);
        return result;
    }
}
