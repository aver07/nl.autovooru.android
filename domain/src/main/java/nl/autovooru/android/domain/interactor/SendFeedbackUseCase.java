package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.repository.CommonRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 09/12/15.
 */
public class SendFeedbackUseCase implements UseCase {

    private final CommonRepository mRepository;

    @Inject
    public SendFeedbackUseCase(@NonNull CommonRepository repository) {
        mRepository = repository;
    }

    public Observable<Boolean> sendFeedback(String subject, String name, String email, String message) {
        return mRepository.sendFeedback(subject, name, email, message);
    }
}
