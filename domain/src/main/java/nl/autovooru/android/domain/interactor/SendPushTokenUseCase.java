package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import nl.autovooru.android.domain.repository.UserDataRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 19/01/16.
 */
public class SendPushTokenUseCase implements UseCase {

    private final UserDataRepository mRepository;

    public SendPushTokenUseCase(@NonNull UserDataRepository repository) {
        mRepository = repository;
    }

    public Observable<Void> execute(@NonNull String deviceToken) {
        return mRepository.sendPushToken(deviceToken);
    }
}
