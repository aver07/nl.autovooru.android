package nl.autovooru.android.domain.model.user;

import java.util.Date;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public class Message {
    private String mMessageId;
    private String mEmail;
    private String mTopic;
    private String mMessage;
    private String mAnswerTopic;
    private Date mCreateDate;

    public Message() {
    }

    public String getMessageId() {
        return mMessageId;
    }

    public void setMessageId(String messageId) {
        mMessageId = messageId;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getTopic() {
        return mTopic;
    }

    public void setTopic(String topic) {
        mTopic = topic;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Date getCreateDate() {
        return mCreateDate;
    }

    public void setCreateDate(Date createDate) {
        mCreateDate = createDate;
    }

    public String getAnswerTopic() {
        return mAnswerTopic;
    }

    public void setAnswerTopic(String answerTopic) {
        mAnswerTopic = answerTopic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        return !(mMessageId != null ? !mMessageId.equals(message.mMessageId) : message.mMessageId != null);

    }

    @Override
    public int hashCode() {
        return mMessageId != null ? mMessageId.hashCode() : 0;
    }
}
