package nl.autovooru.android.domain.model.query;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.autovooru.android.common.utils.StringUtils;
import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.model.machine.MachineFilter;
import nl.autovooru.android.domain.model.machine.details.EditMachineDetails;
import nl.autovooru.android.domain.model.machine.details.Filters;
import nl.autovooru.android.domain.model.machine.details.Info;

/**
 * Created by Antonenko Viacheslav on 05/01/16.
 */
@Parcel
public final class AdvertFieldsQuery {

    Filter mMachineCategory;
    @Nullable
    String mTitle;
    @Nullable
    String mDescription;
    @Nullable
    String mToken;
    @Nullable
    String mPrice;
    @Nullable
    String mDoors;
    @Nullable
    String mPromoPrice;
    @Nullable
    String mMileage;
    @Nullable
    String mCapacity;
    @Nullable
    String mYoutubeLink;
    String mYoutubeImage;
    boolean mFirstOwner;
    boolean mNap;
    @Nullable
    MachineFilter.Options mMark;
    @Nullable
    MachineFilter.Options mModel;
    @Nullable
    MachineFilter.Options mYear;
    @Nullable
    MachineFilter.Options mFuel;
    @Nullable
    MachineFilter.Options mTransmission;
    @Nullable
    MachineFilter.Options mColor;
    @Nullable
    MachineFilter.Options mBody;
    @NonNull
    Map<String, List<MachineFilter.Options>> mOtherFilters = new HashMap<>();
    @NonNull
    List<String> mImages;

    public AdvertFieldsQuery() {

    }

    public AdvertFieldsQuery(Filter machineCategory) {
        mMachineCategory = machineCategory;
    }

    public String getYoutubeImage() {
        return mYoutubeImage;
    }

    public void setYoutubeImage(String youtubeImage) {
        mYoutubeImage = youtubeImage;
    }

    @Nullable
    public String getYoutubeLink() {
        return mYoutubeLink;
    }

    public void setYoutubeLink(@Nullable String youtubeLink) {
        mYoutubeLink = youtubeLink;
    }

    public Filter getMachineCategory() {
        return mMachineCategory;
    }

    public void setMachineCategory(Filter machineCategory) {
        mMachineCategory = machineCategory;
    }

    @Nullable
    public String getTitle() {
        return mTitle;
    }

    public void setTitle(@Nullable String title) {
        mTitle = title;
    }

    @Nullable
    public String getDescription() {
        return mDescription;
    }

    public void setDescription(@Nullable String description) {
        mDescription = description;
    }

    @Nullable
    public String getToken() {
        return mToken;
    }

    public void setToken(@Nullable String token) {
        mToken = token;
    }

    @Nullable
    public String getPrice() {
        return mPrice;
    }

    public void setPrice(@Nullable String price) {
        mPrice = price;
    }

    @Nullable
    public String getDoors() {
        return mDoors;
    }

    public void setDoors(@Nullable String doors) {
        mDoors = doors;
    }

    @Nullable
    public String getPromoPrice() {
        return mPromoPrice;
    }

    public void setPromoPrice(@Nullable String promoPrice) {
        mPromoPrice = promoPrice;
    }

    @Nullable
    public String getMileage() {
        return mMileage;
    }

    public void setMileage(@Nullable String mileage) {
        mMileage = mileage;
    }

    @Nullable
    public String getCapacity() {
        return mCapacity;
    }

    public void setCapacity(@Nullable String capacity) {
        mCapacity = capacity;
    }

    public boolean getFirstOwner() {
        return mFirstOwner;
    }

    public void setFirstOwner(boolean firstOwner) {
        mFirstOwner = firstOwner;
    }

    public boolean getNap() {
        return mNap;
    }

    public void setNap(boolean nap) {
        mNap = nap;
    }

    @Nullable
    public MachineFilter.Options getMark() {
        return mMark;
    }

    public void setMark(@Nullable MachineFilter.Options mark) {
        mMark = mark;
    }

    @Nullable
    public MachineFilter.Options getModel() {
        return mModel;
    }

    public void setModel(@Nullable MachineFilter.Options model) {
        mModel = model;
    }

    @Nullable
    public MachineFilter.Options getYear() {
        return mYear;
    }

    public void setYear(@Nullable MachineFilter.Options year) {
        mYear = year;
    }

    @Nullable
    public MachineFilter.Options getFuel() {
        return mFuel;
    }

    public void setFuel(@Nullable MachineFilter.Options fuel) {
        mFuel = fuel;
    }

    @Nullable
    public MachineFilter.Options getTransmission() {
        return mTransmission;
    }

    public void setTransmission(@Nullable MachineFilter.Options transmission) {
        mTransmission = transmission;
    }

    @Nullable
    public MachineFilter.Options getColor() {
        return mColor;
    }

    public void setColor(@Nullable MachineFilter.Options color) {
        mColor = color;
    }

    @Nullable
    public MachineFilter.Options getBody() {
        return mBody;
    }

    public void setBody(@Nullable MachineFilter.Options body) {
        mBody = body;
    }

    @NonNull
    public Map<String, List<MachineFilter.Options>> getOtherFilters() {
        return mOtherFilters;
    }

    public void setOtherFilters(String key, List<MachineFilter.Options> options) {
        if (options == null || options.isEmpty()) {
            mOtherFilters.remove(key);
        } else {
            mOtherFilters.put(key, options);
        }
    }

    @NonNull
    public List<String> getImages() {
        if (mImages == null) {
            mImages = new ArrayList<>();
        }
        return mImages;
    }

    public void setImages(@NonNull List<String> images) {
        mImages = images;
    }

    public void apply(@Nullable AdvertFieldsQuery query) {
        if (query == null) {
            return;
        }
        mMachineCategory = query.mMachineCategory;
        mTitle = query.mTitle;
        mDescription = query.mDescription;
        mToken = query.mToken;
        mPrice = query.mPrice;
        mDoors = query.mDoors;
        mPromoPrice = query.mPromoPrice;
        mMileage = query.mMileage;
        mCapacity = query.mCapacity;
        mFirstOwner = query.mFirstOwner;
        mNap = query.mNap;
        mMark = query.mMark;
        mModel = query.mModel;
        mYear = query.mYear;
        mFuel = query.mFuel;
        mTransmission = query.mTransmission;
        mColor = query.mColor;
        mBody = query.mBody;
        mOtherFilters = query.mOtherFilters;
        mYoutubeImage = query.mYoutubeImage;
        mYoutubeLink = query.mYoutubeLink;
        mImages = query.mImages;
    }

    @NonNull
    public Map<String, Object> toMap() {
        Map<String, Object> fields = new HashMap<>();
        fields.put("vcat", mMachineCategory.getId());
        if (!StringUtils.isEmpty(mTitle)) {
            fields.put("name", mTitle);
        }

        if (!StringUtils.isEmpty(mDescription)) {
            fields.put("description", mDescription);
        }
        if (!StringUtils.isEmpty(mToken)) {
            fields.put("kenteken", mToken);
        }
        if (!StringUtils.isEmpty(mPrice)) {
            fields.put("price", mPrice);
        }
        if (!StringUtils.isEmpty(mDoors)) {
            fields.put("doors", mDoors);
        }
        if (!StringUtils.isEmpty(mPromoPrice)) {
            fields.put("actie_price", mPromoPrice);
        }
        if (!StringUtils.isEmpty(mMileage)) {
            fields.put("mileage", mMileage);
        }
        if (!StringUtils.isEmpty(mCapacity)) {
            fields.put("capacity", mCapacity);
        }
        if (!StringUtils.isEmpty(mYoutubeLink)) {
            fields.put("video", mYoutubeLink);
        }
        if (mFirstOwner) {
            fields.put("first_owner", Boolean.TRUE.toString());
        }
        if (mNap) {
            fields.put("nap", Boolean.TRUE.toString());
        }
        if (mMark != null) {
            fields.put("vmark", mMark.getValue());
        }
        if (mModel != null) {
            fields.put("vmodel", mModel.getValue());
        }
        if (mYear != null) {
            fields.put("year", mYear.getValue());
        }
        if (mFuel != null) {
            fields.put("vfuel", mFuel.getValue());
        }
        if (mTransmission != null) {
            fields.put("vtrans", mTransmission.getValue());
        }
        if (mColor != null) {
            fields.put("color", mColor.getValue());
        }
        if (mBody != null) {
            fields.put("vbody", mBody.getValue());
        }

        if (!mOtherFilters.isEmpty()) {
            Map<String, String> options = new HashMap<>();
            for (Map.Entry<String, List<MachineFilter.Options>> entry : mOtherFilters.entrySet()) {
                for (MachineFilter.Options value : entry.getValue()) {
                    options.put(value.getValue(), "true");
                }
            }
            if (!options.isEmpty()) {
                fields.put("options", options);
            }
        }

        return fields;
    }

    public void apply(EditMachineDetails editMachineDetails) {
        if (editMachineDetails == null) {
            return;
        }
        final Filters filters = editMachineDetails.getFilters();
        final Info info = editMachineDetails.getInfo();
        final MachineFilter.Options category = filters.getCategory();
        mMachineCategory = new Filter(category.getValue(), category.getName());
        mTitle = editMachineDetails.getContent().getName();
        mDescription = editMachineDetails.getContent().getUserDescription();
        mToken = info.getToken();
        mPrice = info.getPrice();
        mDoors = info.getDoors();
        mPromoPrice = info.getSalePrice();
        mMileage = info.getMileage();
        mCapacity = info.getCapacity();
        mFirstOwner = info.isFirstOwner();
        mNap = info.isNap();
        mMark = filters.getMark();
        mModel = filters.getModel();
        mYear = new MachineFilter.Options();
        mYear.setValue(info.getReleaseYear());
        mYear.setName(info.getReleaseYear());
        mFuel = filters.getFuel();
        mTransmission = filters.getTransmission();
        mColor = filters.getColor();
        mBody = filters.getBody();
        mOtherFilters = new HashMap<>();
        for (MachineFilter filter : filters.getOtherFilters()) {
            mOtherFilters.put(filter.getValue(), filter.getFirstOptionsList());
        }
        mYoutubeLink = info.getVideoUrl();
        mYoutubeImage = info.getVideoImage();
        mImages = editMachineDetails.getImagesList();
    }
}
