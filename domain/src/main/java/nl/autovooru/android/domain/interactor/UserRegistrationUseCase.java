package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.model.auth.AuthorizationToken;
import nl.autovooru.android.domain.repository.AuthorizationRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public class UserRegistrationUseCase implements UseCase {

    private final AuthorizationRepository mRepository;

    @Inject
    public UserRegistrationUseCase(@NonNull AuthorizationRepository repository) {
        mRepository = repository;
    }

    public Observable<AuthorizationToken> register(String name, String surname,
                                                   String postcode, String city,
                                                   String address, String phone,
                                                   String email, String password) {
        return mRepository.registrationNewUser(name, surname, postcode, city, address, phone, email, password);
    }

}
