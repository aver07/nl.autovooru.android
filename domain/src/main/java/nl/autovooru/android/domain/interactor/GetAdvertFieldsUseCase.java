package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import nl.autovooru.android.domain.model.advert.AdvertFields;
import nl.autovooru.android.domain.repository.UserDataRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 05/01/16.
 */
public class GetAdvertFieldsUseCase implements UseCase {

    private final UserDataRepository mRepository;

    @Inject
    public GetAdvertFieldsUseCase(@NonNull UserDataRepository repository) {
        mRepository = repository;
    }

    public Observable<AdvertFields> getFilters(@NonNull String categoryId, @Nullable String markId) {
        return mRepository.getAdvertFields(categoryId, markId);
    }
}
