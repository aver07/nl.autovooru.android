package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.model.advert.YoutubeInfo;
import nl.autovooru.android.domain.repository.UserDataRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 08/01/16.
 */
public class GetYoutubeInfoUseCase implements UseCase {

    private final UserDataRepository mRepository;

    @Inject
    public GetYoutubeInfoUseCase(UserDataRepository repository) {
        mRepository = repository;
    }

    public Observable<YoutubeInfo> execute(@NonNull String youtubeLink) {
        return mRepository.getYoutubeInfo(youtubeLink);
    }
}
