package nl.autovooru.android.domain.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import nl.autovooru.android.domain.repository.CommonRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public class SendBidMessageUseCase implements UseCase {

    private final CommonRepository mRepository;

    @Inject
    public SendBidMessageUseCase(@NonNull CommonRepository repository) {
        mRepository = repository;
    }

    public Observable<Boolean> sendMessage(String messageId, String name, String email, String subject, String message) {
        return mRepository.sendBidMessage(messageId, name, email, subject, message);
    }
}