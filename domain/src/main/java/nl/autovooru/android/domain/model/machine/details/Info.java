package nl.autovooru.android.domain.model.machine.details;

import android.support.annotation.Nullable;

import org.parceler.Parcel;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
@Parcel
public final class Info {

    @Nullable
    String mReleaseYear;
    @Nullable
    String mMileage;
    @Nullable
    String mFuel;
    @Nullable
    String mPrice;
    @Nullable
    String mSalePrice;
    @Nullable
    String mToken;
    @Nullable
    String mMark;
    @Nullable
    String mModel;
    @Nullable
    String mEngineSize;
    @Nullable
    String mBody;
    @Nullable
    String mTransmission;
    @Nullable
    String mVideoUrl;
    @Nullable
    String mVideoImage;
    @Nullable
    String mDoors;
    @Nullable
    String mCapacity;
    boolean mFirstOwner;
    boolean mNap;

    @Nullable
    public String getReleaseYear() {
        return mReleaseYear;
    }

    public void setReleaseYear(@Nullable String releaseYear) {
        mReleaseYear = releaseYear;
    }

    @Nullable
    public String getMileage() {
        return mMileage;
    }

    public void setMileage(@Nullable String mileage) {
        mMileage = mileage;
    }

    @Nullable
    public String getFuel() {
        return mFuel;
    }

    public void setFuel(@Nullable String fuel) {
        mFuel = fuel;
    }

    @Nullable
    public String getPrice() {
        return mPrice;
    }

    public void setPrice(@Nullable String price) {
        mPrice = price;
    }

    @Nullable
    public String getSalePrice() {
        return mSalePrice;
    }

    public void setSalePrice(@Nullable String salePrice) {
        mSalePrice = salePrice;
    }

    @Nullable
    public String getToken() {
        return mToken;
    }

    public void setToken(@Nullable String token) {
        mToken = token;
    }

    @Nullable
    public String getMark() {
        return mMark;
    }

    public void setMark(@Nullable String mark) {
        mMark = mark;
    }

    @Nullable
    public String getModel() {
        return mModel;
    }

    public void setModel(@Nullable String model) {
        mModel = model;
    }

    @Nullable
    public String getEngineSize() {
        return mEngineSize;
    }

    public void setEngineSize(@Nullable String engineSize) {
        mEngineSize = engineSize;
    }

    @Nullable
    public String getBody() {
        return mBody;
    }

    public void setBody(@Nullable String body) {
        mBody = body;
    }

    @Nullable
    public String getTransmission() {
        return mTransmission;
    }

    public void setTransmission(@Nullable String transmission) {
        mTransmission = transmission;
    }

    @Nullable
    public String getVideoUrl() {
        return mVideoUrl;
    }

    public void setVideoUrl(@Nullable String videoUrl) {
        mVideoUrl = videoUrl;
    }

    @Nullable
    public String getVideoImage() {
        return mVideoImage;
    }

    public void setVideoImage(@Nullable String videoImage) {
        mVideoImage = videoImage;
    }

    @Nullable
    public String getDoors() {
        return mDoors;
    }

    public void setDoors(@Nullable String doors) {
        mDoors = doors;
    }

    @Nullable
    public String getCapacity() {
        return mCapacity;
    }

    public void setCapacity(@Nullable String capacity) {
        mCapacity = capacity;
    }

    public boolean isFirstOwner() {
        return mFirstOwner;
    }

    public void setFirstOwner(boolean firstOwner) {
        mFirstOwner = firstOwner;
    }

    public boolean isNap() {
        return mNap;
    }

    public void setNap(boolean nap) {
        mNap = nap;
    }
}

