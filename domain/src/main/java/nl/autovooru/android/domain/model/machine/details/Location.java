package nl.autovooru.android.domain.model.machine.details;

import org.parceler.Parcel;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
@Parcel
public final class Location {

    double mLatitude;
    double mLongitude;

    public Location() {
    }

    public Location(double latitude, double longitude) {
        mLatitude = latitude;
        mLongitude = longitude;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }
}
