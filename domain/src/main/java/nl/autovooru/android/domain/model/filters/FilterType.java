package nl.autovooru.android.domain.model.filters;

import org.parceler.Parcel;

/**
 * Created by Antonenko Viacheslav on 16/11/15.
 */
@Parcel
public class FilterType {

    String mId;
    String mKey;
    String mName;

    public FilterType() {
    }

    public FilterType(String id, String key, String name) {
        mId = id;
        mKey = key;
        mName = name;
    }

    public String getId() {
        return mId;
    }

    public String getKey() {
        return mKey;
    }

    public String getName() {
        return mName;
    }
}
