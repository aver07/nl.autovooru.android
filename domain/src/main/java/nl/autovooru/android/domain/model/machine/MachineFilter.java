package nl.autovooru.android.domain.model.machine;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonenko Viacheslav on 10/12/15.
 */
@Parcel
public class MachineFilter {

    String mValue;
    String mName;
    List<Options> mFirstOptionsList;
    List<Options> mSecondOptionsList;

    public MachineFilter() {
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        mValue = value;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public List<Options> getFirstOptionsList() {
        if (mFirstOptionsList == null) {
            mFirstOptionsList = new ArrayList<>();
        }
        return mFirstOptionsList;
    }

    public void setFirstOptionsList(List<Options> firstOptionsList) {
        mFirstOptionsList = firstOptionsList;
    }

    public List<Options> getSecondOptionsList() {
        if (mSecondOptionsList == null) {
            mSecondOptionsList = new ArrayList<>();
        }
        return mSecondOptionsList;
    }

    public void setSecondOptionsList(List<Options> secondOptionsList) {
        mSecondOptionsList = secondOptionsList;
    }

    @Parcel
    public static class Options {
        String mValue;
        String mName;

        public Options() {
        }

        public String getValue() {
            return mValue;
        }

        public void setValue(String value) {
            mValue = value;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Options options = (Options) o;

            return !(mValue != null ? !mValue.equals(options.mValue) : options.mValue != null);

        }

        @Override
        public int hashCode() {
            return mValue != null ? mValue.hashCode() : 0;
        }

        @Override
        public String toString() {
            return mName;
        }
    }
}
