package nl.autovooru.android.domain.model.auth;

import org.parceler.Parcel;

/**
 * Created by Antonenko Viacheslav on 24/12/15.
 */
@Parcel
public class AuthInfo {

    String mToken;
    String mCode;

    public AuthInfo() {
    }

    public AuthInfo(String token, String code) {
        mToken = token;
        mCode = code;
    }

    public String getToken() {
        return mToken;
    }


    public String getCode() {
        return mCode;
    }

}
