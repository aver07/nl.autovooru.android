package nl.autovooru.android.domain.interactor;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import nl.autovooru.android.domain.model.filters.FilterType;
import nl.autovooru.android.domain.model.machine.details.MachineDetails;
import nl.autovooru.android.domain.repository.MachinesRepository;
import rx.Observable;
import rx.observers.TestSubscriber;

/**
 * Created by Loh on 17/12/15.
 */
public class GetMachineDetailsUseCaseTest {

    private final String mAppLanguage = "en";
    @Mock
    MachineDetails mMachineDetails;
    @Mock
    private MachinesRepository mMockMachinesRepository;
    @Mock
    private FilterType mFilterType;
    private GetMachineDetailsUseCase mUseCase;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mUseCase = new GetMachineDetailsUseCase(mMockMachinesRepository);
    }

    @Test
    public void testGetFilterTypesUseCaseObservableHappyCase() {
        final String machineId = "1104760";
        final Observable<MachineDetails> cache = mUseCase.getDetails(machineId).cache();
        TestSubscriber<MachineDetails> testSubscriber = new TestSubscriber<>();
        cache.subscribe(testSubscriber);

//        TestSubscriber<MachineDetails> testSubscriber = TestSubscriber.create();
//        testSubscriber.
//        doReturn(mMockMachinesRepository.loadMachineDetails(machineId, mAppLanguage)).doReturn(mMachineDetails);
//        mUseCase.getDetails(machineId).subscribe(machineDetails -> {
//            assertNotNull(machineDetails);
//            assertEquals(machineDetails, mMachineDetails);
//            assertEquals(machineDetails.getMachineId(), machineId);
//        });
//
//        verify(mMockMachinesRepository).loadMachineDetails(machineId, mAppLanguage);
//        verifyNoMoreInteractions(mMockMachinesRepository);
    }

}