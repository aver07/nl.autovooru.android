package nl.autovooru.android.domain.interactor;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import nl.autovooru.android.domain.model.filters.FilterType;
import nl.autovooru.android.domain.repository.FiltersRepository;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Created by Antonenko Viacheslav on 25/11/15.
 */
public class GetFilterTypesUseCaseTest {

    private final String mAppLanguage = "en";
    @Mock
    private FiltersRepository mMockFiltersRepository;
    @Mock
    private FilterType mFilterType;
    private GetFilterTypesUseCase mUseCase;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mUseCase = new GetFilterTypesUseCase(mMockFiltersRepository);
    }

    @Test
    public void testGetFilterTypesUseCaseObservableHappyCase() {
        mUseCase.get(mFilterType);

        List<String> filters = new ArrayList<>();
        filters.add(mFilterType.getId());
        verify(mMockFiltersRepository).getFiltersByFilterType(0, 100, filters);
        verifyNoMoreInteractions(mMockFiltersRepository);
    }
}