package nl.autovooru.android.common.utils;

import android.text.TextUtils;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public final class StringUtils {

    private StringUtils() {

    }

    public static boolean isEmpty(CharSequence str) {
        return TextUtils.isEmpty(str) || TextUtils.getTrimmedLength(str) == 0;
    }
}
