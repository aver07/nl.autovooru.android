package nl.autovooru.android.common.utils;

import android.os.Build;

/**
 * Helper class for API version checks
 * <p/>
 * Created by Antonenko Viacheslav on 02/10/15.
 */
public final class ApiUtils {

    private ApiUtils() {

    }

    public static boolean isKindleFire() {
        return "Amazon".equalsIgnoreCase(Build.MANUFACTURER)
                && "Kindle Fire".equalsIgnoreCase(Build.MODEL);
    }

    /**
     * @return <code>true</code> if build version >= API {@value Build.VERSION_CODES#ICE_CREAM_SANDWICH}
     */
    public static boolean hasIceCreamSandwich() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    }

    /**
     * @return <code>true</code> if build version >= API {@value Build.VERSION_CODES#LOLLIPOP_MR1}
     */
    public static boolean hasLOLLIPOP_MR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1;
    }

    /**
     * @return <code>true</code> if build version >= API {@value Build.VERSION_CODES#LOLLIPOP}
     */
    public static boolean hasLOLLIPOP() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    /**
     * @return <code>true</code> if build version >= API {@value Build.VERSION_CODES#HONEYCOMB}
     */
    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    /**
     * @return <code>true</code> if build version >= API {@value Build.VERSION_CODES#GINGERBREAD}
     */
    public static boolean hasGingerbread() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
    }

    /**
     * @return <code>true</code> if build version >= API {@value Build.VERSION_CODES#JELLY_BEAN}
     */
    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    /**
     * @return <code>true</code> if build version >= API {@value Build.VERSION_CODES#JELLY_BEAN_MR2}
     */
    public static boolean hasJellyBeanMR2() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;
    }
}
