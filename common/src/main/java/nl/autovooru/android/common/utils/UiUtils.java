package nl.autovooru.android.common.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import nl.autovooru.android.common.R;

/**
 * Created by Antonenko Viacheslav on 03/10/15.
 */
public final class UiUtils {

    private static Handler sMainThreadHandler;

    private UiUtils() {

    }

    /**
     * @return a {@link Handler} tied to the main thread.
     */
    public static Handler getMainThreadHandler() {
        if (sMainThreadHandler == null) {
            // No need to synchronize -- it's okay to create an extra Handler, which will be used
            // only once and then thrown away.
            sMainThreadHandler = new Handler(Looper.getMainLooper());
        }
        return sMainThreadHandler;
    }

    public static boolean isTablet(@NonNull Context context) {
        return context.getResources().getBoolean(R.bool.is_tablet);
    }

    public static boolean isHandset(@NonNull Context context) {
        return !isTablet(context);
    }

    public static boolean isLandscape(@NonNull Context context) {
        return context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    public static boolean isOnUiThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public static void hideSoftKeyboard(@NonNull Context ctx, IBinder windowToken) {
        final InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(windowToken, 0);
    }

    /**
     * Show soft keyboard
     *
     * @param ctx
     * @param view
     */
    public static void showSoftKeyboard(@NonNull final Context ctx, @NonNull final View view) {
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                final InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.showSoftInput(view, 0);
                }
            }
        }, 50);
    }
}
