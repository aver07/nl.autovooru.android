package nl.autovooru.android.common.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

import nl.autovooru.android.common.BuildConfig;
import nl.autovooru.android.common.utils.L;
import nl.autovooru.android.common.utils.Throttle;
import nl.autovooru.android.common.utils.UiUtils;

/**
 * A custom WebView that is robust to rapid resize events in sequence.
 * <p/>
 * This is useful for a WebView which needs to have a layout of {@code WRAP_CONTENT}, since any
 * contents with percent-based height will force the WebView to infinitely expand (or shrink).
 */
public class RigidWebView extends WebView {

    private static final int MIN_RESIZE_INTERVAL = 200;
    private static final int MAX_RESIZE_INTERVAL = 300;
    private final L LOG = L.getLogger(getClass());
    private int mRealWidth;
    private int mRealHeight;
    private boolean mIgnoreNext;
    private long mLastSizeChangeTime = -1;
    private final Throttle mThrottle = new Throttle(getClass().getName(),
            new Runnable() {
                @Override
                public void run() {
                    performSizeChangeDelayed();
                }
            }, UiUtils.getMainThreadHandler(),
            MIN_RESIZE_INTERVAL, MAX_RESIZE_INTERVAL);

    public RigidWebView(Context context) {
        super(context);
    }

    public RigidWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RigidWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onSizeChanged(int w, int h, int ow, int oh) {
        mRealWidth = w;
        mRealHeight = h;
        long now = System.currentTimeMillis();
        boolean recentlySized = (now - mLastSizeChangeTime < MIN_RESIZE_INTERVAL);

        // It's known that the previous resize event may cause a resize event immediately. If
        // this happens sufficiently close to the last resize event, drop it on the floor.
        if (mIgnoreNext) {
            mIgnoreNext = false;
            if (recentlySized) {
                if (BuildConfig.DEBUG) {
                    LOG.w("Supressing size change in RigidWebView");
                }
                return;
            }
        }

        if (recentlySized) {
            mThrottle.onEvent();
        } else {
            // It's been a sufficiently long time - just perform the resize as normal. This should
            // be the normal code path.
            performSizeChange(ow, oh);
        }
    }

    private void performSizeChange(int ow, int oh) {
        super.onSizeChanged(mRealWidth, mRealHeight, ow, oh);
        mLastSizeChangeTime = System.currentTimeMillis();
    }

    private void performSizeChangeDelayed() {
        mIgnoreNext = true;
        performSizeChange(getWidth(), getHeight());
    }
}
