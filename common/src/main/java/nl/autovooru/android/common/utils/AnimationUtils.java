package nl.autovooru.android.common.utils;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Antonenko Viacheslav on 16/11/15.
 */
public final class AnimationUtils {

    private AnimationUtils() {

    }

    @NonNull
    public static ValueAnimator slideAnimator(@NonNull final View view, int start, int end) {
        final ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();

                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = value;
                view.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }

    public static void expand(@NonNull View view, ValueAnimator animator) {
        //set Visible
        view.setVisibility(View.VISIBLE);
        animator.start();
    }

    public static void collapse(@NonNull final View view) {
        final int finalHeight = view.getHeight();

        final ValueAnimator animator = slideAnimator(view, finalHeight, 0);

        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animator) {
                //Height=0, but it set visibility to GONE
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        animator.start();
    }
}
