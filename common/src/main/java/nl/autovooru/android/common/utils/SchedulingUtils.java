package nl.autovooru.android.common.utils;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewTreeObserver;

/**
 * Static methods that are useful for scheduling actions to occur at a later time.
 *
 * @author Antonenko Viacheslav
 */
public final class SchedulingUtils {

    private SchedulingUtils() {
    }

    /**
     * Runs a piece of code after the next layout run
     *
     * @param view     view
     * @param runnable runnable
     */
    public static void doAfterLayout(@NonNull final View view, @NonNull final Runnable runnable) {
        final ViewTreeObserver.OnGlobalLayoutListener listener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                // layout pass done, unregister for further events
                if (ApiUtils.hasJellyBean()) {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    //noinspection deprecation
                    view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                runnable.run();
            }
        };
        view.getViewTreeObserver().addOnGlobalLayoutListener(listener);
    }

    /**
     * Runs a piece of code just before the next draw.
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void doAfterDraw(@NonNull final View view, @NonNull final Runnable runnable) {
        final ViewTreeObserver.OnDrawListener listener = new ViewTreeObserver.OnDrawListener() {
            @Override
            public void onDraw() {
                view.getViewTreeObserver().removeOnDrawListener(this);
                runnable.run();
            }
        };
        view.getViewTreeObserver().addOnDrawListener(listener);
    }

    public static void doPreDraw(@NonNull final View view, @NonNull final Runnable runnable) {
        final ViewTreeObserver.OnPreDrawListener listener = new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                view.getViewTreeObserver().removeOnPreDrawListener(this);
                runnable.run();
                return true;
            }
        };
        view.getViewTreeObserver().addOnPreDrawListener(listener);
    }
}
