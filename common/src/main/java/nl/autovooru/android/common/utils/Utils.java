package nl.autovooru.android.common.utils;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by Antonenko Viacheslav on 03/10/15.
 */
public class Utils {

    private static final L LOG = L.getLogger(Utils.class);

    private Utils() {
    }

    /**
     * @param context context
     * @return {@code true} if application in debug mode, {@code false} otherwise
     */
    public static boolean isDebuggable(@NonNull Context context) {
        return (0 != (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE));
    }

    /**
     * Check that network is available or connecting
     *
     * @param ctx context
     * @return {@code true} if network connectivity exists or is in the process
     * of being established, {@code false} otherwise.
     */
    public static boolean isNetworkConnected(@NonNull Context ctx) {
        final ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo info = cm.getActiveNetworkInfo();
        return !(info == null || !info.isConnectedOrConnecting());

    }

    /**
     * Check that location services is enabled
     *
     * @param context context
     * @return {@code true} if location services enabled, {@code false} otherwise
     */
    public static boolean isLocationEnabled(@NonNull Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER) || manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    /**
     * Return best location provider
     *
     * @param context context
     * @return string name of best location provider
     */
    public static String getBestLocationProvider(@NonNull Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        return manager.getBestProvider(criteria, true);
    }

    /**
     * Check that external storage available
     *
     * @return {@code true} if storage available, {@code false} otherwise
     */
    public static boolean isExternalStorageAvailable() {
        boolean mExternalStorageAvailable;
        boolean mExternalStorageWriteable;
        String state = Environment.getExternalStorageState();

        switch (state) {
            case Environment.MEDIA_MOUNTED:
                // We can read and write the media
                mExternalStorageAvailable = mExternalStorageWriteable = true;
                break;
            case Environment.MEDIA_MOUNTED_READ_ONLY:
                // We can only read the media
                mExternalStorageAvailable = true;
                mExternalStorageWriteable = false;
                break;
            default:
                // Something else is wrong. It may be one of many other states, but all we need
                //  to know is we can neither read nor write
                mExternalStorageAvailable = mExternalStorageWriteable = false;
                break;
        }

        return mExternalStorageAvailable && mExternalStorageWriteable;
    }

    /**
     * Check that device equipped with camera
     *
     * @param context context
     * @return {@code true} if camera exists, {@code false} otherwise
     */
    public static boolean hasCamera(@NonNull Context context) {
        PackageManager pm = context.getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    /**
     * Check that device equipped with telephony
     *
     * @param context context
     * @return {@code true} if telephony exists, {@code false} otherwise
     */
    public static boolean hasTelephony(@NonNull Context context) {
        PackageManager pm = context.getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
    }

    public static void copyToClipboard(@NonNull Context context, String text) {
        if (ApiUtils.hasHoneycomb()) {
            ClipboardManager clipBoard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clipData = ClipData.newPlainText("", text);
            clipBoard.setPrimaryClip(clipData);
        } else {
            @SuppressWarnings("deprecation")
            android.text.ClipboardManager clipBoard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipBoard.setText(text);
        }
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public static void enableStrictMode() {
        if (ApiUtils.hasGingerbread()) {
            StrictMode.ThreadPolicy.Builder threadPolicyBuilder =
                    new StrictMode.ThreadPolicy.Builder()
                            .detectAll()
                            .penaltyLog();
            final StrictMode.VmPolicy.Builder vmPolicyBuilder =
                    new StrictMode.VmPolicy.Builder()
                            .detectLeakedSqlLiteObjects()
                            .penaltyLog();
            if (ApiUtils.hasHoneycomb()) {
                vmPolicyBuilder.detectLeakedClosableObjects();
            }
            if (ApiUtils.hasJellyBean()) {
                vmPolicyBuilder.detectLeakedRegistrationObjects();
            }
            if (ApiUtils.hasJellyBeanMR2()) {
                vmPolicyBuilder.detectFileUriExposure();
            }

            StrictMode.setThreadPolicy(threadPolicyBuilder.build());
            StrictMode.setVmPolicy(vmPolicyBuilder.build());
        }
    }

    public static String getDeviceInfo(@NonNull Context ctx) {
        PackageInfo packageInfo = null;
        try {
            final PackageManager packageManager = ctx.getPackageManager();
            final String packageName = ctx.getPackageName();
            if (packageManager != null) {
                packageInfo = packageManager.getPackageInfo(packageName, 0);
            }
        } catch (PackageManager.NameNotFoundException e) {
            LOG.e("Can't get PackageInfo;", e);
            packageInfo = null;
        }
        final StringBuilder builder = new StringBuilder();
        builder.append("android|");
        builder.append(Build.BRAND).append("|");
        builder.append(Build.MODEL).append("|");
        builder.append(Build.VERSION.RELEASE).append("|");
        if (packageInfo != null) {
            builder.append(packageInfo.versionName);
        }
        builder.append("|");
        if (packageInfo != null) {
            builder.append(String.valueOf(packageInfo.versionCode));
        }
        builder.append("|");

        final String result = builder.toString();
        LOG.d("Uuid: " + result);
        return result;
    }

    public static String textToHtmlConvertingURLsToLinks(String text) {
        if (TextUtils.isEmpty(text)) {
            return text;
        }


        return text.replaceAll("(\\A|\\s)((http|https|ftp|mailto):\\S+)(\\s|\\z)",
                "$1<a href=\"$2\">$2</a>$4");
    }

    /**
     * Stores an image on the storage
     *
     * @param image       the image to store.
     * @param pictureFile the file in which it must be stored
     */
    public static void storeImage(Bitmap image, File pictureFile) {
        if (pictureFile == null) {
            LOG.d("Error creating media file, check storage permissions: ");
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            LOG.d("File not found: " + e.getMessage());
        } catch (IOException e) {
            LOG.d("Error accessing file: " + e.getMessage());
        }
    }

    public static String capitalize(String str) {
        return capitalize(str, null);
    }

    public static String capitalize(String str, char[] delimiters) {
        if (str == null || str.length() == 0) {
            return str;
        }
        int strLen = str.length();
        StringBuilder buffer = new StringBuilder(strLen);

        int delimitersLen = 0;
        if (delimiters != null) {
            delimitersLen = delimiters.length;
        }

        boolean capitalizeNext = true;
        for (int i = 0; i < strLen; i++) {
            char ch = str.charAt(i);

            boolean isDelimiter = false;
            if (delimiters == null) {
                isDelimiter = Character.isWhitespace(ch);
            } else {
                for (int j = 0; j < delimitersLen; j++) {
                    if (ch == delimiters[j]) {
                        isDelimiter = true;
                        break;
                    }
                }
            }

            if (isDelimiter) {
                buffer.append(ch);
                capitalizeNext = true;
            } else if (capitalizeNext) {
                buffer.append(Character.toTitleCase(ch));
                capitalizeNext = false;
            } else {
                buffer.append(ch);
            }
        }
        return buffer.toString();
    }

    /**
     * Returns the most accurate and timely previously detected location.
     * Where the last result is beyond the specified maximum distance or
     * latency a one-off location update is returned via the {@link android.location.LocationListener}
     * specified in setChangedLocationListener.
     *
     * @param minTime Minimum time required between location updates.
     * @return The most accurate and / or timely previously detected location.
     */
    @Nullable
    public static Location getLastBestLocation(Context ctx, long minTime) {
        Location bestResult = null;
        float bestAccuracy = Float.MAX_VALUE;
        long bestTime = Long.MAX_VALUE;

        LocationManager locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
        // Iterate through all the providers on the system, keeping
        // note of the most accurate result within the acceptable time limit.
        // If no result is found within maxTime, return the newest Location.
        List<String> matchingProviders = locationManager.getAllProviders();
        for (String provider : matchingProviders) {
            Location location = locationManager.getLastKnownLocation(provider);
            if (location != null) {
                float accuracy = location.getAccuracy();
                long time = location.getTime();

                if ((time < minTime && accuracy < bestAccuracy)) {
                    bestResult = location;
                    bestAccuracy = accuracy;
                    bestTime = time;
                } else if (time > minTime && bestAccuracy == Float.MAX_VALUE && time < bestTime) {
                    bestResult = location;
                    bestTime = time;
                }
            }
        }

        return bestResult;
    }

}
