package nl.autovooru.android.data.entity.advert.mapper;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import nl.autovooru.android.data.entity.advert.AdvertFieldsEntity;
import nl.autovooru.android.data.entity.advert.AutoInfoEntity;
import nl.autovooru.android.data.entity.advert.YoutubeInfoEntity;
import nl.autovooru.android.data.entity.machines.MachineFilterEntity;
import nl.autovooru.android.data.entity.machines.MachineSearchFiltersEntity;
import nl.autovooru.android.domain.model.advert.AdvertFields;
import nl.autovooru.android.domain.model.advert.AutoInfo;
import nl.autovooru.android.domain.model.advert.YoutubeInfo;
import nl.autovooru.android.domain.model.machine.MachineFilter;
import nl.autovooru.android.domain.model.machine.MachineSearchFilters;

/**
 * Created by Antonenko Viacheslav on 05/01/16.
 */
public final class AdvertMapper {

    private AdvertMapper() {
        // no-op
    }

    @Nullable
    public static AdvertFields entityToModel(@Nullable AdvertFieldsEntity entity) {
        if (entity == null) {
            return null;
        }

        AdvertFields advertFields = new AdvertFields();

        advertFields.setName(machineFilterEntityToMachineFilter(entity.getName()));
        advertFields.setDescription(machineFilterEntityToMachineFilter(entity.getDescription()));
        advertFields.setToken(machineFilterEntityToMachineFilter(entity.getToken()));
        advertFields.setTopMachineMarks(machineFilterEntityToMachineFilter(entity.getTopMachineMarks()));
        advertFields.setMarks(machineFilterEntityToMachineFilter(entity.getMarks()));
        advertFields.setModels(machineFilterEntityToMachineFilter(entity.getModels()));
        advertFields.setYear(machineFilterEntityToMachineFilter(entity.getYear()));
        advertFields.setFuel(machineFilterEntityToMachineFilter(entity.getFuel()));
        advertFields.setTransmission(machineFilterEntityToMachineFilter(entity.getTransmission()));
        advertFields.setColor(machineFilterEntityToMachineFilter(entity.getColor()));
        advertFields.setBody(machineFilterEntityToMachineFilter(entity.getBody()));
        advertFields.setOtherFilters(machineFilterEntityListToMachineFilterList(entity.getOtherFilters()));
        advertFields.setPrice(machineFilterEntityToMachineFilter(entity.getPrice()));
        advertFields.setDoors(machineFilterEntityToMachineFilter(entity.getDoors()));
        advertFields.setPromoPrice(machineFilterEntityToMachineFilter(entity.getPromoPrice()));
        advertFields.setMileage(machineFilterEntityToMachineFilter(entity.getMileage()));
        advertFields.setEngineCapacity(machineFilterEntityToMachineFilter(entity.getEngineCapacity()));
        advertFields.setFirstOwner(machineFilterEntityToMachineFilter(entity.getFirstOwner()));
        advertFields.setNap(machineFilterEntityToMachineFilter(entity.getNap()));

        return advertFields;
    }

    @Nullable
    public static AutoInfo entityToModel(@Nullable AutoInfoEntity entity) {
        if (entity == null) {
            return null;
        }

        AutoInfo autoInfo = new AutoInfo();

        autoInfo.setCategoryId(entity.getCategoryId());
        autoInfo.setMarkId(entity.getMarkId());
        autoInfo.setModelId(entity.getModelId());
        autoInfo.setBodyId(entity.getBodyId());
        autoInfo.setFuelId(entity.getFuelId());
        autoInfo.setColorId(entity.getColorId());
        autoInfo.setEngineCapacity(entity.getEngineCapacity());
        autoInfo.setToken(entity.getToken());

        return autoInfo;
    }

    @Nullable
    public static YoutubeInfo entityToModel(@Nullable YoutubeInfoEntity entity) {
        if (entity == null) {
            return null;
        }

        YoutubeInfo youtubeInfo = new YoutubeInfo();

        youtubeInfo.setId(entity.getId());
        youtubeInfo.setLinkUrl(entity.getLinkUrl());
        youtubeInfo.setImageUrl(entity.getImageUrl());

        return youtubeInfo;
    }

    @Nullable
    public static MachineSearchFilters machineSearchFiltersEntityToMachineSearchFilters(@Nullable MachineSearchFiltersEntity machineSearchFiltersEntity) {
        if (machineSearchFiltersEntity == null) {
            return null;
        }

        MachineSearchFilters machineSearchFilters = new MachineSearchFilters();

        machineSearchFilters.setMainFilters(machineMainFiltersEntityToMachineMainFilters(machineSearchFiltersEntity.getMainFilters()));
        machineSearchFilters.setToggleFilters(machineToggleFiltersEntityTomachineToggleFilters(machineSearchFiltersEntity.getToggleFilters()));
        machineSearchFilters.setOtherFilters(machineFilterEntityListToMachineFilterList(machineSearchFiltersEntity.getOtherFilters()));

        return machineSearchFilters;
    }

    @Nullable
    public static MachineSearchFilters.MainFilters machineMainFiltersEntityToMachineMainFilters(@Nullable MachineSearchFiltersEntity.MainFiltersEntity mainFiltersEntity) {
        if (mainFiltersEntity == null) {
            return null;
        }

        MachineSearchFilters.MainFilters mainFilters_ = new MachineSearchFilters.MainFilters();

        mainFilters_.setTopMachineMarks(machineFilterEntityToMachineFilter(mainFiltersEntity.getTopMachineMarks()));
        mainFilters_.setMachineMarks(machineFilterEntityToMachineFilter(mainFiltersEntity.getMachineMarks()));
        mainFilters_.setMachineModels(machineFilterEntityToMachineFilter(mainFiltersEntity.getMachineModels()));
        mainFilters_.setMachineYear(machineFilterEntityToMachineFilter(mainFiltersEntity.getMachineYear()));
        mainFilters_.setPrice(machineFilterEntityToMachineFilter(mainFiltersEntity.getPrice()));
        mainFilters_.setFuel(machineFilterEntityToMachineFilter(mainFiltersEntity.getFuel()));
        mainFilters_.setUserType(machineFilterEntityToMachineFilter(mainFiltersEntity.getUserType()));
        mainFilters_.setDistance(machineFilterEntityToMachineFilter(mainFiltersEntity.getDistance()));
        mainFilters_.setMileage(machineFilterEntityToMachineFilter(mainFiltersEntity.getMileage()));
        mainFilters_.setKeywords(machineFilterEntityToMachineFilter(mainFiltersEntity.getKeywords()));
        mainFilters_.setPostcode(machineFilterEntityToMachineFilter(mainFiltersEntity.getPostcode()));

        return mainFilters_;
    }

    @Nullable
    public static MachineFilter machineFilterEntityToMachineFilter(@Nullable MachineFilterEntity filterEntity) {
        if (filterEntity == null) {
            return null;
        }

        MachineFilter machineFilter = new MachineFilter();

        machineFilter.setValue(filterEntity.getValue());
        machineFilter.setName(filterEntity.getName());
        machineFilter.setFirstOptionsList(optionsListToOptionsList(filterEntity.getFirstOptionsList()));
        machineFilter.setSecondOptionsList(optionsListToOptionsList(filterEntity.getSecondOptionsList()));

        return machineFilter;
    }

    @Nullable
    public static MachineSearchFilters.ToggleFilters machineToggleFiltersEntityTomachineToggleFilters(@Nullable MachineSearchFiltersEntity.ToggleFilters entity) {
        if (entity == null) {
            return null;
        }

        MachineSearchFilters.ToggleFilters toggleFilters = new MachineSearchFilters.ToggleFilters();

        toggleFilters.setOfferTime(machineFilterEntityToMachineFilter(entity.getOfferTime()));
        toggleFilters.setTransmission(machineFilterEntityToMachineFilter(entity.getTransmission()));
        toggleFilters.setColor(machineFilterEntityToMachineFilter(entity.getColor()));
        toggleFilters.setBody(machineFilterEntityToMachineFilter(entity.getBody()));

        return toggleFilters;
    }

    @Nullable
    public static MachineFilter.Options optionsEntityToOptions(MachineFilterEntity.Options optionsEntity) {
        if (optionsEntity == null) {
            return null;
        }

        MachineFilter.Options options = new MachineFilter.Options();

        options.setValue(optionsEntity.getValue());
        options.setName(optionsEntity.getName());

        return options;
    }

    @Nullable
    protected static List<MachineFilter> machineFilterEntityListToMachineFilterList(@Nullable List<MachineFilterEntity> list) {
        if (list == null) {
            return null;
        }

        List<MachineFilter> list_ = new ArrayList<MachineFilter>();
        for (MachineFilterEntity machineFilterEntity : list) {
            list_.add(machineFilterEntityToMachineFilter(machineFilterEntity));
        }

        return list_;
    }

    @Nullable
    protected static List<MachineFilter.Options> optionsListToOptionsList(@Nullable List<MachineFilterEntity.Options> list) {
        if (list == null) {
            return null;
        }

        List<MachineFilter.Options> list_ = new ArrayList<MachineFilter.Options>();
        for (MachineFilterEntity.Options options : list) {
            list_.add(optionsEntityToOptions(options));
        }

        return list_;
    }
}
