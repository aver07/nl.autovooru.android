package nl.autovooru.android.data.entity;

import android.support.annotation.Nullable;

import java.util.HashSet;
import java.util.Set;

import nl.autovooru.android.domain.model.query.Order;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public final class OrderMapper {

    private OrderMapper() {
        // no-op
    }

    @Nullable
    public static Order entityToModel(@Nullable OrderEntity entity) {
        if (entity == null) {
            return null;
        }

        Order order = new Order();

        order.setSort(entityToModel(entity.getSort()));
        order.setFields(fieldsEntitySetToFieldsSet(entity.getFields()));

        return order;
    }

    @Nullable
    public static Order.Fields entityToModel(@Nullable OrderEntity.FieldsEntity entity) {
        if (entity == null) {
            return null;
        }

        Order.Fields fields;

        switch (entity) {
            case price:
                fields = Order.Fields.price;
                break;
            case updated:
                fields = Order.Fields.updated;
                break;
            default:
                throw new IllegalArgumentException("Unexpected enum constant: " + entity);
        }

        return fields;
    }

    @Nullable
    public static Order.Sort entityToModel(@Nullable OrderEntity.SortEntity entity) {
        if (entity == null) {
            return null;
        }

        Order.Sort sort;

        switch (entity) {
            case asc:
                sort = Order.Sort.asc;
                break;
            case desc:
                sort = Order.Sort.desc;
                break;
            default:
                throw new IllegalArgumentException("Unexpected enum constant: " + entity);
        }

        return sort;
    }

    @Nullable
    public static OrderEntity modelToEntity(@Nullable Order order) {
        if (order == null) {
            return null;
        }

        OrderEntity orderEntity = new OrderEntity();

        orderEntity.setSort(modelToEntity(order.getSort()));
        orderEntity.setFields(fieldsSetToFieldsEntitySet(order.getFields()));

        return orderEntity;
    }

    @Nullable
    public static OrderEntity.FieldsEntity modelToEntity(@Nullable Order.Fields fields) {
        if (fields == null) {
            return null;
        }

        OrderEntity.FieldsEntity fieldsEntity;

        switch (fields) {
            case price:
                fieldsEntity = OrderEntity.FieldsEntity.price;
                break;
            case updated:
                fieldsEntity = OrderEntity.FieldsEntity.updated;
                break;
            default:
                throw new IllegalArgumentException("Unexpected enum constant: " + fields);
        }

        return fieldsEntity;
    }

    @Nullable
    public static OrderEntity.SortEntity modelToEntity(@Nullable Order.Sort sort) {
        if (sort == null) {
            return null;
        }

        OrderEntity.SortEntity sortEntity;

        switch (sort) {
            case asc:
                sortEntity = OrderEntity.SortEntity.asc;
                break;
            case desc:
                sortEntity = OrderEntity.SortEntity.desc;
                break;
            default:
                throw new IllegalArgumentException("Unexpected enum constant: " + sort);
        }

        return sortEntity;
    }

    @Nullable
    protected static Set<Order.Fields> fieldsEntitySetToFieldsSet(@Nullable Set<OrderEntity.FieldsEntity> set) {
        if (set == null) {
            return null;
        }

        Set<Order.Fields> set_ = new HashSet<Order.Fields>();
        for (OrderEntity.FieldsEntity fieldsEntity : set) {
            set_.add(entityToModel(fieldsEntity));
        }

        return set_;
    }

    @Nullable
    protected static Set<OrderEntity.FieldsEntity> fieldsSetToFieldsEntitySet(@Nullable Set<Order.Fields> set) {
        if (set == null) {
            return null;
        }

        Set<OrderEntity.FieldsEntity> set_ = new HashSet<OrderEntity.FieldsEntity>();
        for (Order.Fields fields : set) {
            set_.add(modelToEntity(fields));
        }

        return set_;
    }
}
