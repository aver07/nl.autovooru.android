package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;

/**
 * Created by Antonenko Viacheslav on 19/01/16.
 */
public class PushQuery {

    @Nullable
    @SerializedName("lang")
    private final String mLanguage;
    @NonNull
    @SerializedName("deviceToken")
    private final String mDeviceToken;
    @NonNull
    @SerializedName("user")
    private AuthInfoEntity mUser;


    public PushQuery(@NonNull String deviceToken, @NonNull AuthInfoEntity authInfo, @Nullable String language) {
        mLanguage = language;
        mUser = authInfo;
        mDeviceToken = deviceToken;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }
}
