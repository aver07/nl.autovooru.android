package nl.autovooru.android.data.exceptions.authorization;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public class InvalidEmailException extends Exception {

    public InvalidEmailException() {
        super();
    }

    public InvalidEmailException(String detailMessage) {
        super(detailMessage);
    }
}
