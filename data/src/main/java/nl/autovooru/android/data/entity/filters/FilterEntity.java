package nl.autovooru.android.data.entity.filters;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 15/11/15.
 */
public final class FilterEntity {

    @SerializedName("af_id")
    private String mId;
    @SerializedName("name")
    private String mName;

    public String getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    @Override
    public String toString() {
        return "FilterEntity{" +
                "mId='" + mId + '\'' +
                ", mName='" + mName + '\'' +
                '}';
    }
}
