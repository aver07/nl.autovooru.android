package nl.autovooru.android.data.entity.machines;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.OrderEntity;
import nl.autovooru.android.data.entity.Page;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;

/**
 * Created by Antonenko Viacheslav on 16/11/15.
 */
public class MachineListQuery {

    @NonNull
    @SerializedName("page")
    private Page mPage;
    @Nullable
    @SerializedName("lang")
    private String mLanguage;
    @Nullable
    @SerializedName("ua_id")
    private String mDealerId;
    @SerializedName("filters")
    @Nullable
    private Map<String, Object> mFilters;
    @SerializedName("order")
    private OrderEntity mOrder;
    @SerializedName("user")
    private AuthInfoEntity mAuthInfo;

    private MachineListQuery(Builder builder) {
        mPage = builder.mPage;
        mLanguage = builder.mLanguage;
        mFilters = builder.mFilters;
        mOrder = builder.mOrder;
        mDealerId = builder.mDealerId;
        mAuthInfo = builder.mAuthInfo;
    }

    public static Builder newBuilder(@NonNull Page page) {
        return new Builder(page);
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }

    public static final class Builder {

        @NonNull
        private Page mPage;
        @Nullable
        private String mLanguage;
        @Nullable
        private Map<String, Object> mFilters;
        @Nullable
        private OrderEntity mOrder;
        @Nullable
        private String mDealerId;
        @Nullable
        private AuthInfoEntity mAuthInfo;

        public Builder(@NonNull Page page) {
            mPage = page;
        }

        public Builder setLanguage(@Nullable String language) {
            mLanguage = language;

            return this;
        }

        public Builder addFilter(@NonNull String key, @NonNull String value) {
            if (mFilters == null) {
                mFilters = new HashMap<>();
            }
            mFilters.put(key, value);

            return this;
        }

        public Builder addFilters(@NonNull Map<String, Object> filters) {
            if (mFilters == null) {
                mFilters = new HashMap<>();
            }
            mFilters.putAll(filters);

            return this;
        }

        public Builder setOrder(OrderEntity order) {
            mOrder = order;

            return this;
        }

        public Builder setDealerId(String dealerId) {
            mDealerId = dealerId;

            return this;
        }

        public Builder setAuthInfo(AuthInfoEntity authInfo) {
            mAuthInfo = authInfo;

            return this;
        }

        public MachineListQuery build() {
            return new MachineListQuery(this);
        }
    }
}
