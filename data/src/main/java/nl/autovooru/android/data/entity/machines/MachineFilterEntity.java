package nl.autovooru.android.data.entity.machines;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Antonenko Viacheslav on 10/12/15.
 */
public final class MachineFilterEntity {

    @SerializedName("value")
    private String mValue;
    @SerializedName("name")
    private String mName;
    @SerializedName("options")
    private List<Options> mFirstOptionsList;
    @Nullable
    @SerializedName("options_1")
    private List<Options> mSecondOptionsList;

    public String getValue() {
        return mValue;
    }

    public String getName() {
        return mName;
    }

    public List<Options> getFirstOptionsList() {
        return mFirstOptionsList;
    }

    @Nullable
    public List<Options> getSecondOptionsList() {
        return mSecondOptionsList;
    }

    public static class Options {
        @SerializedName("value")
        private String mValue;
        @SerializedName("name")
        private String mName;

        public String getValue() {
            return mValue;
        }

        public String getName() {
            return mName;
        }
    }
}
