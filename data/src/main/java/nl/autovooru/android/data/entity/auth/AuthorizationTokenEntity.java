package nl.autovooru.android.data.entity.auth;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
public class AuthorizationTokenEntity {

    @SerializedName("code")
    private String mCode;
    @SerializedName("access_token")
    private String mAccessToken;
    @SerializedName("expire_date")
    private long mExpireDate;
    @SerializedName("ua_id")
    private String mDealerId;

    public String getCode() {
        return mCode;
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    public long getExpireDate() {
        return mExpireDate;
    }

    public String getDealerId() {
        return mDealerId;
    }
}
