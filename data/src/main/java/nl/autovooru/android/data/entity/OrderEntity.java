package nl.autovooru.android.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Antonenko Viacheslav on 02/12/15.
 */
public final class OrderEntity {

    @SerializedName("type")
    public SortEntity mSort;
    @SerializedName("fields")
    public Set<FieldsEntity> mFields = new TreeSet<>();

    public OrderEntity() {
    }

    public SortEntity getSort() {
        return mSort;
    }

    public void setSort(SortEntity sort) {
        mSort = sort;
    }

    public Set<FieldsEntity> getFields() {
        return mFields;
    }

    public void setFields(Set<FieldsEntity> fields) {
        mFields = fields;
    }

    public void addField(FieldsEntity field) {
        mFields.add(field);
    }

    public void removeFiled(FieldsEntity fields) {
        mFields.remove(fields);
    }

    public enum SortEntity {
        asc, desc
    }

    public enum FieldsEntity {
        price, updated
    }
}
