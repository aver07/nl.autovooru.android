package nl.autovooru.android.data.repository.datasource.auth;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import nl.autovooru.android.data.api.AutovooruApiService;
import nl.autovooru.android.data.entity.auth.AuthorizationTokenEntity;
import nl.autovooru.android.data.entity.queries.ForgotPasswordQuery;
import nl.autovooru.android.data.entity.queries.GetAccessTokenQuery;
import nl.autovooru.android.data.entity.queries.GetAuthorizationCodeQuery;
import nl.autovooru.android.data.entity.queries.RegistrationNewOrganizationQuery;
import nl.autovooru.android.data.entity.queries.RegistrationNewsUserQuery;
import nl.autovooru.android.data.exceptions.ValidateServerResponse;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
@Singleton
public class CloudAuthorizationDataStore implements AuthorizationDataStore {

    private final AutovooruApiService mApiService;
    private final String mLanguage;

    @Inject
    public CloudAuthorizationDataStore(AutovooruApiService apiService,
                                       @Named("appLanguage") String language) {
        mApiService = apiService;
        mLanguage = language;
    }

    @Override
    public Observable<AuthorizationTokenEntity> authorize(String login, String password) {
        return mApiService.getAuthorizationCode(new GetAuthorizationCodeQuery(login, password, mLanguage))
                .flatMap(new ValidateServerResponse<>())
//                .flatMap(response -> refreshToken(response.getCode()));
                .flatMap(response -> refreshToken(response.getCode()));
    }

    @Override
    public Observable<AuthorizationTokenEntity> refreshToken(String code) {
        return mApiService.getAccessToken(new GetAccessTokenQuery(code, mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<Void> forgotPassword(String email) {
        return mApiService.forgotPassword(new ForgotPasswordQuery(email, mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<AuthorizationTokenEntity> registrationNewUser(String name, String surname, String postcode, String city, String address, String phone, String email, String password) {
        final RegistrationNewsUserQuery query = new RegistrationNewsUserQuery(name, surname,
                postcode, city, address, phone, email, password, mLanguage);
        return mApiService.registrationNewUser(query)
                .flatMap(new ValidateServerResponse<>())
                .flatMap(token -> refreshToken(token.getCode()));
    }

    @Override
    public Observable<Void> registrationNewOrganization(String name, String surname, String companyName, String autosCount, String postcode, String city, String address, String phone, String email) {
        final RegistrationNewOrganizationQuery query = new RegistrationNewOrganizationQuery(name,
                surname, companyName, autosCount, postcode, city, address, phone, email, mLanguage);
        return mApiService.registrationNewOrganization(query)
                .flatMap(new ValidateServerResponse<>());
    }
}
