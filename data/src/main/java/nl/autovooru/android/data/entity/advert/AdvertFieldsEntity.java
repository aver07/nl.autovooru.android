package nl.autovooru.android.data.entity.advert;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import nl.autovooru.android.data.entity.machines.MachineFilterEntity;

/**
 * Created by Antonenko Viacheslav on 05/01/16.
 */
public class AdvertFieldsEntity {

    @SerializedName("name")
    private MachineFilterEntity mName;
    @SerializedName("description")
    private MachineFilterEntity mDescription;
    @SerializedName("kenteken")
    private MachineFilterEntity mToken;
    @SerializedName("vmark_top")
    private MachineFilterEntity mTopMachineMarks;
    @SerializedName("vmark")
    private MachineFilterEntity mMarks;
    @SerializedName("vmodel")
    private MachineFilterEntity mModels;
    @SerializedName("year")
    private MachineFilterEntity mYear;
    @SerializedName("vfuel")
    private MachineFilterEntity mFuel;
    @SerializedName("vtrans")
    private MachineFilterEntity mTransmission;
    @SerializedName("color")
    private MachineFilterEntity mColor;
    @SerializedName("vbody")
    private MachineFilterEntity mBody;
    @SerializedName("vopt")
    private List<MachineFilterEntity> mOtherFilters;
    @SerializedName("price")
    private MachineFilterEntity mPrice;
    @SerializedName("doors")
    private MachineFilterEntity mDoors;
    @SerializedName("actie_price")
    private MachineFilterEntity mPromoPrice;
    @SerializedName("mileage")
    private MachineFilterEntity mMileage;
    @SerializedName("capacity")
    private MachineFilterEntity mEngineCapacity;
    @SerializedName("first_owner")
    private MachineFilterEntity mFirstOwner;
    @SerializedName("nap")
    private MachineFilterEntity mNap;

    public MachineFilterEntity getName() {
        return mName;
    }

    public MachineFilterEntity getDescription() {
        return mDescription;
    }

    public MachineFilterEntity getToken() {
        return mToken;
    }

    public MachineFilterEntity getTopMachineMarks() {
        return mTopMachineMarks;
    }

    public MachineFilterEntity getMarks() {
        return mMarks;
    }

    public MachineFilterEntity getModels() {
        return mModels;
    }

    public MachineFilterEntity getYear() {
        return mYear;
    }

    public MachineFilterEntity getFuel() {
        return mFuel;
    }

    public MachineFilterEntity getTransmission() {
        return mTransmission;
    }

    public MachineFilterEntity getColor() {
        return mColor;
    }

    public MachineFilterEntity getBody() {
        return mBody;
    }

    public List<MachineFilterEntity> getOtherFilters() {
        return mOtherFilters;
    }

    public MachineFilterEntity getPrice() {
        return mPrice;
    }

    public MachineFilterEntity getDoors() {
        return mDoors;
    }

    public MachineFilterEntity getPromoPrice() {
        return mPromoPrice;
    }

    public MachineFilterEntity getMileage() {
        return mMileage;
    }

    public MachineFilterEntity getEngineCapacity() {
        return mEngineCapacity;
    }

    public MachineFilterEntity getFirstOwner() {
        return mFirstOwner;
    }

    public MachineFilterEntity getNap() {
        return mNap;
    }
}
