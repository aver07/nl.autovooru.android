package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;

/**
 * Created by Antonenko Viacheslav on 10/12/15.
 */
public class BidMessageQuery {

    @Nullable
    @SerializedName("user")
    private final AuthInfoEntity mUser;
    @NonNull
    @SerializedName("name")
    private final String mName;
    @SerializedName("email")
    private final String mEmail;
    @SerializedName("bid")
    private final String mBid;
    @SerializedName("message")
    private final String mMessage;
    @SerializedName("as_id")
    private final String mMachineId;
    @Nullable
    @SerializedName("lang")
    private final String mLanguage;

    public BidMessageQuery(AuthInfoEntity authInfo, @NonNull String name, @NonNull String machineId, @NonNull String email,
                           @NonNull String bid, @NonNull String message, @Nullable String language) {
        mUser = authInfo;
        mName = name;
        mEmail = email;
        mBid = bid;
        mMessage = message;
        mMachineId = machineId;
        mLanguage = language;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }

}
