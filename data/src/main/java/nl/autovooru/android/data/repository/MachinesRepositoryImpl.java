package nl.autovooru.android.data.repository;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import nl.autovooru.android.data.entity.OrderEntity;
import nl.autovooru.android.data.entity.OrderMapper;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;
import nl.autovooru.android.data.entity.auth.mapper.AuthMapper;
import nl.autovooru.android.data.entity.machines.MachineEntityList;
import nl.autovooru.android.data.entity.machines.details.mapper.DetailsMapper;
import nl.autovooru.android.data.entity.machines.mapper.MachineMapper;
import nl.autovooru.android.data.exceptions.RetryWithTokenRefresh;
import nl.autovooru.android.data.repository.datasource.machine.CloudMachinesDataStore;
import nl.autovooru.android.domain.model.auth.AuthInfo;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.domain.model.machine.details.MachineDetails;
import nl.autovooru.android.domain.model.query.Order;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.domain.repository.AuthorizationRepository;
import nl.autovooru.android.domain.repository.MachinesRepository;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 18/11/15.
 */
@Singleton
public class MachinesRepositoryImpl implements MachinesRepository {

    private final CloudMachinesDataStore mCloudMachinesDataStore;
    private final AccountRepository mAccountRepository;
    private final AuthorizationRepository mAuthorizationRepository;

    @Inject
    public MachinesRepositoryImpl(@NonNull CloudMachinesDataStore cloudMachinesDataStore,
                                  @NonNull AccountRepository accountRepository,
                                  @NonNull AuthorizationRepository authorizationRepository) {
        mCloudMachinesDataStore = cloudMachinesDataStore;
        mAccountRepository = accountRepository;
        mAuthorizationRepository = authorizationRepository;
    }

    @Override
    public Observable<MachineListViewModel> loadMachines(int page, int itemPerPage,
                                                         String dealerId,
                                                         Map<String, Object> filters,
                                                         Order order) {
        final AuthInfo authInfo = mAccountRepository.getAuthInfo();
        final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
        final OrderEntity orderEntity = OrderMapper.modelToEntity(order);
        final Observable<MachineEntityList> observable = mCloudMachinesDataStore.loadMachines(page,
                itemPerPage, dealerId, filters, orderEntity, authInfoEntity);

        return observable.map(machineEntityList -> {
            final int total = machineEntityList.getTotal();
            final int currentPage = machineEntityList.getCurrentPage();
            final int totalPages = machineEntityList.getTotalPages();
            final List<Machine> machineList = MachineMapper.machineEntityToMachine(machineEntityList.getMachineList());
            return new MachineListViewModel(total, currentPage, totalPages, machineList);
        });
    }

    @Override
    public Observable<MachineDetails> loadMachineDetails(String machineId) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudMachinesDataStore.loadMachineDetails(machineId, authInfoEntity).map(DetailsMapper::detailEntityToDetails);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }
}
