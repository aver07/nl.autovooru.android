package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public class SendBidMessageQuery {

    @NonNull
    @SerializedName("um_id")
    private final String mMessageId;
    @SerializedName("email")
    private final String mEmail;
    @NonNull
    @SerializedName("name")
    private final String mName;
    @SerializedName("topic")
    private final String mSubject;
    @SerializedName("message")
    private final String mMessage;
    @Nullable
    @SerializedName("lang")
    private final String mLanguage;
    @SerializedName("user")
    private final AuthInfoEntity mUser;

    public SendBidMessageQuery(@NonNull String messageId,
                               @NonNull String name,
                               @NonNull String email, @NonNull String subject,
                               @NonNull String message, @Nullable String language,
                               @Nullable AuthInfoEntity authInfo) {
        mMessageId = messageId;
        mName = name;
        mEmail = email;
        mSubject = subject;
        mMessage = message;
        mLanguage = language;
        mUser = authInfo;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }

}
