package nl.autovooru.android.data.entity.advert;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 08/01/16.
 */
public final class YoutubeInfoEntity {
    @SerializedName("id")
    private String mId;
    @SerializedName("link")
    private String mLinkUrl;
    @SerializedName("img")
    private String mImageUrl;

    public String getId() {
        return mId;
    }

    public String getLinkUrl() {
        return mLinkUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }
}
