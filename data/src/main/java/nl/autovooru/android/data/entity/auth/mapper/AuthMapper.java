package nl.autovooru.android.data.entity.auth.mapper;

import android.support.annotation.Nullable;

import nl.autovooru.android.data.entity.auth.AuthInfoEntity;
import nl.autovooru.android.data.entity.auth.AuthorizationTokenEntity;
import nl.autovooru.android.domain.model.auth.AuthInfo;
import nl.autovooru.android.domain.model.auth.AuthorizationToken;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
public final class AuthMapper {

    private AuthMapper() {
        // no-op
    }

    @Nullable
    public static AuthorizationToken entityToModel(@Nullable AuthorizationTokenEntity entity) {
        if (entity == null) {
            return null;
        }

        final AuthorizationToken model = new AuthorizationToken();
        model.setAccessToken(entity.getAccessToken());
        model.setCode(entity.getCode());
        model.setDealerId(entity.getDealerId());
        model.setExpireDate(entity.getExpireDate());

        return model;
    }

    @Nullable
    public static AuthInfoEntity modelToEntity(@Nullable AuthInfo model) {
        if (model == null) {
            return null;
        }

        final AuthInfoEntity entity = new AuthInfoEntity();
        entity.setCode(model.getCode());
        entity.setToken(model.getToken());

        return entity;
    }
}
