package nl.autovooru.android.data.entity.advert;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 06/01/16.
 */
public final class AutoInfoEntity {
    @SerializedName("vcat")
    @Nullable
    private Integer mCategoryId;
    @SerializedName("vmark")
    @Nullable
    private Integer mMarkId;
    @SerializedName("vmodel")
    @Nullable
    private Integer mModelId;
    @SerializedName("vbody")
    @Nullable
    private Integer mBodyId;
    @SerializedName("vfuel")
    @Nullable
    private Integer mFuelId;
    @SerializedName("color")
    @Nullable
    private Integer mColorId;
    @SerializedName("capacity")
    @Nullable
    private String mEngineCapacity;
    @SerializedName("token")
    @Nullable
    private String mToken;

    @Nullable
    public Integer getCategoryId() {
        return mCategoryId;
    }

    @Nullable
    public Integer getMarkId() {
        return mMarkId;
    }

    @Nullable
    public Integer getModelId() {
        return mModelId;
    }

    @Nullable
    public Integer getBodyId() {
        return mBodyId;
    }

    @Nullable
    public Integer getFuelId() {
        return mFuelId;
    }

    @Nullable
    public Integer getColorId() {
        return mColorId;
    }

    @Nullable
    public String getEngineCapacity() {
        return mEngineCapacity;
    }

    @Nullable
    public String getToken() {
        return mToken;
    }
}
