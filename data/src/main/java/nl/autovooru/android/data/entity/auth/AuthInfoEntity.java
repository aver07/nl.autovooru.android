package nl.autovooru.android.data.entity.auth;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 24/12/15.
 */
public class AuthInfoEntity {

    @SerializedName("access_token")
    private String mToken;
    @SerializedName("code")
    private String mCode;

    public AuthInfoEntity() {
    }

    public AuthInfoEntity(String token, String code) {
        mToken = token;
        mCode = code;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }
}
