package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
public final class GetAccessTokenQuery {

    @SerializedName("user")
    private User mUser;
    @Nullable
    @SerializedName("lang")
    private final String mLanguage;

    public GetAccessTokenQuery(@NonNull String code, String language) {
        mUser = new User(code);
        mLanguage = language;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }

    private static class User {
        @SerializedName("code")
        private String mCode;

        public User(String code) {
            mCode = code;
        }
    }
}
