package nl.autovooru.android.data.entity.machines.mapper;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import nl.autovooru.android.data.entity.machines.MachineFilterEntity;
import nl.autovooru.android.data.entity.machines.MachineSearchFiltersEntity;
import nl.autovooru.android.domain.model.machine.MachineFilter;
import nl.autovooru.android.domain.model.machine.MachineSearchFilters;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
public final class MachineFiltersMapper {

    private MachineFiltersMapper() {
        // no-op
    }

    @Nullable
    public static MachineSearchFilters machineSearchFiltersEntityToMachineSearchFilters(@Nullable MachineSearchFiltersEntity machineSearchFiltersEntity) {
        if (machineSearchFiltersEntity == null) {
            return null;
        }

        MachineSearchFilters machineSearchFilters = new MachineSearchFilters();

        machineSearchFilters.setMainFilters(machineMainFiltersEntityToMachineMainFilters(machineSearchFiltersEntity.getMainFilters()));
        machineSearchFilters.setToggleFilters(machineToggleFiltersEntityTomachineToggleFilters(machineSearchFiltersEntity.getToggleFilters()));
        machineSearchFilters.setOtherFilters(machineFilterEntityListToMachineFilterList(machineSearchFiltersEntity.getOtherFilters()));

        return machineSearchFilters;
    }

    @Nullable
    public static MachineSearchFilters.MainFilters machineMainFiltersEntityToMachineMainFilters(@Nullable MachineSearchFiltersEntity.MainFiltersEntity mainFiltersEntity) {
        if (mainFiltersEntity == null) {
            return null;
        }

        MachineSearchFilters.MainFilters mainFilters = new MachineSearchFilters.MainFilters();

        mainFilters.setTopMachineMarks(machineFilterEntityToMachineFilter(mainFiltersEntity.getTopMachineMarks()));
        mainFilters.setMachineMarks(machineFilterEntityToMachineFilter(mainFiltersEntity.getMachineMarks()));
        mainFilters.setMachineModels(machineFilterEntityToMachineFilter(mainFiltersEntity.getMachineModels()));
        mainFilters.setMachineYear(machineFilterEntityToMachineFilter(mainFiltersEntity.getMachineYear()));
        mainFilters.setPrice(machineFilterEntityToMachineFilter(mainFiltersEntity.getPrice()));
        mainFilters.setFuel(machineFilterEntityToMachineFilter(mainFiltersEntity.getFuel()));
        mainFilters.setUserType(machineFilterEntityToMachineFilter(mainFiltersEntity.getUserType()));
        mainFilters.setDistance(machineFilterEntityToMachineFilter(mainFiltersEntity.getDistance()));
        mainFilters.setMileage(machineFilterEntityToMachineFilter(mainFiltersEntity.getMileage()));
        mainFilters.setKeywords(machineFilterEntityToMachineFilter(mainFiltersEntity.getKeywords()));
        mainFilters.setPostcode(machineFilterEntityToMachineFilter(mainFiltersEntity.getPostcode()));

        return mainFilters;
    }

    @Nullable
    public static MachineFilter machineFilterEntityToMachineFilter(@Nullable MachineFilterEntity filterEntity) {
        if (filterEntity == null) {
            return null;
        }

        MachineFilter machineFilter = new MachineFilter();

        machineFilter.setValue(filterEntity.getValue());
        machineFilter.setName(filterEntity.getName());
        machineFilter.setFirstOptionsList(optionsListToOptionsList(filterEntity.getFirstOptionsList()));
        machineFilter.setSecondOptionsList(optionsListToOptionsList(filterEntity.getSecondOptionsList()));

        return machineFilter;
    }

    @Nullable
    public static MachineSearchFilters.ToggleFilters machineToggleFiltersEntityTomachineToggleFilters(@Nullable MachineSearchFiltersEntity.ToggleFilters entity) {
        if (entity == null) {
            return null;
        }

        MachineSearchFilters.ToggleFilters model = new MachineSearchFilters.ToggleFilters();

        model.setOfferTime(machineFilterEntityToMachineFilter(entity.getOfferTime()));
        model.setTransmission(machineFilterEntityToMachineFilter(entity.getTransmission()));
        model.setColor(machineFilterEntityToMachineFilter(entity.getColor()));
        model.setBody(machineFilterEntityToMachineFilter(entity.getBody()));

        return model;
    }

    @Nullable
    public static MachineFilter.Options optionsEntityToOptions(@Nullable MachineFilterEntity.Options entity) {
        if (entity == null) {
            return null;
        }

        MachineFilter.Options model = new MachineFilter.Options();

        model.setValue(entity.getValue());
        model.setName(entity.getName());

        return model;
    }

    @Nullable
    protected static List<MachineFilter> machineFilterEntityListToMachineFilterList(@Nullable List<MachineFilterEntity> list) {
        if (list == null) {
            return null;
        }

        List<MachineFilter> list_ = new ArrayList<MachineFilter>();
        for (MachineFilterEntity machineFilterEntity : list) {
            list_.add(machineFilterEntityToMachineFilter(machineFilterEntity));
        }

        return list_;
    }

    @Nullable
    protected static List<MachineFilter.Options> optionsListToOptionsList(@Nullable List<MachineFilterEntity.Options> list) {
        if (list == null) {
            return null;
        }

        List<MachineFilter.Options> list_ = new ArrayList<MachineFilter.Options>();
        for (MachineFilterEntity.Options options : list) {
            list_.add(optionsEntityToOptions(options));
        }

        return list_;
    }
}
