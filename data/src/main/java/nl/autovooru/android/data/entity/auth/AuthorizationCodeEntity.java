package nl.autovooru.android.data.entity.auth;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
public final class AuthorizationCodeEntity {

    @SerializedName("code")
    private String mCode;

    public String getCode() {
        return mCode;
    }

}
