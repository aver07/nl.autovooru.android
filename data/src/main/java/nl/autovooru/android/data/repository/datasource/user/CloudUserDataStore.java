package nl.autovooru.android.data.repository.datasource.user;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import nl.autovooru.android.data.api.AutovooruApiService;
import nl.autovooru.android.data.entity.OrderEntity;
import nl.autovooru.android.data.entity.Page;
import nl.autovooru.android.data.entity.advert.AdvertFieldsEntity;
import nl.autovooru.android.data.entity.advert.AutoInfoEntity;
import nl.autovooru.android.data.entity.advert.YoutubeInfoEntity;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;
import nl.autovooru.android.data.entity.machines.MachineEntityList;
import nl.autovooru.android.data.entity.machines.MachineListQuery;
import nl.autovooru.android.data.entity.machines.details.EditMachineDetailsEntity;
import nl.autovooru.android.data.entity.queries.AddAutoQuery;
import nl.autovooru.android.data.entity.queries.AddToFavoritesQuery;
import nl.autovooru.android.data.entity.queries.AdvertQuery;
import nl.autovooru.android.data.entity.queries.AuthorizedQuery;
import nl.autovooru.android.data.entity.queries.AutoInfoQuery;
import nl.autovooru.android.data.entity.queries.DeleteAllBidsQuery;
import nl.autovooru.android.data.entity.queries.DeleteBidMessageQuery;
import nl.autovooru.android.data.entity.queries.DeleteMyAdQuery;
import nl.autovooru.android.data.entity.queries.DeleteUserMessageQuery;
import nl.autovooru.android.data.entity.queries.EditAutoQuery;
import nl.autovooru.android.data.entity.queries.GetMachineBidsQuery;
import nl.autovooru.android.data.entity.queries.GetYoutubeInfoQuery;
import nl.autovooru.android.data.entity.queries.MachineDetailsQuery;
import nl.autovooru.android.data.entity.queries.MyFavoritesQuery;
import nl.autovooru.android.data.entity.queries.MyMachinesBidsQuery;
import nl.autovooru.android.data.entity.queries.PushQuery;
import nl.autovooru.android.data.entity.queries.SaveUserDataQuery;
import nl.autovooru.android.data.entity.queries.SendAnswerQuery;
import nl.autovooru.android.data.entity.queries.UserMessagesQuery;
import nl.autovooru.android.data.entity.user.BidEntity;
import nl.autovooru.android.data.entity.user.MessagesPageEntity;
import nl.autovooru.android.data.entity.user.UserInfoEntity;
import nl.autovooru.android.data.exceptions.ValidatePagingServerResponse;
import nl.autovooru.android.data.exceptions.ValidateServerResponse;
import nl.autovooru.android.data.exceptions.ValidateServerResponseList;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
@Singleton
public class CloudUserDataStore implements UserDataStore {

    private final AutovooruApiService mApiService;
    private final String mLanguage;

    @Inject
    public CloudUserDataStore(@NonNull AutovooruApiService apiService,
                              @Named("appLanguage") String language) {
        mApiService = apiService;
        mLanguage = language;
    }

    @Override
    public Observable<MachineEntityList> myAds(int page, int itemPerPage,
                                               String dealerId,
                                               @Nullable AuthInfoEntity authInfo) {
        final MachineListQuery.Builder builder = MachineListQuery.newBuilder(new Page(Page.Type.profile, itemPerPage, page))
                .setLanguage(mLanguage)
                .setDealerId(dealerId)
                .setAuthInfo(authInfo);

        return mApiService.myAds(builder.build())
                .flatMap(new ValidatePagingServerResponse<>())
                .map(response -> new MachineEntityList(
                        response.getTotal(),
                        response.getPage(),
                        response.getTotalPages(),
                        response.getData()));
    }

    @Override
    public Observable<Void> addToFavorite(@NonNull String machineId, @NonNull AuthInfoEntity authInfo) {
        return mApiService.addToFavorites(new AddToFavoritesQuery(
                authInfo,
                machineId,
                AddToFavoritesQuery.Event.add,
                mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<Void> removeFromFavorite(@NonNull String machineId, @NonNull AuthInfoEntity authInfo) {
        return mApiService.addToFavorites(new AddToFavoritesQuery(
                authInfo,
                machineId,
                AddToFavoritesQuery.Event.remove,
                mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<Void> deleteMyAd(@NonNull String machineId, @NonNull AuthInfoEntity authInfo) {
        return mApiService.deleteMyAd(new DeleteMyAdQuery(authInfo, machineId, mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<List<BidEntity>> getMachineBids(@NonNull String machineId, @NonNull AuthInfoEntity authInfo) {
        return mApiService.getMachineBids(new GetMachineBidsQuery(authInfo, machineId, mLanguage))
                .flatMap(new ValidateServerResponseList<>());
    }

    @Override
    public Observable<Void> deleteBidMessage(@NonNull String bidId, @NonNull AuthInfoEntity authInfo) {
        return mApiService.deleteBidMessage(new DeleteBidMessageQuery(bidId, authInfo, mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<UserInfoEntity> getUserData(@NonNull AuthInfoEntity authInfo) {
        return mApiService.getUserData(new AuthorizedQuery(authInfo, mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<MachineEntityList> getUserFavorites(int page, int itemPerPage, OrderEntity order, @NonNull AuthInfoEntity authInfo) {
        final Page page1 = new Page(Page.Type.profile, itemPerPage, page);
        return mApiService.getUserFavorites(new MyFavoritesQuery(authInfo, order, page1, mLanguage))
                .flatMap(new ValidatePagingServerResponse<>())
                .map(response -> new MachineEntityList(
                        response.getTotal(),
                        response.getPage(),
                        response.getTotalPages(),
                        response.getData()));
    }

    @Override
    public Observable<MachineEntityList> getUserBids(int page, int itemPerPage, @NonNull AuthInfoEntity authInfo) {
        final Page page1 = new Page(Page.Type.profile, itemPerPage, page);
        return mApiService.getUserBids(new MyMachinesBidsQuery(authInfo, page1, mLanguage))
                .flatMap(new ValidatePagingServerResponse<>())
                .map(response -> new MachineEntityList(
                        response.getTotal(),
                        response.getPage(),
                        response.getTotalPages(),
                        response.getData()));
    }

    @Override
    public Observable<Void> deleteAllUserBids(@Nullable String machineId, @NonNull AuthInfoEntity authInfo) {
        return mApiService.deleteAllUserBids(new DeleteAllBidsQuery(machineId, authInfo, mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<Void> deleteUserBid(@NonNull String bidId, @NonNull AuthInfoEntity authInfo) {
        return mApiService.deleteUserBid(new DeleteBidMessageQuery(bidId, authInfo, mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<MessagesPageEntity> getUserMessages(int page, int itemPerPage, @NonNull AuthInfoEntity authInfo) {
        final Page page1 = new Page(Page.Type.profile, itemPerPage, page);
        return mApiService.getUserMessages(new UserMessagesQuery(authInfo, page1, mLanguage))
                .flatMap(new ValidatePagingServerResponse<>())
                .map(response -> new MessagesPageEntity(
                        response.getTotal(),
                        response.getPage(),
                        response.getTotalPages(),
                        response.getData()
                ));
    }

    @Override
    public Observable<Void> sendAnswer(@NonNull String messageId, @NonNull String subject, @NonNull String name, @NonNull String email, @NonNull String message, @NonNull AuthInfoEntity authInfo) {
        return mApiService.sendAnswer(new SendAnswerQuery(messageId, subject, name, email, message, authInfo, mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<Void> deleteMessage(@NonNull String messageId, @NonNull AuthInfoEntity authInfo) {
        return mApiService.deleteUserMessage(new DeleteUserMessageQuery(authInfo, messageId, mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<Void> saveUserData(@NonNull UserInfoEntity userInfo, @NonNull AuthInfoEntity authInfo) {
        return mApiService.saveUserData(new SaveUserDataQuery(userInfo, authInfo, mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<AdvertFieldsEntity> getAdvertFields(@NonNull String categoryId, @Nullable String markId, @NonNull AuthInfoEntity authInfo) {
        return mApiService.getAdvertFields(new AdvertQuery(categoryId, markId, authInfo, mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<AutoInfoEntity> getAutoInfoByToken(@NonNull String token, @NonNull AuthInfoEntity authInfo) {
        return mApiService.getAutoInfoByToken(new AutoInfoQuery(token, authInfo, mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<YoutubeInfoEntity> getYoutubeInfo(@NonNull String youtubeLink, @NonNull AuthInfoEntity authInfo) {
        return mApiService.getYoutubeInfo(new GetYoutubeInfoQuery(youtubeLink, authInfo, mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<Void> addAuto(@NonNull Map<String, Object> fieldsValues, @Nullable List<String> images, @NonNull AuthInfoEntity authInfo) {
        Map<String, RequestBody> imagesBody = null;
        if (images != null && !images.isEmpty()) {
            imagesBody = new HashMap<>();
            for (int i = 0, size = images.size(); i < size; i++) {
                final String imageUrl = images.get(i);
                if (imageUrl.startsWith("http")) {
                    final RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), imageUrl);
                    imagesBody.put("image_" + i, requestBody);
                } else if (imageUrl.startsWith("file://")) {
                    final File file = new File(imageUrl.replace("file://", ""));
                    final RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
                    imagesBody.put("image_" + i, requestBody);
                }
            }
        }
        final AddAutoQuery query = new AddAutoQuery(fieldsValues, authInfo, mLanguage);
        if (imagesBody == null || imagesBody.isEmpty()) {
            return mApiService.addAuto(query)
                    .flatMap(new ValidateServerResponse<>());
        } else {
            return mApiService.addAuto(query, imagesBody)
                    .flatMap(new ValidateServerResponse<>());
        }
    }

    @Override
    public Observable<Void> editAuto(@NonNull String machineId, @NonNull Map<String, Object> fieldsValues,
                                     @Nullable List<String> images,
                                     @NonNull AuthInfoEntity authInfo) {
        Map<String, RequestBody> imagesBody = null;
        if (images != null && !images.isEmpty()) {
            imagesBody = new HashMap<>();
            for (int i = 0, size = images.size(); i < size; i++) {
                final String imageUrl = images.get(i);
                if (imageUrl.startsWith("http")) {
                    final RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), imageUrl);
                    imagesBody.put("image_" + i, requestBody);
                } else if (imageUrl.startsWith("file://")) {
                    final File file = new File(imageUrl.replace("file://", ""));
                    final RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
                    imagesBody.put("image_" + i, requestBody);
                }
            }
        }

        final EditAutoQuery query = new EditAutoQuery(machineId, fieldsValues, authInfo, mLanguage);
        if (imagesBody == null || images.isEmpty()) {
            return mApiService.editAuto(query)
                    .flatMap(new ValidateServerResponse<>());
        } else {
            return mApiService.editAuto(query, imagesBody)
                    .flatMap(new ValidateServerResponse<>());
        }
    }

    @Override
    public Observable<EditMachineDetailsEntity> getMyMachineDetails(@NonNull String machineId, @NonNull AuthInfoEntity authInfo) {
        final Page page = new Page(Page.Type.profile_advert_edit);
        return mApiService.getEditMachineDetails(new MachineDetailsQuery(page, machineId, mLanguage, authInfo))
                .flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<Void> sendPushToken(@NonNull String deviceToken, @NonNull AuthInfoEntity authInfo) {
        return mApiService.sendPushToken(new PushQuery(deviceToken, authInfo, mLanguage))
                .flatMap(new ValidateServerResponse<>());
    }
}
