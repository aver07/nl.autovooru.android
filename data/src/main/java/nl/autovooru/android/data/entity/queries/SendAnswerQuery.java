package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public final class SendAnswerQuery {

    @SerializedName("topic")
    private final String mSubject;
    @SerializedName("email")
    private final String mEmail;
    @SerializedName("message")
    private final String mMessage;
    @Nullable
    @SerializedName("um_id")
    private final String mMessageId;
    @Nullable
    @SerializedName("name")
    private final String mName;
    @Nullable
    @SerializedName("lang")
    private final String mLanguage;
    @SerializedName("user")
    private final AuthInfoEntity mUser;

    public SendAnswerQuery(@NonNull String messageId, @NonNull String subject, @Nullable String name,
                           @NonNull String email, @NonNull String message,
                           @NonNull AuthInfoEntity user, @Nullable String language) {
        mMessageId = messageId;
        mSubject = subject;
        mName = name;
        mEmail = email;
        mMessage = message;
        mLanguage = language;
        mUser = user;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }
}
