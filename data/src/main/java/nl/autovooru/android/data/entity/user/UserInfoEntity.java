package nl.autovooru.android.data.entity.user;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 24/12/15.
 */
public final class UserInfoEntity {

    @SerializedName("ua_id")
    private String mUserId;
    @SerializedName("up_city")
    private String mCity;
    @SerializedName("lp_cc")
    private String mCountry;
    @SerializedName("lp_pc")
    private String mPostcode;
    @SerializedName("org_name")
    private String mCompanyName;
    @SerializedName("up_sname")
    private String mSurname;
    @SerializedName("up_fname")
    private String mName;
    @SerializedName("address")
    private String mAddress;
    @SerializedName("phone")
    private String mPhoneNumber;
    @SerializedName("fax")
    private String mFaxNumber;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("website")
    private String mWebsiteUrl;
    @SerializedName("display_name")
    private String mDisplayName;
    @SerializedName("ua_owner")
    private String mUserType;
    @SerializedName("password")
    private String mPassword;


    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String companyName) {
        mCompanyName = companyName;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getSurname() {
        return mSurname;
    }

    public void setSurname(String surname) {
        mSurname = surname;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getPostcode() {
        return mPostcode;
    }

    public void setPostcode(String postcode) {
        mPostcode = postcode;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return mFaxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        mFaxNumber = faxNumber;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getWebsiteUrl() {
        return mWebsiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        mWebsiteUrl = websiteUrl;
    }

    public String getDisplayName() {
        return mDisplayName;
    }

    public void setDisplayName(String displayName) {
        mDisplayName = displayName;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }
}
