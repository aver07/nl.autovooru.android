package nl.autovooru.android.data.entity.machines.mapper;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import nl.autovooru.android.data.entity.machines.MachineEntity;
import nl.autovooru.android.domain.model.machine.Machine;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
public final class MachineMapper {

    private MachineMapper() {
        // no-op
    }


    @Nullable
    public static List<Machine> machineEntityToMachine(@Nullable List<MachineEntity> machineEntity) {
        if (machineEntity == null) {
            return null;
        }

        List<Machine> list = new ArrayList<Machine>();
        for (MachineEntity machineEntity_ : machineEntity) {
            list.add(machineEntityToMachine(machineEntity_));
        }

        return list;
    }

    @Nullable
    public static Machine machineEntityToMachine(@Nullable MachineEntity machineEntity) {
        if (machineEntity == null) {
            return null;
        }

        Machine machine = new Machine();

        machine.setMachineId(machineEntity.getMachineId());
        machine.setMark(machineEntity.getMark());
        machine.setModel(machineEntity.getModel());
        machine.setMileage(machineEntity.getMileage());
        machine.setFuel(machineEntity.getFuel());
        machine.setPrice(machineEntity.getPrice());
        if (machineEntity.getImages() != null) {
            machine.setImages(new ArrayList<String>(machineEntity.getImages()));
        }
        machine.setName(machineEntity.getName());
        machine.setReleaseYear(machineEntity.getReleaseYear());
        machine.setCity(machineEntity.getCity());
        machine.setAlias(machineEntity.getAlias());
        machine.setBids(machineEntity.getBids());
        machine.setToken(machineEntity.getToken());
        machine.setHits(machineEntity.getHits());
        machine.setDealerId(machineEntity.getDealerId());
        machine.setBidId(machineEntity.getBidId());
        machine.setBid(machineEntity.getBid());
        machine.setPromoPrice(machineEntity.getPromoPrice());
        machine.setFavorite(machineEntity.getFavorite());

        return machine;
    }

}
