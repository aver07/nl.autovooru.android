package nl.autovooru.android.data.entity.machines.details;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Antonenko Viacheslav on 08/01/16.
 */
public final class EditMachineDetailsEntity {

    @SerializedName("as_id")
    private String mMachineId;
    @SerializedName("content")
    private ContentEntity mContent;
    @SerializedName("images")
    private List<String> mImagesList;
    @SerializedName("info")
    private InfoEntity mInfo;
    @SerializedName("filters")
    private FiltersEntity mFilters;

    public String getMachineId() {
        return mMachineId;
    }

    public ContentEntity getContent() {
        return mContent;
    }

    public List<String> getImagesList() {
        return mImagesList;
    }

    public InfoEntity getInfo() {
        return mInfo;
    }

    public FiltersEntity getFilters() {
        return mFilters;
    }
}
