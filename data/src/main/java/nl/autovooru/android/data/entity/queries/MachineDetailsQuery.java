package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.Page;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
public final class MachineDetailsQuery {

    @SerializedName("user")
    private AuthInfoEntity mUser;
    @Nullable
    @SerializedName("lang")
    private final String mLanguage;
    @SerializedName("page")
    private Page mPage;
    @NonNull
    @SerializedName("as_id")
    private String mMachineId;


    public MachineDetailsQuery(Page page, String machineId, String language, AuthInfoEntity authInfo) {
        mLanguage = language;
        mMachineId = machineId;
        mUser = authInfo;
        mPage = page;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }

}
