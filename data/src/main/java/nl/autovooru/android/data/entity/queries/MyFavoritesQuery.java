package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.OrderEntity;
import nl.autovooru.android.data.entity.Page;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public final class MyFavoritesQuery {

    @Nullable
    @SerializedName("lang")
    private final String mLanguage;
    @SerializedName("user")
    private final AuthInfoEntity mUser;
    @SerializedName("order")
    private final OrderEntity mOrder;
    @Nullable
    @SerializedName("page")
    private final Page mPage;

    public MyFavoritesQuery(@NonNull AuthInfoEntity user, OrderEntity order, @Nullable Page page, @Nullable String language) {
        mUser = user;
        mOrder = order;
        mPage = page;
        mLanguage = language;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }
}
