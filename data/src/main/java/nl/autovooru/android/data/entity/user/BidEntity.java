package nl.autovooru.android.data.entity.user;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public final class BidEntity {

    @SerializedName("create_date")
    long mCreateDate;
    @SerializedName("bid")
    String mBid;
    @SerializedName("email")
    String mEmail;
    @SerializedName("um_id")
    String mMessageId;
    @SerializedName("uasb_id")
    String mBidId;

    public long getCreateDate() {
        return mCreateDate;
    }

    public String getBid() {
        return mBid;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getMessageId() {
        return mMessageId;
    }

    public String getBidId() {
        return mBidId;
    }
}
