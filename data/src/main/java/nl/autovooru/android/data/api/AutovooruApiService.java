package nl.autovooru.android.data.api;

import android.support.annotation.Nullable;

import java.util.List;
import java.util.Map;

import nl.autovooru.android.data.entity.BaseListResponse;
import nl.autovooru.android.data.entity.BaseResponse;
import nl.autovooru.android.data.entity.advert.AdvertFieldsEntity;
import nl.autovooru.android.data.entity.advert.AutoInfoEntity;
import nl.autovooru.android.data.entity.advert.YoutubeInfoEntity;
import nl.autovooru.android.data.entity.auth.AuthorizationCodeEntity;
import nl.autovooru.android.data.entity.auth.AuthorizationTokenEntity;
import nl.autovooru.android.data.entity.filters.FilterEntity;
import nl.autovooru.android.data.entity.filters.FilterTypeEntity;
import nl.autovooru.android.data.entity.filters.FiltersQuery;
import nl.autovooru.android.data.entity.machines.MachineEntity;
import nl.autovooru.android.data.entity.machines.MachineListQuery;
import nl.autovooru.android.data.entity.machines.MachineSearchFiltersEntity;
import nl.autovooru.android.data.entity.machines.details.DetailsEntity;
import nl.autovooru.android.data.entity.machines.details.EditMachineDetailsEntity;
import nl.autovooru.android.data.entity.queries.AddAutoQuery;
import nl.autovooru.android.data.entity.queries.AddToFavoritesQuery;
import nl.autovooru.android.data.entity.queries.AdvertQuery;
import nl.autovooru.android.data.entity.queries.AuthorizedQuery;
import nl.autovooru.android.data.entity.queries.AutoInfoQuery;
import nl.autovooru.android.data.entity.queries.BidMessageQuery;
import nl.autovooru.android.data.entity.queries.DeleteAllBidsQuery;
import nl.autovooru.android.data.entity.queries.DeleteBidMessageQuery;
import nl.autovooru.android.data.entity.queries.DeleteMyAdQuery;
import nl.autovooru.android.data.entity.queries.DeleteUserMessageQuery;
import nl.autovooru.android.data.entity.queries.EditAutoQuery;
import nl.autovooru.android.data.entity.queries.FeedbackQuery;
import nl.autovooru.android.data.entity.queries.ForgotPasswordQuery;
import nl.autovooru.android.data.entity.queries.GetAccessTokenQuery;
import nl.autovooru.android.data.entity.queries.GetAuthorizationCodeQuery;
import nl.autovooru.android.data.entity.queries.GetMachineBidsQuery;
import nl.autovooru.android.data.entity.queries.GetYoutubeInfoQuery;
import nl.autovooru.android.data.entity.queries.MachineDetailsQuery;
import nl.autovooru.android.data.entity.queries.MachineFiltersQuery;
import nl.autovooru.android.data.entity.queries.MyFavoritesQuery;
import nl.autovooru.android.data.entity.queries.MyMachinesBidsQuery;
import nl.autovooru.android.data.entity.queries.PushQuery;
import nl.autovooru.android.data.entity.queries.RegistrationNewOrganizationQuery;
import nl.autovooru.android.data.entity.queries.RegistrationNewsUserQuery;
import nl.autovooru.android.data.entity.queries.SaveUserDataQuery;
import nl.autovooru.android.data.entity.queries.SendAnswerQuery;
import nl.autovooru.android.data.entity.queries.SendBidMessageQuery;
import nl.autovooru.android.data.entity.queries.SendMessageQuery;
import nl.autovooru.android.data.entity.queries.UserMessagesQuery;
import nl.autovooru.android.data.entity.user.BidEntity;
import nl.autovooru.android.data.entity.user.MessageEntity;
import nl.autovooru.android.data.entity.user.UserInfoEntity;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Rest API for retrieving data from the network.
 * <p>
 * Created by Antonenko Viacheslav on 02/11/15.
 */
public interface AutovooruApiService {

    @GET("get/filters/types")
    Observable<Response<BaseListResponse<FilterTypeEntity>>> getFiltersTypesList(@Query("defs") FiltersQuery query);

    @GET("get/filters/list")
    Observable<Response<BaseResponse<Map<String, List<FilterEntity>>>>> getFiltersList(@Query("defs") FiltersQuery query);

    @GET("get/auto/list")
    Observable<Response<BaseListResponse<MachineEntity>>> getMachineListWithFilters(@Query("defs") MachineListQuery query);

    @GET("forms/feedback")
    Observable<Response<BaseResponse<Void>>> sendFeedback(@Query("defs") FeedbackQuery query);

    @GET("forms/bericht")
    Observable<Response<BaseResponse<Void>>> sendMessage(@Query("defs") SendMessageQuery query);

    @GET("forms/bericht")
    Observable<Response<BaseResponse<Void>>> sendBidMessage(@Query("defs") SendBidMessageQuery query);

    @GET("forms/bid")
    Observable<Response<BaseResponse<Void>>> sendBid(@Query("defs") BidMessageQuery query);

    @GET("get/search_block/list")
    Observable<Response<BaseResponse<MachineSearchFiltersEntity>>> getMachineSearchFilters(@Query("defs") MachineFiltersQuery query);

    @GET("get/auto/list")
    Observable<Response<BaseResponse<DetailsEntity>>> getMachineDetails(@Query("defs") MachineDetailsQuery query);

    @GET("user/auth")
    Observable<Response<BaseResponse<AuthorizationCodeEntity>>> getAuthorizationCode(@Query("defs") GetAuthorizationCodeQuery query);

    @GET("user/get_token")
    Observable<Response<BaseResponse<AuthorizationTokenEntity>>> getAccessToken(@Query("defs") GetAccessTokenQuery query);

    @GET("user/restore")
    Observable<Response<BaseResponse<Void>>> forgotPassword(@Query("defs") ForgotPasswordQuery query);

    @GET("user/reg")
    Observable<Response<BaseResponse<AuthorizationCodeEntity>>> registrationNewUser(@Query("defs") RegistrationNewsUserQuery query);

    @GET("user/reg")
    Observable<Response<BaseResponse<Void>>> registrationNewOrganization(@Query("defs") RegistrationNewOrganizationQuery query);

    @GET("get/auto/list")
    Observable<Response<BaseListResponse<MachineEntity>>> myAds(@Query("defs") MachineListQuery query);

    @GET("user/change_favorites")
    Observable<Response<BaseResponse<Void>>> addToFavorites(@Query("defs") AddToFavoritesQuery query);

    @GET("user/get_mijn_bids")
    Observable<Response<BaseListResponse<MachineEntity>>> getUserBids(@Query("defs") MyMachinesBidsQuery query);

    @GET("user/get_data")
    Observable<Response<BaseResponse<UserInfoEntity>>> getUserData(@Query("defs") AuthorizedQuery query);

    @GET("user/set_data")
    Observable<Response<BaseResponse<Void>>> saveUserData(@Query("defs") SaveUserDataQuery query);

    @GET("user/get_favorites")
    Observable<Response<BaseListResponse<MachineEntity>>> getUserFavorites(@Query("defs") MyFavoritesQuery query);

    @GET("set/auto/delete")
    Observable<Response<BaseResponse<Void>>> deleteMyAd(@Query("defs") DeleteMyAdQuery query);

    @GET("messages/get_bids")
    Observable<Response<BaseListResponse<BidEntity>>> getMachineBids(@Query("defs") GetMachineBidsQuery query);

    @GET("messages/delete_bid")
    Observable<Response<BaseResponse<Void>>> deleteBidMessage(@Query("defs") DeleteBidMessageQuery query);

    @GET("messages/delete_all_bid")
    Observable<Response<BaseResponse<Void>>> deleteAllUserBids(@Query("defs") DeleteAllBidsQuery query);

    @GET("messages/delete_mijn_bid")
    Observable<Response<BaseResponse<Void>>> deleteUserBid(@Query("defs") DeleteBidMessageQuery query);

    @GET("messages/get_mijn_bericht")
    Observable<Response<BaseListResponse<MessageEntity>>> getUserMessages(@Query("defs") UserMessagesQuery query);

    @GET("forms/bericht")
    Observable<Response<BaseResponse<Void>>> sendAnswer(@Query("defs") SendAnswerQuery query);

    @GET("messages/delete_mijn_bericht")
    Observable<Response<BaseResponse<Void>>> deleteUserMessage(@Query("defs") DeleteUserMessageQuery query);

    @GET("get/search_block/advert_fields")
    Observable<Response<BaseResponse<AdvertFieldsEntity>>> getAdvertFields(@Query("defs") AdvertQuery query);

    @GET("set/auto/token")
    Observable<Response<BaseResponse<AutoInfoEntity>>> getAutoInfoByToken(@Query("defs") AutoInfoQuery query);

    @GET("set/auto/youtube_api")
    Observable<Response<BaseResponse<YoutubeInfoEntity>>> getYoutubeInfo(@Query("defs") GetYoutubeInfoQuery query);

    @Multipart
    @POST("set/auto/edit")
    Observable<Response<BaseResponse<Void>>> addAuto(@Query("defs") AddAutoQuery query, @PartMap @Nullable Map<String, RequestBody> images);

    @GET("set/auto/edit")
    Observable<Response<BaseResponse<Void>>> addAuto(@Query("defs") AddAutoQuery query);

    @GET("set/auto/edit")
    Observable<Response<BaseResponse<Void>>> editAuto(@Query("defs") EditAutoQuery query);

    @Multipart
    @POST("set/auto/edit")
    Observable<Response<BaseResponse<Void>>> editAuto(@Query("defs") EditAutoQuery query, @PartMap @Nullable Map<String, RequestBody> images);

    @GET("get/auto/list")
    Observable<Response<BaseResponse<EditMachineDetailsEntity>>> getEditMachineDetails(@Query("defs") MachineDetailsQuery query);

    @GET("user/add_user_device")
    Observable<Response<BaseResponse<Void>>> sendPushToken(@Query("defs") PushQuery query);

}
