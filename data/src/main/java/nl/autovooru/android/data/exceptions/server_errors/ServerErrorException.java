package nl.autovooru.android.data.exceptions.server_errors;

/**
 * Created by Antonenko Viacheslav on 21/07/16.
 */
public class ServerErrorException extends Exception {

    public ServerErrorException(int serverErrorCode) {
        super("Server error code: " + serverErrorCode);
    }
}
