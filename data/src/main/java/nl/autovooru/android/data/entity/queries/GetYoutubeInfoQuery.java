package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;

/**
 * Created by Antonenko Viacheslav on 08/01/16.
 */
public final class GetYoutubeInfoQuery {

    @Nullable
    @SerializedName("lang")
    private final String mLanguage;
    @SerializedName("youtube")
    private Youtube mYoutube;
    @SerializedName("user")
    private AuthInfoEntity mUser;

    public GetYoutubeInfoQuery(@NonNull String link, @NonNull AuthInfoEntity authInfo, @Nullable String language) {
        mYoutube = new Youtube(link);
        mUser = authInfo;
        mLanguage = language;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }

    private static class Youtube {
        @SerializedName("link")
        @NonNull
        private String mLink;

        public Youtube(@NonNull String link) {
            mLink = link;
        }
    }
}
