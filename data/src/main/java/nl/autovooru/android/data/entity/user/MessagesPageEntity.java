package nl.autovooru.android.data.entity.user;

import java.util.List;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public final class MessagesPageEntity {
    private int mTotal;
    private int mCurrentPage;
    private int mTotalPages;
    private List<MessageEntity> mEntites;

    public MessagesPageEntity(int total, int currentPage, int totalPages, List<MessageEntity> entites) {
        mTotal = total;
        mCurrentPage = currentPage;
        mTotalPages = totalPages;
        mEntites = entites;
    }

    public int getTotal() {
        return mTotal;
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public int getTotalPages() {
        return mTotalPages;
    }

    public List<MessageEntity> getEntites() {
        return mEntites;
    }
}
