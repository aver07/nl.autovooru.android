package nl.autovooru.android.data.repository.datasource.machine;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import nl.autovooru.android.data.api.AutovooruApiService;
import nl.autovooru.android.data.entity.OrderEntity;
import nl.autovooru.android.data.entity.Page;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;
import nl.autovooru.android.data.entity.machines.MachineEntityList;
import nl.autovooru.android.data.entity.machines.MachineListQuery;
import nl.autovooru.android.data.entity.machines.details.DetailsEntity;
import nl.autovooru.android.data.entity.queries.MachineDetailsQuery;
import nl.autovooru.android.data.exceptions.ValidatePagingServerResponse;
import nl.autovooru.android.data.exceptions.ValidateServerResponse;
import rx.Observable;

/**
 * {@link MachinesDataStore} implementation based on connection to the api (Cloud).
 * Created by Antonenko Viacheslav on 17/11/15.
 */
@Singleton
public class CloudMachinesDataStore implements MachinesDataStore {

    private final AutovooruApiService mApiService;
    private final String mLanguage;

    @Inject
    public CloudMachinesDataStore(@NonNull AutovooruApiService apiService, @Named("appLanguage") String language) {
        mApiService = apiService;
        mLanguage = language;
    }

    @Override
    public Observable<MachineEntityList> loadMachines(int page, int itemPerPage,
                                                      String dealerId,
                                                      Map<String, Object> filters,
                                                      OrderEntity order,
                                                      @Nullable AuthInfoEntity authInfo) {

        final MachineListQuery query = MachineListQuery.newBuilder(new Page(Page.Type.catalog, itemPerPage, page))
                .setLanguage(mLanguage)
                .addFilters(filters)
                .setOrder(order)
                .setDealerId(dealerId)
                .setAuthInfo(authInfo)
                .build();
        return mApiService.getMachineListWithFilters(query)
                .flatMap(new ValidatePagingServerResponse<>())
                .map(response -> new MachineEntityList(
                        response.getTotal(),
                        response.getPage(),
                        response.getTotalPages(),
                        response.getData()));
    }

    @Override
    public Observable<DetailsEntity> loadMachineDetails(String machineId,
                                                        @Nullable AuthInfoEntity authInfo) {
        final Page page = new Page(Page.Type.advert);
        final MachineDetailsQuery query = new MachineDetailsQuery(page, machineId, mLanguage, authInfo);
        return mApiService.getMachineDetails(query).flatMap(new ValidateServerResponse<>());
    }

}
