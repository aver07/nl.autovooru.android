package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;

/**
 * Created by Antonenko Viacheslav on 08/01/16.
 */
public final class AddAutoQuery {

    @SerializedName("lang")
    private String mLanguage;
    @SerializedName("user")
    private AuthInfoEntity mUser;
    @SerializedName("advert_data")
    private Map<String, Object> mAdvertData;

    public AddAutoQuery(@NonNull Map<String, Object> advertData, @NonNull AuthInfoEntity user, @Nullable String language) {
        mLanguage = language;
        mUser = user;
        mAdvertData = advertData;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }
}
