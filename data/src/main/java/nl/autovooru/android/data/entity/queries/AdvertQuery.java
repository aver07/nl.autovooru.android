package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;

/**
 * Created by Antonenko Viacheslav on 05/01/16.
 */
public final class AdvertQuery {

    @Nullable
    @SerializedName("lang")
    private String mLanguage;
    @SerializedName("filters")
    private Map<String, Object> mFilters;
    @NonNull
    @SerializedName("user")
    private AuthInfoEntity mAuthInfo;

    public AdvertQuery(@NonNull String categoryId, @Nullable String markId, @NonNull AuthInfoEntity authInfo, @Nullable String language) {
        mFilters = new HashMap<>();
        mFilters.put("vcat", categoryId);
        if (markId != null) {
            mFilters.put("vmark", markId);
        }
        mAuthInfo = authInfo;
        mLanguage = language;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }
}
