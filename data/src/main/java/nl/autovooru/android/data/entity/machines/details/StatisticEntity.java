package nl.autovooru.android.data.entity.machines.details;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 18/12/15.
 */
public final class StatisticEntity {

    @SerializedName("hits")
    String mHits;

    public String getHits() {
        return mHits;
    }
}
