package nl.autovooru.android.data.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 30/10/15.
 */
public class ResponseCode implements Parcelable {

    public static final Creator<ResponseCode> CREATOR = new Creator<ResponseCode>() {
        @Override
        public ResponseCode createFromParcel(Parcel in) {
            return new ResponseCode(in);
        }

        @Override
        public ResponseCode[] newArray(int size) {
            return new ResponseCode[size];
        }
    };
    private static final String SUCCESS = "success";

    @NonNull
    @SerializedName("code")
    private String mCode;
    @Nullable
    @SerializedName("text")
    private String mText;
    @SerializedName("error_code")
    private int mErrorCode;

    public ResponseCode() {
        /* no-op */
    }

    protected ResponseCode(Parcel in) {
        mCode = in.readString();
        mText = in.readString();
        mErrorCode = in.readInt();
    }

    public boolean isSuccess() {
        return TextUtils.equals(mCode, SUCCESS);
    }

    @NonNull
    public String getCode() {
        return mCode;
    }

    @Nullable
    public String getText() {
        return mText;
    }

    public int getErrorCode() {
        return mErrorCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(mCode);
        dest.writeString(mText);
        dest.writeInt(mErrorCode);
    }
}
