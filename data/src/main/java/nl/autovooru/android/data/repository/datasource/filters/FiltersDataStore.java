package nl.autovooru.android.data.repository.datasource.filters;

import android.support.annotation.Nullable;

import java.util.List;
import java.util.Map;

import nl.autovooru.android.data.entity.filters.FilterEntity;
import nl.autovooru.android.data.entity.filters.FilterTypeEntity;
import nl.autovooru.android.data.entity.machines.MachineSearchFiltersEntity;
import rx.Observable;

/**
 * Interface that represents a data store from where data is retrieved.
 * <p>
 * Created by Antonenko Viacheslav on 16/11/15.
 */
public interface FiltersDataStore {

    Observable<List<FilterTypeEntity>> filterTypesList();

    Observable<Map<String, List<FilterEntity>>> filtersListByFilterType(int page,
                                                                        int itemsPerPage,
                                                                        List<String> filterTypeKeys);

    Observable<MachineSearchFiltersEntity> getMachineSearchFilters(@Nullable Map<String, String> filters);
}
