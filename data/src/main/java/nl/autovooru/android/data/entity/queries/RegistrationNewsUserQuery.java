package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public final class RegistrationNewsUserQuery {

    @Nullable
    @SerializedName("lang")
    private final String mLanguage;
    @SerializedName("user")
    private User mUser;

    public RegistrationNewsUserQuery(@Nullable String name, @Nullable String surname,
                                     @NonNull String postcode, @NonNull String city,
                                     @NonNull String address, @NonNull String phone,
                                     @NonNull String email, @NonNull String password,
                                     @Nullable String language) {
        mLanguage = language;
        mUser = new User(name, surname, postcode, city, address, phone, email, password);
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }

    private static class User {
        @SerializedName("up_fname")
        private final String mName;
        @SerializedName("up_sname")
        private final String mSurname;
        @SerializedName("lp_pc")
        private final String mPostcode;
        @SerializedName("up_city")
        private final String mCity;
        @SerializedName("address")
        private final String mAddress;
        @SerializedName("phone")
        private final String mPhone;
        @SerializedName("type")
        private final String mType = "pp";
        @SerializedName("email")
        private final String mEmail;
        @SerializedName("password")
        private final String mPassword;

        public User(String name, String surname, String postcode, String city,
                    String address, String phone, String email, String password) {
            mName = name;
            mSurname = surname;
            mPostcode = postcode;
            mCity = city;
            mAddress = address;
            mPhone = phone;
            mEmail = email;
            mPassword = password;
        }
    }
}
