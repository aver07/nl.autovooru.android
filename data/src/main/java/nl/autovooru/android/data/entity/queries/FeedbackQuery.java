package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;

/**
 * Created by Antonenko Viacheslav on 09/12/15.
 */
public class FeedbackQuery {

    @SerializedName("topic")
    private final String mSubject;
    @SerializedName("name")
    private final String mName;
    @SerializedName("email")
    private final String mEmail;
    @SerializedName("message")
    private final String mMessage;
    @Nullable
    @SerializedName("lang")
    private final String mLanguage;

    public FeedbackQuery(@NonNull String subject, @NonNull String name, @NonNull String email,
                         @NonNull String message, @Nullable String language) {
        mSubject = subject;
        mName = name;
        mEmail = email;
        mMessage = message;
        mLanguage = language;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }
}
