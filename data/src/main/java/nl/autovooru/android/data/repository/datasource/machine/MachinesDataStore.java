package nl.autovooru.android.data.repository.datasource.machine;

import android.support.annotation.Nullable;

import java.util.Map;

import nl.autovooru.android.data.entity.OrderEntity;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;
import nl.autovooru.android.data.entity.machines.MachineEntityList;
import nl.autovooru.android.data.entity.machines.details.DetailsEntity;
import rx.Observable;

/**
 * Interface that represents a data store from where data is retrieved.
 * <p>
 * Created by Antonenko Viacheslav on 17/11/15.
 */
public interface MachinesDataStore {

    Observable<MachineEntityList> loadMachines(int page, int itemPerPage,
                                               String dealerId,
                                               Map<String, Object> filters,
                                               OrderEntity order,
                                               @Nullable AuthInfoEntity authInfo);

    Observable<DetailsEntity> loadMachineDetails(String machineId, @Nullable AuthInfoEntity authInfo);
}
