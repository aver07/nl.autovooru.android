package nl.autovooru.android.data.repository.datasource.user;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;
import java.util.Map;

import nl.autovooru.android.data.entity.OrderEntity;
import nl.autovooru.android.data.entity.advert.AdvertFieldsEntity;
import nl.autovooru.android.data.entity.advert.AutoInfoEntity;
import nl.autovooru.android.data.entity.advert.YoutubeInfoEntity;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;
import nl.autovooru.android.data.entity.machines.MachineEntityList;
import nl.autovooru.android.data.entity.machines.details.EditMachineDetailsEntity;
import nl.autovooru.android.data.entity.user.BidEntity;
import nl.autovooru.android.data.entity.user.MessagesPageEntity;
import nl.autovooru.android.data.entity.user.UserInfoEntity;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public interface UserDataStore {

    Observable<MachineEntityList> myAds(int page, int itemPerPage, String dealerId,
                                        @Nullable AuthInfoEntity authInfo);

    Observable<Void> addToFavorite(String machineId, @NonNull AuthInfoEntity authInfo);

    Observable<Void> removeFromFavorite(String machineId, @NonNull AuthInfoEntity authInfo);

    Observable<Void> deleteMyAd(@NonNull String machineId, @NonNull AuthInfoEntity authInfo);

    Observable<List<BidEntity>> getMachineBids(@NonNull String machineId,
                                               @NonNull AuthInfoEntity authInfo);

    Observable<Void> deleteBidMessage(@NonNull String bidId, @NonNull AuthInfoEntity authInfo);

    Observable<UserInfoEntity> getUserData(@NonNull AuthInfoEntity authInfo);

    Observable<MachineEntityList> getUserFavorites(int page, int itemPerPage, OrderEntity order, @NonNull AuthInfoEntity authInfo);

    Observable<MachineEntityList> getUserBids(int page, int itemPerPage, @NonNull AuthInfoEntity authInfo);

    Observable<Void> deleteAllUserBids(@Nullable String machineId, @NonNull AuthInfoEntity authInfo);

    Observable<Void> deleteUserBid(@NonNull String bidId, @NonNull AuthInfoEntity authInfo);

    Observable<MessagesPageEntity> getUserMessages(int page, int itemPerPage, @NonNull AuthInfoEntity authInfo);

    Observable<Void> sendAnswer(@NonNull String messageId, @NonNull String name, @NonNull String subject,
                                @NonNull String email, @NonNull String message,
                                @NonNull AuthInfoEntity authInfo);

    Observable<Void> deleteMessage(@NonNull String messageId, @NonNull AuthInfoEntity authInfo);

    Observable<Void> saveUserData(@NonNull UserInfoEntity userInfo, @NonNull AuthInfoEntity authInfo);

    Observable<AdvertFieldsEntity> getAdvertFields(@NonNull String categoryId, @Nullable String markId, @NonNull AuthInfoEntity authInfo);

    Observable<AutoInfoEntity> getAutoInfoByToken(@NonNull String token, @NonNull AuthInfoEntity authInfo);

    Observable<YoutubeInfoEntity> getYoutubeInfo(@NonNull String youtubeLink, @NonNull AuthInfoEntity authInfo);

    Observable<Void> addAuto(@NonNull Map<String, Object> fieldsValues, List<String> imagesUrl, @NonNull AuthInfoEntity authInfo);

    Observable<Void> editAuto(@NonNull String machineId, @NonNull Map<String, Object> fieldsValues,
                              List<String> imagesUrl, @NonNull AuthInfoEntity authInfo);

    Observable<EditMachineDetailsEntity> getMyMachineDetails(@NonNull String machineId, @NonNull AuthInfoEntity authInfo);

    Observable<Void> sendPushToken(@NonNull String deviceToken, @NonNull AuthInfoEntity authInfo);
}
