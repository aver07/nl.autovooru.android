package nl.autovooru.android.data.entity.filters;

import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.Page;

/**
 * Created by Antonenko Viacheslav on 05/11/15.
 */
public class FiltersQuery {

    @Nullable
    @SerializedName("page")
    private final Page mPage;
    @Nullable
    @SerializedName("filter")
    private final List<String> mFilters;
    @Nullable
    @SerializedName("lang")
    private final String mLanguage;

    private FiltersQuery(Builder builder) {
        mPage = builder.mPage;
        mFilters = builder.mFilters;
        mLanguage = builder.mLanguage;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(FiltersQuery copy) {
        Builder builder = new Builder();
        builder.mPage = copy.mPage;
        builder.mFilters = copy.mFilters;
        builder.mLanguage = copy.mLanguage;
        return builder;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }


    public static final class Builder {
        @Nullable
        private Page mPage;
        @Nullable
        private List<String> mFilters;
        @Nullable
        private String mLanguage;

        private Builder() {
        }

        public Builder page(@Nullable Page page) {
            mPage = page;
            return this;
        }

        public Builder filters(@Nullable List<String> filters) {
            mFilters = filters;
            return this;
        }

        public Builder language(@Nullable String language) {
            mLanguage = language;
            return this;
        }

        public FiltersQuery build() {
            return new FiltersQuery(this);
        }
    }
}
