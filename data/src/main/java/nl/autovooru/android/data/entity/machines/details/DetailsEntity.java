package nl.autovooru.android.data.entity.machines.details;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import nl.autovooru.android.data.entity.machines.MachineEntity;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
public final class DetailsEntity {

    @SerializedName("as_id")
    private String mMachineId;
    @SerializedName("ua_id")
    private String mDealerId;
    @SerializedName("profile")
    private ProfileEntity mProfile;
    @SerializedName("content")
    private ContentEntity mContent;
    @SerializedName("images")
    private List<String> mImagesList;
    @SerializedName("info")
    private InfoEntity mInfo;
    @SerializedName("filters")
    private FiltersEntity mFilters;
    @SerializedName("stats")
    private StatisticEntity mStatistic;
    @SerializedName("similar_auto")
    private List<MachineEntity> mSimilarAutos;
    @SerializedName("is_favorite")
    private boolean isFavorite;

    public String getMachineId() {
        return mMachineId;
    }

    public String getDealerId() {
        return mDealerId;
    }

    public ProfileEntity getProfile() {
        return mProfile;
    }

    public ContentEntity getContent() {
        return mContent;
    }

    public List<String> getImagesList() {
        return mImagesList;
    }

    public InfoEntity getInfo() {
        return mInfo;
    }

    public FiltersEntity getFilters() {
        return mFilters;
    }

    public StatisticEntity getStatistic() {
        return mStatistic;
    }

    public List<MachineEntity> getSimilarAutos() {
        return mSimilarAutos;
    }

    public boolean getIsFavorite() {
        return isFavorite;
    }
}
