package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public final class RegistrationNewOrganizationQuery {

    @Nullable
    @SerializedName("lang")
    private final String mLanguage;
    @SerializedName("user")
    private User mUser;

    public RegistrationNewOrganizationQuery(@Nullable String name, @Nullable String surname,
                                            @NonNull String companyName, @NonNull String autosCount,
                                            @NonNull String postcode, @NonNull String city,
                                            @NonNull String address, @NonNull String phone, @NonNull String email,
                                            @Nullable String language) {
        mLanguage = language;
        mUser = new User(name, surname, companyName, autosCount, postcode, city, address, phone, email);
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }

    private static class User {
        @SerializedName("up_fname")
        private final String mName;
        @SerializedName("up_sname")
        private final String mSurname;
        @SerializedName("company_name")
        private final String mCompanyName;
        @SerializedName("autos_count")
        private final String mAutosCount;
        @SerializedName("lp_pc")
        private final String mPostcode;
        @SerializedName("up_city")
        private final String mCity;
        @SerializedName("address")
        private final String mAddress;
        @SerializedName("phone")
        private final String mPhone;
        @SerializedName("type")
        private final String mType = "org";
        @SerializedName("email")
        private final String mEmail;

        public User(String name, String surname, String companyName, String autosCount, String postcode, String city, String address, String phone, String email) {
            mName = name;
            mSurname = surname;
            mCompanyName = companyName;
            mAutosCount = autosCount;
            mPostcode = postcode;
            mCity = city;
            mAddress = address;
            mPhone = phone;
            mEmail = email;
        }
    }
}
