package nl.autovooru.android.data.exceptions;

import nl.autovooru.android.data.exceptions.authorization.EmailExistsException;
import nl.autovooru.android.data.exceptions.authorization.InvalidCredentialsException;
import nl.autovooru.android.data.exceptions.authorization.InvalidEmailException;
import nl.autovooru.android.data.exceptions.authorization.InvalidFormDataException;
import nl.autovooru.android.data.exceptions.authorization.TokenExpiredException;
import nl.autovooru.android.data.exceptions.machines.InvalidYoutubeLinkException;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public final class TransformErrorCodesToException {

    public static Exception transform(int errorCode, String text) {
        switch (errorCode) {
            case 1:
                return new InvalidEmailException(text);
            case 4:
                return new TokenExpiredException();
            case 9:
                return new InvalidCredentialsException();
            case 18:
                return new InvalidYoutubeLinkException();
            case 19:
                return new EmailExistsException();
            case 21:
                return new InvalidFormDataException();
            default:
                return new RuntimeException("Unknown error code: " + errorCode);
        }
    }
}
