package nl.autovooru.android.data.entity.queries;

import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

import nl.autovooru.android.common.utils.Base64;

/**
 * Created by Antonenko Viacheslav on 10/12/15.
 */
public class MachineFiltersQuery {

    @Nullable
    @SerializedName("filters")
    private final Map<String, String> mFilters;
    @Nullable
    @SerializedName("lang")
    private final String mLanguage;

    public MachineFiltersQuery(String language, Map<String, String> filters) {
        mLanguage = language;
        mFilters = filters;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }
}
