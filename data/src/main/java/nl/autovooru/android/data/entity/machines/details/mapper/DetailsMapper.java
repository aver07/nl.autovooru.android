package nl.autovooru.android.data.entity.machines.details.mapper;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import nl.autovooru.android.data.entity.machines.MachineEntity;
import nl.autovooru.android.data.entity.machines.MachineFilterEntity;
import nl.autovooru.android.data.entity.machines.details.ContentEntity;
import nl.autovooru.android.data.entity.machines.details.DetailsEntity;
import nl.autovooru.android.data.entity.machines.details.EditMachineDetailsEntity;
import nl.autovooru.android.data.entity.machines.details.FiltersEntity;
import nl.autovooru.android.data.entity.machines.details.InfoEntity;
import nl.autovooru.android.data.entity.machines.details.LocationEntity;
import nl.autovooru.android.data.entity.machines.details.ProfileEntity;
import nl.autovooru.android.data.entity.machines.details.StatisticEntity;
import nl.autovooru.android.data.entity.machines.mapper.MachineFiltersMapper;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.domain.model.machine.MachineFilter;
import nl.autovooru.android.domain.model.machine.details.Content;
import nl.autovooru.android.domain.model.machine.details.EditMachineDetails;
import nl.autovooru.android.domain.model.machine.details.Filters;
import nl.autovooru.android.domain.model.machine.details.Info;
import nl.autovooru.android.domain.model.machine.details.Location;
import nl.autovooru.android.domain.model.machine.details.MachineDetails;
import nl.autovooru.android.domain.model.machine.details.Profile;
import nl.autovooru.android.domain.model.machine.details.Statistic;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
public final class DetailsMapper {

    private DetailsMapper() {
        // no-op
    }

    @Nullable
    public static Content contentEntityToContent(@Nullable ContentEntity contentEntity) {
        if (contentEntity == null) {
            return null;
        }

        Content content = new Content();

        content.setAlias(contentEntity.getAlias());
        content.setName(contentEntity.getName());
        content.setUserDescription(contentEntity.getUserDescription());
        content.setGeneratedDescription(contentEntity.getGeneratedDescription());

        return content;
    }

    @Nullable
    public static MachineDetails detailEntityToDetails(@Nullable DetailsEntity detailsEntity) {
        if (detailsEntity == null) {
            return null;
        }

        MachineDetails machineDetails = new MachineDetails();

        machineDetails.setStatistic(statisticEntityToStatistic(detailsEntity.getStatistic()));
        machineDetails.setMachineId(detailsEntity.getMachineId());
        machineDetails.setDealerId(detailsEntity.getDealerId());
        machineDetails.setProfile(profileEntityToProfile(detailsEntity.getProfile()));
        machineDetails.setContent(contentEntityToContent(detailsEntity.getContent()));
        if (detailsEntity.getImagesList() != null) {
            machineDetails.setImagesList(new ArrayList<String>(detailsEntity.getImagesList()));
        }
        machineDetails.setInfo(infoEntityToInfo(detailsEntity.getInfo()));
        machineDetails.setFilters(filtersEntityToFilters(detailsEntity.getFilters()));
        machineDetails.setSimilarAutos(machineEntityListToMachineList(detailsEntity.getSimilarAutos()));
        machineDetails.setIsFavorite(detailsEntity.getIsFavorite());

        return machineDetails;
    }

    @Nullable
    public static Filters filtersEntityToFilters(@Nullable FiltersEntity entity) {
        if (entity == null) {
            return null;
        }

        Filters model = new Filters();

        model.setOtherFilters(machineFilterEntityListToMachineFilterList(entity.getOtherFilters()));
        model.setColor(MachineFiltersMapper.optionsEntityToOptions(entity.getColor()));
        model.setBody(MachineFiltersMapper.optionsEntityToOptions(entity.getBody()));
        model.setCategory(MachineFiltersMapper.optionsEntityToOptions(entity.getCategory()));
        model.setFuel(MachineFiltersMapper.optionsEntityToOptions(entity.getFuel()));
        model.setMark(MachineFiltersMapper.optionsEntityToOptions(entity.getMark()));
        model.setModel(MachineFiltersMapper.optionsEntityToOptions(entity.getModel()));
        model.setTransmission(MachineFiltersMapper.optionsEntityToOptions(entity.getTransmission()));

        return model;
    }

    @Nullable
    public static Info infoEntityToInfo(@Nullable InfoEntity infoEntity) {
        if (infoEntity == null) {
            return null;
        }

        Info info = new Info();

        info.setReleaseYear(infoEntity.getReleaseYear());
        info.setMileage(infoEntity.getMileage());
        info.setFuel(infoEntity.getFuel());
        info.setPrice(infoEntity.getPrice());
        info.setSalePrice(infoEntity.getSalePrice());
        info.setToken(infoEntity.getToken());
        info.setMark(infoEntity.getMark());
        info.setModel(infoEntity.getModel());
        info.setEngineSize(infoEntity.getEngineSize());
        info.setBody(infoEntity.getBody());
        info.setTransmission(infoEntity.getTransmission());
        info.setVideoUrl(infoEntity.getVideoUrl());
        info.setVideoImage(infoEntity.getVideoImage());
        info.setDoors(infoEntity.getDoors());
        info.setCapacity(infoEntity.getCapacity());
        info.setFirstOwner(infoEntity.isFirstOwner());
        info.setNap(infoEntity.isNap());

        return info;
    }

    @Nullable
    public static Location locationEntityToLocation(@Nullable LocationEntity locationEntity) {
        if (locationEntity == null) {
            return null;
        }

        Location location = new Location();

        location.setLatitude(locationEntity.getLatitude());
        location.setLongitude(locationEntity.getLongitude());

        return location;
    }

    @Nullable
    public static Profile profileEntityToProfile(@Nullable ProfileEntity profileEntity) {
        if (profileEntity == null) {
            return null;
        }

        Profile profile_ = new Profile();

        profile_.setPostCode(profileEntity.getPostCode());
        profile_.setCity(profileEntity.getCity());
        profile_.setAddress(profileEntity.getAddress());
        profile_.setOrganizationName(profileEntity.getOrganizationName());
        profile_.setPhone(profileEntity.getPhone());
        profile_.setEmail(profileEntity.getEmail());
        profile_.setWebsiteUrl(profileEntity.getWebsiteUrl());
        profile_.setLocations(locationEntityListToLocationArrayList(profileEntity.getLocations()));
        profile_.setDealerType(profileEntity.getDealerType());

        return profile_;
    }

    @Nullable
    public static Statistic statisticEntityToStatistic(@Nullable StatisticEntity statisticEntity) {
        if (statisticEntity == null) {
            return null;
        }

        Statistic statistic_ = new Statistic();

        statistic_.setHits(statisticEntity.getHits());

        return statistic_;
    }

    @Nullable
    public static Machine machineEntityToMachine(@Nullable MachineEntity machineEntity) {
        if (machineEntity == null) {
            return null;
        }

        Machine machine_ = new Machine();

        machine_.setMachineId(machineEntity.getMachineId());
        machine_.setMark(machineEntity.getMark());
        machine_.setModel(machineEntity.getModel());
        machine_.setMileage(machineEntity.getMileage());
        machine_.setFuel(machineEntity.getFuel());
        machine_.setPrice(machineEntity.getPrice());
        if (machineEntity.getImages() != null) {
            machine_.setImages(new ArrayList<String>(machineEntity.getImages()));
        }
        machine_.setName(machineEntity.getName());
        machine_.setReleaseYear(machineEntity.getReleaseYear());
        machine_.setCity(machineEntity.getCity());
        machine_.setAlias(machineEntity.getAlias());
        machine_.setBids(machineEntity.getBids());
        machine_.setToken(machineEntity.getToken());
        machine_.setHits(machineEntity.getHits());
        machine_.setDealerId(machineEntity.getDealerId());
        machine_.setBidId(machineEntity.getBidId());
        machine_.setBid(machineEntity.getBid());
        machine_.setPromoPrice(machineEntity.getPromoPrice());
        machine_.setFavorite(machineEntity.getFavorite());

        return machine_;
    }

    @Nullable
    public static EditMachineDetails entityToModel(@Nullable EditMachineDetailsEntity entity) {
        if (entity == null) {
            return null;
        }

        EditMachineDetails editMachineDetails = new EditMachineDetails();

        editMachineDetails.setMachineId(entity.getMachineId());
        editMachineDetails.setContent(contentEntityToContent(entity.getContent()));
        if (entity.getImagesList() != null) {
            editMachineDetails.setImagesList(new ArrayList<String>(entity.getImagesList()));
        }
        editMachineDetails.setInfo(infoEntityToInfo(entity.getInfo()));
        editMachineDetails.setFilters(filtersEntityToFilters(entity.getFilters()));

        return editMachineDetails;
    }

    @Nullable
    protected static List<Machine> machineEntityListToMachineList(@Nullable List<MachineEntity> list) {
        if (list == null) {
            return null;
        }

        List<Machine> list_ = new ArrayList<Machine>();
        for (MachineEntity machineEntity : list) {
            list_.add(machineEntityToMachine(machineEntity));
        }

        return list_;
    }

    @Nullable
    protected static List<MachineFilter> machineFilterEntityListToMachineFilterList(@Nullable List<MachineFilterEntity> list) {
        if (list == null) {
            return null;
        }

        List<MachineFilter> list_ = new ArrayList<MachineFilter>();
        for (MachineFilterEntity machineFilterEntity : list) {
            list_.add(MachineFiltersMapper.machineFilterEntityToMachineFilter(machineFilterEntity));
        }

        return list_;
    }

    @Nullable
    protected static ArrayList<Location> locationEntityListToLocationArrayList(@Nullable List<LocationEntity> list) {
        if (list == null) {
            return null;
        }

        ArrayList<Location> arrayList = new ArrayList<Location>();
        for (LocationEntity locationEntity : list) {
            arrayList.add(locationEntityToLocation(locationEntity));
        }

        return arrayList;
    }
}
