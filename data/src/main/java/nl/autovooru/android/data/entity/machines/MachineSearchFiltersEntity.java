package nl.autovooru.android.data.entity.machines;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Antonenko Viacheslav on 10/12/15.
 */
public final class MachineSearchFiltersEntity {

    @SerializedName("main_filters")
    private MainFiltersEntity mMainFilters;
    @SerializedName("toggle_filters")
    private ToggleFilters mToggleFilters;
    @SerializedName("other_filters")
    private List<MachineFilterEntity> mOtherFilters;

    public MainFiltersEntity getMainFilters() {
        return mMainFilters;
    }

    public ToggleFilters getToggleFilters() {
        return mToggleFilters;
    }

    public List<MachineFilterEntity> getOtherFilters() {
        return mOtherFilters;
    }


    public static class MainFiltersEntity {
        @SerializedName("vmark_top")
        private MachineFilterEntity mTopMachineMarks;
        @SerializedName("vmark")
        private MachineFilterEntity mMachineMarks;
        @SerializedName("vmodel")
        private MachineFilterEntity mMachineModels;
        @SerializedName("year")
        private MachineFilterEntity mMachineYear;
        @SerializedName("price")
        private MachineFilterEntity mPrice;
        @SerializedName("vfuel")
        private MachineFilterEntity mFuel;
        @SerializedName("user_type")
        private MachineFilterEntity mUserType;
        @SerializedName("distance")
        private MachineFilterEntity mDistance;
        @SerializedName("mileage")
        private MachineFilterEntity mMileage;
        @SerializedName("keywords")
        private MachineFilterEntity mKeywords;
        @SerializedName("post_code")
        private MachineFilterEntity mPostcode;

        public MachineFilterEntity getTopMachineMarks() {
            return mTopMachineMarks;
        }

        public MachineFilterEntity getMachineMarks() {
            return mMachineMarks;
        }

        public MachineFilterEntity getMachineModels() {
            return mMachineModels;
        }

        public MachineFilterEntity getMachineYear() {
            return mMachineYear;
        }

        public MachineFilterEntity getPrice() {
            return mPrice;
        }

        public MachineFilterEntity getFuel() {
            return mFuel;
        }

        public MachineFilterEntity getUserType() {
            return mUserType;
        }

        public MachineFilterEntity getDistance() {
            return mDistance;
        }

        public MachineFilterEntity getMileage() {
            return mMileage;
        }

        public MachineFilterEntity getKeywords() {
            return mKeywords;
        }

        public MachineFilterEntity getPostcode() {
            return mPostcode;
        }
    }

    public static class ToggleFilters {
        @SerializedName("offer_time")
        MachineFilterEntity mOfferTime;
        @SerializedName("vtrans")
        MachineFilterEntity mTransmission;
        @SerializedName("color")
        MachineFilterEntity mColor;
        @SerializedName("vbody")
        MachineFilterEntity mBody;

        public MachineFilterEntity getOfferTime() {
            return mOfferTime;
        }

        public MachineFilterEntity getTransmission() {
            return mTransmission;
        }

        public MachineFilterEntity getColor() {
            return mColor;
        }

        public MachineFilterEntity getBody() {
            return mBody;
        }
    }

}
