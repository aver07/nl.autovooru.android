package nl.autovooru.android.data.exceptions;

import nl.autovooru.android.common.utils.L;
import nl.autovooru.android.data.exceptions.authorization.TokenExpiredException;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.domain.repository.AuthorizationRepository;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public final class RetryWithTokenRefresh implements Func1<Observable<? extends Throwable>, Observable<?>> {

    private static final L LOG = L.getLogger(RetryWithTokenRefresh.class);

    private final AuthorizationRepository mAuthorizationRepository;
    private final AccountRepository mAccountRepository;

    public RetryWithTokenRefresh(AuthorizationRepository authorizationRepository,
                                 AccountRepository accountRepository) {
        mAuthorizationRepository = authorizationRepository;
        mAccountRepository = accountRepository;
    }

    @Override
    public Observable<?> call(Observable<? extends Throwable> attempts) {
        LOG.d("RetryWithTokenRefresh call");
        return attempts.zipWith(Observable.range(1, 1), (throwable, i) -> {
            LOG.d("Retry: " + i);
            return throwable;
        }).flatMap(throwable -> {
            if (throwable instanceof TokenExpiredException && mAccountRepository.isLoggedIn()) {
                return mAuthorizationRepository.authorize(
                        mAccountRepository.getEmail(), mAccountRepository.getPassword())
                        .doOnNext(mAccountRepository::updateToken);
            } else {
                return Observable.error(throwable);
            }
        });
    }
}
