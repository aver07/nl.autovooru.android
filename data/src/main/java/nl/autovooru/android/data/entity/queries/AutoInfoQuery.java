package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;

/**
 * Created by Antonenko Viacheslav on 06/01/16.
 */
public final class AutoInfoQuery {

    @SerializedName("advert_data")
    private final AdvertData mAdvertData;
    @SerializedName("user")
    private final AuthInfoEntity mUser;
    @Nullable
    @SerializedName("lang")
    private String mLanguage;

    public AutoInfoQuery(@NonNull String token, @NonNull AuthInfoEntity user, String language) {
        mAdvertData = new AdvertData(token);
        mUser = user;
        mLanguage = language;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }

    private static class AdvertData {
        @SerializedName("token")
        private String mToken;

        public AdvertData(@NonNull String token) {
            mToken = token;
        }
    }
}
