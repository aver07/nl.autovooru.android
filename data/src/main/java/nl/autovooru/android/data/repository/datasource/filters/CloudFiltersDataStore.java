package nl.autovooru.android.data.repository.datasource.filters;

import android.support.annotation.Nullable;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import nl.autovooru.android.data.api.AutovooruApiService;
import nl.autovooru.android.data.entity.Page;
import nl.autovooru.android.data.entity.filters.FilterEntity;
import nl.autovooru.android.data.entity.filters.FilterTypeEntity;
import nl.autovooru.android.data.entity.filters.FiltersQuery;
import nl.autovooru.android.data.entity.machines.MachineSearchFiltersEntity;
import nl.autovooru.android.data.entity.queries.MachineFiltersQuery;
import nl.autovooru.android.data.exceptions.ValidateServerResponse;
import nl.autovooru.android.data.exceptions.ValidateServerResponseList;
import rx.Observable;

/**
 * {@link FiltersDataStore} implementation based on connection to the api (Cloud).
 * <p>
 * Created by Antonenko Viacheslav on 16/11/15.
 */
@Singleton
public class CloudFiltersDataStore implements FiltersDataStore {

    private final AutovooruApiService mApiService;
    private final String mLanguage;

    @Inject
    public CloudFiltersDataStore(AutovooruApiService apiService, @Named("appLanguage") String language) {
        mApiService = apiService;
        mLanguage = language;
    }

    @Override
    public Observable<List<FilterTypeEntity>> filterTypesList() {
        final FiltersQuery query = FiltersQuery.newBuilder().language(mLanguage).build();
        return mApiService.getFiltersTypesList(query).flatMap(new ValidateServerResponseList<>());
    }

    @Override
    public Observable<Map<String, List<FilterEntity>>> filtersListByFilterType(int page,
                                                                               int itemsPerPage,
                                                                               List<String> filterTypeKeys) {
        final FiltersQuery query = FiltersQuery.newBuilder()
                .page(new Page(null, itemsPerPage, page))
                .language(mLanguage)
                .filters(filterTypeKeys)
                .build();
        return mApiService.getFiltersList(query).flatMap(new ValidateServerResponse<>());
    }

    @Override
    public Observable<MachineSearchFiltersEntity> getMachineSearchFilters(@Nullable Map<String, String> filters) {
        final MachineFiltersQuery query = new MachineFiltersQuery(mLanguage, filters);
        return mApiService.getMachineSearchFilters(query).flatMap(new ValidateServerResponse<>());
    }

}
