package nl.autovooru.android.data.repository;

import javax.inject.Inject;
import javax.inject.Singleton;

import nl.autovooru.android.data.entity.auth.mapper.AuthMapper;
import nl.autovooru.android.data.repository.datasource.auth.CloudAuthorizationDataStore;
import nl.autovooru.android.domain.model.auth.AuthorizationToken;
import nl.autovooru.android.domain.repository.AuthorizationRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
@Singleton
public class AuthorizationRepositoryImpl implements AuthorizationRepository {

    private final CloudAuthorizationDataStore mAuthorizationDataStore;

    @Inject
    public AuthorizationRepositoryImpl(CloudAuthorizationDataStore cloudAuthorizationDataStore) {
        mAuthorizationDataStore = cloudAuthorizationDataStore;
    }

    @Override
    public Observable<AuthorizationToken> authorize(String login, String password) {
        return mAuthorizationDataStore.authorize(login, password).map(AuthMapper::entityToModel);
    }

    @Override
    public Observable<AuthorizationToken> refreshToken(String code) {
        return mAuthorizationDataStore.refreshToken(code).map(AuthMapper::entityToModel);
    }

    @Override
    public Observable<Void> forgotPassword(String email) {
        return mAuthorizationDataStore.forgotPassword(email);
    }

    @Override
    public Observable<AuthorizationToken> registrationNewUser(String name, String surname, String postcode, String city, String address, String phone, String email, String password) {
        return mAuthorizationDataStore.registrationNewUser(name, surname, postcode, city, address, phone, email, password).map(AuthMapper::entityToModel);
    }

    @Override
    public Observable<Void> registrationNewOrganization(String name, String surname, String companyName, String autosCount, String postcode, String city, String address, String phone, String email) {
        return mAuthorizationDataStore.registrationNewOrganization(name, surname, companyName, autosCount, postcode, city, address, phone, email);
    }
}
