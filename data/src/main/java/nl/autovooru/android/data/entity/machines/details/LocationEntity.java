package nl.autovooru.android.data.entity.machines.details;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
public final class LocationEntity {

    @SerializedName("latitude")
    private double mLatitude;
    @SerializedName("longitude")
    private double mLongitude;

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }
}
