package nl.autovooru.android.data.entity;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Antonenko Viacheslav on 15/11/15.
 */
public class BaseListResponse<T> {

    @Nullable
    @SerializedName("data")
    private List<T> mData;
    @SerializedName("response")
    private ResponseCode mResponseCode;
    @SerializedName("page")
    private int mPage;
    @SerializedName("total_pages")
    private int mTotalPages;
    @SerializedName("total")
    private int mTotal;

    @Nullable
    public List<T> getData() {
        return mData;
    }

    @NonNull
    public ResponseCode getResponseCode() {
        return mResponseCode;
    }

    public int getPage() {
        return mPage;
    }

    public int getTotalPages() {
        return mTotalPages;
    }

    public int getTotal() {
        return mTotal;
    }

    @Override
    public String toString() {
        return "BaseListResponse{" +
                "mData=" + mData +
                ", mResponseCode=" + mResponseCode +
                '}';
    }
}