package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;
import nl.autovooru.android.data.entity.user.UserInfoEntity;

/**
 * Created by Antonenko Viacheslav on 29/12/15.
 */
public final class SaveUserDataQuery {

    @Nullable
    @SerializedName("lang")
    private final String mLanguage;
    @SerializedName("user")
    private final AuthInfoEntity mUser;
    @SerializedName("user_data")
    private final UserInfoEntity mUserInfo;

    public SaveUserDataQuery(@NonNull UserInfoEntity userInfo, @NonNull AuthInfoEntity user, @Nullable String language) {
        mUserInfo = userInfo;
        mUser = user;
        mLanguage = language;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }
}
