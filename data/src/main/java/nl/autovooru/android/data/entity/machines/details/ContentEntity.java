package nl.autovooru.android.data.entity.machines.details;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
public final class ContentEntity {

    @Nullable
    @SerializedName("alias")
    private String mAlias;
    @Nullable
    @SerializedName("name")
    private String mName;
    @Nullable
    @SerializedName("description_html")
    private String mUserDescription;
    @Nullable
    @SerializedName("description")
    private String mGeneratedDescription;

    @Nullable
    public String getAlias() {
        return mAlias;
    }

    @Nullable
    public String getName() {
        return mName;
    }

    @Nullable
    public String getUserDescription() {
        return mUserDescription;
    }

    @Nullable
    public String getGeneratedDescription() {
        return mGeneratedDescription;
    }
}
