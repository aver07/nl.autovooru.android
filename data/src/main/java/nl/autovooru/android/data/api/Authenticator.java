package nl.autovooru.android.data.api;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

/**
 * Created by Antonenko Viacheslav on 15/11/15.
 */
public class Authenticator implements okhttp3.Authenticator {

    private static final String HEADER_AUTHORIZATION = "Authorization";

    private final String mUsername;
    private final String mPassword;

    public Authenticator(String username, String password) {
        mUsername = username;
        mPassword = password;
    }

    @Override
    public Request authenticate(Route route, Response response) throws IOException {
        String credential = Credentials.basic(mUsername, mPassword);
        return response.request()
                .newBuilder()
                .header(HEADER_AUTHORIZATION, credential)
                .build();
    }
}
