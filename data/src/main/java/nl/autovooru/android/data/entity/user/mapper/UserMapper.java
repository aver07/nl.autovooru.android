package nl.autovooru.android.data.entity.user.mapper;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import nl.autovooru.android.data.entity.user.BidEntity;
import nl.autovooru.android.data.entity.user.MessageEntity;
import nl.autovooru.android.data.entity.user.MessagesPageEntity;
import nl.autovooru.android.data.entity.user.UserInfoEntity;
import nl.autovooru.android.domain.model.user.Bid;
import nl.autovooru.android.domain.model.user.Message;
import nl.autovooru.android.domain.model.user.MessagesPage;
import nl.autovooru.android.domain.model.user.UserInfo;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public final class UserMapper {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

    @Nullable
    public static Bid entityToModel(BidEntity entity) {
        if (entity == null) {
            return null;
        }

        final Bid model = new Bid();
        model.setBid(entity.getBid());
        model.setBidId(entity.getBidId());
        model.setCreateDate(entity.getCreateDate());
        model.setEmail(entity.getEmail());
        model.setMessageId(entity.getMessageId());

        return model;
    }

    @NonNull
    public static List<Bid> entityListToModelList(List<BidEntity> entities) {
        if (entities == null || entities.isEmpty()) {
            return new ArrayList<>();
        }

        List<Bid> bids = new ArrayList<>(entities.size());

        for (BidEntity entity : entities) {
            final Bid bid = entityToModel(entity);
            if (bid != null) {
                bids.add(bid);
            }
        }

        return bids;
    }

    @Nullable
    public static UserInfo entityToModel(@Nullable UserInfoEntity entity) {
        if (entity == null) {
            return null;
        }

        final UserInfo model = new UserInfo();
        model.setEmail(entity.getEmail());
        model.setAddress(entity.getAddress());
        model.setCity(entity.getCity());
        model.setCompanyName(entity.getCompanyName());
        model.setCountry(entity.getCountry());
        model.setDisplayName(entity.getDisplayName());
        model.setFaxNumber(entity.getFaxNumber());
        model.setPhoneNumber(entity.getPhoneNumber());
        model.setPostcode(entity.getPostcode());
        model.setUserId(entity.getUserId());
        model.setUserType(entity.getUserType());
        model.setWebsiteUrl(entity.getWebsiteUrl());
        model.setName(entity.getName());
        model.setPassword(entity.getPassword());
        model.setSurname(entity.getSurname());

        return model;
    }

    @Nullable
    public static UserInfoEntity modelToEntity(@Nullable UserInfo model) {
        if (model == null) {
            return null;
        }

        final UserInfoEntity entity = new UserInfoEntity();
        entity.setEmail(model.getEmail());
        entity.setAddress(model.getAddress());
        entity.setCity(model.getCity());
        entity.setCompanyName(model.getCompanyName());
        entity.setCountry(model.getCountry());
        entity.setDisplayName(model.getDisplayName());
        entity.setFaxNumber(model.getFaxNumber());
        entity.setPhoneNumber(model.getPhoneNumber());
        entity.setPostcode(model.getPostcode());
        entity.setUserId(model.getUserId());
        entity.setUserType(model.getUserType());
        entity.setWebsiteUrl(model.getWebsiteUrl());
        entity.setName(model.getName());
        entity.setPassword(model.getPassword());
        entity.setSurname(model.getSurname());

        return entity;
    }

    @Nullable
    public static Message entityToModel(@Nullable MessageEntity entity) {
        if (entity == null) {
            return null;
        }

        final Message model = new Message();
        model.setEmail(entity.getEmail());
        model.setAnswerTopic(entity.getAnswerTopic());
        model.setMessage(entity.getMessage());
        model.setMessageId(entity.getMessageId());
        model.setTopic(entity.getTopic());
        if (TextUtils.isEmpty(entity.getCreateDate())) {
            model.setCreateDate(null);
        } else {
            try {
                model.setCreateDate(DATE_FORMAT.parse(entity.getCreateDate()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return model;
    }

    @Nullable
    public static List<Message> entitiesToModels(@Nullable List<MessageEntity> entities) {
        if (entities == null) {
            return null;
        }

        List<Message> list = new ArrayList<>(entities.size());
        for (MessageEntity messageEntity : entities) {
            final Message message = entityToModel(messageEntity);
            if (message != null) {
                list.add(message);
            }
        }

        return list;
    }

    @Nullable
    public static MessagesPage pageEntityToPage(@Nullable MessagesPageEntity entity) {
        if (entity == null) {
            return null;
        }

        MessagesPage messagesPage = new MessagesPage();

        messagesPage.setTotal(entity.getTotal());
        messagesPage.setCurrentPage(entity.getCurrentPage());
        messagesPage.setTotalPages(entity.getTotalPages());
        messagesPage.setEntites(entitiesToModels(entity.getEntites()));

        return messagesPage;
    }

}
