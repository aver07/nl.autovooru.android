package nl.autovooru.android.data.entity.filters.mapper;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import nl.autovooru.android.data.entity.filters.FilterTypeEntity;
import nl.autovooru.android.domain.model.filters.FilterType;

/**
 * Created by Antonenko Viacheslav on 18/11/15.
 */
@Singleton
public class FilterTypeEntityDataMapper {

    @Inject
    public FilterTypeEntityDataMapper() {
    }

    @Nullable
    public FilterType transform(@Nullable FilterTypeEntity filterTypeEntity) {
        FilterType filterType = null;
        if (filterTypeEntity != null) {
            filterType = new FilterType(
                    filterTypeEntity.getId(),
                    filterTypeEntity.getKey(),
                    filterTypeEntity.getName());
        }

        return filterType;
    }

    @NonNull
    public List<FilterType> transform(@Nullable List<FilterTypeEntity> filterTypeEntityList) {
        List<FilterType> filterTypes = new ArrayList<>();
        FilterType filterType;
        if (filterTypeEntityList != null) {
            for (FilterTypeEntity filterTypeEntity : filterTypeEntityList) {
                filterType = transform(filterTypeEntity);
                if (filterType != null) {
                    filterTypes.add(filterType);
                }
            }
        }

        return filterTypes;
    }
}
