package nl.autovooru.android.data.entity.queries;

import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public final class DeleteBidMessageQuery {

    @SerializedName("message")
    private final Message mMessage;
    @Nullable
    @SerializedName("lang")
    private final String mLanguage;
    @SerializedName("user")
    private final AuthInfoEntity mUser;

    public DeleteBidMessageQuery(String bidId, AuthInfoEntity authInfo, String language) {
        mLanguage = language;
        mMessage = new Message(bidId);
        mUser = authInfo;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }

    public static class Message {
        @SerializedName("uasb_id")
        private final String mBidId;

        public Message(String bidId) {
            mBidId = bidId;
        }
    }
}
