package nl.autovooru.android.data.entity.filters;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 30/10/15.
 */
public final class FilterTypeEntity {

    @SerializedName("id")
    private String mId;
    @SerializedName("parent_id")
    private String mParentId;
    @SerializedName("key")
    private String mKey;
    @SerializedName("name")
    private String mName;

    public String getId() {
        return mId;
    }

    public String getParentId() {
        return mParentId;
    }

    public String getKey() {
        return mKey;
    }

    public String getName() {
        return mName;
    }

    @Override
    public String toString() {
        return "FilterTypeEntity{" +
                "mId='" + mId + '\'' +
                ", mParentId='" + mParentId + '\'' +
                ", mKey='" + mKey + '\'' +
                ", mName='" + mName + '\'' +
                '}';
    }

}
