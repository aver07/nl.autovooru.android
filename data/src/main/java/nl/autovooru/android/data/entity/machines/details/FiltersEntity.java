package nl.autovooru.android.data.entity.machines.details;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import nl.autovooru.android.data.entity.machines.MachineFilterEntity;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
public class FiltersEntity {

    @Nullable
    @SerializedName("color")
    private MachineFilterEntity.Options mColor;
    @Nullable
    @SerializedName("vbody")
    private MachineFilterEntity.Options mBody;
    @Nullable
    @SerializedName("vcat")
    private MachineFilterEntity.Options mCategory;
    @Nullable
    @SerializedName("vfuel")
    private MachineFilterEntity.Options mFuel;
    @Nullable
    @SerializedName("vmark")
    private MachineFilterEntity.Options mMark;
    @Nullable
    @SerializedName("vmodel")
    private MachineFilterEntity.Options mModel;
    @Nullable
    @SerializedName("vtrans")
    private MachineFilterEntity.Options mTransmission;
    @Nullable
    @SerializedName("vopt")
    private List<MachineFilterEntity> mOtherFilters;

    @Nullable
    public List<MachineFilterEntity> getOtherFilters() {
        return mOtherFilters;
    }

    @Nullable
    public MachineFilterEntity.Options getColor() {
        return mColor;
    }

    @Nullable
    public MachineFilterEntity.Options getBody() {
        return mBody;
    }

    @Nullable
    public MachineFilterEntity.Options getCategory() {
        return mCategory;
    }

    @Nullable
    public MachineFilterEntity.Options getFuel() {
        return mFuel;
    }

    @Nullable
    public MachineFilterEntity.Options getMark() {
        return mMark;
    }

    @Nullable
    public MachineFilterEntity.Options getModel() {
        return mModel;
    }

    @Nullable
    public MachineFilterEntity.Options getTransmission() {
        return mTransmission;
    }
}
