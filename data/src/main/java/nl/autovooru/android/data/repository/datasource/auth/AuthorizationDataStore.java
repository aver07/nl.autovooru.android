package nl.autovooru.android.data.repository.datasource.auth;

import nl.autovooru.android.data.entity.auth.AuthorizationTokenEntity;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 21/12/15.
 */
public interface AuthorizationDataStore {

    Observable<AuthorizationTokenEntity> authorize(String login, String password);

    Observable<AuthorizationTokenEntity> refreshToken(String code);

    Observable<Void> forgotPassword(String email);

    Observable<AuthorizationTokenEntity> registrationNewUser(String name, String surname,
                                                             String postcode, String city,
                                                             String address, String phone,
                                                             String email, String password);

    Observable<Void> registrationNewOrganization(String name, String surname, String companyName,
                                                 String autosCount, String postcode, String city,
                                                 String address, String phone, String email);

}
