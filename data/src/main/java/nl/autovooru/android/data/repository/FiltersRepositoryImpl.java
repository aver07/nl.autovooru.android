package nl.autovooru.android.data.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import nl.autovooru.android.data.entity.filters.FilterEntity;
import nl.autovooru.android.data.entity.filters.mapper.FilterEntityDataMapper;
import nl.autovooru.android.data.entity.filters.mapper.FilterTypeEntityDataMapper;
import nl.autovooru.android.data.entity.machines.mapper.MachineFiltersMapper;
import nl.autovooru.android.data.repository.datasource.filters.CloudFiltersDataStore;
import nl.autovooru.android.domain.model.filters.Filter;
import nl.autovooru.android.domain.model.filters.FilterType;
import nl.autovooru.android.domain.model.machine.MachineSearchFilters;
import nl.autovooru.android.domain.repository.FiltersRepository;
import rx.Observable;

/**
 * {@link FiltersRepository} for retrieving data.
 * <p>
 * Created by Antonenko Viacheslav on 16/11/15.
 */
@Singleton
public class FiltersRepositoryImpl implements FiltersRepository {

    private final CloudFiltersDataStore mCloudFiltersDataStore;
    private final FilterEntityDataMapper mFilterEntityDataMapper;
    private final FilterTypeEntityDataMapper mFilterTypeEntityDataMapper;

    @Inject
    public FiltersRepositoryImpl(CloudFiltersDataStore cloudFiltersDataStore,
                                 FilterEntityDataMapper filterEntityDataMapper,
                                 FilterTypeEntityDataMapper filterTypeEntityDataMapper) {
        mFilterTypeEntityDataMapper = filterTypeEntityDataMapper;
        mFilterEntityDataMapper = filterEntityDataMapper;
        mCloudFiltersDataStore = cloudFiltersDataStore;
    }

    @Override
    public Observable<List<FilterType>> getFilterTypes() {
        return mCloudFiltersDataStore.filterTypesList().map(mFilterTypeEntityDataMapper::transform);
    }

    @Override
    public Observable<Map<String, List<Filter>>> getFiltersByFilterType(int page,
                                                                        int itemsPerPage,
                                                                        List<String> filterTypeKeys) {
        return mCloudFiltersDataStore.filtersListByFilterType(
                page,
                itemsPerPage,
                filterTypeKeys).map(response -> {
            Map<String, List<Filter>> map = new HashMap<>();
            for (Map.Entry<String, List<FilterEntity>> entry : response.entrySet()) {
                final List<Filter> values = mFilterEntityDataMapper.transform(entry.getValue());
                map.put(entry.getKey(), values);
            }
            return map;
        });
    }

    @Override
    public Observable<MachineSearchFilters> getMachineSearchFilters(Map<String, String> filters) {
        return mCloudFiltersDataStore.getMachineSearchFilters(filters).map(MachineFiltersMapper::machineSearchFiltersEntityToMachineSearchFilters);
    }
}
