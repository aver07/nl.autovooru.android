package nl.autovooru.android.data.entity.machines;

import java.util.List;

/**
 * Created by Antonenko Viacheslav on 25/11/15.
 */
public class MachineEntityList {

    private int mTotal;
    private int mCurrentPage;
    private int mTotalPages;
    private List<MachineEntity> mMachineList;

    public MachineEntityList(int total, int page, int totalPages, List<MachineEntity> machineList) {
        mTotal = total;
        mCurrentPage = page;
        mTotalPages = totalPages;
        mMachineList = machineList;
    }

    public int getTotal() {
        return mTotal;
    }

    public List<MachineEntity> getMachineList() {
        return mMachineList;
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public int getTotalPages() {
        return mTotalPages;
    }
}
