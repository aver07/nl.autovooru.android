package nl.autovooru.android.data.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import nl.autovooru.android.data.entity.OrderEntity;
import nl.autovooru.android.data.entity.OrderMapper;
import nl.autovooru.android.data.entity.advert.mapper.AdvertMapper;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;
import nl.autovooru.android.data.entity.auth.mapper.AuthMapper;
import nl.autovooru.android.data.entity.machines.MachineEntityList;
import nl.autovooru.android.data.entity.machines.details.mapper.DetailsMapper;
import nl.autovooru.android.data.entity.machines.mapper.MachineMapper;
import nl.autovooru.android.data.entity.user.UserInfoEntity;
import nl.autovooru.android.data.entity.user.mapper.UserMapper;
import nl.autovooru.android.data.exceptions.RetryWithTokenRefresh;
import nl.autovooru.android.data.repository.datasource.user.CloudUserDataStore;
import nl.autovooru.android.domain.model.advert.AdvertFields;
import nl.autovooru.android.domain.model.advert.AutoInfo;
import nl.autovooru.android.domain.model.advert.YoutubeInfo;
import nl.autovooru.android.domain.model.auth.AuthInfo;
import nl.autovooru.android.domain.model.machine.Machine;
import nl.autovooru.android.domain.model.machine.details.EditMachineDetails;
import nl.autovooru.android.domain.model.query.Order;
import nl.autovooru.android.domain.model.user.Bid;
import nl.autovooru.android.domain.model.user.MessagesPage;
import nl.autovooru.android.domain.model.user.UserInfo;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.domain.repository.AuthorizationRepository;
import nl.autovooru.android.domain.repository.UserDataRepository;
import nl.autovooru.android.domain.viewmodels.MachineListViewModel;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
@Singleton
public class UserDataRepositoryImpl implements UserDataRepository {

    private final CloudUserDataStore mCloudUserDataStore;
    private final AuthorizationRepository mAuthorizationRepository;
    private final AccountRepository mAccountRepository;

    @Inject
    public UserDataRepositoryImpl(CloudUserDataStore cloudUserDataStore,
                                  AuthorizationRepository authorizationRepository,
                                  AccountRepository accountRepository) {
        mCloudUserDataStore = cloudUserDataStore;
        mAuthorizationRepository = authorizationRepository;
        mAccountRepository = accountRepository;
    }

    @Override
    public Observable<MachineListViewModel> myAds(int page, int itemPerPage) {
        final AuthInfo authInfo = mAccountRepository.getAuthInfo();
        final String userId = mAccountRepository.getUserId();
        final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
        final Observable<MachineEntityList> observable = mCloudUserDataStore.myAds(page, itemPerPage,
                userId, authInfoEntity);
        return observable.map(machineEntityList -> {
            final int total = machineEntityList.getTotal();
            final int currentPage = machineEntityList.getCurrentPage();
            final int totalPages = machineEntityList.getTotalPages();
            final List<Machine> machineList = MachineMapper.machineEntityToMachine(machineEntityList.getMachineList());
            return new MachineListViewModel(total, currentPage, totalPages, machineList);
        });
    }

    @Override
    public Observable<Void> addToFavorite(@NonNull String machineId) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.addToFavorite(machineId, authInfoEntity);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<Void> removeFromFavorite(@NonNull String machineId) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.removeFromFavorite(machineId, authInfoEntity);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));

    }

    @Override
    public Observable<Void> deleteMyAd(@NonNull String machineId) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.deleteMyAd(machineId, authInfoEntity);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<List<Bid>> getMachineBids(@NonNull String machineId) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.getMachineBids(machineId, authInfoEntity)
                    .map(UserMapper::entityListToModelList);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<Void> deleteBidMessage(@NonNull String bidId) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.deleteBidMessage(bidId, authInfoEntity);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<UserInfo> getUserData() {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.getUserData(authInfoEntity);
        }).map(UserMapper::entityToModel)
                .retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<MachineListViewModel> getUserFavorites(int page, int itemPerPage, Order order) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            final OrderEntity orderEntity = OrderMapper.modelToEntity(order);
            return mCloudUserDataStore.getUserFavorites(page, itemPerPage, orderEntity, authInfoEntity);
        }).map(machineEntityList -> {
            final int total = machineEntityList.getTotal();
            final int currentPage = machineEntityList.getCurrentPage();
            final int totalPages = machineEntityList.getTotalPages();
            final List<Machine> machineList = MachineMapper.machineEntityToMachine(machineEntityList.getMachineList());
            return new MachineListViewModel(total, currentPage, totalPages, machineList);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<MachineListViewModel> getUserBids(int page, int itemPerPage) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.getUserBids(page, itemPerPage, authInfoEntity);
        }).map(machineEntityList -> {
            final int total = machineEntityList.getTotal();
            final int currentPage = machineEntityList.getCurrentPage();
            final int totalPages = machineEntityList.getTotalPages();
            final List<Machine> machineList = MachineMapper.machineEntityToMachine(machineEntityList.getMachineList());
            return new MachineListViewModel(total, currentPage, totalPages, machineList);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<Void> deleteAllUsersBids(@Nullable String machineId) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.deleteAllUserBids(machineId, authInfoEntity);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<Void> deleteUserBid(@NonNull String bidId) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.deleteUserBid(bidId, authInfoEntity);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));

    }

    @Override
    public Observable<MessagesPage> getUserMessages(int page, int itemPerPage) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.getUserMessages(page, itemPerPage, authInfoEntity);
        }).map(UserMapper::pageEntityToPage)
                .retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<Void> sendAnswer(@NonNull String messageId, @NonNull String subject,
                                       @NonNull String name, @NonNull String email,
                                       @NonNull String message) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.sendAnswer(messageId, subject, name, email, message, authInfoEntity);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<Void> deleteMessage(@NonNull String messageId) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.deleteMessage(messageId, authInfoEntity);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<Void> saveUserData(@NonNull UserInfo userInfo) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            final UserInfoEntity userInfoEntity = UserMapper.modelToEntity(userInfo);
            return mCloudUserDataStore.saveUserData(userInfoEntity, authInfoEntity);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<AdvertFields> getAdvertFields(@NonNull String categoryId, @Nullable String markId) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.getAdvertFields(categoryId, markId, authInfoEntity)
                    .map(AdvertMapper::entityToModel);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<AutoInfo> getAutoInfoByToken(@NonNull String token) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.getAutoInfoByToken(token, authInfoEntity)
                    .map(AdvertMapper::entityToModel);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<YoutubeInfo> getYoutubeInfo(@NonNull String youtubeLink) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.getYoutubeInfo(youtubeLink, authInfoEntity)
                    .map(AdvertMapper::entityToModel);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<Void> addAuto(@NonNull Map<String, Object> fieldsValues, @Nullable List<String> images) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.addAuto(fieldsValues, images, authInfoEntity);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<Void> editAuto(@NonNull String machineId,
                                     @NonNull Map<String, Object> fieldsValues,
                                     @Nullable List<String> images) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.editAuto(machineId, fieldsValues, images, authInfoEntity);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<EditMachineDetails> getMyMachineDetails(@NonNull String machineId) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.getMyMachineDetails(machineId, authInfoEntity)
                    .map(DetailsMapper::entityToModel);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<Void> sendPushToken(@NonNull String deviceToken) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mCloudUserDataStore.sendPushToken(deviceToken, authInfoEntity);
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }
}

