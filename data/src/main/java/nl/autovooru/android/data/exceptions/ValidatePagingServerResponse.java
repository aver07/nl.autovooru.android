package nl.autovooru.android.data.exceptions;

import nl.autovooru.android.data.entity.BaseListResponse;
import nl.autovooru.android.data.entity.ResponseCode;
import nl.autovooru.android.data.exceptions.server_errors.ServerErrorException;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public class ValidatePagingServerResponse<T> implements Func1<Response<BaseListResponse<T>>, Observable<BaseListResponse<T>>> {

    @Override
    public Observable<BaseListResponse<T>> call(Response<BaseListResponse<T>> response) {
        if (response.isSuccessful()) {
            final ResponseCode responseCode = response.body().getResponseCode();
            if (responseCode.isSuccess()) {
                return Observable.just(response.body());
            } else {
                return Observable.error(TransformErrorCodesToException.transform(
                        responseCode.getErrorCode(), responseCode.getText()
                ));
            }
        } else {
            return Observable.error(new ServerErrorException(response.code()));
        }
    }

}

