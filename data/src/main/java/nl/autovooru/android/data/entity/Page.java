package nl.autovooru.android.data.entity;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 05/11/15.
 */
public class Page {

    @Nullable
    @SerializedName("type")
    private Type mType;
    @SerializedName("max")
    private Integer mPerPage;
    @SerializedName("num")
    private Integer mPage;

    public Page(@Nullable Type type) {
        mType = type;
    }

    public Page(@Nullable Type type, Integer perPage, Integer page) {
        mType = type;
        mPerPage = perPage;
        mPage = page;
    }

    public enum Type {
        catalog("catalog"), advert("advert"), profile("profile"), profile_advert_edit("profile_advert_edit");

        private final String value;

        Type(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }
    }

}
