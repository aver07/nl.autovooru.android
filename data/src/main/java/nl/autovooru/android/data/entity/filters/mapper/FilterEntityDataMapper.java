package nl.autovooru.android.data.entity.filters.mapper;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import nl.autovooru.android.data.entity.filters.FilterEntity;
import nl.autovooru.android.domain.model.filters.Filter;

/**
 * Created by Antonenko Viacheslav on 16/11/15.
 */
@Singleton
public class FilterEntityDataMapper {

    @Inject
    public FilterEntityDataMapper() {
    }

    @Nullable
    public Filter transform(@Nullable FilterEntity filterEntity) {
        Filter filter = null;
        if (filterEntity != null) {
            filter = new Filter(filterEntity.getId(), filterEntity.getName());
        }

        return filter;
    }

    @NonNull
    public List<Filter> transform(@Nullable List<FilterEntity> filterEntityList) {
        List<Filter> filterList = new ArrayList<>();
        Filter filter;
        if (filterEntityList != null) {
            for (FilterEntity filterEntity : filterEntityList) {
                filter = transform(filterEntity);
                if (filter != null) {
                    filterList.add(filter);
                }
            }
        }

        return filterList;
    }
}
