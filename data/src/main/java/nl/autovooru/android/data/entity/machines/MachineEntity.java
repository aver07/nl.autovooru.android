package nl.autovooru.android.data.entity.machines;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Antonenko Viacheslav on 16/11/15.
 */
public final class MachineEntity {

    @SerializedName("as_id")
    private String mMachineId;
    @SerializedName("ua_id")
    private String mDealerId;
    @SerializedName("name")
    private String mName;
    @SerializedName("vmark")
    private String mMark;
    @SerializedName("vmodel")
    private String mModel;
    @SerializedName("vfuel")
    private String mFuel;
    @SerializedName("release_date")
    private String mReleaseDate;
    @SerializedName("release_year")
    private String mReleaseYear;
    // TODO: 17/11/15 Change mMileage to long
    @SerializedName("mileage")
    private String mMileage;
    // TODO: 17/11/15 Ask server side to change field price type to double
    @SerializedName("price")
    private float mPrice;
    @SerializedName("oprice")
    private String mPromoPrice;
    @SerializedName("images")
    private List<String> mImages;
    @SerializedName("up_city")
    private String mCity;
    @SerializedName("alias")
    private String mAlias;
    @SerializedName("bids")
    private String mBids;
    @SerializedName("token")
    private String mToken;
    @SerializedName("hits")
    private String mHits;
    @SerializedName("uasb_id")
    private String mBidId;
    @SerializedName("bid")
    private String mBid;
    @SerializedName("is_favorite")
    private boolean mFavorite;

    public String getMachineId() {
        return mMachineId;
    }

    public String getName() {
        return mName;
    }

    public String getMark() {
        return mMark;
    }

    public String getModel() {
        return mModel;
    }

    public String getFuel() {
        return mFuel;
    }

    public String getReleaseDate() {
        return mReleaseDate;
    }

    public String getMileage() {
        return mMileage;
    }

    public float getPrice() {
        return mPrice;
    }

    public List<String> getImages() {
        return mImages;
    }

    public String getReleaseYear() {
        return mReleaseYear;
    }

    public String getCity() {
        return mCity;
    }

    public String getAlias() {
        return mAlias;
    }

    public String getBids() {
        return mBids;
    }

    public String getToken() {
        return mToken;
    }

    public String getHits() {
        return mHits;
    }

    public String getDealerId() {
        return mDealerId;
    }

    public String getBidId() {
        return mBidId;
    }

    public String getBid() {
        return mBid;
    }

    public String getPromoPrice() {
        return mPromoPrice;
    }

    public boolean getFavorite() {
        return mFavorite;
    }
}
