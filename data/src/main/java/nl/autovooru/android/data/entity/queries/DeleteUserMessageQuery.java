package nl.autovooru.android.data.entity.queries;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public final class DeleteUserMessageQuery {

    @SerializedName("user")
    private AuthInfoEntity mAuthInfo;
    @SerializedName("message")
    private Message mMessage;
    @SerializedName("lang")
    private String mLanguage;

    public DeleteUserMessageQuery(AuthInfoEntity authInfo, String messageId, String language) {
        mAuthInfo = authInfo;
        mMessage = new Message(messageId);
        mLanguage = language;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }

    private static class Message {
        @SerializedName("um_id")
        private String mMessageId;

        public Message(String messageId) {
            mMessageId = messageId;
        }
    }
}
