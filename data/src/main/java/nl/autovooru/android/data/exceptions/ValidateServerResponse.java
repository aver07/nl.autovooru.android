package nl.autovooru.android.data.exceptions;

import nl.autovooru.android.data.entity.BaseResponse;
import nl.autovooru.android.data.entity.ResponseCode;
import nl.autovooru.android.data.exceptions.server_errors.ServerErrorException;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Antonenko Viacheslav on 22/12/15.
 */
public class ValidateServerResponse<T> implements Func1<Response<BaseResponse<T>>, Observable<T>> {

    @Override
    public Observable<T> call(Response<BaseResponse<T>> response) {
        if (response.isSuccessful()) {
            final ResponseCode responseCode = response.body().getResponseCode();
            if (responseCode.isSuccess()) {
                return Observable.just(response.body().getData());
            } else {
                return Observable.error(TransformErrorCodesToException.transform(
                        responseCode.getErrorCode(), responseCode.getText()));
            }
        } else {
            return Observable.error(new ServerErrorException(response.code()));
        }
    }

}
