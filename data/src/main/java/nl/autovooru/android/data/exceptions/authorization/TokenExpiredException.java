package nl.autovooru.android.data.exceptions.authorization;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public class TokenExpiredException extends Exception {

    public TokenExpiredException() {
        super();
    }

    public TokenExpiredException(String detailMessage) {
        super(detailMessage);
    }
}
