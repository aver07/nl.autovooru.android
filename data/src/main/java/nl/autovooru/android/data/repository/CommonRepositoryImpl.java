package nl.autovooru.android.data.repository;

import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import nl.autovooru.android.data.api.AutovooruApiService;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;
import nl.autovooru.android.data.entity.auth.mapper.AuthMapper;
import nl.autovooru.android.data.entity.queries.BidMessageQuery;
import nl.autovooru.android.data.entity.queries.FeedbackQuery;
import nl.autovooru.android.data.entity.queries.SendBidMessageQuery;
import nl.autovooru.android.data.entity.queries.SendMessageQuery;
import nl.autovooru.android.data.exceptions.RetryWithTokenRefresh;
import nl.autovooru.android.data.exceptions.ValidateServerResponse;
import nl.autovooru.android.domain.model.auth.AuthInfo;
import nl.autovooru.android.domain.repository.AccountRepository;
import nl.autovooru.android.domain.repository.AuthorizationRepository;
import nl.autovooru.android.domain.repository.CommonRepository;
import rx.Observable;

/**
 * Created by Antonenko Viacheslav on 09/12/15.
 */
@Singleton
public class CommonRepositoryImpl implements CommonRepository {

    private final AutovooruApiService mApiService;
    private final String mLanguage;
    private final AccountRepository mAccountRepository;
    private final AuthorizationRepository mAuthorizationRepository;

    @Inject
    public CommonRepositoryImpl(@NonNull AutovooruApiService apiService,
                                @Named("appLanguage") String language,
                                @NonNull AccountRepository accountRepository,
                                @NonNull AuthorizationRepository authorizationRepository) {
        mApiService = apiService;
        mLanguage = language;
        mAccountRepository = accountRepository;
        mAuthorizationRepository = authorizationRepository;
    }

    @Override
    public rx.Observable<Boolean> sendFeedback(String subject, String name, String email, String message) {
        return mApiService.sendFeedback(new FeedbackQuery(subject, name, email, message, mLanguage))
                .flatMap(new ValidateServerResponse<>())
                .flatMap(aVoid -> Observable.just(true));
    }

    @Override
    public Observable<Boolean> sendMessage(String machineId, String name, String email, String subject, String message) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mApiService.sendMessage(new SendMessageQuery(machineId, name, email, subject, message, mLanguage, authInfoEntity))
                    .flatMap(new ValidateServerResponse<>())
                    .flatMap(aVoid -> Observable.just(true));
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }

    @Override
    public Observable<Boolean> sendBid(String name, String email, String bid, String message, String machineId) {
        final AuthInfo authInfo = mAccountRepository.getAuthInfo();
        final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
        return mApiService.sendBid(new BidMessageQuery(authInfoEntity, name, machineId, email, bid, message, mLanguage))
                .flatMap(new ValidateServerResponse<>())
                .flatMap(aVoid -> Observable.just(true));
    }

    @Override
    public Observable<Boolean> sendBidMessage(String messageId, String name, String email, String subject, String message) {
        return Observable.defer(() -> {
            final AuthInfo authInfo = mAccountRepository.getAuthInfo();
            final AuthInfoEntity authInfoEntity = AuthMapper.modelToEntity(authInfo);
            return mApiService.sendBidMessage(new SendBidMessageQuery(messageId, name, email, subject, message, mLanguage, authInfoEntity))
                    .flatMap(new ValidateServerResponse<>())
                    .flatMap(aVoid -> Observable.just(true));
        }).retryWhen(new RetryWithTokenRefresh(mAuthorizationRepository, mAccountRepository));
    }
}
