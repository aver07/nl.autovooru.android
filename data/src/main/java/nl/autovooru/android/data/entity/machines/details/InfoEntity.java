package nl.autovooru.android.data.entity.machines.details;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
public final class InfoEntity {

    @Nullable
    @SerializedName("release_year")
    private String mReleaseYear;
    @Nullable
    @SerializedName("mileage")
    private String mMileage;
    @Nullable
    @SerializedName("fuel")
    private String mFuel;
    @Nullable
    @SerializedName("price")
    private String mPrice;
    @Nullable
    @SerializedName("oprice")
    private String mSalePrice;
    @Nullable
    @SerializedName("token")
    private String mToken;
    @Nullable
    @SerializedName("mark")
    private String mMark;
    @Nullable
    @SerializedName("model")
    private String mModel;
    @Nullable
    @SerializedName("capacity_ad")
    private String mEngineSize;
    @Nullable
    @SerializedName("vbody")
    private String mBody;
    @Nullable
    @SerializedName("trans")
    private String mTransmission;
    @Nullable
    @SerializedName("video")
    private String mVideoUrl;
    @Nullable
    @SerializedName("video_image")
    private String mVideoImage;
    @Nullable
    @SerializedName("doors")
    private String mDoors;
    @Nullable
    @SerializedName("capacity")
    private String mCapacity;
    @SerializedName("first_owner")
    private boolean mFirstOwner;
    @SerializedName("nap")
    private boolean mNap;

    @Nullable
    public String getReleaseYear() {
        return mReleaseYear;
    }

    @Nullable
    public String getMileage() {
        return mMileage;
    }

    @Nullable
    public String getFuel() {
        return mFuel;
    }

    @Nullable
    public String getPrice() {
        return mPrice;
    }

    @Nullable
    public String getSalePrice() {
        return mSalePrice;
    }

    @Nullable
    public String getToken() {
        return mToken;
    }

    @Nullable
    public String getMark() {
        return mMark;
    }

    @Nullable
    public String getModel() {
        return mModel;
    }

    @Nullable
    public String getEngineSize() {
        return mEngineSize;
    }

    @Nullable
    public String getBody() {
        return mBody;
    }

    @Nullable
    public String getTransmission() {
        return mTransmission;
    }

    @Nullable
    public String getVideoUrl() {
        return mVideoUrl;
    }

    @Nullable
    public String getVideoImage() {
        return mVideoImage;
    }

    @Nullable
    public String getDoors() {
        return mDoors;
    }

    @Nullable
    public String getCapacity() {
        return mCapacity;
    }

    public boolean isFirstOwner() {
        return mFirstOwner;
    }

    public boolean isNap() {
        return mNap;
    }
}
