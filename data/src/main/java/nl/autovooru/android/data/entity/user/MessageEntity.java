package nl.autovooru.android.data.entity.user;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonenko Viacheslav on 28/12/15.
 */
public final class MessageEntity {

    @SerializedName("um_id")
    private String mMessageId;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("topic")
    private String mTopic;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("create_date")
    private String mCreateDate;
    @SerializedName("answer_topic")
    private String mAnswerTopic;

    public MessageEntity() {
    }

    public String getMessageId() {
        return mMessageId;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getTopic() {
        return mTopic;
    }

    public String getMessage() {
        return mMessage;
    }

    public String getCreateDate() {
        return mCreateDate;
    }

    public String getAnswerTopic() {
        return mAnswerTopic;
    }
}
