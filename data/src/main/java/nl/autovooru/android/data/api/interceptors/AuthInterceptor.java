package nl.autovooru.android.data.api.interceptors;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Antonenko Viacheslav on 29/12/15.
 */
public class AuthInterceptor implements Interceptor {

    private static final String HEADER_AUTHORIZATION = "Authorization";

    private final String mCredential;

    public AuthInterceptor(String username, String password) {
        mCredential = Credentials.basic(username, password);
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        Request.Builder requestBuilder = original.newBuilder()
                .header(HEADER_AUTHORIZATION, mCredential);

        Request request = requestBuilder.build();
        return chain.proceed(request);
    }
}
