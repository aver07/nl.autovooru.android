package nl.autovooru.android.data.entity.machines.details;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Antonenko Viacheslav on 17/12/15.
 */
public final class ProfileEntity {

    @Nullable
    @SerializedName("post_code")
    private String mPostCode;
    @Nullable
    @SerializedName("city")
    private String mCity;
    @Nullable
    @SerializedName("address")
    private String mAddress;
    @Nullable
    @SerializedName("org_name")
    private String mOrganizationName;
    @Nullable
    @SerializedName("phone")
    private String mPhone;
    @Nullable
    @SerializedName("email")
    private String mEmail;
    @Nullable
    @SerializedName("website")
    private String mWebsiteUrl;
    @Nullable
    @SerializedName("location")
    List<LocationEntity> mLocations;
    @Nullable
    @SerializedName("ua_owner")
    private String mDealerType;

    @Nullable
    public String getPostCode() {
        return mPostCode;
    }

    @Nullable
    public String getCity() {
        return mCity;
    }

    @Nullable
    public String getAddress() {
        return mAddress;
    }

    @Nullable
    public String getOrganizationName() {
        return mOrganizationName;
    }

    @Nullable
    public String getPhone() {
        return mPhone;
    }

    @Nullable
    public String getEmail() {
        return mEmail;
    }

    @Nullable
    public String getWebsiteUrl() {
        return mWebsiteUrl;
    }

    @Nullable
    public List<LocationEntity> getLocations() {
        return mLocations;
    }

    @Nullable
    public String getDealerType() {
        return mDealerType;
    }
}
