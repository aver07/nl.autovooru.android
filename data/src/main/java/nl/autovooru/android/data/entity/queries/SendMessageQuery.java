package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;

/**
 * Created by Antonenko Viacheslav on 10/12/15.
 */
public class SendMessageQuery {

    @SerializedName("topic")
    private final String mSubject;
    @SerializedName("email")
    private final String mEmail;
    @SerializedName("message")
    private final String mMessage;
    @Nullable
    @SerializedName("as_id")
    private final String mMachineId;
    @Nullable
    @SerializedName("name")
    private final String mName;
    @Nullable
    @SerializedName("lang")
    private final String mLanguage;
    @SerializedName("user")
    private final AuthInfoEntity mUser;

    public SendMessageQuery(@Nullable String machineId, @Nullable String name,
                            @NonNull String email, @NonNull String subject,
                            @NonNull String message, @Nullable String language,
                            @Nullable AuthInfoEntity user) {
        mMachineId = machineId;
        mName = name;
        mEmail = email;
        mSubject = subject;
        mMessage = message;
        mLanguage = language;
        mUser = user;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }

}
