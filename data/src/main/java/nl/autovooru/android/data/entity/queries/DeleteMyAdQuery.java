package nl.autovooru.android.data.entity.queries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import nl.autovooru.android.common.utils.Base64;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;

/**
 * Created by Antonenko Viacheslav on 25/12/15.
 */
public final class DeleteMyAdQuery {

    @SerializedName("user")
    private AuthInfoEntity mUser;
    @SerializedName("as_id")
    private String mMachineId;
    @Nullable
    @SerializedName("lang")
    private String mLanguage;

    public DeleteMyAdQuery(@NonNull AuthInfoEntity user, @NonNull String machineId, @Nullable String language) {
        mUser = user;
        mMachineId = machineId;
        mLanguage = language;
    }

    @Override
    public String toString() {
        final Gson gson = new Gson();

        final String json = gson.toJson(this);

        return Base64.encodeToString(json.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
    }
}
