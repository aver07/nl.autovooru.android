package nl.autovooru.android.data;


import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import nl.autovooru.android.data.api.Authenticator;
import nl.autovooru.android.data.api.AutovooruApiService;
import nl.autovooru.android.data.api.Pager;
import nl.autovooru.android.data.api.interceptors.AuthInterceptor;
import nl.autovooru.android.data.api.interceptors.HttpLoggingInterceptor;
import nl.autovooru.android.data.entity.BaseListResponse;
import nl.autovooru.android.data.entity.Page;
import nl.autovooru.android.data.entity.auth.AuthInfoEntity;
import nl.autovooru.android.data.entity.auth.AuthorizationTokenEntity;
import nl.autovooru.android.data.entity.filters.FiltersQuery;
import nl.autovooru.android.data.entity.machines.MachineEntity;
import nl.autovooru.android.data.entity.machines.MachineListQuery;
import nl.autovooru.android.data.entity.queries.AdvertQuery;
import nl.autovooru.android.data.entity.queries.AuthorizedQuery;
import nl.autovooru.android.data.entity.queries.GetAccessTokenQuery;
import nl.autovooru.android.data.entity.queries.GetAuthorizationCodeQuery;
import nl.autovooru.android.data.entity.queries.MachineDetailsQuery;
import nl.autovooru.android.data.entity.queries.MyFavoritesQuery;
import nl.autovooru.android.data.entity.queries.MyMachinesBidsQuery;
import nl.autovooru.android.data.entity.queries.UserMessagesQuery;
import nl.autovooru.android.data.entity.user.MessageEntity;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Antonenko Viacheslav on 15/11/15.
 */
public class ApiUnitTest {

    private static final String USERNAME = "prcode";
    private static final String PASSWORD = "CrjhjKtnj";
    private static final String SERVER_URI = "http://api.autovooru.nl";

    private AutovooruApiService mApiService;

    private static OkHttpClient prepareOkHttpClient(final String username, final String password) {
        final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(new AuthInterceptor(username, password))
                .authenticator(new Authenticator(username, password))
                .build();
    }

    private static Retrofit prepareRetrofit(final String baseUrl, final OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .validateEagerly(true).build();
    }

    @Before
    public void init() {
        final OkHttpClient okHttpClient = prepareOkHttpClient(USERNAME, PASSWORD);
        final Retrofit retrofit = prepareRetrofit(SERVER_URI, okHttpClient);
        mApiService = retrofit.create(AutovooruApiService.class);
    }

    @Test
    public void checkFilterLists() throws Exception {
        final FiltersQuery query = FiltersQuery.newBuilder().build();
        mApiService.getFiltersTypesList(query).subscribe(response -> {
            assertNotNull(response);
            assertNotNull(response.body().getResponseCode());
            assertEquals("Check response code", "success", response.body().getResponseCode().getCode());
            assertNotNull(response.body().getData());
            assertTrue(response.body().getData().size() == 10);
        });
    }

    @Test
    public void checkGetFilterItemsBySingleFilterId() throws Exception {
        List<String> ids = new ArrayList<>();
        ids.add("5");
        final FiltersQuery query = FiltersQuery.newBuilder().filters(ids).build();
        mApiService.getFiltersList(query).subscribe(response -> {
            assertNotNull(response);
            assertNotNull(response.body().getResponseCode());
            assertEquals("Check response code", "success", response.body().getResponseCode().getCode());
            assertTrue(response.body().getData().containsKey("vcat"));
        });
    }

    @Test
    public void checkGetFilterItemsByMultipleFilterIds() throws Exception {
        List<String> ids = new ArrayList<>();
        ids.add("5");
        ids.add("1");
        final FiltersQuery query = FiltersQuery.newBuilder().filters(ids).build();
        mApiService.getFiltersList(query).subscribe(response -> {
            assertNotNull(response);
            assertNotNull(response.body().getResponseCode());
            assertEquals("Check response code", "success", response.body().getResponseCode().getCode());
            assertTrue(response.body().getData().containsKey("vcat"));
            assertTrue(response.body().getData().containsKey("vmark"));
        });
    }

    @Test
    public void checkGetMachineList() throws Exception {
        Page page = new Page(Page.Type.catalog, 5, 1);
        final MachineListQuery query = MachineListQuery.newBuilder(page).addFilter("5", "17").build();
        mApiService.getMachineListWithFilters(query).subscribe(response -> {
            assertNotNull(response);
            assertNotNull(response.body().getResponseCode());
            assertEquals("Check response code", "success", response.body().getResponseCode().getCode());
            assertNotNull(response.body().getData());
            assertTrue(response.body().getData().size() > 0);
        });
    }

    @Test
    public void checkLoadingMore() throws Exception {
        final MachineListQuery query = MachineListQuery.newBuilder(new Page(Page.Type.catalog, 5, 0))
                .addFilter("5", "17")
                .build();

        final Pager<Response<BaseListResponse<MachineEntity>>, Response<BaseListResponse<MachineEntity>>> pager = Pager.create(new Pager.PagingFunction<Response<BaseListResponse<MachineEntity>>>() {
            @Override
            public Observable<Response<BaseListResponse<MachineEntity>>> call(Response<BaseListResponse<MachineEntity>> response) {
                if (response.body().getPage() <= 3) {
                    int nextPage = response.body().getPage() + 1;
                    final MachineListQuery query = MachineListQuery.newBuilder(new Page(Page.Type.catalog, 5, nextPage))
                            .addFilter("5", "17")
                            .build();
                    return mApiService.getMachineListWithFilters(query);
                } else {
                    return Pager.finish();
                }
            }
        });
        pager.page(mApiService.getMachineListWithFilters(query)).subscribe(machineBaseListResponse -> {
            System.out.println(machineBaseListResponse);
            pager.next();
        });
    }

    @Test
    public void checkMachineDetailPage() throws Exception {
        final Page page = new Page(Page.Type.advert);
        final MachineDetailsQuery query = new MachineDetailsQuery(page, "1104760", "en", null);
        mApiService.getMachineDetails(query).subscribe(response -> {
            assertNotNull(response);
            assertNotNull(response.body().getResponseCode());
            assertEquals("Check response code", "success", response.body().getResponseCode().getCode());
            assertNotNull(response.body().getData());
            assertNotNull("Empty content", response.body().getData().getContent());
            assertNotNull("Empty dealerId", response.body().getData().getDealerId());
            assertNotNull("Empty filters", response.body().getData().getFilters());
            assertNotNull("Empty content", response.body().getData().getInfo());
            assertNotNull("Empty machineId", response.body().getData().getMachineId());
        });
    }

    @Test
    public void authorization() throws Exception {
        String login = "aver07@ya.ru";
        String password = "vKM8mp4B";
    }

    @Test
    public void getUserFavorites() throws Exception {
        String login = "antslava@gmail.com";
        String password = "test";

        mApiService.getAuthorizationCode(new GetAuthorizationCodeQuery(login, password, "en"))
                .flatMap(response -> mApiService.getAccessToken(new GetAccessTokenQuery(response.body().getData().getCode(), "en")))
                .flatMap(response -> {
                    final AuthorizationTokenEntity tokenEntity = response.body().getData();
                    final AuthInfoEntity authInfoEntity = new AuthInfoEntity(tokenEntity.getAccessToken(), tokenEntity.getCode());
                    final MyFavoritesQuery query = new MyFavoritesQuery(authInfoEntity, null, null, "en");
                    return mApiService.getUserFavorites(query).map(baseListResponseResponse -> baseListResponseResponse.body());
                }).toBlocking().subscribe(new Subscriber<BaseListResponse<MachineEntity>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                System.out.printf(e.getMessage());
            }

            @Override
            public void onNext(BaseListResponse<MachineEntity> machineEntityBaseListResponse) {
//                System.out.printf(machineEntityBaseListResponse);
            }
        });
    }

    @Test
    public void getUserBids() throws Exception {
        String login = "antslava@gmail.com";
        String password = "test";

        mApiService.getAuthorizationCode(new GetAuthorizationCodeQuery(login, password, "en"))
                .flatMap(response -> mApiService.getAccessToken(new GetAccessTokenQuery(response.body().getData().getCode(), "en")))
                .flatMap(response -> {
                    final AuthorizationTokenEntity tokenEntity = response.body().getData();
                    final AuthInfoEntity authInfoEntity = new AuthInfoEntity(tokenEntity.getAccessToken(), tokenEntity.getCode());
                    final Page page = new Page(Page.Type.profile, 10, 0);
                    final MyMachinesBidsQuery query = new MyMachinesBidsQuery(authInfoEntity, page, "en");
                    return mApiService.getUserBids(query).map(baseListResponseResponse -> baseListResponseResponse.body());
                }).toBlocking().subscribe(new Subscriber<BaseListResponse<MachineEntity>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                System.out.printf(e.getMessage());
            }

            @Override
            public void onNext(BaseListResponse<MachineEntity> machineEntityBaseListResponse) {
                System.out.println(machineEntityBaseListResponse);
            }
        });
    }

    @Test
    public void getUserMessages() throws Exception {
        String login = "antslava@gmail.com";
        String password = "test";

        mApiService.getAuthorizationCode(new GetAuthorizationCodeQuery(login, password, "en"))
                .flatMap(response -> mApiService.getAccessToken(new GetAccessTokenQuery(response.body().getData().getCode(), "en")))
                .flatMap(response -> {
                    final AuthorizationTokenEntity tokenEntity = response.body().getData();
                    final AuthInfoEntity authInfoEntity = new AuthInfoEntity(tokenEntity.getAccessToken(), tokenEntity.getCode());
                    final Page page = new Page(Page.Type.profile, 10, 0);
                    final UserMessagesQuery query = new UserMessagesQuery(authInfoEntity, page, "en");
                    return mApiService.getUserMessages(query).map(baseListResponseResponse -> baseListResponseResponse.body());
                }).toBlocking().subscribe(new Subscriber<BaseListResponse<MessageEntity>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(BaseListResponse<MessageEntity> messageEntityBaseListResponse) {

            }
        });
    }

    @Test
    public void getUserData() throws Exception {
        String login = "antslava@gmail.com";
        String password = "test";

        mApiService.getAuthorizationCode(new GetAuthorizationCodeQuery(login, password, "en"))
                .flatMap(response -> mApiService.getAccessToken(new GetAccessTokenQuery(response.body().getData().getCode(), "en")))
                .flatMap(response -> {
                    final AuthorizationTokenEntity tokenEntity = response.body().getData();
                    final AuthInfoEntity authInfoEntity = new AuthInfoEntity(tokenEntity.getAccessToken(), tokenEntity.getCode());
                    final Page page = new Page(Page.Type.profile, 10, 0);
                    final AuthorizedQuery query = new AuthorizedQuery(authInfoEntity, "en");
                    return mApiService.getUserData(query);
                }).toBlocking().subscribe();
    }

    @Test
    public void getAdvertFields() throws Exception {
        String login = "antslava@gmail.com";
        String password = "test";

        mApiService.getAuthorizationCode(new GetAuthorizationCodeQuery(login, password, "en"))
                .flatMap(response -> mApiService.getAccessToken(new GetAccessTokenQuery(response.body().getData().getCode(), "en")))
                .flatMap(response -> {
                    final AuthorizationTokenEntity tokenEntity = response.body().getData();
                    final AuthInfoEntity authInfoEntity = new AuthInfoEntity(tokenEntity.getAccessToken(), tokenEntity.getCode());

                    final AdvertQuery query = new AdvertQuery("17", null, authInfoEntity, "en");
                    return mApiService.getAdvertFields(query);
                }).toBlocking().subscribe();
    }
}
