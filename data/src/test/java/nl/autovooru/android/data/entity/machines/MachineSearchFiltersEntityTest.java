package nl.autovooru.android.data.entity.machines;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import nl.autovooru.android.data.api.Authenticator;
import nl.autovooru.android.data.api.AutovooruApiService;
import nl.autovooru.android.data.api.interceptors.AuthInterceptor;
import nl.autovooru.android.data.api.interceptors.HttpLoggingInterceptor;
import nl.autovooru.android.data.entity.queries.MachineFiltersQuery;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Antonenko Viacheslav on 12/12/15.
 */
public class MachineSearchFiltersEntityTest {

    private static final String USERNAME = "prcode";
    private static final String PASSWORD = "CrjhjKtnj";
    private static final String SERVER_URI = "http://api.autovooru.nl";

    private AutovooruApiService mApiService;

    private static OkHttpClient prepareOkHttpClient(final String username, final String password) {
        final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(new AuthInterceptor(username, password))
                .authenticator(new Authenticator(username, password))
                .build();
    }

    private static Retrofit prepareRetrofit(final String baseUrl, final OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .validateEagerly(true).build();
    }

    @Before
    public void init() {
        final OkHttpClient okHttpClient = prepareOkHttpClient(USERNAME, PASSWORD);
        final Retrofit retrofit = prepareRetrofit(SERVER_URI, okHttpClient);
        mApiService = retrofit.create(AutovooruApiService.class);
    }

    @Test
    public void loadMachineSearchFilters() {
        final HashMap<String, String> params = new HashMap<>();
        mApiService.getMachineSearchFilters(new MachineFiltersQuery("en", params))
                .subscribe(response -> {
                    assertNotNull(response);
                    assertNotNull(response.body().getResponseCode());
                    assertEquals("Check response code", "success", response.body().getResponseCode().getCode());
                    assertNotNull("Response not empty", response.body().getData());
                });

    }
}